#!/bin/bash

set -e

# Install node moudels
docker compose -f docker-compose.test.yml build
docker compose -f docker-compose.test.yml run --rm backend pnpm install

# Init/Update database
docker compose -f docker-compose.test.yml run --rm backend pnpm run typeorm:migrate

if [[ "$1" == "init" ]]; then
	exit 0
fi

# Run tests
set +e

status=0

if [[ "$1" == "coverage" ]]; then
	docker compose -f docker-compose.test.yml run --rm backend pnpm run test:cov || status=1
else
	docker compose -f docker-compose.test.yml run --rm backend pnpm run test $@ || status=1
fi
docker compose -f docker-compose.test.yml down

exit $status
