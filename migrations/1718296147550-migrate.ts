import { MigrationInterface, QueryRunner } from "typeorm";

export class Migrate1718296147550 implements MigrationInterface {
    name = 'Migrate1718296147550'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "worker_stats" ("uuid" uuid NOT NULL DEFAULT uuid_generate_v4(), "jobs_processed" integer NOT NULL DEFAULT '0', "jobs_failed" integer NOT NULL DEFAULT '0', "requests_made" integer NOT NULL DEFAULT '0', "worker_queue_max" integer NOT NULL DEFAULT '0', "worker_memory_max" integer NOT NULL DEFAULT '0', "worker_job_time_min" integer NOT NULL DEFAULT '0', "worker_job_time_max" integer NOT NULL DEFAULT '0', "worker_job_time_average" integer NOT NULL DEFAULT '0', "cache_size_max" integer NOT NULL DEFAULT '0', "cache_size_dead" integer NOT NULL DEFAULT '0', "created_at" TIMESTAMP NOT NULL, "merge_type" integer NOT NULL, CONSTRAINT "PK_6d92468c7eab64592f62487a9a0" PRIMARY KEY ("uuid"))`);
        await queryRunner.query(`CREATE TABLE "user_session_store" ("uuid" uuid NOT NULL DEFAULT uuid_generate_v4(), "token_uuid" text, "browser_name" text NOT NULL, "browser_version" integer NOT NULL, "os" text NOT NULL, "created_at" TIMESTAMP NOT NULL, "updated_at" TIMESTAMP NOT NULL, CONSTRAINT "PK_ab6195828ffb39d5a2e5642a574" PRIMARY KEY ("uuid"))`);
        await queryRunner.query(`CREATE TABLE "emoji_fetch" ("instance" text NOT NULL, "updated_at" TIMESTAMP NOT NULL, CONSTRAINT "PK_e15600516264c1248c9f5cd70c2" PRIMARY KEY ("instance"))`);
        await queryRunner.query(`CREATE TABLE "application_store" ("uuid" uuid NOT NULL DEFAULT uuid_generate_v4(), "domain" text NOT NULL, "type" character varying NOT NULL, "key" text NOT NULL, "secret" text NOT NULL, "token" text, "streaming_url" text, "created_at" TIMESTAMP NOT NULL, "updated_at" TIMESTAMP NOT NULL, CONSTRAINT "UQ_67074c3831ba3fcddbeb6ee3c73" UNIQUE ("domain"), CONSTRAINT "PK_bfdc02496d9cc90d614354db3fe" PRIMARY KEY ("uuid"))`);
        await queryRunner.query(`CREATE TABLE "account_store" ("uuid" uuid NOT NULL DEFAULT uuid_generate_v4(), "username" text NOT NULL, "token" text NOT NULL DEFAULT '', "application_uuid" uuid NOT NULL, CONSTRAINT "PK_2fe103a3539743c0ce948b39652" PRIMARY KEY ("uuid"))`);
        await queryRunner.query(`CREATE TABLE "config_store" ("uuid" uuid NOT NULL DEFAULT uuid_generate_v4(), "value" text NOT NULL, "updated_at" TIMESTAMP NOT NULL, CONSTRAINT "PK_aa34d44e618e0b55e1585a5bc5d" PRIMARY KEY ("uuid"))`);
        await queryRunner.query(`CREATE TABLE "user_store" ("uuid" uuid NOT NULL DEFAULT uuid_generate_v4(), "main_uuid" uuid NOT NULL, "config_uuid" uuid, CONSTRAINT "PK_24c7fb65ee6474802da3232d831" PRIMARY KEY ("uuid"))`);
        await queryRunner.query(`CREATE TABLE "emoji_cache" ("identifier" text NOT NULL, "url" text NOT NULL, "fallback" text, "static_url" text, CONSTRAINT "PK_17af2182de24755dbb43f72184e" PRIMARY KEY ("identifier"))`);
        await queryRunner.query(`CREATE TABLE "user_accounts" ("user_store_uuid" uuid NOT NULL, "account_store_uuid" uuid NOT NULL, CONSTRAINT "PK_cd50bb279fb5644418e95c8a01a" PRIMARY KEY ("user_store_uuid", "account_store_uuid"))`);
        await queryRunner.query(`CREATE INDEX "IDX_fd172ee6a36c5a86592623da85" ON "user_accounts" ("user_store_uuid") `);
        await queryRunner.query(`CREATE INDEX "IDX_d5de5c6ed861ebb6d8a12ac991" ON "user_accounts" ("account_store_uuid") `);
        await queryRunner.query(`CREATE TABLE "user_sessions" ("user_store_uuid" uuid NOT NULL, "user_session_store_uuid" uuid NOT NULL, CONSTRAINT "PK_54072396bffbb42782a601b03f9" PRIMARY KEY ("user_store_uuid", "user_session_store_uuid"))`);
        await queryRunner.query(`CREATE INDEX "IDX_69851c647039f3170944aea752" ON "user_sessions" ("user_store_uuid") `);
        await queryRunner.query(`CREATE INDEX "IDX_a2e570f070c808afed46750074" ON "user_sessions" ("user_session_store_uuid") `);
        await queryRunner.query(`ALTER TABLE "account_store" ADD CONSTRAINT "FK_139f31fe994fed77118924e8b5a" FOREIGN KEY ("application_uuid") REFERENCES "application_store"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_store" ADD CONSTRAINT "FK_8cdfcb465d4d567bc2c2aecba97" FOREIGN KEY ("main_uuid") REFERENCES "account_store"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_store" ADD CONSTRAINT "FK_4a400f5f86fa25b6b123520cef1" FOREIGN KEY ("config_uuid") REFERENCES "config_store"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_accounts" ADD CONSTRAINT "FK_fd172ee6a36c5a86592623da857" FOREIGN KEY ("user_store_uuid") REFERENCES "user_store"("uuid") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "user_accounts" ADD CONSTRAINT "FK_d5de5c6ed861ebb6d8a12ac9917" FOREIGN KEY ("account_store_uuid") REFERENCES "account_store"("uuid") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "user_sessions" ADD CONSTRAINT "FK_69851c647039f3170944aea752c" FOREIGN KEY ("user_store_uuid") REFERENCES "user_store"("uuid") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "user_sessions" ADD CONSTRAINT "FK_a2e570f070c808afed46750074f" FOREIGN KEY ("user_session_store_uuid") REFERENCES "user_session_store"("uuid") ON DELETE CASCADE ON UPDATE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_sessions" DROP CONSTRAINT "FK_a2e570f070c808afed46750074f"`);
        await queryRunner.query(`ALTER TABLE "user_sessions" DROP CONSTRAINT "FK_69851c647039f3170944aea752c"`);
        await queryRunner.query(`ALTER TABLE "user_accounts" DROP CONSTRAINT "FK_d5de5c6ed861ebb6d8a12ac9917"`);
        await queryRunner.query(`ALTER TABLE "user_accounts" DROP CONSTRAINT "FK_fd172ee6a36c5a86592623da857"`);
        await queryRunner.query(`ALTER TABLE "user_store" DROP CONSTRAINT "FK_4a400f5f86fa25b6b123520cef1"`);
        await queryRunner.query(`ALTER TABLE "user_store" DROP CONSTRAINT "FK_8cdfcb465d4d567bc2c2aecba97"`);
        await queryRunner.query(`ALTER TABLE "account_store" DROP CONSTRAINT "FK_139f31fe994fed77118924e8b5a"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_a2e570f070c808afed46750074"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_69851c647039f3170944aea752"`);
        await queryRunner.query(`DROP TABLE "user_sessions"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_d5de5c6ed861ebb6d8a12ac991"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_fd172ee6a36c5a86592623da85"`);
        await queryRunner.query(`DROP TABLE "user_accounts"`);
        await queryRunner.query(`DROP TABLE "emoji_cache"`);
        await queryRunner.query(`DROP TABLE "user_store"`);
        await queryRunner.query(`DROP TABLE "config_store"`);
        await queryRunner.query(`DROP TABLE "account_store"`);
        await queryRunner.query(`DROP TABLE "application_store"`);
        await queryRunner.query(`DROP TABLE "emoji_fetch"`);
        await queryRunner.query(`DROP TABLE "user_session_store"`);
        await queryRunner.query(`DROP TABLE "worker_stats"`);
    }

}
