#!/bin/bash
set -e

filename="package.json"
if [ ! -f "$filename" ]; then
	cd backend
fi

pnpm run typeorm:migrate

exec "$@"
