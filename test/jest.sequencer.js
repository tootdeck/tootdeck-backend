const Sequencer = require('@jest/test-sequencer').default;

class CustomSequencer extends Sequencer {
	sort(tests) {
		const secret_test = 'secret.spec.ts';
		const database = 'database';

		tests.sort((a, b) => {
			if (a.path.includes(database) || b.path.includes(database)) {
				return -1;
			}

			return 0;
		});

		const index = tests.findIndex((x) => x.path.includes(secret_test));
		const secret = tests.splice(index, 1);

		return [...secret, ...tests];
	}
}

module.exports = CustomSequencer;
