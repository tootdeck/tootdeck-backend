import type { Config } from 'jest';

const config: Config = {
	rootDir: '../src',
	maxWorkers: 1,
	maxConcurrency: 1,
	forceExit: true,
	detectOpenHandles: true,
	logHeapUsage: true,
	testSequencer: '../test/jest.sequencer.js',
	moduleFileExtensions: ['js', 'json', 'ts'],
	testRegex: '.*\\.spec\\.ts$',
	transform: {
		'^.+\\.ts$': 'ts-jest',
	},
	collectCoverageFrom: ['**/*.ts', '!**/main.ts'],
	coveragePathIgnorePatterns: [
		'^.*.module.ts?$',
		'^.*.types.ts?$',
		'^.*.property.ts?$',
		'^.*.logger.ts?$',
		'^.*.config.ts?$',
		'^.*.controller.ts?$',
		'^.*.gateway.ts?$',
		'^.*.guard.ts?$',
		'^.*.middleware.ts?$',
		'^.*.entity.ts?$',
		'src/mirror',
		'src/lib',
		'src/worker',
		'src/websocket',
	],
	coverageDirectory: '../coverage',
	testEnvironment: 'node',
	testTimeout: 30000,
};

export default config;
