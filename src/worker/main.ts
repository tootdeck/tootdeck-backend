import { NestFactory } from '@nestjs/core';
import { Transport, MicroserviceOptions } from '@nestjs/microservices';
import { FastifyAdapter } from '@nestjs/platform-fastify';

import { WorkerModule } from './worker.module';

async function bootstrap() {
	const app = await NestFactory.create(WorkerModule, new FastifyAdapter());

	app.connectMicroservice<MicroserviceOptions>({
		transport: Transport.TCP,
		options: {
			host: '0.0.0.0',
			port: 5001,
		},
	});

	app.enableShutdownHooks();

	await app.startAllMicroservices();
	await app.listen(3000, '0.0.0.0');
}

bootstrap();
