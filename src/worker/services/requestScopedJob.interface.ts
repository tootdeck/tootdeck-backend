import { randomUUID } from 'crypto';

export class RequestScopedJob {
	readonly jobUUID = randomUUID();
}
