import { Inject, Injectable, OnModuleDestroy, OnModuleInit } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { ClientProxy } from '@nestjs/microservices';
import { Interval } from '@nestjs/schedule';
import { InternalAxiosRequestConfig } from 'axios';
import { randomUUID, UUID } from 'crypto';
import { lookup } from 'dns';
import { hostname } from 'os';
import { existsSync, mkdirSync } from 'fs';

import { TimesService } from '../../app/services/utils/times.service';
import { AxiosProxy } from '../../app/services/axiosProxy.service';
import { OptionalFeaturesService } from '../../app/services/utils/optionalFeatures.service';

import { AxiosLimitService } from './axiosLimit.service';

import { Logger } from '../../utils/logger';

import { STORE_EMOJI, STORE_PATH, STORE_STATIC } from '../constants';

import { Workers } from '../types';

@Injectable()
export class WorkerService implements OnModuleInit, OnModuleDestroy {
	private readonly logger = new Logger(WorkerService.name);
	private readonly uuid = randomUUID();
	private readonly started_at = new Date();
	private ip: string;
	private requests_count: number = 0;

	constructor(
		@Inject('MAIN_PROCESS') private main: ClientProxy,
		private readonly OptionalFeaturesService: OptionalFeaturesService,
		private readonly AxiosLimitService: AxiosLimitService,
		private readonly HttpService: HttpService,
		private readonly AxiosProxy: AxiosProxy,
		private readonly TimesService: TimesService,
	) {
		if (this.OptionalFeaturesService.CACHE_STATUS_REACTIONS) {
			if (!existsSync(STORE_PATH)) {
				mkdirSync(STORE_PATH);
			}

			if (!existsSync(STORE_EMOJI)) {
				mkdirSync(STORE_EMOJI);
			}

			if (!existsSync(STORE_STATIC)) {
				mkdirSync(STORE_STATIC);
			}
		}

		this.logger.log(
			'constructor',
			`Feature CACHE_STATUS_INTERACTIONS ${
				this.OptionalFeaturesService.CACHE_STATUS_INTERACTIONS ? 'enabled' : 'disabled'
			}`,
		);
		this.logger.log(
			'constructor',
			`Feature CACHE_STATUS_REACTIONS ${
				this.OptionalFeaturesService.CACHE_STATUS_REACTIONS ? 'enabled' : 'disabled'
			}`,
		);
		this.logger.log(
			'constructor',
			`Feature CACHE_TWITTER_EMBED ${
				this.OptionalFeaturesService.CACHE_TWITTER_EMBED ? 'enabled' : 'disabled'
			}`,
		);

		this.AxiosProxy.setWorker();
		this.AxiosLimitService.setlimit(150);
		this.HttpService.axiosRef.interceptors.request.use(
			// @ts-ignore
			(config: InternalAxiosRequestConfig & { jobUUID: UUID }) => {
				if (config.jobUUID) {
					this.AxiosLimitService.increment(config.jobUUID, config.url!);
				}
				return config;
			},
		);
	}

	/**
	 * Init
	 */
	// #region

	async getIP() {
		const ip = await new Promise((resolve: (value: string) => void, reject) => {
			lookup(hostname(), { family: 4 }, (err, address) => {
				if (err) {
					reject(err);
				}
				resolve(address);
			});
		}).catch((e) => console.error(e));
		if (!ip) {
			this.logger.error('init', 'UNABLE DETERMINE WORKER IP');
			process.exit(1);
		}

		return ip;
	}

	async onModuleInit() {
		this.ip = await this.getIP();

		const data: Workers.Register = {
			uuid: this.uuid,
			ip: this.ip,
		};

		setTimeout(
			() =>
				this.main.emit(Workers.Event.Register, data).subscribe({
					next: () => {
						this.logger.log('init', `UUID=${data.uuid}`);
						this.logger.log('init', `IP=${data.ip}`);
					},
				}),
			2000,
		);
	}

	async onModuleDestroy() {
		this.main.emit(Workers.Event.Unregister, this.uuid);
	}

	// #endregion

	/**
	 * Lifecycle
	 */
	// #region

	@Interval(15000)
	private healthcheck() {
		const data: Workers.Register = {
			uuid: this.uuid,
			ip: this.ip,
		};
		this.main.emit(Workers.Event.Healthcheck, data);
	}

	// #endregion

	resetRequestsCount() {
		this.requests_count = 0;
	}

	getUUID() {
		return this.uuid;
	}

	/**
	 * Job
	 */
	// #region

	jobFinish<T>(uuid: UUID, result: T): Workers.JobResult<T> {
		this.requests_count += this.AxiosLimitService.delete(uuid);

		return {
			stats: {
				uuid: this.uuid,
				started_at: this.started_at,
				memory: process.memoryUsage().rss,
				times: this.TimesService.getTimes(),
				requests_made: this.requests_count,
			},
			result,
		};
	}

	// #endregion
}
