import { Injectable, Scope } from '@nestjs/common';
import { Redis } from 'ioredis';
import { AxiosRequestConfig } from 'axios';
import { deserialize, serialize } from 'v8';

import { Entity } from '../../lib/megalodon/entities';

import { OptionalFeaturesService } from '../../app/services/utils/optionalFeatures.service';
import { AxiosCache } from '../../app/services/axiosCache.service';
import { InstanceCacheService } from '../../app/services/instanceCache.service';
import { EmojiCacheService } from './emojiCache.service';
import { RequestsLimitReached } from './axiosLimit.service';
import { FailedDomainsService } from '../../app/services/failedDomain.service';

import { EmojiCache } from '../../database/entities/emoji.cache.entity';

import { Logger } from '../../utils/logger';

import {
	BasicInteraction,
	EmojiReaction,
	Interactions,
} from '../../dashboard/properties/redis/interactions.property';

import { InstanceType } from '../../database/types';
import { RedisExpiry, RedisExpiryToken, RedisInteraction, RedisState } from '../../app/types';
import { RequestScopedJob } from './requestScopedJob.interface';
import { RedisNamespace } from '../../app/constants/redis';
import { RedisService } from '@liaoliaots/nestjs-redis';

@Injectable({ scope: Scope.REQUEST })
export class InteractionsService extends RequestScopedJob {
	private readonly logger = new Logger(InteractionsService.name);
	private readonly redis_interactions: Redis;

	constructor(
		private readonly AxiosCache: AxiosCache,
		private readonly OptionalFeaturesService: OptionalFeaturesService,
		private readonly EmojiCacheService: EmojiCacheService,
		private readonly InstanceCacheService: InstanceCacheService,
		private readonly FailedDomainsService: FailedDomainsService,
		RedisService: RedisService,
	) {
		super();
		this.redis_interactions = RedisService.getOrThrow(RedisNamespace.StatusesInteractions);
	}

	/**
	 * Utils
	 */
	// #region

	private axiosConfig(config: AxiosRequestConfig<any>) {
		return {
			...config,
			jobUUID: this.jobUUID,
			addPrefix: 'InteractionsService',
		};
	}

	private async removeRedisLeftovers(hostname: string) {
		return this.redis_interactions
			.keys(`https://${hostname}/*`)
			.then(async (keys) =>
				Promise.all(
					keys.map(async (key) => {
						const value = await this.redis_interactions
							.getBuffer(key)
							.then((r) => (r ? (deserialize(r) as RedisInteraction) : null));
						return !value || value.state !== RedisState.Ok ? key : undefined;
					}),
				),
			)
			.then(async (keys_to_del) => {
				const keys = keys_to_del.filter((x) => x) as Array<string>;
				if (!keys.length) {
					return;
				}
				await this.redis_interactions.del(keys);
			})
			.then(() => true);
	}

	private setInteractionCache(
		hostname: string,
		interaction: RedisInteraction,
		expiry?: RedisExpiry,
	) {
		if (expiry?.token === RedisExpiryToken.NX) {
			return this.redis_interactions.set(
				hostname,
				serialize(interaction),
				// @ts-ignore
				expiry.token,
			);
		}

		return expiry
			? // @ts-ignore
				this.redis_interactions.set(
					hostname,
					serialize(interaction),
					// @ts-ignore
					expiry.token,
					expiry.time,
				)
			: this.redis_interactions.set(hostname, serialize(interaction));
	}

	private getInteractionCache(url: string) {
		return this.redis_interactions
			.getBuffer(url)
			.then<RedisInteraction | undefined>((buffer) => (buffer ? deserialize(buffer) : null));
	}

	private async handleRetry(href: string, code: number, reason: string) {
		const { hostname } = new URL(href);

		const promises: Array<Promise<any>> = [];

		switch (code) {
			case 404:
				promises.push(
					this.setInteractionCache(href, {
						state: RedisState.Dead,
						value: reason,
					}),
				);
				promises.push(
					this.FailedDomainsService.excludeDomain(hostname).then((excluded) =>
						excluded ? this.removeRedisLeftovers(hostname) : false,
					),
				);
				break;
			case 429:
				promises.push(
					this.setInteractionCache(
						href,
						{
							state: RedisState.Retry,
							value: 'To many requests',
						},
						{
							token: RedisExpiryToken.EX,
							time: 30 * 60,
						},
					),
				);
				promises.push(
					this.FailedDomainsService.setExcludedDomain(
						hostname,
						{
							state: RedisState.Retry,
							value: 'To many requests',
						},
						{
							token: RedisExpiryToken.EX,
							time: 30 * 60,
						},
					).then(() => this.removeRedisLeftovers(hostname)),
				);
				break;
			case 500: // Internal Server Error
			case 502: // Bad Gateway
			case 503: // Service Unavailable
			case 504: // Gateway Timeout
			case 520: // Cloudflare: web server returns an unknown error
			case 521: // Cloudflare: web server is down
			case 522: // Cloudflare: connection timed out
			case 523: // Cloudflare: origin is unreachable
			case 525: // Cloudflare: SSL handshake failed
				promises.push(
					this.setInteractionCache(
						href,
						{
							state: RedisState.Retry,
							value: 'Server not responding',
						},
						{
							token: RedisExpiryToken.EX,
							time: 60 * 60,
						},
					),
				);
				promises.push(
					this.FailedDomainsService.excludeDomain(hostname).then((excluded) =>
						excluded ? this.removeRedisLeftovers(hostname) : false,
					),
				);
				break;
			default:
				promises.push(
					this.FailedDomainsService.setExcludedDomain(
						hostname,
						{
							state: RedisState.Dead,
							value: `${code}`,
						},
						{
							token: RedisExpiryToken.NX,
						},
					).then(() => this.removeRedisLeftovers(hostname)),
				);
				break;
		}

		return Promise.all(promises);
	}

	private getCacheExpiresDate(created_at: string): number {
		const date = new Date(created_at).valueOf();
		const now = new Date().valueOf();
		const offset = now - date;
		const clamp = Math.min(Math.max(offset * 2, 1800000), 86400000); // Clamp between 30min and 24h
		return Math.round(clamp / 1000);
	}

	private isStoreNeeded(interactions: Partial<Interactions>): boolean {
		return (
			!!interactions?.application ||
			!!interactions?.emoji_reactions?.length ||
			!!interactions?.favourites_count ||
			!!interactions?.reblogs_count ||
			!!interactions?.replies_count
		);
	}

	// #endregion

	/**
	 * GetStatusID
	 */
	// #region

	private getStatusID(href: string) {
		const url = new URL(href);
		const path = url.pathname.split('/').filter((x) => x);

		/**
		 * https://masto.ai/users/AlexWinter/statuses/111507726043523253
		 * AlexWinter/statuses/111507726043523253
		 * 111507726043523253
		 *
		 * https://masto.ai/@AlexWinter/111507726043523253
		 * 111507726043523253
		 *
		 * https://seafoam.space/notice/AcNgW2ttKZqSqXIe9o
		 * AcNgW2ttKZqSqXIe9o
		 *
		 * https://misskey.design/notes/9mqssbedqb
		 * 9mqssbedqb
		 *
		 * https://social.translunar.academy/objects/c1896ee4-2f3a-4cb0-81eb-944d58ffd43e
		 * objects/c1896ee4-2f3a-4cb0-81eb-944d58ffd43e
		 * c1896ee4-2f3a-4cb0-81eb-944d58ffd43e
		 */

		if (path.length === 0 || path.length > 4 || url.search !== '' || !path[0]) {
			return undefined;
		}

		if (['users', 'notice', 'objects', 'notes'].includes(path[0]) || path[0].includes('@')) {
			path.shift();
		}

		if (path?.[1] === 'statuses') {
			path.shift();
			path.shift();
		}

		if (path.length !== 1) {
			return undefined;
		}

		return path[0];
	}

	private async getInstanceType(url: string): Promise<InstanceType | undefined> {
		const { hostname } = new URL(url);
		const type = await this.InstanceCacheService.get(hostname);
		if (!type) {
			await this.FailedDomainsService.excludeDomain(
				hostname,
				'Unable to parse, instance is maybe down',
			).then((excluded) => (excluded ? this.removeRedisLeftovers(hostname) : false));
			return undefined;
		}

		return type;
	}

	private async getPleromaTrueURL(href: string): Promise<string | undefined> {
		const request = this.AxiosCache.request(
			this.axiosConfig({
				method: 'GET',
				url: href,
			}),
			20000,
		);
		return request.promise
			.then((r) => r.request?.res.responseUrl)
			.catch(async (e) => {
				// Abort current job
				if (e instanceof RequestsLimitReached) {
					throw e;
				}

				const { hostname } = new URL(href);

				await this.FailedDomainsService.excludeDomain(
					hostname,
					`Failed to get Pleroma True URL\n${e.response?.status ?? e}`,
				).then((excluded) => (excluded ? this.removeRedisLeftovers(hostname) : false));

				return undefined;
			});
	}

	private async getInstanceTypeNStatusID(
		href: string,
	): Promise<
		{ type: InstanceType; status_id: string; pleroma_url: string | undefined } | undefined
	> {
		const type = await this.getInstanceType(href);
		if (!type) {
			return undefined;
		}

		let pleroma_url: string | undefined;
		if (type === InstanceType.Pleroma && href.includes('objects')) {
			pleroma_url = await this.getPleromaTrueURL(href);
			if (!pleroma_url) {
				return undefined;
			}

			href = pleroma_url;
		}

		const status_id = this.getStatusID(href);
		if (!status_id) {
			this.logger.warn('getInstanceTypeNStatusID', `Failed to retrieve id from ${href}`);
			return undefined;
		}

		return {
			type,
			status_id,
			pleroma_url,
		};
	}

	// #endregion

	/**
	 * parseInteractions
	 */
	// #region

	private getBasicInteractions(type: InstanceType, status: any): BasicInteraction {
		let replies_count: number = 0;
		let reblogs_count: number = 0;
		let favourites_count: number = 0;
		if (type === InstanceType.Misskey) {
			replies_count = status.repliesCount || 0;
			reblogs_count = status.renoteCount || 0;
			favourites_count = status.reactions
				? (status.reactions['❤'] ?? 0) + (status.reactions['⭐'] ?? 0) || 0
				: 0;
		} else {
			replies_count = status.replies_count;
			reblogs_count = status.reblogs_count;
			favourites_count = status.favourites_count;
		}

		return {
			replies_count,
			reblogs_count,
			favourites_count,
		};
	}

	private getEmojiDBIdentifier(hostname: string, name: string): string {
		let identifier: string = name;
		if (identifier.split('@').length === 1) {
			identifier = this.EmojiCacheService.getIdentifier(hostname, name);
		}
		return identifier;
	}

	private async storeEmojiReactionInDB(
		hostname: string,
		name: string,
		url: string,
	): Promise<EmojiCache | null> {
		let cache: EmojiCache | null = null;
		if (url) {
			const identifier: string = this.getEmojiDBIdentifier(hostname, name);
			cache = await this.EmojiCacheService.get(undefined, hostname, name, false);
			if (!cache) {
				return this.EmojiCacheService.store({
					identifier,
					url,
					fallback: null,
					static_url: null,
				});
			}
		}

		return cache;
	}

	private async handleEmojiReactionForMastodonPleroma(
		hostname: string,
		reaction: any,
	): Promise<EmojiReaction> {
		// Native emoji
		if (!reaction.url) {
			return {
				count: reaction.count,
				name: reaction.name,
				url: undefined,
				fallback: undefined,
				static_url: undefined,
			};
		}

		const identifier = (reaction.name as string).split('@');
		let cache: EmojiCache | null = null;
		// Custom emoji from the same instance
		if (identifier.length === 1) {
			cache = await this.storeEmojiReactionInDB(hostname, reaction.name, reaction.url);
		}
		// Custom emoji from another instance
		else {
			cache = await this.EmojiCacheService.get(undefined, hostname, identifier[0]!, true);
		}

		return {
			name: cache?.identifier ?? reaction.name,
			count: reaction.count,
			url: cache?.url ?? reaction.url,
			fallback: cache?.fallback ?? undefined,
			static_url: cache?.static_url ?? undefined,
		};
	}

	private async getEmojiReactionsMastodon(
		status: any,
		hostname: string,
	): Promise<Array<EmojiReaction>> {
		// "reactions": [
		// 		{
		// 		  "name": "verified",
		// 		  "count": 1,
		// 		  "url": "https://cdn.rivals.space/custom_emojis/images/000/085/594/original/828a181fb7bb7b6f.png",
		// 		  "static_url": "https://cdn.rivals.space/custom_emojis/images/000/085/594/static/828a181fb7bb7b6f.png"
		// 		}
		//   ],
		if (!status.reactions) {
			return [];
		}

		// await this.EmojiCacheService.prefetch(hostname);

		return Promise.all(
			status.reactions.map((reaction: any) =>
				this.handleEmojiReactionForMastodonPleroma(hostname, reaction),
			),
		);
	}

	private async getEmojiReactionsPleroma(
		status: any,
		hostname: string,
	): Promise<Array<EmojiReaction>> {
		// "emoji_reactions": [
		// 		{
		// 		  "account_ids": [
		// 			"AapsKLudlKjMIss4dE"
		// 		  ],
		// 		  "count": 1,
		// 		  "me": false,
		// 		  "name": "neofox_hug@eepy.moe",
		// 		  "url": "https://media.0w0.is/proxy/SnznH_M5edj2fAiu5ivXHRwB2X8/aHR0cHM6Ly9lZXB5Lm1vZS9maWxlcy8xOGEwYTdlZS01MzhhLTRlN2EtODAyZC1mZTk0YzE3ZjVmN2Y/18a0a7ee-538a-4e7a-802d-fe94c17f5f7f"
		// 		}
		//   ],

		if (!status?.emoji_reactions) {
			return [];
		}

		// await this.EmojiCacheService.prefetch(hostname);

		return Promise.all(
			status.emoji_reactions.map((reaction: any) =>
				this.handleEmojiReactionForMastodonPleroma(hostname, reaction),
			),
		);
	}

	private async getEmojiReactionsMisskey(
		status: any,
		hostname: string,
	): Promise<Array<EmojiReaction>> {
		// "reactions": {
		// 		"❤": 7,
		// 		":yoi@.:": 5,
		// 		":meiga@.:": 30,
		// 		":kakkee@.:": 10,
		// 		":oshare@.:": 1,
		// 		":tensai@.:": 2,
		// 		":kakkoyo@.:": 3,
		// 		":saikou3@.:": 1,
		// 		":suteki2@.:": 1,
		// 		":beautiful@.:": 2,
		// 		":subarashii@.:": 38,
		// 		":kakkoyosugi@.:": 22,
		// 		":kakkoii@mfmf.club:": 1,
		// 		":beautiful@oekakiskey.com:": 1,
		// 		":haisensu@selfdown-69.com:": 1
		//  },
		//  "reactionEmojis": {
		// 		"kakkoii@mfmf.club": "https://mfmf.s3.ap-northeast-1.wasabisys.com/emoji/kakkoii.png",
		// 		"beautiful@oekakiskey.com": "https://storage.googleapis.com/oekakiskey/drive/57952b1f-03f5-4b42-ba96-5865fc874f3d.png",
		// 		"haisensu@selfdown-69.com": "https://file.selfdown-69.com/null/aa702afb-1e91-4fcb-aef9-31e7695f2929.gif"
		//  }

		if (!status?.reactions) {
			return [];
		}

		const emoji_reactions: Array<EmojiReaction> = [];
		const entries = Object.entries(status.reactions) as Array<[string, number]>;
		const promises: Array<Promise<void>> = [];
		for (const [name, count] of entries) {
			const promise = async () => {
				const no_colons = name.replace(/:/g, '');
				// Native emoji
				if (!no_colons.includes('@')) {
					emoji_reactions.push({
						name: no_colons,
						count,
						url: undefined,
						fallback: undefined,
						static_url: undefined,
					});
					return;
				}

				const identifier = no_colons.split('@');
				// Custom emoji from the same instance
				if (identifier[1] !== '.') {
					const url =
						status.reactionEmojis?.[no_colons] ??
						status.emojis?.find(
							(emoji: { name: string; url: string }) => emoji.name === no_colons,
						)?.url;
					if (!url) {
						// Emoji no longer available on the remote instance (dead link)
						return;
					}
					const cache = await this.storeEmojiReactionInDB(hostname, no_colons, url);

					emoji_reactions.push({
						name: no_colons,
						count,
						url: cache?.url ?? url,
						fallback: cache?.fallback ?? undefined,
						static_url: cache?.static_url ?? undefined,
					});
					return;
				}

				// Custom emoji from another instance
				const emoji = await this.EmojiCacheService.get(
					undefined,
					hostname,
					identifier[0]!,
					true,
				);
				if (emoji) {
					emoji_reactions.push({
						name: emoji.identifier,
						count,
						url: emoji?.url,
						fallback: emoji?.fallback ?? undefined,
						static_url: emoji?.static_url ?? undefined,
					});
				}
			};

			promises.push(promise());
		}

		await Promise.all(promises);

		return emoji_reactions;
	}

	private async getEmojiReactions(
		type: InstanceType,
		hostname: string,
		status: any,
	): Promise<Array<EmojiReaction> | null> {
		switch (type) {
			case InstanceType.Mastodon:
				return this.getEmojiReactionsMastodon(status, hostname);
			case InstanceType.Pleroma:
				return this.getEmojiReactionsPleroma(status, hostname);
			case InstanceType.Misskey:
				return this.getEmojiReactionsMisskey(status, hostname);
			case InstanceType.Friendica:
				return null;
		}
	}

	private getApplication(type: InstanceType, status: any): string | null {
		switch (type) {
			case InstanceType.Mastodon:
				// application: {
				// 		name: "Tusker"
				// 		website: null
				// }
				return status.application?.name ?? null;
			case InstanceType.Pleroma:
			case InstanceType.Misskey:
			case InstanceType.Friendica:
				return null;
		}
	}

	private async parseInteractions(
		type: InstanceType,
		hostname: string,
		status: any,
	): Promise<Interactions> {
		try {
			const basic = this.getBasicInteractions(type, status);
			const emoji_reactions = this.OptionalFeaturesService.CACHE_STATUS_REACTIONS
				? ((await this.getEmojiReactions(type, hostname, status)) ?? [])
				: [];
			const application = this.getApplication(type, status);

			return {
				...basic,
				emoji_reactions,
				application,
			};
		} catch (e) {
			this.logger.error('parseInteractions', '');
			console.error(e);
			throw e;
		}
	}

	// #endregion

	/**
	 * Service
	 */
	// #region

	private async fetchStatus(
		type: InstanceType,
		hostname: string,
		status_id: string,
	): Promise<any | string | undefined> {
		let endpoint: string;
		let method: string;
		let body: object;
		if (type === InstanceType.Misskey) {
			endpoint = '/api/notes/show';
			method = 'POST';
			body = { noteId: status_id };
		} else {
			endpoint = '/api/v1/statuses/' + status_id;
			method = 'GET';
			body = {};
		}

		const url = 'https://' + hostname + endpoint;

		const request = this.AxiosCache.request(
			this.axiosConfig({
				method,
				url,
				headers: {
					'Content-Type': 'application/json',
				},
				data: body,
			}),
			20000,
		);

		return request.promise
			.then((r) => {
				if (r.status !== 200) {
					throw r;
				}

				return r.data;
			})
			.catch((e) => {
				// Abort current job
				if (e instanceof RequestsLimitReached) {
					throw e;
				}

				return {
					code: e.response?.status ?? 0,
					reason: `Unable to fetch\n${e.response?.status ?? e}`,
				};
			});
	}

	private mergeReactions(
		stored_emoji_reactions: Array<EmojiReaction>,
		emoji_reactions: Array<EmojiReaction>,
	) {
		const all: Array<EmojiReaction> = [...stored_emoji_reactions, ...emoji_reactions];
		const merged: Array<EmojiReaction> = [];
		while (all.length) {
			const current_index = all.length - 1;
			const reaction = all.splice(all.length - 1, 1)[0]!;

			const found_index = all.findIndex(
				(stored_reaction) => stored_reaction.name === reaction.name,
			);
			if (found_index !== -1 && found_index !== current_index) {
				const found = all.splice(found_index, 1)[0]!;

				if (reaction.count < found.count) {
					reaction.count = found.count;
				}
			}

			merged.push(reaction);
		}

		return merged;
	}

	/**
	 * @returns `Interactions & { hostname: string }` on success
	 * @returns `undefined` on error
	 */
	private async fetchInteractions(
		entity: Entity.Status,
		href: string,
	): Promise<(Interactions & { hostname: string }) | undefined> {
		const { hostname } = new URL(href);
		const parsed = await this.getInstanceTypeNStatusID(href);
		if (!parsed) {
			return undefined;
		}

		const status = await this.fetchStatus(parsed.type, hostname, parsed.status_id);
		if (status?.code && status?.reason) {
			await this.handleRetry(href, status.code, status.reason);
			return undefined;
		}
		if (!status) {
			await this.handleRetry(href, 0, 'Fetched status is undefined [This should not happen]');
			return undefined;
		}

		const interactions = await this.parseInteractions(parsed.type, hostname, status);

		if (this.OptionalFeaturesService.CACHE_STATUS_REACTIONS) {
			/**
			 * If there is no emoji reactions on the remote instance
			 * Check if there is some emoji reactions on the current instance
			 * This enable the posibilty to get the emoji reactions on a status not being hosted on an instance supporting it
			 * Try to merge existing emojis with the newly fetched ones
			 * Try to increment the reaction count on each one
			 */
			if (!interactions.emoji_reactions.length && entity.emoji_reactions.length) {
				let emoji_reactions = await Promise.all(
					entity.emoji_reactions.map((reaction) =>
						this.handleEmojiReactionForMastodonPleroma(hostname, reaction),
					),
				);

				const stored = await this.getInteractionCache(href);
				if (stored && stored.state === RedisState.Ok) {
					emoji_reactions = this.mergeReactions(
						stored.value.emoji_reactions,
						emoji_reactions,
					);
				}

				interactions.emoji_reactions = emoji_reactions;
			}
		}

		let promises: Array<Promise<any>> = [];
		if (!this.isStoreNeeded(interactions)) {
			if (parsed.pleroma_url) {
				promises.push(
					this.setInteractionCache(
						parsed.pleroma_url,
						{
							state: RedisState.Retry,
							value: 'No store needed',
						},
						{ token: RedisExpiryToken.EX, time: 1800 },
					),
				);
			}

			promises.push(
				this.setInteractionCache(
					href,
					{
						state: RedisState.Retry,
						value: 'No store needed',
					},
					{ token: RedisExpiryToken.EX, time: 1800 },
				),
			);
		} else {
			const expires = this.getCacheExpiresDate(entity.created_at);

			if (parsed.pleroma_url) {
				promises.push(
					this.setInteractionCache(
						parsed.pleroma_url,
						{
							state: RedisState.Ok,
							value: interactions,
						},
						{ token: RedisExpiryToken.EX, time: expires },
					),
				);
			}

			promises.push(
				this.setInteractionCache(
					href,
					{
						state: RedisState.Ok,
						value: interactions,
					},
					{ token: RedisExpiryToken.EX, time: expires },
				),
			);
		}

		await Promise.all(promises);

		return {
			hostname,
			...interactions,
		};
	}

	/**
	 * @returns `Interactions` on success
	 * @returns `null` when cached and state isn't Ok
	 * @returns `undefined` on error
	 */
	async fetch(entity: Entity.Status, url: string): Promise<Interactions | null | undefined> {
		const interactions = await this.getInteractionCache(url);
		if (interactions && interactions.state !== RedisState.Ok) {
			return null;
		}

		if (!interactions) {
			const result = await this.fetchInteractions(entity, url);
			if (!result) {
				return undefined;
			}

			const { hostname, ...fetched } = result;
			return fetched;
		}

		return interactions.value;
	}

	async store(entity: Entity.Status) {
		const { uri, url } = entity;
		const href = uri ?? url;
		const type = await this.getInstanceType(href);
		if (!type) {
			return;
		}

		const { hostname } = new URL(href);

		const stored = await this.getInteractionCache(href);
		let interactions = await this.parseInteractions(type, hostname, entity);

		if (stored && stored.state === RedisState.Ok) {
			interactions.emoji_reactions = this.mergeReactions(
				stored.value.emoji_reactions,
				interactions.emoji_reactions,
			);
		}

		if (!this.isStoreNeeded(interactions)) {
			await this.setInteractionCache(
				href,
				{
					state: RedisState.Retry,
					value: 'No store needed',
				},
				{ token: RedisExpiryToken.EX, time: 1800 },
			);
			return;
		}

		await this.setInteractionCache(
			href,
			{
				state: RedisState.Ok,
				value: interactions,
			},
			{ token: RedisExpiryToken.EX, time: this.getCacheExpiresDate(entity.created_at) },
		);
	}

	// #endregion
}
