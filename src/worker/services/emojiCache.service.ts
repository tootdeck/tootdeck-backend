import { Injectable, Scope } from '@nestjs/common';
import { AxiosRequestConfig } from 'axios';
import { createWriteStream, existsSync } from 'fs';
const ffmpeg = require('fluent-ffmpeg');
// @ts-ignore for types
import FFMPEG from 'fluent-ffmpeg';

import { DBEmojiCacheService } from '../../database/services/database.emoji.cache.service';
import { DBEmojiFetchService } from '../../database/services/database.emoji.fetch.service';
import { AxiosCache } from '../../app/services/axiosCache.service';
import { CacheManager } from '../../app/services/cacheManager.service';
import { InstanceCacheService } from '../../app/services/instanceCache.service';
import { RequestsLimitReached } from './axiosLimit.service';

import { EmojiCache } from '../../database/entities/emoji.cache.entity';

import { Logger } from '../../utils/logger';

import { InstanceType } from '../../database/types';
import { RequestScopedJob } from './requestScopedJob.interface';
import { STORE_EMOJI, STORE_STATIC } from '../constants';

interface MastodonAPIEmoji {
	shortcode: string;
	url: string;
	static_url: string | null;
	// "visible_in_picker": true
	// "category": "trollfaces"
}

type MastodonAPIEmojiResponse = Array<MastodonAPIEmoji>;

interface MisskeyEmojiResponse {
	id: string;
	// "aliases": Array<string>,
	name: string;
	// "category": string,
	// "host": null,
	url: string;
	// "license": null,
	// "isSensitive": false,
	// "localOnly": false,
	// "roleIdsThatCanBeUsedThisEmojiAsReaction": []
}

@Injectable({ scope: Scope.REQUEST })
export class EmojiCacheService extends RequestScopedJob {
	private readonly logger = new Logger(EmojiCacheService.name);
	private readonly TIMEOUT: number = 20000;

	constructor(
		private readonly AxiosCache: AxiosCache,
		private readonly Cache: CacheManager,
		private readonly InstanceCacheService: InstanceCacheService,
		private readonly DBEmojiCacheService: DBEmojiCacheService,
		private readonly DBEmojiFetchService: DBEmojiFetchService,
	) {
		super();
	}

	private axiosConfig(config: AxiosRequestConfig<any>) {
		return {
			...config,
			jobUUID: this.jobUUID,
			addPrefix: 'EmojiCacheService',
		};
	}

	private async generateStaticURL(filename: string): Promise<string | null> {
		const static_filename = filename + '.webp';
		const result = await new Promise((resolve, reject) => {
			ffmpeg()
				.input(STORE_EMOJI + filename)
				.frames(1)
				.videoCodec('libwebp')
				.save(STORE_STATIC + static_filename)
				.on('end', () => {
					resolve(true);
				})
				.on('error', reject);
		})
			.then((r) => true)
			.catch((e) => {
				this.logger.error('generateStaticURL', filename);
				console.error(e);
				return false;
			});

		if (!result) {
			return null;
		}

		return static_filename;
	}

	private async fetchEmojiData(identifier: string, url: string): Promise<string | null> {
		const filename = identifier;
		const writer = createWriteStream(STORE_EMOJI + filename);

		const request = this.AxiosCache.request(
			this.axiosConfig({
				method: 'GET',
				url,
				responseType: 'stream',
			}),
			this.TIMEOUT,
		);
		if (!request.cached) {
			const response = await request.promise
				.then((r) => {
					if (r.status !== 200) {
						throw r;
					}
					r.data.pipe(writer);
					return r;
				})
				.catch((e) => {
					this.logger.warn(
						'fetchEmojiData',
						`${e.response?.status ?? e} ${identifier} ${url}`,
					);
					return null;
				});
			if (!response) {
				return null;
			}

			return new Promise<void>((resolve, reject) => {
				writer.on('finish', resolve);
				writer.on('error', reject);
			})
				.then(() => filename)
				.catch((e) => {
					this.logger.error('fetchEmojiData', filename);
					console.error(e);
					return null;
				});
		}

		return filename;
	}

	private async fetchEmojiMastodonAPI(
		hostname: string,
		identifier: string,
	): Promise<EmojiCache | null> {
		const stored_date = await this.DBEmojiFetchService.get(hostname);
		const now = new Date();
		if (stored_date && now.valueOf() - stored_date.updated_at.valueOf() < 604800000) {
			return null;
		}

		const url = 'https://' + hostname + '/api/v1/custom_emojis';
		const request = this.AxiosCache.request(
			this.axiosConfig({
				method: 'GET',
				url,
			}),
			this.TIMEOUT,
		);
		const emojis = await request.promise
			.then((r) => r.data as MastodonAPIEmojiResponse)
			.catch((e) => {
				if (e instanceof RequestsLimitReached) {
					throw e;
				}

				this.logger.warn('fetchEmojiMastodonAPI', `${e.response?.status ?? e} ${url}`);
				return null;
			});

		if (!emojis) {
			return null;
		}

		if (!request.cached) {
			const stored = await this.DBEmojiCacheService.createOrUpdate(
				hostname,
				emojis.map((emoji) => ({
					identifier: this.getIdentifier(hostname, emoji.shortcode),
					url: emoji.url,
					fallback: null,
					static_url: emoji.static_url ?? null,
				})),
			);
			await this.DBEmojiFetchService.createOrUpdate(hostname);

			if (!stored) {
				return null;
			}

			return stored.find((emoji) => emoji?.identifier === identifier) ?? null;
		}

		return this.DBEmojiCacheService.get(identifier);
	}

	private async fetchEmojiMisskey(
		hostname: string,
		identifier: string,
		emoji_name: string,
	): Promise<EmojiCache | null> {
		const url = 'https://' + hostname + '/api/emoji?name=' + emoji_name;
		const request = this.AxiosCache.request(
			this.axiosConfig({
				method: 'GET',
				url,
			}),
			this.TIMEOUT,
		);

		const emoji = await request.promise
			.then((r) => r.data as MisskeyEmojiResponse)
			.catch((e) => {
				if (e instanceof RequestsLimitReached) {
					throw e;
				}

				this.logger.warn('fetchEmojiMisskey', `${e.response?.status ?? e} ${url}`);
				return null;
			});

		if (!emoji) {
			return null;
		}

		return this.store({
			identifier,
			url: emoji.url,
			fallback: null,
			static_url: null,
		});
	}

	getIdentifier(hostname: string, emoji_name: string): string {
		return emoji_name + '@' + hostname;
	}

	private async fetchEmoji(
		type: InstanceType | undefined,
		hostname: string,
		emoji_name: string,
		identifier?: string,
	): Promise<EmojiCache | null | undefined> {
		if (!identifier) {
			identifier = this.getIdentifier(hostname, emoji_name);
		}
		switch (type) {
			case InstanceType.Mastodon:
			case InstanceType.Pleroma:
				return this.fetchEmojiMastodonAPI(hostname, identifier);
			case InstanceType.Misskey:
				return this.fetchEmojiMisskey(hostname, identifier, emoji_name);
			case undefined:
				const request = this.Cache.await(`getInsanceType:${hostname}`, () =>
					this.InstanceCacheService.get(hostname),
				);
				const found_type = await request.promise;
				if (found_type) {
					return this.fetchEmoji(found_type, hostname, emoji_name, identifier);
				}
				return found_type;
		}

		return undefined;
	}

	async get(
		type: InstanceType | undefined,
		hostname: string,
		emoji_name: string,
		fetch: boolean,
	): Promise<EmojiCache | null> {
		const identifier = this.getIdentifier(hostname, emoji_name);
		let emoji = await this.DBEmojiCacheService.get(identifier);
		if (emoji) {
			return emoji;
		}

		if (fetch) {
			const response = await this.Cache.await(
				`fetchEmoji:${identifier}`,
				() => this.fetchEmoji(type, hostname, emoji_name, identifier),
				this.TIMEOUT,
			).promise;

			return response ?? null;
		}

		return null;
	}

	private async generateStaticURLcdn(identifier: string, cdn: string) {
		return this.Cache.await(`getStaticURL:${identifier}`, async () => {
			const filename = await this.fetchEmojiData(identifier, cdn);
			if (!filename) {
				return null;
			}

			return this.generateStaticURL(filename);
		}).promise;
	}

	async getStaticURL(identifier: string, cdn?: string): Promise<string | null | undefined> {
		let emoji = await this.DBEmojiCacheService.get(identifier);
		if (!emoji) {
			// Not found in DB
			if (!cdn) {
				return null;
			}

			if (existsSync(STORE_STATIC + identifier + '.webp')) {
				// Found in local folder
				return identifier + '.webp';
			}

			// Fetching it
			const [name, domain] = identifier.split('@') as [string, string];
			const response = await this.fetchEmoji(undefined, domain, name);
			if (response === null) {
				// Unable to fetch
				return null;
			}
			if (response === undefined) {
				// Unable to determine instance type, using cdn fallback method
				return this.generateStaticURLcdn(identifier, cdn);
			}

			// Fetch successful
			emoji = response;
		}

		if (emoji.static_url) {
			return emoji.static_url;
		}

		return this.Cache.await(
			`getStaticURL:${identifier}`,
			async () => {
				const filename = await this.fetchEmojiData(identifier, emoji!.url);
				if (!filename) {
					return null;
				}

				const static_filename = await this.generateStaticURL(filename);
				const update = await this.DBEmojiCacheService.update({
					identifier,
					url: emoji!.url,
					fallback: filename,
					static_url: static_filename ?? 'DEAD',
				});
				if (!update) {
					return null;
				}

				return update.static_url;
			},
			this.TIMEOUT,
		).promise;
	}

	async store(emoji: EmojiCache) {
		return this.DBEmojiCacheService.create(emoji);
	}
}
