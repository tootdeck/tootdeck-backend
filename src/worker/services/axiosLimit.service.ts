import { Injectable } from '@nestjs/common';
import { UUID } from 'crypto';

import { Logger } from '../../utils/logger';

export class RequestsLimitReached extends Error {
	constructor() {
		super('Number of allowed requests reached.');
	}
}

@Injectable()
export class AxiosLimitService {
	private readonly logger = new Logger(AxiosLimitService.name);
	private limit: number = 0;
	private count = new Map<string, Array<string>>();

	private checkLimit(uuid: UUID, count: Array<string>): void {
		if (this.limit === 0) {
			return;
		}

		if (count.length >= this.limit) {
			this.logger.error('checkLimit', 'Requests limit reached');
			console.error('Requests stack', count);
			this.count.delete(uuid);
			throw new RequestsLimitReached();
		}
	}

	setlimit(value: number): void {
		if (value < 0) {
			throw new Error('Limit must be a postive number.');
		}
		this.limit = value;
	}

	increment(uuid: UUID, url: string): void {
		let count = this.count.get(uuid);
		if (!count) {
			count = [];
			this.count.set(uuid, count);
		}

		count.push(url);

		this.checkLimit(uuid, count);
	}

	delete(uuid: UUID): number {
		const count = this.count.get(uuid) ?? [];

		this.count.delete(uuid);

		return count.length;
	}
}
