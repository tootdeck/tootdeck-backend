import { Injectable, Scope } from '@nestjs/common';
import { Redis } from 'ioredis';
import { deserialize, serialize } from 'v8';
import { AxiosRequestConfig } from 'axios';

import { AxiosCache } from '../../app/services/axiosCache.service';

import { RedisNamespace } from '../../app/constants/redis';

import {
	RedisTwitterEmbedDead,
	RedisTwitterEmbedOK,
	TwitterResponse,
} from '../../dashboard/properties/redis/twitter.property';

import { RedisExpiryToken, RedisState } from '../../app/types';
import { RequestScopedJob } from './requestScopedJob.interface';
import { RedisService } from '@liaoliaots/nestjs-redis';

@Injectable({ scope: Scope.REQUEST })
export class TwitterEmbedService extends RequestScopedJob {
	private readonly Redis: Redis;

	constructor(
		private readonly AxiosCache: AxiosCache,
		RedisService: RedisService,
	) {
		super();
		this.Redis = RedisService.getOrThrow(RedisNamespace.TwitterEmbed);
	}

	private axiosConfig(config: AxiosRequestConfig<any>) {
		return {
			...config,
			jobUUID: this.jobUUID,
			addPrefix: 'TwitterEmbedService',
		};
	}

	private async setRedis(url: string, data: RedisTwitterEmbedOK | RedisTwitterEmbedDead) {
		return this.Redis.set(url, serialize(data), RedisExpiryToken.EX, 60 * 60 * 24 * 7);
	}

	private getRedis(url: string) {
		return this.Redis.getBuffer(url).then((r) => (r ? deserialize(r) : r));
	}

	private async fetchTwitter(url: string) {
		const { pathname } = new URL(url);
		const vx = 'https://api.vxtwitter.com';

		const request = this.AxiosCache.request<TwitterResponse>(
			this.axiosConfig({
				method: 'GET',
				url: vx + pathname,
				timeout: 20 * 1000,
			}),
		);
		return request.promise
			.then((r) => {
				if (r.status !== 200) {
					throw r;
				}

				const response: RedisTwitterEmbedOK = {
					state: RedisState.Ok,
					value: r.data,
				};

				return response;
			})
			.catch((e) => {
				const response: RedisTwitterEmbedDead = {
					state: RedisState.Dead,
					value: `${e.response?.status ?? e}`,
				};

				return response;
			});
	}

	async fetch(url: string): Promise<RedisTwitterEmbedOK | RedisTwitterEmbedDead> {
		const cache = await this.getRedis(url);
		if (cache) {
			return cache;
		}

		const response = await this.fetchTwitter(url);

		await this.setRedis(url, response);

		return response;
	}
}
