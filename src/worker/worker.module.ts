import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { HttpModule } from '@nestjs/axios';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { ScheduleModule } from '@nestjs/schedule';

import { JobsController } from './controllers/jobs.controller';
import { HealthController } from './controllers/health.controller';

import { AxiosProxy } from '../app/services/axiosProxy.service';
import { AxiosCache } from '../app/services/axiosCache.service';
import { CacheManager } from '../app/services/cacheManager.service';
import { OptionalFeaturesService } from '../app/services/utils/optionalFeatures.service';
import { TimesService } from '../app/services/utils/times.service';
import { DBEmojiCacheService } from '../database/services/database.emoji.cache.service';
import { DBEmojiFetchService } from '../database/services/database.emoji.fetch.service';
import { WorkerService } from './services/worker.service';
import { InstanceCacheService } from '../app/services/instanceCache.service';
import { AxiosLimitService } from './services/axiosLimit.service';
import { TwitterEmbedService } from './services/twitterEmbed.service';
import { AppService } from '../app/services/app.service';
import { FailedDomainsService } from '../app/services/failedDomain.service';
import { InteractionsService } from './services/interaction.service';
import { EmojiCacheService } from './services/emojiCache.service';

import { EmojiCache } from '../database/entities/emoji.cache.entity';
import { EmojiFetch } from '../database/entities/emoji.fetch.entity';

import { typeormConfig } from '../app/configs/typeorm.config';
import { RedisConfig } from '../app/configs/redis.config';
import { RedisDB } from '../app/constants/redis';

@Module({
	imports: [
		ConfigModule.forRoot({ isGlobal: true }),
		HttpModule,
		RedisConfig([
			RedisDB.StatusesInteractions,
			RedisDB.ExcludedDomains,
			RedisDB.InstanceCache,
			RedisDB.TwitterEmbed,
			RedisDB.FailedDomain,
		]),
		...typeormConfig([EmojiCache, EmojiFetch]),
		ScheduleModule.forRoot(),
		ClientsModule.registerAsync({
			clients: [
				{
					name: 'MAIN_PROCESS',
					imports: [ConfigModule],
					inject: [ConfigService],
					useFactory: async (configService: ConfigService) => ({
						transport: Transport.TCP,
						options: {
							host: configService.getOrThrow<string>('MAIN_PROCESS_HOST'),
							port: configService.getOrThrow<number>('MAIN_PROCESS_PORT'),
						},
					}),
				},
			],
		}),
	],
	controllers: [JobsController, HealthController],
	providers: [
		AxiosProxy,
		AxiosCache,
		AxiosLimitService,
		CacheManager,
		InstanceCacheService,
		OptionalFeaturesService,
		TimesService,
		FailedDomainsService,
		DBEmojiCacheService,
		DBEmojiFetchService,
		AppService,
		WorkerService,
		InteractionsService,
		EmojiCacheService,
		TwitterEmbedService,
	],
})
export class WorkerModule {}
