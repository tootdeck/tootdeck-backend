import { UUID } from 'crypto';

import { ResponseTime } from '../app/services/utils/times.service';

export namespace Workers {
	export enum Event {
		Register = 'register',
		Unregister = 'unregister',
		Healthcheck = 'healthcheck',
	}

	export interface Stats {
		uuid: string;
		started_at: Date;
		memory: number;
		requests_made: number;
		times: ResponseTime;
	}

	export interface JobResult<T> {
		stats: Stats;
		result: T;
	}

	export interface Register {
		uuid: UUID;
		ip: string;
	}
}
