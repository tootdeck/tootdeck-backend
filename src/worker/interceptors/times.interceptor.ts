import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Observable, tap } from 'rxjs';

import { TimesService } from '../../app/services/utils/times.service';

@Injectable()
export class TimesInterceptor implements NestInterceptor {
	constructor(private readonly TimesService: TimesService) {}

	intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
		const now = new Date().valueOf();
		return next.handle().pipe(
			tap(() => {
				this.TimesService.addTime(new Date().valueOf() - now);
			}),
		);
	}
}
