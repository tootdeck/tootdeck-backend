export const STORE_PATH = 'custom_emojis';
export const STORE_EMOJI = STORE_PATH + '/original/';
export const STORE_STATIC = STORE_PATH + '/static/';
