import { Controller, Get, Post } from '@nestjs/common';

import { WorkerService } from '../services/worker.service';

@Controller()
export class HealthController {
	constructor(private readonly WorkerService: WorkerService) {}

	@Get('ping')
	ping(): string {
		return 'PONG';
	}

	@Post('reset_requests_count')
	resetRequestsCount(): void {
		this.WorkerService.resetRequestsCount();
	}
}
