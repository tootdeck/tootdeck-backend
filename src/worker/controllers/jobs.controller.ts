import { Controller, UseInterceptors } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { randomUUID } from 'crypto';

import { Entity } from '../../lib/megalodon/entities';

import { InteractionsService } from '../services/interaction.service';
import { EmojiCacheService } from '../services/emojiCache.service';
import { WorkerService } from '../services/worker.service';
import { TwitterEmbedService } from '../services/twitterEmbed.service';

import { TimesInterceptor } from '../interceptors/times.interceptor';

import { Interactions } from '../../dashboard/properties/redis/interactions.property';
import {
	RedisTwitterEmbedDead,
	RedisTwitterEmbedOK,
} from '../../dashboard/properties/redis/twitter.property';

import { Workers } from '../types';

export enum AvailableJobs {
	InteractionsFetch = 1,
	InteractionsStore = 2,
	EmojiGetStatic = 3,
	TwitterEmbed = 4,
}

export type InteractionsFetchJobData = { entity: Entity.Status; url: string };
export type InteractionsStoreJobData = { entity: Entity.Status };
export type EmojiGetStaticJobData = { identifier: string; cdn?: string | undefined };
export type TwitterEmbedJobData = { url: string };

export type InteractionsFetchCallbackData = Interactions | null | undefined;
export type EmojiGetStaticCallbackData = string | null | undefined;
export type TwitterEmbedCallbackData =
	| RedisTwitterEmbedOK
	| RedisTwitterEmbedDead
	| null
	| undefined;

@UseInterceptors(TimesInterceptor)
@Controller()
export class JobsController {
	constructor(
		private readonly MainService: WorkerService,
		private readonly TwitterEmbedService: TwitterEmbedService,
		private readonly InteractionsService: InteractionsService,
		private readonly EmojiCacheService: EmojiCacheService,
	) {}

	@MessagePattern(AvailableJobs.InteractionsFetch)
	async fetch(
		data: InteractionsFetchJobData,
	): Promise<Workers.JobResult<InteractionsFetchCallbackData>> {
		return this.InteractionsService.fetch(data.entity, data.url).then((r) =>
			this.MainService.jobFinish(this.InteractionsService.jobUUID, r),
		);
	}

	@MessagePattern(AvailableJobs.InteractionsStore)
	async store(data: InteractionsStoreJobData): Promise<Workers.JobResult<void>> {
		return this.InteractionsService.store(data.entity).then((r) =>
			this.MainService.jobFinish(this.InteractionsService.jobUUID, r),
		);
	}

	@MessagePattern(AvailableJobs.EmojiGetStatic)
	async getStaticURL(
		data: EmojiGetStaticJobData,
	): Promise<Workers.JobResult<EmojiGetStaticCallbackData>> {
		return this.EmojiCacheService.getStaticURL(data.identifier, data.cdn).then((r) =>
			this.MainService.jobFinish(this.InteractionsService.jobUUID, r),
		);
	}

	@MessagePattern(AvailableJobs.TwitterEmbed)
	async fetchTwitterEmbed(
		data: TwitterEmbedJobData,
	): Promise<Workers.JobResult<TwitterEmbedCallbackData>> {
		return this.TwitterEmbedService.fetch(data.url).then((r) =>
			this.MainService.jobFinish(this.TwitterEmbedService.jobUUID, r),
		);
	}
}
