import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { WorkerStats } from '../entities/worker.stats.entity';

import { Logger } from '../../utils/logger';

import { WorkerStatsMerge } from '../types';

@Injectable()
export class DBWorkerStatsService {
	private readonly logger = new Logger(DBWorkerStatsService.name);

	constructor(@InjectRepository(WorkerStats) private readonly repo: Repository<WorkerStats>) {}

	private error(fn: string, text: string, e?: any) {
		this.logger.error(fn, text);
		if (e) {
			console.error(e);
		}
		return null;
	}

	async get(limit: number): Promise<Array<WorkerStats> | null> {
		if (limit <= 0) {
			return [];
		}

		return this.repo
			.createQueryBuilder()
			.select([
				'jobs_processed',
				'jobs_failed',
				'requests_made',
				'worker_queue_max',
				'worker_memory_max',
				'worker_job_time_min',
				'worker_job_time_max',
				'worker_job_time_average',
				'cache_size_max',
				'cache_size_dead',
				'created_at',
				'merge_type',
			])
			.orderBy('created_at', 'DESC')
			.limit(limit)
			.getRawMany()
			.catch((e) => this.error('get', `Unable to get ${limit} worker stats`, e));
	}

	async getLast(): Promise<WorkerStats | null> {
		return this.repo
			.createQueryBuilder()
			.select()
			.orderBy('created_at', 'DESC')
			.limit(1)
			.getOne()
			.catch((e) => this.error('get', `Unable to get latest worker stats`, e));
	}

	async create(record: Partial<WorkerStats>): Promise<WorkerStats | null> {
		const entry = this.repo.create({
			...record,
			created_at: new Date(),
			merge_type: WorkerStatsMerge.NotMerged,
		});

		return this.repo
			.save(entry)
			.catch((e) =>
				this.error(
					'create',
					`Unable to create worker stats record for ${entry.created_at}`,
					e,
				),
			);
	}

	@Cron(CronExpression.EVERY_DAY_AT_MIDNIGHT, {
		disabled: process.env['NODE_ENV'] === 'test',
	})
	async mergeIntoDaily(): Promise<boolean | null> {
		return this.merge(WorkerStatsMerge.NotMerged);
	}

	@Cron(CronExpression.EVERY_WEEK, {
		disabled: process.env['NODE_ENV'] === 'test',
	})
	async mergeIntoWeekly(): Promise<boolean | null> {
		return this.merge(WorkerStatsMerge.Daily);
	}

	async merge(merge: WorkerStatsMerge): Promise<boolean | null> {
		if (merge === WorkerStatsMerge.Weekly) {
			throw new Error(`Merging Weekly records isn't supported.`);
		}

		let limit: number = 0;
		switch (merge) {
			case WorkerStatsMerge.NotMerged: // => WorkerStatsMerge.Daily
				limit = 60 * 24;
				break;
			case WorkerStatsMerge.Daily: // => WorkerStatsMerge.Weekly
				/**
				 *      60 * 24 => one day (NotMerged)
				 *  one day / 5 => Daily
				 *    Daily * 6 => 6 days of merged records
				 */
				limit = ((60 * 24) / 5) * 6;
				break;
		}

		const type =
			merge === WorkerStatsMerge.NotMerged
				? 'Not merged'
				: merge === WorkerStatsMerge.Daily
					? 'Daily'
					: '';

		const entities: Array<WorkerStats> | null = await this.repo
			.createQueryBuilder()
			.select()
			.where({ merge_type: merge })
			.orderBy('created_at', 'ASC')
			.getMany()
			.catch((e) => this.error('merge', `Unable to get ${type} records`, e));
		if (!entities) {
			return false;
		}

		const entities_len = entities.length;
		if (entities_len <= limit) {
			this.logger.log(
				'merge',
				`Not enough records to merge, databse has currently ${entities_len} records, the merger need more than ${limit} records`,
			);
			return false;
		}

		const del_entities: Array<WorkerStats> = [];
		const new_entities: Array<WorkerStats> = [];

		while (entities.length) {
			let slice: Array<WorkerStats> = [];
			let len = 0;
			switch (merge) {
				case WorkerStatsMerge.NotMerged: // => WorkerStatsMerge.Daily
					len = 5;
					break;
				case WorkerStatsMerge.Daily: // => WorkerStatsMerge.Weekly
					len = 4;
					break;
			}

			slice = entities.splice(0, len);
			/**
			 * Needs 5 NotMerged records to do Daily
			 * Needs 4 Daily records to do Weekly
			 */
			if (slice.length < len) {
				break;
			}

			/**
			 * Needs 60 * 24 NotMerged records to do Daily
			 * if entities.length isn't 60 * 24 break because
			 * the database doesn't have a full day of records
			 *
			 * Needs 60 * 30 Daily records to do Weekly
			 * if entities.length isn't 60 * 30 break beacuse
			 * the database doesn't have 7 days of records
			 */
			if (entities.length <= limit) {
				break;
			}

			const merged = slice.reduce((previous, current) => ({
				// Only save latest values
				jobs_processed: Math.max(previous.jobs_processed, current.jobs_processed),
				jobs_failed: Math.max(previous.jobs_failed, current.jobs_failed),
				requests_made: Math.max(previous.requests_made, current.requests_made),
				// Merge values
				worker_queue_max: previous.worker_queue_max + current.worker_queue_max,
				worker_memory_max: Math.max(previous.worker_memory_max, current.worker_memory_max),
				worker_job_time_min: Math.min(
					previous.worker_job_time_min,
					current.worker_job_time_min,
				),
				worker_job_time_max: Math.max(
					previous.worker_job_time_max,
					current.worker_job_time_max,
				),
				worker_job_time_average:
					previous.worker_job_time_average + current.worker_job_time_average,
				cache_size_max: Math.max(previous.cache_size_max, current.cache_size_max),
				cache_size_dead: Math.max(previous.cache_size_dead, current.cache_size_dead),
				// Ignored values
				created_at: current.created_at,
				merge_type: current.merge_type,
				uuid: current.uuid,
			}));

			const worker_job_time_average = Math.round(
				merged.worker_job_time_average / slice.length,
			);

			let merge_type: WorkerStatsMerge;
			if (merge === WorkerStatsMerge.NotMerged) {
				merge_type = WorkerStatsMerge.Daily;
			} else if (merge === WorkerStatsMerge.Daily) {
				merge_type = WorkerStatsMerge.Weekly;
			}

			const entry = this.repo.create({
				jobs_processed: merged.jobs_processed,
				jobs_failed: merged.jobs_failed,
				requests_made: merged.requests_made,
				worker_queue_max: merged.worker_queue_max,
				worker_memory_max: merged.worker_memory_max,
				worker_job_time_min: merged.worker_job_time_min,
				worker_job_time_max: merged.worker_job_time_max,
				worker_job_time_average: worker_job_time_average,
				cache_size_max: merged.cache_size_max,
				cache_size_dead: merged.cache_size_dead,
				created_at: merged.created_at,
				merge_type: merge_type!,
			});

			new_entities.push(entry);
			del_entities.push(...slice);

			this.logger.verbose(
				'merge',
				`Merging ${type}: ${new_entities.length * len}/${entities_len - limit}`,
			);
		}

		if (!del_entities.length || !new_entities.length) {
			return false;
		}

		const del = await this.repo
			.delete(del_entities.map((e) => e.uuid))
			.catch((e) => this.error('merge', `Unable to delete previous entries`, e));
		if (!del) {
			return false;
		}

		return this.repo
			.save(new_entities)
			.then((r) => !!r.length)
			.catch((e) => this.error('merge', `Unable to create merged entries`, e));
	}
}
