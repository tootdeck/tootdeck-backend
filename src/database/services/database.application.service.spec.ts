import { Test, TestingModule } from '@nestjs/testing';

import { FullAppModule } from '../../app/app.module';

import { DBApplicationService } from './database.application.service';

import { InstanceType } from '../types';

describe('DBApplicationService', () => {
	let Application: DBApplicationService;

	let application_uuid = '';
	let application_domain = 'example.com';
	let application_key = 't52jyR5LOeqAK4PhXqI7rktO7CZaD6OPaRohyFVrgzI=';
	let application_secret = 'qlUrKaKNhIuUZrt1pxvGrOnxNdrMDyqPp+qkvhn4Fzw=';
	let application_token = 'HNPLb/zOpHB66YonMo5pL8LUEQUQfrOBK8NGmCKh4d4=';
	let application_streaming_url = `wss://${application_domain}/api/streaming`;
	let application_creation_at = new Date();
	let application_updated_at = new Date();

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule(FullAppModule).compile();

		Application = app.get<DBApplicationService>(DBApplicationService);
	});

	describe('create', () => {
		test('Create a valid application', async () => {
			await Application.create(
				application_domain,
				InstanceType.Mastodon,
				application_key,
				application_secret,
			).then((entity) => {
				// @ts-ignore
				application_uuid = entity?.uuid;
				// @ts-ignore
				application_creation_at = entity?.created_at;
				// @ts-ignore
				application_updated_at = application_creation_at;

				expect(entity).toMatchObject({
					domain: application_domain,
					key: application_key,
					secret: application_secret,
					type: InstanceType.Mastodon,
				});
			});
		});

		test('Create a valid duplicate', async () => {
			await Application.create(
				application_domain,
				InstanceType.Mastodon,
				application_key,
				application_secret,
			).then((entity) => expect(entity).toBeNull());
		});

		test('Create invalid entries', async () => {
			await Application.create(
				undefined as any,
				InstanceType.Mastodon,
				application_key,
				application_secret,
			).then((entity) => expect(entity).toBeNull());
			await Application.create(
				application_domain,
				undefined as any,
				application_key,
				application_secret,
			).then((entity) => expect(entity).toBeNull());
			await Application.create(
				application_domain,
				InstanceType.Mastodon,
				undefined as any,
				application_secret,
			).then((entity) => expect(entity).toBeNull());
			await Application.create(
				application_domain,
				InstanceType.Mastodon,
				application_key,
				undefined as any,
			).then((entity) => expect(entity).toBeNull());
		});
	});

	describe('get', () => {
		test('Get a valid application', async () => {
			await Application.get(application_domain).then((entity) =>
				expect(entity).toMatchObject({
					uuid: application_uuid,
					domain: application_domain,
					key: application_key,
					secret: application_secret,
					type: InstanceType.Mastodon,
				}),
			);
		});

		test('Get invalid entries', async () => {
			await Application.get(undefined as any).then((entity) => expect(entity).toBeNull());
			await Application.get('domain').then((entity) => expect(entity).toBeNull());
		});
	});

	describe('updateLastUsage', () => {
		test('Update last usage of a valid application', async () => {
			await Application.updateLastUsage(application_domain).then((entity) => {
				expect(entity).toMatchObject({
					uuid: application_uuid,
					domain: application_domain,
					key: application_key,
					secret: application_secret,
					type: InstanceType.Mastodon,
					created_at: application_creation_at,
				});
				expect(entity?.updated_at !== application_updated_at).toBeTruthy();
			});
		});

		test('Update last usage of an invalid application', async () => {
			await Application.updateLastUsage('domain').then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	describe('updateCredentials', () => {
		application_key = 'kBUMYwb1EM//OPkffBwFDjukSBRYVLo0qSo0he/ZIrU=';
		application_secret = '3iyAjnCNsHrUTtcjqbWxEiJ3jPkRsVUdcH3ZMEBGUAY';

		test('Update credentials of a valid application', async () => {
			await Application.updateCredentials(
				application_domain,
				application_key,
				application_secret,
			).then((entity) => {
				expect(entity).toMatchObject({
					uuid: application_uuid,
					domain: application_domain,
					key: application_key,
					secret: application_secret,
					type: InstanceType.Mastodon,
					created_at: application_creation_at,
				});
			});
		});

		test('Update credentials of an invalid application', async () => {
			await Application.updateCredentials('domain', application_key, application_secret).then(
				(entity) => {
					expect(entity).toBeNull();
				},
			);
			await Application.updateCredentials(
				application_domain,
				undefined as any,
				application_secret,
			).then((entity) => {
				expect(entity).toBeNull();
			});
			await Application.updateCredentials(
				application_domain,
				application_key,
				undefined as any,
			).then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	describe('updateToken', () => {
		test('Add token to a valid application', async () => {
			await Application.updateToken(application_domain, application_token).then((entity) => {
				expect(entity).toMatchObject({
					uuid: application_uuid,
					domain: application_domain,
					key: application_key,
					secret: application_secret,
					type: InstanceType.Mastodon,
					token: application_token,
					created_at: application_creation_at,
				});
			});
		});

		test('Add token to an invalid application', async () => {
			await Application.updateToken('domain', application_token).then((entity) => {
				expect(entity).toBeNull();
			});
			await Application.updateToken(undefined as any, application_token).then((entity) => {
				expect(entity).toBeNull();
			});
		});

		test('Add invalid token to a application', async () => {
			await Application.updateToken(application_domain, undefined as any).then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	describe('updateStreamingURL', () => {
		test('Add streaming url to a valid application', async () => {
			await Application.updateStreamingURL(
				application_domain,
				application_streaming_url,
			).then((entity) => {
				expect(entity).toMatchObject({
					uuid: application_uuid,
					domain: application_domain,
					key: application_key,
					secret: application_secret,
					type: InstanceType.Mastodon,
					token: application_token,
					streaming_url: application_streaming_url,
					created_at: application_creation_at,
				});
			});
		});

		test('Add streaming url to an invalid application', async () => {
			await Application.updateStreamingURL('domain', application_streaming_url).then(
				(entity) => {
					expect(entity).toBeNull();
				},
			);
			await Application.updateStreamingURL(undefined as any, application_streaming_url).then(
				(entity) => {
					expect(entity).toBeNull();
				},
			);
		});

		test('Add invalid streaming url to a application', async () => {
			await Application.updateStreamingURL(application_domain, undefined as any).then(
				(entity) => {
					expect(entity).toBeNull();
				},
			);
		});
	});

	describe('delete', () => {
		test('Delete a valid application', async () => {
			await Application.delete(application_domain).then((entity) => {
				expect(entity).toBeTruthy();
			});
		});

		test('Delete an invalid application', async () => {
			await Application.delete('domain').then((entity) => {
				expect(entity).toBeNull();
			});
			await Application.delete(undefined as any).then((entity) => {
				expect(entity).toBeNull();
			});
		});

		test('Delete a valid application previously deleted', async () => {
			await Application.delete(application_domain).then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	afterAll(async () => {
		await Application.delete(application_domain).catch(() => {});
	});
});
