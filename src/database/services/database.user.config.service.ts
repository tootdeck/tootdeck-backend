import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { UserStore } from '../entities/user.entity';

import { Logger } from '../../utils/logger';

import { ConfigStore } from '../entities/config.entity';

@Injectable()
export class DBUserConfigService {
	private readonly logger = new Logger(DBUserConfigService.name);

	constructor(@InjectRepository(ConfigStore) private readonly repo: Repository<ConfigStore>) {}

	private error(fn: string, text: string, e?: any) {
		this.logger.error(fn, text);
		if (e) {
			console.error(e);
		}
		return null;
	}

	async createOrUpdate(user: UserStore, config_value: string): Promise<ConfigStore | null> {
		let config = user.config;
		if (config) {
			config.value = config_value;
			config.updated_at = new Date();
		} else {
			config = this.repo.create({
				value: config_value,
				updated_at: new Date(),
			});
		}

		return this.repo
			.save(config)
			.catch((e) =>
				this.error(
					'add',
					`Unable to store user config for ${user.main.username}@${user.main.application.domain}`,
					e,
				),
			);
	}

	async delete(config_uuid: string): Promise<boolean | null> {
		return this.repo
			.delete(config_uuid)
			.then((r) => !!r.affected)
			.catch((e) => this.error('delete', `Unable to delete config ${config_uuid}`, e));
	}
}
