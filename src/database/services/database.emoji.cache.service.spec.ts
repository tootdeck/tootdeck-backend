import { Test, TestingModule } from '@nestjs/testing';

import { FullAppModule } from '../../app/app.module';

import { DBEmojiCacheService } from './database.emoji.cache.service';

describe('DBEmojiCacheService', () => {
	let EmojiCache: DBEmojiCacheService;

	let application_domain = 'example.com';
	const identifier = `joy@${application_domain}`;
	const emoji = {
		identifier,
		fallback: null,
		url: `https://${application_domain}/emoji/joy.png`,
		static_url: null,
	};

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule(FullAppModule).compile();

		EmojiCache = app.get<DBEmojiCacheService>(DBEmojiCacheService);
	});

	describe('create', () => {
		const value = structuredClone(emoji);

		test('Create a valid emoji', async () => {
			await EmojiCache.create(value).then((entity) => {
				expect(entity).toMatchObject(value);
			});
		});

		test('Create an invalid emoji', async () => {
			value.identifier = '';

			await EmojiCache.create(value).then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	describe('createOrUpdate', () => {
		const array = [
			structuredClone(emoji),
			structuredClone(emoji),
			structuredClone(emoji),
			structuredClone(emoji),
			structuredClone(emoji),
		].map((v, i) => {
			v.identifier = i + v.identifier;
			return v;
		});

		test('Create new valid emojis', async () => {
			await EmojiCache.createOrUpdate(application_domain, array).then((entities) => {
				expect(entities).toMatchObject(array);
			});
		});

		test('Nothing to change', async () => {
			await EmojiCache.createOrUpdate(application_domain, array).then((entities) => {
				expect(entities).toMatchObject(array);
			});
		});

		test('5 to remove, 1 to add', async () => {
			const new_emoji = structuredClone(emoji);
			await EmojiCache.createOrUpdate(application_domain, [new_emoji]).then((entities) => {
				expect(entities).toMatchObject([new_emoji]);
			});
		});
	});

	describe('get', () => {
		test('Get a valid emoji', async () => {
			await EmojiCache.get(identifier).then((entity) => {
				expect(entity).toMatchObject(emoji);
			});
		});

		test('Get an invalid emoji', async () => {
			await EmojiCache.get('emoji').then((entity) => {
				expect(entity).toBeNull();
			});
			await EmojiCache.get(undefined as any).then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	describe('getAllFromInstance', () => {
		test('Get all emojis from a domain', async () => {
			await EmojiCache.getAllFromInstance(application_domain).then((entities) => {
				expect(entities?.length).toBe(1);
			});
		});
		test('Get all emojis from an invalid domain', async () => {
			await EmojiCache.getAllFromInstance('domain').then((entities) => {
				expect(entities?.length).toBe(0);
			});
			await EmojiCache.getAllFromInstance('').then((entities) => {
				expect(entities).toBeNull();
			});
			await EmojiCache.getAllFromInstance(undefined as any).then((entities) => {
				expect(entities).toBeNull();
			});
		});
	});

	describe('update', () => {
		const value = structuredClone(emoji);

		test('Update one emoji', async () => {
			// @ts-ignore
			value.static_url = 'static';

			await EmojiCache.update(value).then((entity) => {
				expect(entity).toMatchObject({
					...entity,
					static_url: 'static',
				});
			});
		});

		test('Update one non existing emoji', async () => {
			value.identifier = '@' + identifier;

			await EmojiCache.update(value).then((entity) => {
				expect(entity).toBeNull();
			});
		});

		test('Update one invalid emoji', async () => {
			value.identifier = '';

			await EmojiCache.update(value).then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	describe('delete', () => {
		test('Delete one emoji', async () => {
			await EmojiCache.delete(emoji.identifier).then((r) => {
				expect(r).toBeTruthy();
			});
		});

		test('Delete one non existing emoji', async () => {
			await EmojiCache.delete('id').then((r) => {
				expect(r).toBeFalsy();
			});
		});
	});

	afterAll(async () => {
		await EmojiCache.getAllFromInstance(application_domain)
			.then((r) =>
				Promise.all(r?.map((e) => EmojiCache.delete(e.identifier).catch(() => {})) ?? []),
			)
			.catch(() => {});
	});
});
