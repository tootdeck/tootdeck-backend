import { Test, TestingModule } from '@nestjs/testing';
import { randomUUID } from 'crypto';

import { FullAppModule } from '../../app/app.module';

import { DBApplicationService } from './database.application.service';
import { DBAccountService } from './database.account.service';

import { InstanceType } from '../types';

describe('DBApplicationService', () => {
	let Application: DBApplicationService;
	let Account: DBAccountService;

	let application_uuid = '';
	let application_domain = 'example.com';
	let application_key = 't52jyR5LOeqAK4PhXqI7rktO7CZaD6OPaRohyFVrgzI=';
	let application_secret = 'qlUrKaKNhIuUZrt1pxvGrOnxNdrMDyqPp+qkvhn4Fzw=';
	let application_creation_at = new Date();
	let application_updated_at = new Date();

	let account_uuid = '';
	let account_username = 'tootdeck';
	let account_domain = application_domain;
	let account_token = 'xOOnkfBjAtRqtAmqWFNZEhDsjetgaBIFccIg1EPLqdM=';

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule(FullAppModule).compile();

		Application = app.get<DBApplicationService>(DBApplicationService);
		Account = app.get<DBAccountService>(DBAccountService);

		/**
		 * Create a valid application
		 */
		await Application.create(
			application_domain,
			InstanceType.Mastodon,
			application_key,
			application_secret,
		).then((entity) => {
			application_uuid = entity?.uuid ?? '';
			application_creation_at = entity?.created_at ?? new Date();
			application_updated_at = application_creation_at;
		});
	});

	describe('create', () => {
		test('Create a valid account', async () => {
			await Account.create(account_username, account_domain, account_token).then((entity) => {
				// @ts-ignore
				account_uuid = entity?.uuid;

				expect(entity).toMatchObject({
					username: account_username,
					token: account_token,
					application: {
						domain: account_domain,
					},
				});
			});
		});

		test('Create a vaild duplicate account', async () => {
			await Account.create(account_username, account_domain, account_token).then((entity) => {
				expect(entity).toBeNull();
			});
		});

		test('Create an invaild account', async () => {
			await Account.create(undefined as any, account_domain, account_token).then((entity) => {
				expect(entity).toBeNull();
			});
			await Account.create(account_username, undefined as any, account_token).then(
				(entity) => {
					expect(entity).toBeNull();
				},
			);
		});

		test('Create a vaild account with an invalid token', async () => {
			await Account.create(account_username, account_domain, undefined as any).then(
				(entity) => {
					expect(entity).toBeNull();
				},
			);
			await Account.create(account_username, account_domain, '').then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	describe('get', () => {
		test('Get a valid account', async () => {
			await Account.get(account_username, account_domain).then((entity) => {
				expect(entity).toMatchObject({
					uuid: account_uuid,
					username: account_username,
					token: account_token,
					application: {
						domain: account_domain,
					},
				});
			});
		});

		test('Get an invalid account', async () => {
			await Account.get('usernmae', account_domain).then((entity) => {
				expect(entity).toBeNull();
			});
			await Account.get(undefined as any, account_domain).then((entity) => {
				expect(entity).toBeNull();
			});
			await Account.get(account_username, undefined as any).then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	describe('getByUUID', () => {
		test('Get an invalid account', async () => {
			await Account.getByUUID(account_uuid).then((entity) => {
				expect(entity).toMatchObject({
					uuid: account_uuid,
					username: account_username,
					token: account_token,
					application: {
						domain: account_domain,
					},
				});
			});
		});

		test('Get an invalid account', async () => {
			await Account.getByUUID(randomUUID()).then((entity) => {
				expect(entity).toBeNull();
			});
			await Account.getByUUID(undefined as any).then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	describe('update', () => {
		account_token = 'x5WX8qSuQWxohEc7uhOTBYSWNxpgO/aiOGRHaOegoV4=';

		test('Update a valid account', async () => {
			await Account.updateToken(account_username, account_domain, account_token).then(
				(entity) => {
					account_uuid = entity?.uuid ?? '';

					expect(entity).toMatchObject({
						username: account_username,
						token: account_token,
						application: {
							domain: account_domain,
						},
					});
				},
			);
		});

		test('Update an invaild account', async () => {
			await Account.updateToken(undefined as any, account_domain, account_token).then(
				(entity) => {
					expect(entity).toBeNull();
				},
			);
			await Account.updateToken(account_username, undefined as any, account_token).then(
				(entity) => {
					expect(entity).toBeNull();
				},
			);
		});

		test('Update a vaild account with an invalid token', async () => {
			await Account.updateToken(account_username, account_domain, undefined as any).then(
				(entity) => {
					expect(entity).toBeNull();
				},
			);
			await Account.updateToken(account_username, account_domain, '').then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	describe('delete', () => {
		test('Delete a valid account', async () => {
			await Account.delete(account_username, account_domain).then((entity) => {
				expect(entity).toBeTruthy();
			});
		});

		test('Delete an invalid account', async () => {
			await Account.delete(account_username, 'domain').then((entity) => {
				expect(entity).toBeNull();
			});
			await Account.delete(account_username, undefined as any).then((entity) => {
				expect(entity).toBeNull();
			});
			await Account.delete('username', account_domain).then((entity) => {
				expect(entity).toBeNull();
			});
			await Account.delete(undefined as any, account_domain).then((entity) => {
				expect(entity).toBeNull();
			});
		});

		test('Delete a valid account previously deleted', async () => {
			await Account.delete(account_username, account_domain).then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	afterAll(async () => {
		await Account.delete(account_username, account_domain).catch(() => {});
		await Application.delete(application_domain).catch(() => {});
	});
});
