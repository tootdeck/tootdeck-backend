import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, ILike, QueryFailedError, Repository } from 'typeorm';

import { AppService } from '../../app/services/app.service';

import { EmojiCache } from '../entities/emoji.cache.entity';

import { Logger } from '../../utils/logger';

@Injectable()
export class DBEmojiCacheService {
	private readonly logger = new Logger(DBEmojiCacheService.name);

	constructor(
		private readonly AppService: AppService,
		@InjectRepository(EmojiCache) private readonly repo: Repository<EmojiCache>,
	) {
		/**
		 * Maintenance script
		 */
		if (this.AppService.isMainThread()) {
			this.maintenance();
		}
	}

	private error(fn: string, text: string, e?: any) {
		this.logger.error(fn, text);
		if (e) {
			console.error(e);
		}
		return null;
	}

	/**
	 * Maintenance script
	 */
	private async maintenance() {
		const entries = await this.repo.find({
			where: {
				url: ILike('%?url%'),
			},
		});

		if (!entries.length) {
			return;
		}

		this.logger.verbose('maintenance', `Updating DBEmojiCache entries`);

		this.chunk(
			entries.map((entry) => this.fixProxyURL(entry)),
			this.repo.save.bind(this.repo),
			(e) => this.error('maintenance', `Unable to update emojis`, e),
		);
	}

	async get(identifier: string): Promise<EmojiCache | null> {
		if (!identifier) {
			return null;
		}

		return this.repo
			.findOne({
				where: { identifier },
			})
			.catch((e) =>
				this.error('get', `Unable to get emoji with identifier ${identifier}`, e),
			);
	}

	async getAllFromInstance(instance: string): Promise<Array<EmojiCache> | null> {
		if (!instance) {
			return null;
		}

		return this.repo
			.find({
				where: { identifier: ILike(`%${instance}`) },
			})
			.catch((e) =>
				this.error('getAllFromInstance', `Unable to get emojis for domain ${instance}`, e),
			);
	}

	private fixProxyURL(emoji: EmojiCache) {
		if (emoji.url && emoji.url.includes('?url')) {
			emoji.url = new URL(emoji.url).searchParams.get('url')!;
		}

		return emoji;
	}

	private async chunk<T, R>(
		data: Array<T>,
		fn: (arg: Array<T>) => Promise<R>,
		error_cb: (e: any) => null,
	): Promise<Array<R> | null> {
		const ret: Array<R> = [];

		while (data.length) {
			this.logger.verbose('chunk', `${data.length}/1000`);
			const chunk = data.splice(0, 1000);
			const response = await fn(chunk)
				.then((r) => ret.push(r))
				.catch(error_cb);
			if (!response) {
				return null;
			}
		}

		return ret;
	}

	async createOrUpdate(
		instance: string,
		new_emojis: Array<EmojiCache>,
	): Promise<Array<EmojiCache> | null> {
		const db_emojis = await this.getAllFromInstance(instance);
		if (!db_emojis || !db_emojis.length) {
			// Emojis from this instances aren't stored yet, creating db records for all
			return this.chunk<EmojiCache, EmojiCache>(
				new_emojis.map((emoji) => this.repo.create(this.fixProxyURL(emoji))),
				this.repo.save.bind(this.repo),
				(e) => this.error('createOrUpdate', `Unable to store emojis for ${instance}`, e),
			).then((r) => r?.flat() ?? null);
		}

		/**
		 * Some of these emojis from this instance exist,
		 * creating db records only for the new ones,
		 * deleting db records for the missing ones,
		 * updating existing db records.
		 */
		const to_delete = db_emojis.filter(
			(db) => !new_emojis.find((emoji) => emoji.identifier === db.identifier),
		);
		const to_create = new_emojis
			.filter((emoji) => !db_emojis.find((db) => db.identifier === emoji.identifier))
			.map((emoji) => this.repo.create(this.fixProxyURL(emoji)));

		await Promise.all([
			to_delete.length
				? this.chunk<string, DeleteResult>(
						to_delete.map((del) => del.identifier),
						this.repo.delete.bind(this.repo),
						(e) =>
							this.error('delete', `Unable to delete old emojis for ${instance}`, e),
					)
				: undefined,
			to_create.length
				? this.chunk<EmojiCache, EmojiCache>(
						to_create,
						this.repo.save.bind(this.repo),
						(e) => this.error('create', `Unable to store emojis for ${instance}`, e),
					)
				: undefined,
		]);

		return new_emojis;
	}

	async create(emoji: EmojiCache): Promise<EmojiCache | null> {
		if (!emoji || !emoji.identifier) {
			return null;
		}

		const fix = this.fixProxyURL(emoji);

		return this.repo.save(fix).catch((e) => {
			if (
				e instanceof QueryFailedError &&
				((e as any).code === 23505 || e.message.includes('duplicate'))
			) {
				return emoji;
			}
			return this.error('create', `Unable to store emoji ${emoji.identifier}`, e);
		});
	}

	async update(emoji: EmojiCache): Promise<EmojiCache | null> {
		if (!emoji) {
			return null;
		}

		const entity = await this.get(emoji.identifier);
		if (!entity) {
			return this.error('update', `Emoji doesn't exist ${emoji.identifier}`);
		}

		const fix = this.fixProxyURL(emoji);

		return this.repo
			.save(fix)
			.catch((e) => this.error('update', `Unable to update emoji ${emoji.identifier}`, e));
	}

	async delete(identifier: string): Promise<boolean | null> {
		const entry = await this.get(identifier);
		if (!entry) {
			this.logger.warn(
				'delete',
				`Attempting to delete emoji ${identifier} but it doesn't exist`,
			);
			return null;
		}

		return this.repo
			.delete(identifier)
			.then((r) => !!r.affected)
			.catch((e) => this.error('delete', `Unable to delete emoji ${identifier}`, e));
	}
}
