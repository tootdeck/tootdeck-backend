import { Test, TestingModule } from '@nestjs/testing';
import { randomUUID } from 'crypto';

import { FullAppModule } from '../../app/app.module';

import { DBApplicationService } from './database.application.service';
import { DBAccountService } from './database.account.service';
import { DBUserService } from './database.user.service';
import { BrowserFingerprintService } from '../../auth/services/utils/fingerprint.service';

import { AccountStore } from '../entities/account.entity';
import { UserStore } from '../entities/user.entity';

import { DeleteUser, InstanceType } from '../types';
import { Fingerprint as FingerprintType } from '../../auth/types/fingerprint.types';

describe('DBUserService', () => {
	let Application: DBApplicationService;
	let Account: DBAccountService;
	let User: DBUserService;
	let Fingerprint: BrowserFingerprintService;

	const application_domain = 'example.com';
	const application_key = 't52jyR5LOeqAK4PhXqI7rktO7CZaD6OPaRohyFVrgzI=';
	const application_secret = 'qlUrKaKNhIuUZrt1pxvGrOnxNdrMDyqPp+qkvhn4Fzw=';

	const account_username = 'tootdeck';
	const account_domain = application_domain;
	const account_token = 'xOOnkfBjAtRqtAmqWFNZEhDsjetgaBIFccIg1EPLqdM=';

	let user: UserStore;
	let main: AccountStore;
	let secondary: AccountStore;

	let jwt_uuid = randomUUID();
	let fingerprint: FingerprintType;
	let session_uuid: string;

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule(FullAppModule).compile();

		Application = app.get<DBApplicationService>(DBApplicationService);
		Account = app.get<DBAccountService>(DBAccountService);
		User = app.get<DBUserService>(DBUserService);
		Fingerprint = app.get<BrowserFingerprintService>(BrowserFingerprintService);

		/**
		 * Create a valid application
		 */
		await Application.create(
			application_domain,
			InstanceType.Mastodon,
			application_key,
			application_secret,
		);

		/**
		 * Create a valid main account
		 */
		main = await Account.create(account_username, account_domain, account_token).then(
			(entity) => entity!,
		);

		/**
		 * Create a valid secondary account
		 */
		secondary = await Account.create('secondary', account_domain, account_token).then(
			(entity) => entity!,
		);

		fingerprint = Fingerprint.get(
			'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/125.0.0.0 Safari/537.36',
			'95.186.43.213',
		);
	});

	describe('create', () => {
		test('Create a valid user', async () => {
			await User.create(main).then((entity) => {
				// @ts-ignore
				user = entity;

				expect(entity).toMatchObject({
					main: {
						username: account_username,
						token: account_token,
						application: {
							domain: account_domain,
						},
					},
				});
			});
		});

		test('Create an invalid user', async () => {
			await User.create(undefined as any).then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	describe('addAccount', () => {
		test('Add a secondary account to an user', async () => {
			await User.addAccount(user, secondary).then((entity) => {
				expect(entity).toMatchObject({
					main,
					secondary: [secondary],
				});
			});
		});

		test('Add main account to the same user', async () => {
			await User.addAccount(user, main).then((entity) => {
				expect(entity).toBeNull();
			});
		});

		test('Add an invalid secondary account to an user', async () => {
			await User.addAccount(user, undefined as any).then((entity) => {
				expect(entity).toBeNull();
			});
		});

		test('Add a valid secondary account to an invalid user', async () => {
			await User.addAccount(undefined as any, secondary).then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	describe('get', () => {
		test('Get an user based on an account', async () => {
			await User.get(main).then((entity) => {
				expect(entity).toMatchObject({
					main,
					secondary: [secondary],
				});
			});
		});

		test('Get an invalid user', async () => {
			await User.get(secondary).then((entity) => {
				expect(entity).toBeNull();
			});
			await User.get(undefined as any).then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	describe('getAllPartial', () => {
		test('Get a partial user', async () => {
			await User.getAllPartial().then((entity) => {
				expect(entity).toMatchObject([
					{
						main: {
							uuid: main.uuid,
							username: main.username,
							domain: main.application.domain,
						},
						secondary: [
							{
								uuid: secondary.uuid,
								username: secondary.username,
								domain: secondary.application.domain,
							},
						],
					},
				]);
			});
		});
	});

	describe('getInMainOrSecondary', () => {
		test('Get an user based on an account', async () => {
			await User.getInMainOrSecondary(main).then((entity) => {
				expect(entity).toMatchObject([
					{
						main,
						secondary: [secondary],
					},
				]);
			});
			await User.getInMainOrSecondary(secondary).then((entity) => {
				expect(entity).toMatchObject([
					{
						main,
						secondary: [secondary],
					},
				]);
			});
		});

		test('Get an invalid user', async () => {
			await User.getInMainOrSecondary(undefined as any).then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	describe('getByUUID', () => {
		test('Get an user based on an account', async () => {
			await User.getByUUID(user.uuid).then((entity) => {
				expect(entity).toMatchObject({
					main,
					secondary: [secondary],
				});
			});
		});

		test('Get an invalid user', async () => {
			await User.getByUUID(main.uuid).then((entity) => {
				expect(entity).toBeNull();
			});
			await User.getByUUID(undefined as any).then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	/**
	 * Session
	 */
	// #region

	describe('createSession', () => {
		test('Create a session and linking it the a user', async () => {
			await User.createSession(user.uuid, jwt_uuid, fingerprint).then((entity) => {
				// @ts-ignore
				session_uuid = entity?.uuid;

				expect(entity).toMatchObject({
					token_uuid: jwt_uuid,
					browser_name: 'Chrome',
					browser_version: 125,
					os: 'Windows',
				});
			});
		});

		test('Create a valid session and linking it to an invalid user', async () => {
			await User.createSession(randomUUID(), jwt_uuid, fingerprint).then((entity) => {
				expect(entity).toBeNull();
			});
		});

		test('Create an invalid session', async () => {
			await User.createSession(user.uuid, undefined as any, fingerprint).then((entity) => {
				expect(entity).toBeNull();
			});
			await User.createSession(user.uuid, jwt_uuid, undefined as any).then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	describe('getWithSessions', () => {
		test('Get a user with its sessions', async () => {
			await User.getWithSessions(user.uuid).then((entity) => {
				expect(entity).toMatchObject({
					session: [
						{
							token_uuid: jwt_uuid,
							browser_name: 'Chrome',
							browser_version: 125,
							os: 'Windows',
						},
					],
				});
			});
		});

		test('Get a invalid user', async () => {
			await User.getWithSessions(randomUUID()).then((entity) => {
				expect(entity).toBeNull();
			});
			await User.getWithSessions(undefined as any).then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	describe('deleteSessions', () => {
		test('Get a user with its sessions', async () => {
			await User.deleteSessions(user.uuid, [session_uuid]).then((entity) => {
				expect(entity).toBe(true);
			});
		});

		test('Get a invalid user', async () => {
			await User.deleteSessions(randomUUID(), [session_uuid]).then((entity) => {
				expect(entity).toBe(false);
			});
			await User.deleteSessions(user.uuid, []).then((entity) => {
				expect(entity).toBe(false);
			});
			await User.deleteSessions(undefined as any, [session_uuid]).then((entity) => {
				expect(entity).toBeNull();
			});
			await User.deleteSessions(user.uuid, undefined as any).then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	// #endregion

	/**
	 * Config
	 */
	// #region

	describe('addConfig', () => {
		test('Create a config and linking it to an user', async () => {
			await User.addConfig(user, 'config').then((entity) => {
				// @ts-ignore
				user = entity;

				expect(entity).toMatchObject({
					main,
					secondary: [secondary],
					config: {
						value: 'config',
					},
				});
			});
		});

		test('Create an invalid config', async () => {
			await User.addConfig(undefined as any, 'config').then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	describe('getConfig', () => {
		test('Get a config linked to an user', async () => {
			await User.getConfig(user.uuid).then((entity) => {
				expect(entity).toMatchObject({
					value: 'config',
				});
			});
		});

		test('Get a config linked to an invalid user', async () => {
			await User.getConfig(randomUUID()).then((entity) => {
				expect(entity).toBeNull();
			});
			await User.getConfig(undefined as any).then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	describe('deleteConfig', () => {
		test('Delete a config linked to an user', async () => {
			await User.deleteConfig(user).then((entity) => {
				expect(entity).toBeTruthy();
			});
		});

		test('Delete a config already deleted', async () => {
			await User.deleteConfig(user).then((entity) => {
				expect(entity).toBeFalsy();
			});
		});

		test('Delete a config linked to an invalid user', async () => {
			await User.deleteConfig(undefined as any).then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	// #endregion

	describe('unlinkAccount', () => {
		test('Remove a secondary account from an user', async () => {
			await User.unlinkAccount(main, secondary).then((entity) => {
				expect(entity).toBe(DeleteUser.RemoveAccount);
			});
		});

		test('Invalid values', async () => {
			await User.unlinkAccount(undefined as any, secondary).then((entity) => {
				expect(entity).toBe(DeleteUser.False);
			});
			await User.unlinkAccount(main, undefined as any).then((entity) => {
				expect(entity).toBe(DeleteUser.False);
			});
			await User.unlinkAccount(secondary, main).then((entity) => {
				expect(entity).toBe(DeleteUser.False);
			});
		});
	});

	describe('delete', () => {
		test('Delete user and associated accounts if not used anywhere else', async () => {
			await User.delete(main).then((entity) => {
				expect(entity).toBe(DeleteUser.RemoveAccount);
			});
		});

		test('Delete invalid user', async () => {
			await User.delete(secondary).then((entity) => {
				expect(entity).toBe(DeleteUser.False);
			});
			await User.delete(undefined as any).then((entity) => {
				expect(entity).toBe(DeleteUser.False);
			});
		});
	});

	afterAll(async () => {
		await User.deleteConfig(user).catch(() => {});
		await User.deleteSessions(user.uuid, [session_uuid]).catch(() => {});
		await User.delete(main).catch(() => {});
		await Account.delete(account_username, account_domain).catch(() => {});
		await Account.delete('secondary', account_domain).catch(() => {});
		await Application.delete(application_domain).catch(() => {});
	});
});
