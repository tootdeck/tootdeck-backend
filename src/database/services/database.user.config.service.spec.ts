import { Test, TestingModule } from '@nestjs/testing';

import { FullAppModule } from '../../app/app.module';

import { DBApplicationService } from './database.application.service';
import { DBAccountService } from './database.account.service';
import { DBUserService } from './database.user.service';
import { DBUserConfigService } from './database.user.config.service';

import { AccountStore } from '../entities/account.entity';
import { UserStore } from '../entities/user.entity';
import { ConfigStore } from '../entities/config.entity';

import { InstanceType } from '../types';

describe('DBUserConfigService', () => {
	let Application: DBApplicationService;
	let Account: DBAccountService;
	let User: DBUserService;
	let UserConfig: DBUserConfigService;

	let application_domain = 'example.com';
	let application_key = 't52jyR5LOeqAK4PhXqI7rktO7CZaD6OPaRohyFVrgzI=';
	let application_secret = 'qlUrKaKNhIuUZrt1pxvGrOnxNdrMDyqPp+qkvhn4Fzw=';

	let account_username = 'tootdeck';
	let account_domain = application_domain;
	let account_token = 'xOOnkfBjAtRqtAmqWFNZEhDsjetgaBIFccIg1EPLqdM=';

	let user: UserStore;
	let main: AccountStore;

	let config: ConfigStore;

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule(FullAppModule).compile();

		Application = app.get<DBApplicationService>(DBApplicationService);
		Account = app.get<DBAccountService>(DBAccountService);
		User = app.get<DBUserService>(DBUserService);
		UserConfig = app.get<DBUserConfigService>(DBUserConfigService);

		/**
		 * Create a valid application
		 */
		await Application.create(
			application_domain,
			InstanceType.Mastodon,
			application_key,
			application_secret,
		);

		/**
		 * Create a valid main account
		 */
		main = await Account.create(account_username, account_domain, account_token).then(
			(entity) => entity!,
		);

		/**
		 * Create a valid user
		 */
		user = await User.create(main).then((entity) => entity!);
	});

	describe('add', () => {
		test('Create a config', async () => {
			await UserConfig.createOrUpdate(user, 'config').then((entity) => {
				// @ts-ignore
				config = entity;

				expect(entity).toMatchObject({
					value: 'config',
				});
			});
		});
	});

	describe('delete', () => {
		test('Delete a config based on a user', async () => {
			await UserConfig.delete(config.uuid).then((entity) => {
				expect(entity).toBeTruthy();
			});
		});

		test('Delete a config based on an invalid user', async () => {
			await UserConfig.delete(undefined as any).then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	afterAll(async () => {
		await User.deleteConfig(user).catch(() => {});
		await User.delete(main).catch(() => {});
		await Account.delete(account_username, account_domain).catch(() => {});
		await Application.delete(application_domain).catch(() => {});
	});
});
