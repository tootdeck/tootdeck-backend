import { Test, TestingModule } from '@nestjs/testing';

import { FullAppModule } from '../../app/app.module';

import { DBEmojiFetchService } from './database.emoji.fetch.service';

describe('DBEmojiFetchService', () => {
	let EmojiFetch: DBEmojiFetchService;

	let application_domain = 'example.com';
	let updated_at: Date;

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule(FullAppModule).compile();

		EmojiFetch = app.get<DBEmojiFetchService>(DBEmojiFetchService);
	});

	describe('createOrUpdate', () => {
		test('Create', async () => {
			await EmojiFetch.createOrUpdate(application_domain).then((entity) => {
				// @ts-ignore
				updated_at = entity?.updated_at;

				expect(entity).toMatchObject({
					instance: application_domain,
				});
			});
		});

		test('Update', async () => {
			await EmojiFetch.createOrUpdate(application_domain).then((entity) => {
				expect(entity).toMatchObject({
					instance: application_domain,
				});
				expect(entity?.updated_at.valueOf() !== updated_at.valueOf()).toBeTruthy();
			});
		});
	});

	describe('get', () => {
		test('Get a valid entry', async () => {
			await EmojiFetch.get(application_domain).then((entity) => {
				expect(entity).toMatchObject({
					instance: application_domain,
				});
			});
		});

		test('Get an invalid entry', async () => {
			await EmojiFetch.get('domain').then((entity) => {
				expect(entity).toBeNull();
			});
			await EmojiFetch.get(undefined as any).then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	describe('delete', () => {
		test('Delete an entry', async () => {
			await EmojiFetch.delete(application_domain).then((entity) => {
				expect(entity).toBeTruthy();
			});
		});

		test('Delete an already deleted entry', async () => {
			await EmojiFetch.delete(application_domain).then((entity) => {
				expect(entity).toBeNull();
			});
		});

		test('Delete an invalid entry', async () => {
			await EmojiFetch.delete('domain').then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	afterAll(async () => {
		await EmojiFetch.delete(application_domain).catch(() => {});
	});
});
