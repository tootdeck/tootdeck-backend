import { Inject, Injectable, forwardRef } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ILike, Repository } from 'typeorm';

import { DBApplicationService } from './database.application.service';

import { AccountStore } from '../entities/account.entity';

import { Logger } from '../../utils/logger';

@Injectable()
export class DBAccountService {
	private readonly logger = new Logger(DBAccountService.name);

	constructor(
		@InjectRepository(AccountStore) private readonly repo: Repository<AccountStore>,
		@Inject(forwardRef(() => DBApplicationService))
		private readonly DBApplicationService: DBApplicationService,
	) {}

	private error(fn: string, text: string, e?: any) {
		this.logger.error(fn, text);
		if (e) {
			console.error(e);
		}
		return null;
	}

	async get(username: string, domain: string): Promise<AccountStore | null> {
		if (!username || !domain) {
			return null;
		}

		return this.repo
			.findOne({
				where: { username: ILike(username), application: { domain } },
				relations: ['application'],
			})
			.catch((e) => this.error('get', `Unable to find account for ${username}@${domain}`, e));
	}

	async getByUUID(uuid: string): Promise<AccountStore | null> {
		if (!uuid) {
			return null;
		}

		return this.repo
			.findOne({ where: { uuid }, relations: ['application'] })
			.catch((e) => this.error('getByUUID', `Unable to find account with uuid ${uuid}`, e));
	}

	async create(username: string, domain: string, token: string): Promise<AccountStore | null> {
		if (!username || !domain || !token) {
			return null;
		}

		const exist = await this.get(username, domain);
		if (exist) {
			return this.error('create', `Account already exist for ${username}@${domain}`);
		}

		const application = await this.DBApplicationService.get(domain);
		if (!application) {
			return this.error('create', `${domain} doesn't exist`);
		}

		const entity = this.repo.create({
			username,
			token,
			application,
		});

		return this.repo
			.save(entity)
			.catch((e) =>
				this.error('create', `Unable to store account for ${username}@${domain}`, e),
			);
	}

	async updateToken(
		username: string,
		domain: string,
		token: string,
	): Promise<AccountStore | null> {
		if (!username || !domain) {
			return null;
		}

		if (!token) {
			return this.error('update', `Invalid token for ${username}@${domain}`);
		}

		const entity = await this.get(username, domain);
		if (!entity) {
			return this.error('update', `Account doesn't exist for ${username}@${domain}`);
		}

		entity.token = token;

		return this.repo
			.save(entity)
			.catch((e) =>
				this.error('update', `Unable to update account for ${username}@${domain}`, e),
			);
	}

	async delete(username: string, domain: string) {
		if (!username || !domain) {
			return null;
		}

		const entry = await this.get(username, domain);
		if (!entry) {
			this.logger.warn(
				'store',
				`Attempting to delete account ${username}@${domain} but it doesn't exist`,
			);
			return null;
		}

		return this.repo
			.delete(entry.uuid)
			.catch((e) =>
				this.error('delete', `Unable to delete account ${username}@${domain}`, e),
			);
	}
}
