import { Test, TestingModule } from '@nestjs/testing';
import { DataSource, Repository } from 'typeorm';

import { FullAppModule } from '../../app/app.module';

import { DBWorkerStatsService } from './database.worker.stats.service';

import { WorkerStats as WorkerStatsEntity } from '../entities/worker.stats.entity';

import { WorkerStatsMerge } from '../types';

describe('DBWorkerStatsService', () => {
	let WorkerStats: DBWorkerStatsService;
	let WorkersStatsRepo: Repository<WorkerStatsEntity>;

	function randomInt(min: number, max: number) {
		return Math.floor(Math.random() * (max - min + 1) + min);
	}

	const value_1 = {
		jobs_processed: 0,
		jobs_failed: 0,
		requests_made: 0,
		worker_queue_max: 0,
		worker_memory_max: 0,
		worker_job_time_min: 0,
		worker_job_time_max: 0,
		worker_job_time_average: 0,
		cache_size_max: 0,
		cache_size_dead: 0,
		merge_type: WorkerStatsMerge.NotMerged,
	};

	const value_2 = {
		jobs_processed: 1,
		jobs_failed: 1,
		requests_made: 1,
		worker_queue_max: 1,
		worker_memory_max: 1,
		cache_size_max: 1,
		cache_size_dead: 1,
		merge_type: WorkerStatsMerge.NotMerged,
	};

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule(FullAppModule).compile();

		WorkerStats = app.get<DBWorkerStatsService>(DBWorkerStatsService);
		WorkersStatsRepo = app.get(DataSource).getRepository(WorkerStatsEntity);
	});

	describe('create', () => {
		test('Create a full stats entity', async () => {
			await WorkerStats.create(value_1).then((entity) => {
				expect(entity).toMatchObject(value_1);
			});
		});

		test('Create a partial stats entity', async () => {
			await WorkerStats.create(value_2).then((entity) => {
				expect(entity).toMatchObject(value_2);
			});
		});
	});

	describe('get', () => {
		test('Get all stats', async () => {
			await WorkerStats.get(9999).then((entities) => {
				expect(entities?.length).toBe(2);
			});
		});

		test('Get last stat', async () => {
			await WorkerStats.get(1).then((entities) => {
				expect(entities?.length).toBe(1);
				expect(entities?.[0]).toMatchObject(value_2);
			});
		});

		test('Get 0 stats', async () => {
			await WorkerStats.get(0).then((entities) => {
				expect(entities?.length).toBe(0);
			});
		});
	});

	describe('getLast', () => {
		test('Get last stats', async () => {
			await WorkerStats.getLast().then((entity) => {
				expect(entity).toMatchObject(value_2);
			});
		});
	});

	describe('merge', () => {
		test('Merge with no values', async () => {
			/**
			 * Not enough records
			 */
			await WorkerStats.mergeIntoDaily().then((result) => {
				expect(result).toBe(false);
			});
			await WorkerStats.mergeIntoWeekly().then((result) => {
				expect(result).toBe(false);
			});
		});

		test('Merge daily', async () => {
			/**
			 * Generate 1 day of records
			 */
			const daily_limit = 60 * 24;
			const promises = [];
			for (let i = 0; i < daily_limit + 3; i++) {
				const rand = randomInt(0, 999999);
				promises.push(
					WorkerStats.create({
						jobs_processed: rand,
						jobs_failed: rand,
						requests_made: rand,
						worker_queue_max: rand,
						worker_memory_max: rand,
						worker_job_time_min: rand,
						worker_job_time_max: rand,
						worker_job_time_average: rand,
						cache_size_max: rand,
						cache_size_dead: rand,
					}),
				);
			}
			await Promise.all(promises);

			/**
			 * Still not enough records
			 */
			await WorkerStats.mergeIntoDaily().then((result) => {
				expect(result).toBe(false);
			});

			/**
			 * Generate one last record
			 */
			const rand = randomInt(0, 999999);
			await WorkerStats.create({
				jobs_processed: rand,
				jobs_failed: rand,
				requests_made: rand,
				worker_queue_max: rand,
				worker_memory_max: rand,
				worker_job_time_min: rand,
				worker_job_time_max: rand,
				worker_job_time_average: rand,
				cache_size_max: rand,
				cache_size_dead: rand,
			});

			await WorkerStats.mergeIntoDaily().then((result) => {
				expect(result).toBe(true);
			});

			await WorkerStats.get(9999).then((entities) => {
				expect(entities?.length).toBe(daily_limit + 2);
			});

			/**
			 * Not enough records
			 */
			await WorkerStats.mergeIntoDaily().then((result) => {
				expect(result).toBe(false);
			});
		});

		test('Merge weekly', async () => {
			/**
			 * Generate 6 days of records
			 */
			const daily_limit = 60 * 24;
			const weekly_limit = daily_limit * 6;
			{
				const promises = [];
				for (let i = 0; i < weekly_limit; i++) {
					const rand = randomInt(0, 999999);
					promises.push(
						WorkerStats.create({
							jobs_processed: rand,
							jobs_failed: rand,
							requests_made: rand,
							worker_queue_max: rand,
							worker_memory_max: rand,
							worker_job_time_min: rand,
							worker_job_time_max: rand,
							worker_job_time_average: rand,
							cache_size_max: rand,
							cache_size_dead: rand,
						}),
					);
				}
				await Promise.all(promises);
			}

			/**
			 * Merging 6 days of records
			 */
			await WorkerStats.mergeIntoDaily().then((result) => {
				expect(result).toBe(true);
			});

			await WorkerStats.get(9999).then((entities) => {
				expect(entities?.length).toBe(daily_limit + Math.floor(weekly_limit / 5) + 2);
			});

			/**
			 * Still not enough records
			 */
			await WorkerStats.mergeIntoWeekly().then((result) => {
				expect(result).toBe(false);
			});

			/**
			 * Generate lasts records
			 */
			{
				const promises = [];
				for (let i = 0; i < 5 * 4; i++) {
					const rand = randomInt(0, 999999);
					promises.push(
						WorkerStats.create({
							jobs_processed: rand,
							jobs_failed: rand,
							requests_made: rand,
							worker_queue_max: rand,
							worker_memory_max: rand,
							worker_job_time_min: rand,
							worker_job_time_max: rand,
							worker_job_time_average: rand,
							cache_size_max: rand,
							cache_size_dead: rand,
						}),
					);
				}
				await Promise.all(promises);
				await WorkerStats.mergeIntoDaily().then((result) => {
					expect(result).toBe(true);
				});
			}

			/**
			 * Merging 6 days of merged records
			 */
			await WorkerStats.mergeIntoWeekly().then((result) => {
				expect(result).toBe(true);
			});

			/**
			 * Not enough records
			 */
			await WorkerStats.mergeIntoWeekly().then((result) => {
				expect(result).toBe(false);
			});
		});
	});

	afterAll(async () => {
		await WorkersStatsRepo.clear().catch(() => {});
	});
});
