import { Test, TestingModule } from '@nestjs/testing';
import { randomUUID } from 'crypto';

import { FullAppModule } from '../../app/app.module';

import { DBUserSessionService } from './database.user.session.service';
import { BrowserFingerprintService } from '../../auth/services/utils/fingerprint.service';

import { Fingerprint as FingerprintType } from '../../auth/types/fingerprint.types';

describe('DBUserSessionService', () => {
	let Fingerprint: BrowserFingerprintService;
	let UserSession: DBUserSessionService;

	let jwt_uuid = randomUUID();
	let new_token_uuid = randomUUID();
	let fingerprint: FingerprintType;
	let session_uuid: string;

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule(FullAppModule).compile();

		UserSession = app.get<DBUserSessionService>(DBUserSessionService);
		Fingerprint = app.get<BrowserFingerprintService>(BrowserFingerprintService);

		fingerprint = Fingerprint.get(
			'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/125.0.0.0 Safari/537.36',
			'95.186.43.213',
		);
	});

	describe('create', () => {
		test('Create a session', async () => {
			await UserSession.create(jwt_uuid, fingerprint).then((entity) => {
				// @ts-ignore
				session_uuid = entity?.uuid;

				expect(entity).toMatchObject({
					token_uuid: jwt_uuid,
					browser_name: 'Chrome',
					browser_version: 125,
					os: 'Windows',
				});
			});
		});

		test('Create an invalid session', async () => {
			await UserSession.create(undefined as any, fingerprint).then((entity) => {
				expect(entity).toBeNull();
			});
			await UserSession.create('', fingerprint).then((entity) => {
				expect(entity).toBeNull();
			});
			await UserSession.create(jwt_uuid, undefined as any).then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	describe('updateToken', () => {
		test('Update a session token', async () => {
			await UserSession.updateToken(jwt_uuid, new_token_uuid).then((entity) => {
				expect(entity).toMatchObject({
					uuid: session_uuid,
					token_uuid: new_token_uuid,
					browser_name: 'Chrome',
					browser_version: 125,
					os: 'Windows',
				});
			});
		});

		test('Update a session token with invalid params', async () => {
			await UserSession.updateToken(randomUUID(), new_token_uuid).then((entity) => {
				expect(entity).toBeNull();
			});
			await UserSession.updateToken(undefined as any, new_token_uuid).then((entity) => {
				expect(entity).toBeNull();
			});
			await UserSession.updateToken(jwt_uuid, undefined as any).then((entity) => {
				expect(entity).toBeNull();
			});
			await UserSession.updateToken(new_token_uuid, undefined as any).then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	describe('revokeToken', () => {
		test('Remove token from session', async () => {
			await UserSession.revokeToken(new_token_uuid).then((entity) => {
				expect(entity).toBe(true);
			});
		});

		test('Remove invalid token from session', async () => {
			await UserSession.revokeToken(randomUUID()).then((entity) => {
				expect(entity).toBe(false);
			});
			await UserSession.revokeToken(undefined as any).then((entity) => {
				expect(entity).toBeNull();
			});
		});
	});

	describe('delete', () => {
		test('Remove a session', async () => {
			await UserSession.delete([session_uuid]).then((entity) => {
				expect(entity).toBe(true);
			});
		});

		test('Remove an invalid session', async () => {
			await UserSession.delete([randomUUID()]).then((entity) => {
				expect(entity).toBe(false);
			});
			await UserSession.delete(undefined as any).then((entity) => {
				expect(entity).toBe(false);
			});
		});
	});

	afterAll(async () => {
		await UserSession.delete([session_uuid]);
	});
});
