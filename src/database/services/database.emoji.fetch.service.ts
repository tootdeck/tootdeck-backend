import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';

import { EmojiFetch } from '../entities/emoji.fetch.entity';

import { Logger } from '../../utils/logger';

@Injectable()
export class DBEmojiFetchService {
	private readonly logger = new Logger(DBEmojiFetchService.name);

	constructor(@InjectRepository(EmojiFetch) private readonly repo: Repository<EmojiFetch>) {}

	private error(fn: string, text: string, e?: any) {
		this.logger.error(fn, text);
		if (e) {
			console.error(e);
		}
		return null;
	}

	async get(instance: string): Promise<EmojiFetch | null> {
		if (!instance) {
			return null;
		}

		return this.repo
			.findOne({
				where: { instance },
			})
			.catch((e) => this.error('get', `Unable to get emoji cache date for ${instance}`, e));
	}

	async createOrUpdate(instance: string): Promise<EmojiFetch | null> {
		if (!instance) {
			return null;
		}

		const entry = this.repo.create({
			instance,
			updated_at: new Date(),
		});

		return this.repo
			.save(entry)
			.catch((e) =>
				this.error(
					'createOrUpdate',
					`Unable to store emoji cache fetch date for ${instance}`,
					e,
				),
			);
	}

	async delete(instance: string): Promise<DeleteResult | null> {
		if (!instance) {
			return null;
		}

		const entry = await this.get(instance);
		if (!entry) {
			return null;
		}

		return this.repo
			.delete(instance)
			.catch((e) => this.error('delete', `Unable to delete emoji ${instance}`, e));
	}
}
