import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { UserSessionStore } from '../entities/user.session.entity';
import { UserStore } from '../entities/user.entity';

import { Logger } from '../../utils/logger';

import { Fingerprint } from '../../auth/types/fingerprint.types';

@Injectable()
export class DBUserSessionService {
	private readonly logger = new Logger(DBUserSessionService.name);

	constructor(
		@InjectRepository(UserSessionStore) private readonly repo: Repository<UserSessionStore>,
		@InjectRepository(UserStore) private readonly user_repo: Repository<UserStore>,
	) {}

	private error(fn: string, text: string, e?: any) {
		this.logger.error(fn, text);
		if (e) {
			console.error(e);
		}
		return null;
	}

	private async getByToken(token_uuid: string): Promise<UserSessionStore | null> {
		return this.repo
			.findOne({
				where: { token_uuid },
			})
			.catch((e) => this.error('', `Unable to find session with token ${token_uuid}`, e));
	}

	async updateToken(
		old_token_uuid: string,
		new_token_uuid: string,
	): Promise<UserSessionStore | null> {
		if (!old_token_uuid || !new_token_uuid) {
			return null;
		}

		const session = await this.getByToken(old_token_uuid);
		if (!session) {
			return null;
		}

		session.token_uuid = new_token_uuid;
		session.updated_at = new Date();

		return this.repo
			.save(session)
			.catch((e) =>
				this.error('update', `Unable to update session with token ${old_token_uuid}`, e),
			);
	}

	async create(token_uuid: string, fingerprint: Fingerprint): Promise<UserSessionStore | null> {
		if (!token_uuid || !fingerprint) {
			return null;
		}

		const session = this.repo.create({
			token_uuid,
			browser_name: fingerprint.browser.name ?? 'unknown',
			browser_version: fingerprint.browser.version,
			created_at: new Date(),
			os: fingerprint.os.name ?? 'unknown',
			updated_at: new Date(),
		});

		return this.repo
			.save(session)
			.catch((e) => this.error('create', `Unable to create user session`, e));
	}

	async revokeToken(token_uuid: string): Promise<boolean | null> {
		if (!token_uuid) {
			return null;
		}

		const session = await this.getByToken(token_uuid);
		if (!session) {
			return false;
		}

		session.token_uuid = '';

		return this.repo
			.save(session)
			.then(() => true)
			.catch((e) =>
				this.error(
					'revokeTokenByUUID',
					`Unable to revoke token ${token_uuid} for session ${session.uuid}`,
					e,
				),
			);
	}

	async delete(sessions_uuid: Array<string>): Promise<boolean | null> {
		if (!sessions_uuid) {
			return false;
		}

		return this.repo
			.delete(sessions_uuid)
			.then((r) => !!r.affected)
			.catch((e) =>
				this.error('delete', `Unable to delete session ${sessions_uuid.join(', ')}`, e),
			);
	}
}
