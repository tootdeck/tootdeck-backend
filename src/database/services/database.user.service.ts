import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { DBUserConfigService } from './database.user.config.service';
import { DBUserSessionService } from './database.user.session.service';

import { UserStore } from '../entities/user.entity';
import { AccountStore } from '../entities/account.entity';
import { UserSessionStore } from '../entities/user.session.entity';
import { ConfigStore } from '../entities/config.entity';

import { Logger } from '../../utils/logger';

import { DeleteUser } from '../types';
import { Fingerprint } from '../../auth/types/fingerprint.types';

interface PartialAccount {
	uuid: string;
	username: string;
	domain: string;
}
interface PartialUser {
	main: PartialAccount;
	secondary: Array<PartialAccount>;
}

@Injectable()
export class DBUserService {
	private readonly logger = new Logger(DBUserService.name);

	constructor(
		@InjectRepository(UserStore) private readonly repo: Repository<UserStore>,
		private readonly DBUserSessionService: DBUserSessionService,
		private readonly DBUserConfigService: DBUserConfigService,
	) {}

	private error(fn: string, text: string, e?: any) {
		this.logger.error(fn, text);
		if (e) {
			console.error(e);
		}
		return null;
	}

	/**
	 * Utils
	 */
	//#region

	private async usedIn(account: AccountStore) {
		const used = await this.repo
			.find({
				where: [{ main: { uuid: account.uuid } }, { secondary: { uuid: account.uuid } }],
				relations: ['secondary', 'secondary.application'],
			})
			.catch((e) =>
				this.error(
					'usedIn',
					`Unable to find user for ${account.username}@${account.application.domain}`,
					e,
				),
			);

		if (!used) {
			return null;
		}

		if (!used.length) {
			return false;
		}

		return true;
	}

	private async getMain(account: AccountStore) {
		return this.repo
			.findOne({
				where: { main: { uuid: account.uuid } },
				relations: ['main'],
			})
			.catch((e) =>
				this.error(
					'getMain',
					`Unable to find user for ${account.username}@${account.application.domain}`,
					e,
				),
			);
	}

	//#endregion

	/**
	 * Service
	 */
	//#region

	async get(account: AccountStore) {
		if (!account) {
			return null;
		}

		return this.repo
			.findOne({
				where: { main: { uuid: account.uuid } },
				relations: ['main', 'main.application', 'secondary', 'secondary.application'],
			})
			.catch((e) =>
				this.error(
					'get',
					`Unable to find user for ${account.username}@${account.application.domain}`,
					e,
				),
			);
	}

	async getAllPartial(): Promise<Array<PartialUser> | null> {
		return this.repo
			.find({
				relations: ['main', 'main.application', 'secondary', 'secondary.application'],
			})
			.then((r) =>
				r.map((user) => ({
					main: {
						uuid: user.main.uuid,
						username: user.main.username,
						domain: user.main.application.domain,
					},
					secondary: user.secondary.map((secondary) => ({
						uuid: secondary.uuid,
						username: secondary.username,
						domain: secondary.application.domain,
					})),
				})),
			)
			.catch((e) => this.error('getAll', `Unable to get users`, e));
	}

	async getInMainOrSecondary(account: AccountStore) {
		if (!account) {
			return null;
		}

		return this.repo
			.find({
				where: [{ main: { uuid: account.uuid } }, { secondary: { uuid: account.uuid } }],
				relations: ['main', 'main.application', 'secondary', 'secondary.application'],
			})
			.catch((e) =>
				this.error(
					'getInMainOrSecondary',
					`Unable to find user for ${account.username}@${account.application.domain}`,
					e,
				),
			);
	}

	async getByUUID(uuid: string) {
		if (!uuid) {
			return null;
		}

		return this.repo
			.findOne({
				where: { uuid },
				relations: [
					'main',
					'main.application',
					'secondary',
					'secondary.application',
					'config',
				],
			})
			.catch((e) => this.error('getByUUID', `Unable to find user with uuid ${uuid}`, e));
	}

	async create(account: AccountStore) {
		if (!account) {
			return null;
		}

		const exist = await this.getMain(account);
		if (exist) {
			return this.error(
				'create',
				`Account already exist for ${account.username}@${account.application.domain}`,
			);
		}

		const entity = this.repo.create({
			main: account,
		});

		return this.repo
			.save(entity)
			.catch((e) =>
				this.error(
					'create',
					`Unable to store account for ${account.username}@${account.application.domain}`,
					e,
				),
			);
	}

	async addAccount(user: UserStore, new_account: AccountStore): Promise<UserStore | null>;
	async addAccount(account: AccountStore, new_account: AccountStore): Promise<UserStore | null>;
	async addAccount(
		user_or_account: UserStore | AccountStore,
		new_account: AccountStore,
	): Promise<UserStore | null> {
		if (!new_account) {
			return null;
		}

		let entry: UserStore | null = null;
		if (user_or_account instanceof UserStore) {
			entry = user_or_account;
		} else if (user_or_account instanceof AccountStore) {
			entry = await this.get(user_or_account);
			if (!entry) {
				return this.error(
					'addAccount',
					`No user with main account ${user_or_account.username}@${user_or_account.application.domain}`,
				);
			}
		}
		if (!entry) {
			return null;
		}

		if (
			entry.main.uuid === new_account.uuid ||
			entry.secondary?.find((a) => a.uuid === new_account.uuid)
		) {
			return this.error(
				'addAccount',
				`Account ${new_account.username}@${new_account.application.domain} already exist for ${entry.main.username}@${entry.main.application.domain}`,
			);
		}

		entry.addSecondary(new_account);

		return this.repo
			.save(entry)
			.catch((e) =>
				this.error(
					'addAccount',
					`Unable to store account for ${entry!.main.username}@${
						entry!.main.application.domain
					}`,
					e,
				),
			);
	}

	async unlinkAccount(account: AccountStore, remove_account: AccountStore): Promise<DeleteUser> {
		if (!account || !remove_account) {
			return DeleteUser.False;
		}

		const entry = await this.get(account);
		if (!entry) {
			this.logger.warn(
				'unlinkAccount',
				`Attempting to remove account ${remove_account.username}@${remove_account.application.domain} but user for ${account.username}@${account.application.domain} doesn't exist`,
			);
			return DeleteUser.False;
		}

		if (!entry.removeSecondary(remove_account)) {
			this.logger.warn(
				'unlinkAccount',
				`Attempting to remove account ${remove_account.username}@${remove_account.application.domain} for ${account.username}@${account.application.domain} but it doesn't exist`,
			);
			return DeleteUser.False;
		}

		const del = await this.repo
			.save(entry)
			.catch((e) =>
				this.error(
					'unlinkAccount',
					`Unable to remove account ${account.username}@${account.application.domain}`,
					e,
				),
			);
		if (!del) {
			return DeleteUser.False;
		}

		const used = await this.usedIn(remove_account);
		if (used === null) {
			return DeleteUser.False;
		}
		if (used === false) {
			return DeleteUser.RemoveAccount;
		}

		return DeleteUser.True;
	}

	async delete(account: AccountStore): Promise<DeleteUser> {
		if (!account) {
			return DeleteUser.False;
		}

		const entry = await this.get(account);
		if (!entry) {
			this.logger.warn(
				'delete',
				`Attempting to delete user ${account.username}@${account.application.domain} but it doesn't exist`,
			);
			return DeleteUser.False;
		}

		const del_config = await this.deleteConfig(entry);
		if (del_config === null) {
			return DeleteUser.False;
		}

		const del = await this.repo
			.delete(entry.uuid)
			.catch((e) =>
				this.error(
					'delete',
					`Unable to delete user ${account.username}@${account.application.domain}`,
					e,
				),
			);
		if (!del) {
			return DeleteUser.False;
		}

		const used = await this.usedIn(account);
		if (used === null) {
			return DeleteUser.False;
		}
		if (used === false) {
			return DeleteUser.RemoveAccount;
		}

		return DeleteUser.True;
	}

	//#endregion

	/**
	 * Session
	 */
	//#region

	async getWithSessions(uuid: string, session_uuid?: string): Promise<UserStore | null> {
		if (!uuid) {
			return null;
		}

		return this.repo
			.findOne({
				where: session_uuid ? { uuid, session: { uuid: session_uuid } } : { uuid },
				relations: ['main', 'main.application', 'session'],
			})
			.catch((e) => this.error('getSessions', `Unable to find user ${uuid}`, e));
	}

	async createSession(
		user_uuid: string,
		token_uuid: string,
		fingerprint: Fingerprint,
	): Promise<UserSessionStore | null> {
		const user = await this.getWithSessions(user_uuid);
		if (!user) {
			return null;
		}

		const session = await this.DBUserSessionService.create(token_uuid, fingerprint);
		if (!session) {
			this.logger.error(
				'createSession',
				`Unable to create user session for ${user.main.username}@${user.main.application.domain}`,
			);
			return null;
		}

		user.addSession(session);

		const update = await this.repo
			.save(user)
			.catch((e) =>
				this.error(
					'addSession',
					`Unable to create session for ${user.main.username}@${user.main.application.domain}`,
					e,
				),
			);

		if (!update) {
			await this.DBUserSessionService.delete([session.uuid]);
			return null;
		}

		return session;
	}

	async deleteSessions(user_uuid: string, sessions_uuid: Array<string>): Promise<boolean | null> {
		if (!user_uuid || !sessions_uuid) {
			return null;
		}

		if (!sessions_uuid.length) {
			return false;
		}

		const user = await this.getWithSessions(user_uuid);
		if (!user) {
			return false;
		}

		const result =
			user.session.map((session) => user.removeSession(session.uuid)).filter((x) => !x)
				.length === 0;
		if (!result) {
			return false;
		}

		const update = await this.repo
			.save(user)
			.catch((e) =>
				this.error(
					'deleteSession',
					`Unable to delete session for ${user.main.username}@${user.main.application.domain}`,
					e,
				),
			);
		if (!update) {
			return false;
		}

		return this.DBUserSessionService.delete(sessions_uuid);
	}

	//#endregion

	/**
	 * Config
	 */
	//#region

	async getConfig(uuid: string): Promise<ConfigStore | null> {
		if (!uuid) {
			return null;
		}

		return this.repo
			.findOne({
				where: { uuid },
				relations: ['config'],
			})
			.then((r) => r?.config ?? null)
			.catch((e) => this.error('getConfig', `Unable to find user with uuid ${uuid}`, e));
	}

	async addConfig(user: UserStore, config_value: string): Promise<UserStore | null> {
		if (!user) {
			return null;
		}

		const config = await this.DBUserConfigService.createOrUpdate(user, config_value);
		if (!config) {
			return null;
		}

		user.config = config;

		return this.repo
			.save(user)
			.catch((e) =>
				this.error(
					'addConfig',
					`Unable to update user for ${user.main.username}@${user.main.application.domain}`,
					e,
				),
			);
	}

	async deleteConfig(user: UserStore): Promise<boolean | null> {
		if (!user) {
			return null;
		}

		const config_uuid = user.config?.uuid;
		if (!config_uuid) {
			return false;
		}

		user.config = null;

		const update = await this.repo
			.save(user)
			.catch((e) =>
				this.error(
					'deleteConfig',
					`Unable to delete user config for ${user.main.username}@${user.main.application.domain}`,
					e,
				),
			);
		if (!update) {
			return null;
		}

		return this.DBUserConfigService.delete(config_uuid);
	}

	//#endregion
}
