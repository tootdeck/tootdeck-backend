export enum InstanceType {
	Mastodon = 'mastodon',
	Pleroma = 'pleroma',
	Misskey = 'misskey',
	Friendica = 'friendica',
}

export enum DeleteUser {
	False = 0,
	True = 1,
	RemoveAccount = 2,
}

// prettier-ignore
export enum WorkerStatsMerge {
	NotMerged = 0,	//  1min between records =>  60 x 24       => 1440 records a day
	Daily = 1,		//  2min between records =>  (60 / 2) x 24 =>  720 records/days
	Weekly = 2		//    1h between records =>   1 x 24       =>   24 records/days
	/**
	 * 24 records/days => 8 760 records/years
	 * 10 years => 87 600 records
	 */
}
