import { Entity, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { AccountStore } from './account.entity';
import { ConfigStore } from './config.entity';
import { UserSessionStore } from './user.session.entity';

@Entity()
export class UserStore {
	@PrimaryGeneratedColumn('uuid')
	uuid: string;

	@ManyToOne((type) => AccountStore, (user) => user.uuid, {
		nullable: false,
	})
	main: AccountStore;

	/**
	 * Secondary
	 */
	// #region

	@ManyToMany((type) => AccountStore, (user) => user.uuid, {
		nullable: false,
	})
	@JoinTable({
		name: 'user_accounts',
	})
	secondary: AccountStore[];

	addSecondary(account: AccountStore) {
		if (!this.secondary) {
			this.secondary = [];
		}
		this.secondary.push(account);
	}

	removeSecondary(account: AccountStore | string): boolean {
		if (!this.secondary) {
			return false;
		}

		const prev = this.secondary.length;

		if (typeof account === 'string') {
			this.secondary = this.secondary.filter((a) => a.uuid !== account);
		} else {
			this.secondary = this.secondary.filter((a) => a.uuid !== account.uuid);
		}

		return prev !== this.secondary.length;
	}

	// #endregion

	@ManyToOne((type) => ConfigStore, (config) => config.uuid, {
		nullable: true,
	})
	config: ConfigStore | null;

	/**
	 * Secondary
	 */
	// #region

	@ManyToMany((type) => UserSessionStore, (session) => session.uuid, {
		nullable: false,
	})
	@JoinTable({
		name: 'user_sessions',
	})
	session: UserSessionStore[];

	addSession(session: UserSessionStore) {
		if (!this.session) {
			this.session = [];
		}
		this.session.push(session);
	}

	removeSession(session: UserSessionStore | string): boolean {
		if (!this.session) {
			return false;
		}

		const prev = this.session.length;

		if (typeof session === 'string') {
			this.session = this.session.filter((a) => a.uuid !== session);
		} else {
			this.session = this.session.filter((a) => a.uuid !== session.uuid);
		}

		return prev !== this.session.length;
	}

	// #endregion
}
