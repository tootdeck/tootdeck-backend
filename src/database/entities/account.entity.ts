import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { ApplicationStore } from './application.entity';

@Entity()
export class AccountStore {
	@PrimaryGeneratedColumn('uuid')
	uuid: string;

	@Column({ nullable: false, type: 'text' })
	username: string;

	@Column({ nullable: false, type: 'text', default: '' })
	token: string;

	@ManyToOne((type) => ApplicationStore, (app) => app.uuid, {
		nullable: false,
	})
	application: ApplicationStore;
}
