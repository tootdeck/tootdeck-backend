import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

import { WorkerStatsMerge } from '../types';

@Entity()
export class WorkerStats {
	@PrimaryGeneratedColumn('uuid')
	uuid: string;

	@Column({ default: 0 })
	jobs_processed: number;

	@Column({ default: 0 })
	jobs_failed: number;

	@Column({ default: 0 })
	requests_made: number;

	@Column({ default: 0 })
	worker_queue_max: number;

	@Column({ default: 0 })
	worker_memory_max: number;

	@Column({ default: 0 })
	worker_job_time_min: number;

	@Column({ default: 0 })
	worker_job_time_max: number;

	@Column({ default: 0 })
	worker_job_time_average: number;

	@Column({ default: 0 })
	cache_size_max: number;

	@Column({ default: 0 })
	cache_size_dead: number;

	@Column({ nullable: false })
	created_at: Date;

	@Column({ nullable: false, type: 'int4', enum: WorkerStatsMerge })
	merge_type: WorkerStatsMerge;
}
