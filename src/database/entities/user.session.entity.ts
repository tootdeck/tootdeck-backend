import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class UserSessionStore {
	@PrimaryGeneratedColumn('uuid')
	uuid: string;

	@Column({ nullable: true, type: 'text' })
	token_uuid: string | null;

	@Column({ nullable: false, type: 'text' })
	browser_name: string;

	@Column({ nullable: false })
	browser_version: number;

	@Column({ nullable: false, type: 'text' })
	os: string;

	@Column({ nullable: false })
	created_at: Date;

	@Column({ nullable: false })
	updated_at: Date;
}
