import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class EmojiCache {
	@PrimaryColumn({ nullable: false, type: 'text' })
	identifier: string;

	@Column({ nullable: false, type: 'text' })
	url: string;

	@Column({ nullable: true, type: 'text' })
	fallback: string | null;

	@Column({ nullable: true, type: 'text' })
	static_url: string | null;
}
