import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class EmojiFetch {
	@PrimaryColumn({ type: 'text' })
	instance: string;

	@Column({ nullable: false })
	updated_at: Date;
}
