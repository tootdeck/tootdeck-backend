import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

import { InstanceType } from '../types';

@Entity()
export class ApplicationStore {
	@PrimaryGeneratedColumn('uuid')
	uuid: string;

	@Column({ nullable: false, unique: true, type: 'text' })
	domain: string;

	@Column({ nullable: false, type: 'text', enum: InstanceType })
	type: InstanceType;

	@Column({ nullable: false, type: 'text' })
	key: string;

	@Column({ nullable: false, type: 'text' })
	secret: string;

	@Column({ nullable: true, type: 'text' })
	token: string;

	@Column({ nullable: true, type: 'text' })
	streaming_url: string;

	@Column({ nullable: false })
	created_at: Date;

	@Column({ nullable: false })
	updated_at: Date;
}
