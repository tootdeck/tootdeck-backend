import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify';
import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { WsAdapter } from '@nestjs/platform-ws';
import fastifyCookie from '@fastify/cookie';
import fastifyMultipart from '@fastify/multipart';
import helmet from 'helmet';
import { EventEmitter } from 'events';

import { AppModule } from './app/app.module';

import checkEnv from './env/check';
import { generateSecret } from './secret';
import { MicroserviceOptions } from '@nestjs/microservices';
import { ConfigService } from '@nestjs/config';
import { MAIN_PROCESS } from './app/configs/microservice.config';

async function bootstrap() {
	checkEnv();

	const secret = await generateSecret();
	if (!secret) {
		process.exit(1);
	}

	const app = await NestFactory.create<NestFastifyApplication>(
		AppModule,
		new FastifyAdapter({ bodyLimit: 100 * 1024 * 1024 }),
	);

	app.enableCors();
	app.setGlobalPrefix('api');

	app.useWebSocketAdapter(new WsAdapter(app));

	EventEmitter.defaultMaxListeners = 100;

	if (!process.env['PROD']) {
		app.use(
			helmet({
				contentSecurityPolicy: {
					directives: {
						defaultSrc: [`'self'`],
						styleSrc: [`'self'`, `'unsafe-inline'`],
						imgSrc: [`'self'`, 'data:', 'validator.swagger.io'],
						scriptSrc: [`'self'`, `https: 'unsafe-inline'`],
					},
				},
			}),
		);

		const config = new DocumentBuilder()
			.setTitle('TootDeck API')
			.setVersion('0.9')
			.addSecurity('InstanceHandle', {
				type: 'apiKey',
				in: 'header',
				name: 'Handle',
				description: 'Instance handle',
				scheme: 'basic',
			})
			.build();
		const document = SwaggerModule.createDocument(app, config);
		SwaggerModule.setup('api', app, document, {
			swaggerOptions: {
				tagsSorter: 'alpha',
			},
		});
	}

	app.use(helmet({ contentSecurityPolicy: { useDefaults: true } }));
	// @ts-ignore
	await app.register(fastifyCookie);
	// @ts-ignore
	await app.register(fastifyMultipart);

	// app.connectMicroservice<MicroserviceOptions>({
	// 	transport: Transport.TCP,
	// 	options: { port: 3001 },
	// });

	// await app.startAllMicroservices();

	/**
	 * Workaround
	 * I'm unable to catch this WS error
	 * I've already set the on("error") and onerror on the WS instance
	 * Nothing works, fuck it
	 */
	process.on('uncaughtException', (err) => {
		if (err?.message === 'WebSocket was closed before the connection was established') {
			return;
		}

		throw err;
	});

	/**
	 * Setup microservices
	 */
	const configService = app.get(ConfigService);
	app.connectMicroservice<MicroserviceOptions>(MAIN_PROCESS(configService));
	await app.startAllMicroservices();

	await app.listen(3000, '0.0.0.0');
}

bootstrap();
