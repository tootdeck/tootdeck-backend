import { ConfigService } from '@nestjs/config';
import { DataSource } from 'typeorm';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';

import checkEnv from '../env/check';

checkEnv();

const configService = new ConfigService();

export default new DataSource({
	type: 'postgres',
	host: configService.getOrThrow<string>('PSQL_HOST'),
	port: configService.getOrThrow<number>('PSQL_PORT'),
	username: configService.getOrThrow<string>('PSQL_USERNAME'),
	password: configService.getOrThrow<string>('PSQL_PASSWORD'),
	database: configService.getOrThrow<string>('PSQL_DATABASE'),
	namingStrategy: new SnakeNamingStrategy(),
	migrations: [__dirname + '/../../migrations/*.ts'],
	entities: [__dirname + '/../**/*.entity.ts'],
});
