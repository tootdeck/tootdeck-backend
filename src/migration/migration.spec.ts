import datasource from './migration.config';

describe('Migration', () => {
	it('datasource', async () => {
		expect(!!(await datasource.initialize())).toBe(true);
	});
});
