import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { FastifyRequest } from 'fastify';

import { BrowserFingerprintService } from '../services/utils/fingerprint.service';
import { OAuthService } from '../services/oauth.service';

import { Logger } from '../../utils/logger';

import { RequestOAuthGuard } from '../types/guards.types';

@Injectable()
export class OAuthGuard implements CanActivate {
	private readonly logger = new Logger(OAuthGuard.name);

	constructor(
		private readonly OAuthService: OAuthService,
		private readonly BrowserFingerprint: BrowserFingerprintService,
	) {}

	/**
	 * @throws UnauthorizedException
	 * @throws InternalServerErrorException
	 */
	async canActivate(context: ExecutionContext): Promise<boolean> {
		const request = context.switchToHttp().getRequest() as FastifyRequest;
		const token = request.cookies['s'] as string;

		if (!token) {
			this.logger.warn('OAuthGuard', 'No token');
			throw new UnauthorizedException();
		}

		// Get fingerprint
		const user_agent = request.headers['user-agent'];
		const ip = request.headers['x-real-ip'] as string | undefined;
		const fingerprint = this.BrowserFingerprint.get(user_agent, ip);

		const session = await this.OAuthService.validate(token);
		if (!session) {
			this.logger.verbose(null, 'Invalid JWT [4]');
			throw new UnauthorizedException();
		}

		(request as RequestOAuthGuard)['fingerprint'] = fingerprint;
		(request as RequestOAuthGuard)['oauth_session'] = session;

		return true;
	}
}
