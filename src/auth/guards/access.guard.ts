import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { FastifyRequest } from 'fastify';

import { JWEService } from '../services/jwe.service';

import { Logger } from '../../utils/logger';

import { RequestAccessGuard } from '../types/guards.types';
import { JWEType } from '../types/jwe.types';

@Injectable()
export class AccessGuard implements CanActivate {
	private readonly logger = new Logger(AccessGuard.name);

	constructor(private readonly JWEService: JWEService) {}

	/**
	 * @throws UnauthorizedException
	 * @throws InternalServerErrorException
	 */
	async canActivate(context: ExecutionContext): Promise<boolean> {
		const request = context.switchToHttp().getRequest() as FastifyRequest;

		const token = request.cookies['a'];

		if (!token) {
			this.logger.warn('AccessGuard', 'No token');
			throw new UnauthorizedException();
		}

		const session = await this.JWEService.validate(JWEType.Access, token);
		if (!session) {
			throw new UnauthorizedException();
		}

		(request as RequestAccessGuard)['access_session'] = session;

		return true;
	}
}
