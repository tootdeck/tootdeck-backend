export const JWETime = {
	Access: () => 60 * 20, // 20m
	Refresh: () => 60 * 60 * 24 * 2, // 2d
	OAuth: () => 60 * 10, // 10m
	Delete: () => 60 * 2, // 2m
};
