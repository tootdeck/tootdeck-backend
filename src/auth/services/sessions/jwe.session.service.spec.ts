import { Test, TestingModule } from '@nestjs/testing';
import { randomUUID } from 'crypto';

import { FullAppModule } from '../../../app/app.module';

import { JWEService } from '../jwe.service';
import { BrowserFingerprintService } from '../utils/fingerprint.service';
import { DBApplicationService } from '../../../database/services/database.application.service';
import { DBAccountService } from '../../../database/services/database.account.service';
import { DBUserService } from '../../../database/services/database.user.service';
import { JWESessionService } from './jwe.session.service';

import { UserStore } from '../../../database/entities/user.entity';
import { AccountStore } from '../../../database/entities/account.entity';

import { Fingerprint } from '../../types/fingerprint.types';
import { JWEType } from '../../types/jwe.types';
import { InstanceType } from '../../../database/types';

describe('JWESessionService', () => {
	let Application: DBApplicationService;
	let Account: DBAccountService;
	let User: DBUserService;
	let JWE: JWEService;
	let JWESession: JWESessionService;
	let BrowserFingerprint: BrowserFingerprintService;
	let fingerprint: Fingerprint;

	const application_domain = 'example.com';
	const application_key = 't52jyR5LOeqAK4PhXqI7rktO7CZaD6OPaRohyFVrgzI=';
	const application_secret = 'qlUrKaKNhIuUZrt1pxvGrOnxNdrMDyqPp+qkvhn4Fzw=';

	const user_agent =
		'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/125.0.0.0 Safari/537.36';
	const ip = '95.186.43.213';

	let account_username = 'tootdeck';
	let account_domain = application_domain;
	let account_token = 'xOOnkfBjAtRqtAmqWFNZEhDsjetgaBIFccIg1EPLqdM=';

	let token = 'token';
	let access_token_uuid = randomUUID();
	let refresh_token_uuid = randomUUID();

	let user: UserStore;
	let main: AccountStore;

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule(FullAppModule).compile();

		Application = app.get<DBApplicationService>(DBApplicationService);
		Account = app.get<DBAccountService>(DBAccountService);
		User = app.get<DBUserService>(DBUserService);
		JWE = app.get<JWEService>(JWEService);
		JWESession = app.get<JWESessionService>(JWESessionService);
		BrowserFingerprint = app.get<BrowserFingerprintService>(BrowserFingerprintService);
		fingerprint = BrowserFingerprint.get(user_agent, ip);

		/**
		 * Create a valid application
		 */
		await Application.create(
			application_domain,
			InstanceType.Mastodon,
			application_key,
			application_secret,
		);

		/**
		 * Create a valid main account
		 */
		main = await Account.create(account_username, account_domain, account_token).then(
			(entity) => entity!,
		);

		/**
		 * Create a valid user
		 */
		user = await User.create(main).then((entity) => entity!);
	});

	describe('createAccess', () => {
		test('Create a valid entry', async () => {
			await JWESession.createAccess(
				user.uuid,
				access_token_uuid,
				token,
				refresh_token_uuid,
			).then((result) => {
				expect(result).toMatchObject({
					token_uuid: access_token_uuid,
					user_uuid: user.uuid,
					token: token,
					linked_to_token_uuid: refresh_token_uuid,
				});
			});
		});

		test('Create an invalid entry', async () => {
			await JWESession.createAccess('', access_token_uuid, token, refresh_token_uuid).then(
				(result) => {
					expect(result).toBeNull();
				},
			);
			await JWESession.createAccess(user.uuid, '', token, refresh_token_uuid).then(
				(result) => {
					expect(result).toBeNull();
				},
			);
			await JWESession.createAccess(
				user.uuid,
				access_token_uuid,
				'',
				refresh_token_uuid,
			).then((result) => {
				expect(result).toBeNull();
			});
			await JWESession.createAccess(user.uuid, access_token_uuid, token, '').then(
				(result) => {
					expect(result).toBeNull();
				},
			);
		});
	});

	describe('createRefresh', () => {
		test('Create a valid entry', async () => {
			await JWESession.createRefresh(
				user.uuid,
				refresh_token_uuid,
				token,
				access_token_uuid,
				fingerprint,
				false,
			).then((result) => {
				expect(result).toMatchObject({
					token_uuid: refresh_token_uuid,
					user_uuid: user.uuid,
					token: token,
					linked_to_token_uuid: access_token_uuid,
				});
			});
		});

		test('Create an invalid entry', async () => {
			await JWESession.createRefresh(
				'',
				refresh_token_uuid,
				token,
				access_token_uuid,
				fingerprint,
				false,
			).then((result) => {
				expect(result).toBeNull();
			});
			await JWESession.createRefresh(
				user.uuid,
				'',
				token,
				access_token_uuid,
				fingerprint,
				false,
			).then((result) => {
				expect(result).toBeNull();
			});
			await JWESession.createRefresh(
				user.uuid,
				refresh_token_uuid,
				'',
				access_token_uuid,
				fingerprint,
				false,
			).then((result) => {
				expect(result).toBeNull();
			});
			await JWESession.createRefresh(
				user.uuid,
				refresh_token_uuid,
				token,
				'',
				fingerprint,
				false,
			).then((result) => {
				expect(result).toBeNull();
			});
			await JWESession.createRefresh(
				user.uuid,
				refresh_token_uuid,
				token,
				access_token_uuid,
				{} as any,
				false,
			).then((result) => {
				expect(result).toBeNull();
			});
		});

		test('Refresh a valid entry', async () => {
			let new_access_token_uuid = randomUUID();
			let new_refresh_token_uuid = randomUUID();

			await JWESession.createRefresh(
				user.uuid,
				new_refresh_token_uuid,
				token,
				new_access_token_uuid,
				fingerprint,
				true,
				refresh_token_uuid,
			).then((result) => {
				expect(result).toMatchObject({
					token_uuid: new_refresh_token_uuid,
					user_uuid: user.uuid,
					token: token,
					linked_to_token_uuid: new_access_token_uuid,
				});
			});
		});

		test('Refresh an invalid entry', async () => {
			await JWESession.createRefresh(
				user.uuid,
				randomUUID(),
				token,
				randomUUID(),
				fingerprint,
				true,
				'',
			).then((result) => {
				expect(result).toBeNull();
			});
			await JWESession.createRefresh(
				user.uuid,
				randomUUID(),
				token,
				randomUUID(),
				fingerprint,
				true,
				'invalid',
			).then((result) => {
				expect(result).toBeNull();
			});
		});
	});

	describe('get', () => {
		test('Get a valid entry', async () => {
			await JWESession.get(JWEType.Access, access_token_uuid).then((result) => {
				expect(result).toMatchObject({
					token_uuid: access_token_uuid,
					user_uuid: user.uuid,
					token: 'token',
				});
			});
			await JWESession.get(JWEType.Refresh, refresh_token_uuid).then((result) => {
				expect(result).toMatchObject({
					token_uuid: refresh_token_uuid,
					user_uuid: user.uuid,
					token: 'token',
				});
			});
		});

		test('Get an invalid entry', async () => {
			await JWESession.get(JWEType.Access, refresh_token_uuid).then((result) => {
				expect(result).toBeNull();
			});
			await JWESession.get(JWEType.Refresh, access_token_uuid).then((result) => {
				expect(result).toBeNull();
			});
			await JWESession.get('invalid' as any, access_token_uuid).then((result) => {
				expect(result).toBeNull();
			});
		});
	});

	describe('delete', () => {
		test('Delete a valid entry', async () => {
			await JWESession.delete(JWEType.Access, access_token_uuid).then((result) => {
				expect(result).toBe(true);
			});
			await JWESession.delete(JWEType.Refresh, refresh_token_uuid).then((result) => {
				expect(result).toBe(true);
			});
		});

		test('Delete an invalid entry', async () => {
			await JWESession.delete(JWEType.Access, access_token_uuid).then((result) => {
				expect(result).toBe(false);
			});
			await JWESession.delete(JWEType.Refresh, refresh_token_uuid).then((result) => {
				expect(result).toBe(false);
			});
		});
	});

	afterAll(async () => {
		await User.getWithSessions(user.uuid)
			.then((result) =>
				User.deleteSessions(user.uuid, result?.session.map((s) => s.uuid) ?? []).catch(
					() => {},
				),
			)
			.catch(() => {});
		await User.delete(main).catch(() => {});
		await Account.delete(account_username, account_domain).catch(() => {});
		await Application.delete(application_domain).catch(() => {});
	});
});
