import { Inject, Injectable, forwardRef } from '@nestjs/common';
import { Redis } from 'ioredis';
import { RedisService } from '@liaoliaots/nestjs-redis';
import { deserialize, serialize } from 'v8';

import { OpenSessionService } from './open.session.service';

import { Logger } from '../../../utils/logger';

import { RedisNamespace } from '../../../app/constants/redis';
import { JWETime } from '../../config';

import { JWESession } from '../../types/sessions.types';
import { JWEType } from '../../types/jwe.types';
import { Fingerprint } from '../../types/fingerprint.types';
import { RedisExpiryToken } from '../../../app/types';

@Injectable()
export class JWESessionService {
	private readonly logger = new Logger(JWESessionService.name);
	private readonly redis_access: Redis;
	private readonly redis_refresh: Redis;

	constructor(
		@Inject(forwardRef(() => OpenSessionService))
		private readonly OpenSessionService: OpenSessionService,
		RedisService: RedisService,
	) {
		this.redis_access = RedisService.getOrThrow(RedisNamespace.AccessToken);
		this.redis_refresh = RedisService.getOrThrow(RedisNamespace.RefreshToken);

		if (process.env['NODE_ENV'] !== 'test') {
			this.redis_refresh.on('ready', () => {
				this.redis_refresh.config('SET', 'notify-keyspace-events', 'Ex');
			});
		}
	}

	/**
	 * Utils
	 */
	//#region

	private async _seralize(redis: Redis, data: JWESession, expires: number): Promise<boolean> {
		const { token_uuid } = data;
		try {
			return !!(await redis.set(token_uuid, serialize(data), RedisExpiryToken.EX, expires));
		} catch (e) {
			this.logger.error(`_seralize`, '');
			console.error(e);
			return false;
		}
	}

	private async _deseralize(redis: Redis, uuid: string): Promise<JWESession | null> {
		try {
			const data = await redis.getBuffer(uuid);
			if (!data) {
				return null;
			}
			return deserialize(data);
		} catch (e) {
			this.logger.error(`_deseralize`, '');
			console.error(e);
			return null;
		}
	}

	private async seralize(type: JWEType, data: JWESession): Promise<JWESession | null> {
		let result: boolean = false;

		switch (type) {
			case JWEType.Access:
				result = await this._seralize(this.redis_access, data, JWETime.Access());
				break;
			case JWEType.Refresh:
				result = await this._seralize(this.redis_refresh, data, JWETime.Refresh());
				break;
			default:
				return null;
		}
		if (!result) {
			return null;
		}

		return data;
	}

	private async deseralize(type: JWEType, uuid: string) {
		switch (type) {
			case JWEType.Access:
				return this._deseralize(this.redis_access, uuid);
				break;
			case JWEType.Refresh:
				return this._deseralize(this.redis_refresh, uuid);
				break;
			default:
				return null;
		}
	}

	//#endregion

	/**
	 * Service
	 */
	//#region

	async get(type: JWEType, token_uuid: string): Promise<JWESession | null> {
		return this.deseralize(type, token_uuid);
	}

	async createAccess(
		user_uuid: string,
		token_uuid: string,
		token: string,
		linked_to_token_uuid: string,
	): Promise<JWESession | null> {
		if (!user_uuid || !token_uuid || !token || !linked_to_token_uuid) {
			return null;
		}

		const session: JWESession = {
			token_uuid,
			user_uuid,
			token,
			linked_to_token_uuid,
		};

		const add = await this.seralize(JWEType.Access, session);

		return add;
	}

	async createRefresh(
		user_uuid: string,
		token_uuid: string,
		token: string,
		linked_to_token_uuid: string,
		fingerprint: Fingerprint,
		refreshing: boolean,
		old_token_uuid?: string,
	): Promise<JWESession | null> {
		if (
			!user_uuid ||
			!token_uuid ||
			!token ||
			!linked_to_token_uuid ||
			!fingerprint ||
			!Object.keys(fingerprint).length ||
			(refreshing && !old_token_uuid)
		) {
			return null;
		}

		const session: JWESession = {
			token_uuid,
			user_uuid,
			token,
			linked_to_token_uuid,
		};

		const add = await this.seralize(JWEType.Refresh, session);
		if (!add) {
			this.logger.warn('createRefresh', `Failed to create refresh token ${token_uuid}`);
			return null;
		}

		if (refreshing) {
			const update = await this.OpenSessionService.updateToken(old_token_uuid!, token_uuid);
			if (!update) {
				this.logger.warn('createRefresh', `Failed to update token ${old_token_uuid}`);
				await this.redis_refresh.del(session.token_uuid);
				return null;
			}
		} else {
			const open_session = await this.OpenSessionService.create(
				user_uuid,
				token_uuid,
				fingerprint,
			);
			if (!open_session) {
				this.logger.warn(
					'createRefresh',
					`Failed to create session with token ${token_uuid}`,
				);
				await this.redis_refresh.del(session.token_uuid);
				return null;
			}
		}

		return add;
	}

	async delete(type: JWEType, token_uuid: string, delete_session?: boolean): Promise<boolean> {
		try {
			let ret: Buffer | null = null;
			switch (type) {
				case JWEType.Access:
					ret = await this.redis_access.getdelBuffer(token_uuid);
					break;
				case JWEType.Refresh:
					ret = await this.redis_refresh.getdelBuffer(token_uuid);
					break;
			}

			if (type === JWEType.Refresh && ret && delete_session) {
				const parse = deserialize(ret) as JWESession;
				const open_session = await this.OpenSessionService.revokeTokenByUUID(token_uuid);
				if (!open_session) {
					this.logger.warn(
						'delete',
						`Failed to remove session with token ${token_uuid} from user ${parse.user_uuid}`,
					);
				}
			}

			return !!ret;
		} catch (e) {
			this.logger.error(`delete`, '');
			console.error(e);
			return false;
		}
	}

	//#endregion
}
