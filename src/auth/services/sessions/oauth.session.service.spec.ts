import { Test, TestingModule } from '@nestjs/testing';
import { ConfigModule } from '@nestjs/config';
import { isUUID } from 'class-validator';
import { randomUUID } from 'crypto';

import { OAuthSessionService } from './oauth.session.service';
import { BrowserFingerprintService } from '../utils/fingerprint.service';
import { OptionalFeaturesService } from '../../../app/services/utils/optionalFeatures.service';

import { RedisConfig } from '../../../app/configs/redis.config';

import { RedisDB } from '../../../app/constants/redis';

import { OAuthSession } from '../../types/sessions.types';

describe('OAuthSessionService', () => {
	let service: OAuthSessionService;
	let token_uuid: string;
	let entry: OAuthSession;

	const test_domain = 'https://localhost';
	const user_uuid = randomUUID();

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule({
			imports: [RedisConfig([RedisDB.OAuthToken]), ConfigModule.forRoot({ isGlobal: true })],
			providers: [OAuthSessionService, BrowserFingerprintService, OptionalFeaturesService],
		}).compile();

		service = app.get(OAuthSessionService);
	});

	describe('create', () => {
		test('Create a valid entry', async () => {
			await service
				.create(user_uuid, test_domain, {
					add_account: false,
					account_uuid: '',
				})
				.then((result) => {
					// @ts-ignore
					entry = result;
					token_uuid = result!.token_uuid;

					expect(isUUID(result!.token_uuid)).toBe(true);
					expect(test_domain === result!.domain).toBe(true);
				});
		});

		test('Creating an existing entry', async () => {
			await service
				.create(user_uuid, test_domain, {
					add_account: false,
					account_uuid: '',
				})
				.then((result) => {
					// @ts-ignore
					token_uuid = result!.token_uuid;

					expect(result).toMatchObject(entry);
				});
		});
	});

	describe('get', () => {
		test('Get a valid entry', async () => {
			await service.get(token_uuid).then((result) => {
				expect(result).toMatchObject({
					token_uuid: token_uuid,
					domain: test_domain,
					account_uuid: '',
					user_uuid: user_uuid,
					options: {
						add_account: false,
						account_uuid: '',
					},
				} as OAuthSession);
			});
		});

		test('Get an invalid entry', async () => {
			await service.get('nothing').then((result) => {
				expect(result).toBeNull();
			});
		});
	});

	describe('delete', () => {
		test('Deleting a valid entry', async () => {
			await service.delete(token_uuid).then((result) => {
				expect(result).toBe(true);
			});
		});

		test('Deleting an invalid entry', async () => {
			await service.delete('nothing').then((result) => {
				expect(result).toBe(false);
			});
		});
	});
});
