import { Injectable } from '@nestjs/common';
import { randomUUID } from 'crypto';
import { Redis } from 'ioredis';
import { RedisService } from '@liaoliaots/nestjs-redis';
import { deserialize, serialize } from 'v8';

import { Logger } from '../../../utils/logger';

import { RedisNamespace } from '../../../app/constants/redis';
import { JWETime } from '../../config';

import { UserDeleteSession } from '../../types/sessions.types';
import { RedisExpiryToken } from '../../../app/types';

@Injectable()
export class UserDeleteSessionService {
	private readonly logger = new Logger(UserDeleteSessionService.name);
	private readonly redis: Redis;

	constructor(RedisService: RedisService) {
		this.redis = RedisService.getOrThrow(RedisNamespace.UserDelete);
	}

	/**
	 * Utils
	 */
	//#region

	private async seralize(data: UserDeleteSession): Promise<boolean> {
		const { token_uuid: uuid } = data;
		try {
			return !!(await this.redis.set(
				uuid,
				serialize(data),
				RedisExpiryToken.EX,
				JWETime.Delete(),
			));
		} catch (e) {
			this.logger.error(`seralize`, '');
			console.error(e);
			return false;
		}
	}

	private async deseralize(uuid: string): Promise<UserDeleteSession | null> {
		try {
			const data = await this.redis.getBuffer(uuid);
			if (!data) {
				return null;
			}
			return deserialize(data);
		} catch (e) {
			this.logger.error(`deseralize`, '');
			console.error(e);
			return null;
		}
	}

	private async find(user_uuid: string) {
		const all = await this.redis.keys('*');

		for (const key of all) {
			const found = await this.get(key);
			if (!found) {
				continue;
			}
			if (found.user_uuid === user_uuid) {
				return found;
			}
		}
		return null;
	}

	//#endregion

	/**
	 * Service
	 */
	//#region

	async get(uuid: string): Promise<UserDeleteSession | null> {
		return this.deseralize(uuid);
	}

	async create(user_uuid: string): Promise<UserDeleteSession | null> {
		const session: UserDeleteSession = {
			token_uuid: randomUUID(),
			code: (Math.random() + 1).toString(36).substring(2).toUpperCase(),
			user_uuid,
			token: '',
		};

		const exist = await this.find(user_uuid);
		if (exist) {
			return exist;
		}

		while (!!(await this.deseralize(session.token_uuid))) {
			session.token_uuid = randomUUID();
		}

		const result = await this.seralize(session);
		if (!result) {
			return null;
		}

		return session;
	}

	async updateJWE(token_uuid: string, token: string): Promise<UserDeleteSession | null> {
		const session = await this.get(token_uuid);
		if (!session || !token_uuid || !token) {
			return null;
		}

		session.token = token;

		const update = await this.seralize(session);
		if (!update) {
			return null;
		}

		return session;
	}

	async delete(uuid: string) {
		try {
			return !!(await this.redis.del(uuid));
		} catch (e) {
			this.logger.error(`delete`, '');
			console.error(e);
			return false;
		}
	}
	//#endregion
}
