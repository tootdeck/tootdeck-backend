import { Test, TestingModule } from '@nestjs/testing';
import { JwtService } from '@nestjs/jwt';
import { randomUUID } from 'crypto';

import { FullAppModule } from '../../../app/app.module';

import { BrowserFingerprintService } from '../utils/fingerprint.service';
import { OpenSessionService } from './open.session.service';
import { DBApplicationService } from '../../../database/services/database.application.service';
import { DBAccountService } from '../../../database/services/database.account.service';
import { DBUserService } from '../../../database/services/database.user.service';

import { UserSessionStore } from '../../../database/entities/user.session.entity';
import { UserStore } from '../../../database/entities/user.entity';
import { AccountStore } from '../../../database/entities/account.entity';

import { Fingerprint } from '../../types/fingerprint.types';
import { InstanceType } from '../../../database/types';
import { JWESessionService } from './jwe.session.service';

describe('OpenSessionService', () => {
	let Application: DBApplicationService;
	let Account: DBAccountService;
	let User: DBUserService;
	let JWESession: JWESessionService;
	let OpenSession: OpenSessionService;
	let BrowserFingerprint: BrowserFingerprintService;
	let fingerprint: Fingerprint;

	const application_domain = 'example.com';
	const application_key = 't52jyR5LOeqAK4PhXqI7rktO7CZaD6OPaRohyFVrgzI=';
	const application_secret = 'qlUrKaKNhIuUZrt1pxvGrOnxNdrMDyqPp+qkvhn4Fzw=';

	const user_agent =
		'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/125.0.0.0 Safari/537.36';
	const ip = '95.186.43.213';

	let account_username = 'tootdeck';
	let account_domain = application_domain;
	let account_token = 'xOOnkfBjAtRqtAmqWFNZEhDsjetgaBIFccIg1EPLqdM=';

	let token_uuid = randomUUID();
	let new_token_uuid = randomUUID();
	let session: UserSessionStore;

	let user: UserStore;
	let main: AccountStore;

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule(FullAppModule).compile();

		Application = app.get<DBApplicationService>(DBApplicationService);
		Account = app.get<DBAccountService>(DBAccountService);
		User = app.get<DBUserService>(DBUserService);
		OpenSession = app.get<OpenSessionService>(OpenSessionService);
		JWESession = app.get<JWESessionService>(JWESessionService);
		BrowserFingerprint = app.get<BrowserFingerprintService>(BrowserFingerprintService);
		fingerprint = BrowserFingerprint.get(user_agent, ip);

		/**
		 * Create a valid application
		 */
		await Application.create(
			application_domain,
			InstanceType.Mastodon,
			application_key,
			application_secret,
		);

		/**
		 * Create a valid main account
		 */
		main = await Account.create(account_username, account_domain, account_token).then(
			(entity) => entity!,
		);

		/**
		 * Create a valid user
		 */
		user = await User.create(main).then((entity) => entity!);
	});

	describe('create', () => {
		test('Create a valid session', async () => {
			await OpenSession.create(user.uuid, token_uuid, fingerprint).then((result) => {
				// @ts-ignore
				session = result;

				expect(result).toBeTruthy();
			});
		});

		test('Create an invalid session', async () => {
			await OpenSession.create(undefined as any, token_uuid, fingerprint).then((result) => {
				expect(result).toBeNull();
			});
			await OpenSession.create(user.uuid, undefined as any, fingerprint).then((result) => {
				expect(result).toBeNull();
			});
			await OpenSession.create(user.uuid, token_uuid, undefined as any).then((result) => {
				expect(result).toBeNull();
			});
		});
	});

	describe('get', () => {
		test('Get all sessions opened by this user', async () => {
			await OpenSession.get(user.uuid).then((result) => {
				expect(result?.length).toBe(1);
				expect(result?.[0]).toMatchObject(session);
			});
		});

		test('Get all sessions opened by this invalid user', async () => {
			await OpenSession.get(randomUUID()).then((result) => {
				expect(result).toBeNull();
			});
		});
	});

	describe('updateToken', () => {
		test('Update token for a valid session', async () => {
			await OpenSession.updateToken(token_uuid, new_token_uuid).then((result) => {
				expect(result).toMatchObject({ uuid: session.uuid, token_uuid: new_token_uuid });
			});
		});

		test('Update token for an invalid session', async () => {
			await OpenSession.updateToken(undefined as any, new_token_uuid).then((result) => {
				expect(result).toBeNull();
			});
			await OpenSession.updateToken(token_uuid, undefined as any).then((result) => {
				expect(result).toBeNull();
			});
		});
	});

	describe('revokeTokenBySessionUUID', () => {
		test('Revoke token for a valid session UUID', async () => {
			await OpenSession.revokeTokenBySessionUUID(user.uuid, session.uuid).then((result) => {
				expect(result).toBe(true);
			});
		});

		test('Revoke token for an invalid session UUID', async () => {
			await OpenSession.revokeTokenBySessionUUID(user.uuid, randomUUID()).then((result) => {
				expect(result).toBeFalsy();
			});
			await OpenSession.revokeTokenBySessionUUID(user.uuid, undefined as any).then(
				(result) => {
					expect(result).toBeFalsy();
				},
			);
			await OpenSession.revokeTokenBySessionUUID(undefined as any, session.uuid).then(
				(result) => {
					expect(result).toBeFalsy();
				},
			);
		});
	});

	describe('revokeTokenByUUID', () => {
		test('Revoke token for a valid token', async () => {
			await OpenSession.create(user.uuid, token_uuid, fingerprint).then((result) => {
				// @ts-ignore
				session = result;

				expect(result).toBeTruthy();
			});

			await OpenSession.revokeTokenByUUID(token_uuid).then((result) => {
				expect(result).toBe(true);
			});
		});

		test('Revoke token for an invalid token', async () => {
			await OpenSession.revokeTokenByUUID(randomUUID()).then((result) => {
				expect(result).toBeFalsy();
			});
			await OpenSession.revokeTokenByUUID(undefined as any).then((result) => {
				expect(result).toBeFalsy();
			});
		});
	});

	describe('revokeToken', () => {
		test('Revoke token for a valid user', async () => {
			await OpenSession.create(user.uuid, token_uuid, fingerprint).then((result) => {
				// @ts-ignore
				session = result;

				expect(result).toBeTruthy();
			});

			await JWESession.createRefresh(
				user.uuid,
				token_uuid,
				'token',
				randomUUID(),
				fingerprint,
				false,
			).then(async (jwe) => {
				expect(jwe).toBeTruthy();
			});

			await OpenSession.get(user.uuid).then(async (sessions) => {
				const session = sessions?.find((session) => token_uuid === session.token_uuid);

				expect(session).toBeTruthy();

				await OpenSession.revokeToken(user.uuid, session!.token_uuid!).then((result) => {
					expect(result).toBe(true);
				});
			});
		});

		test('Revoke token for an invalid user', async () => {
			await OpenSession.revokeToken(user.uuid, randomUUID()).then((result) => {
				expect(result).toBeFalsy();
			});
			await OpenSession.revokeToken(user.uuid, undefined as any).then((result) => {
				expect(result).toBeFalsy();
			});
			await OpenSession.revokeToken(undefined as any, session.uuid).then((result) => {
				expect(result).toBeFalsy();
			});
		});
	});

	describe('delete', () => {
		test('Delete a valid session', async () => {
			await OpenSession.create(user.uuid, token_uuid, fingerprint).then((result) => {
				// @ts-ignore
				session = result;

				expect(result).toBeTruthy();
			});

			await OpenSession.delete(user.uuid, session.uuid).then((result) => {
				expect(result).toBe(true);
			});
		});

		test('Delete an invalid session', async () => {
			await OpenSession.delete(user.uuid, randomUUID()).then((result) => {
				expect(result).toBeFalsy();
			});
			await OpenSession.delete(user.uuid, undefined as any).then((result) => {
				expect(result).toBeFalsy();
			});
			await OpenSession.delete(undefined as any, session.uuid).then((result) => {
				expect(result).toBeFalsy();
			});
		});
	});

	afterAll(async () => {
		await User.getWithSessions(user.uuid)
			.then((result) =>
				User.deleteSessions(user.uuid, result?.session.map((s) => s.uuid) ?? []).catch(
					() => {},
				),
			)
			.catch(() => {});
		await User.delete(main).catch(() => {});
		await Account.delete(account_username, account_domain).catch(() => {});
		await Application.delete(application_domain).catch(() => {});
	});
});
