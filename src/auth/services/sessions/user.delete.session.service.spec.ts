import { Test, TestingModule } from '@nestjs/testing';
import { isUUID } from 'class-validator';
import { randomUUID } from 'crypto';

import { UserDeleteSessionService } from './user.delete.session.service';

import { RedisConfig } from '../../../app/configs/redis.config';

import { RedisDB } from '../../../app/constants/redis';

import { UserDeleteSession } from '../../types/sessions.types';

describe('UserDeleteSessionService', () => {
	let UserDeleteSession: UserDeleteSessionService;
	let uuid: string;

	const test_uuid = randomUUID();
	const test_token = 'ty0osll63wjgwd3bpj2oz22l4y1tc4c883pvvryi359xx9rdbxj';

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule({
			imports: [RedisConfig([RedisDB.UserDelete])],
			providers: [UserDeleteSessionService],
		}).compile();

		UserDeleteSession = app.get(UserDeleteSessionService);
	});

	describe('create', () => {
		let entry: UserDeleteSession;

		test('Creating a valid session', async () => {
			await UserDeleteSession.create(test_uuid).then((result) => {
				// @ts-ignore
				entry = result;

				expect(isUUID(result!.token_uuid)).toBe(true);
				expect(test_uuid === result!.user_uuid).toBe(true);
				expect(result!.code).toBeTruthy();
			});
		});

		test('Creating an existing session', async () => {
			await UserDeleteSession.create(test_uuid).then((result) => {
				// @ts-ignore
				uuid = result.token_uuid;

				expect(result).toMatchObject(entry);
			});
		});
	});

	describe('updateJWE', () => {
		test('Updating a valid session', async () => {
			await UserDeleteSession.updateJWE(uuid, test_token).then((result) => {
				expect(result).toMatchObject({
					token_uuid: uuid,
					user_uuid: test_uuid,
					token: test_token,
				} as UserDeleteSession);
			});
		});

		test('Updating an invalid session', async () => {
			expect(await UserDeleteSession.updateJWE('nothing', test_token)).toBeNull();
			expect(await UserDeleteSession.updateJWE(uuid, '')).toBeNull();
		});
	});

	describe('get', () => {
		test('Get a valid session', async () => {
			expect(await UserDeleteSession.get(uuid)).toMatchObject({
				token_uuid: uuid,
				user_uuid: test_uuid,
				token: test_token,
			} as UserDeleteSession);
		});

		test('Get an invalid session', async () => {
			expect(await UserDeleteSession.get('nothing')).toBeNull();
		});
	});

	describe('delete', () => {
		test('Delete a valid session', async () => {
			expect(await UserDeleteSession.delete(uuid)).toBe(true);
		});

		test('Delete an invalid session', async () => {
			expect(await UserDeleteSession.delete('nothing')).toBe(false);
		});
	});
});
