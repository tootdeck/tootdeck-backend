import { Injectable } from '@nestjs/common';
import { randomUUID } from 'crypto';
import { Redis } from 'ioredis';
import { RedisService } from '@liaoliaots/nestjs-redis';
import { deserialize, serialize } from 'v8';

import { Logger } from '../../../utils/logger';

import { RedisNamespace } from '../../../app/constants/redis';
import { JWETime } from '../../config';

import { OAuthSession, OAuthSessionPartial } from '../../types/sessions.types';
import { OAuthAutorizeOptions } from '../../types/oauth.types';
import { RedisExpiryToken } from '../../../app/types';

@Injectable()
export class OAuthSessionService {
	private readonly logger = new Logger(OAuthSessionService.name);

	private readonly redis: Redis;

	constructor(RedisService: RedisService) {
		this.redis = RedisService.getOrThrow(RedisNamespace.OAuthToken);
	}

	/**
	 * Utils
	 */
	//#region

	private async seralize(data: OAuthSessionPartial | OAuthSession): Promise<boolean> {
		try {
			return !!(await this.redis.set(
				data.token_uuid,
				serialize(data),
				RedisExpiryToken.EX,
				JWETime.OAuth(),
			));
		} catch (e) {
			this.logger.error(`seralize`, '');
			console.error(e);
			return false;
		}
	}

	private async deseralize(uuid: string): Promise<OAuthSessionPartial | OAuthSession | null> {
		try {
			const data = await this.redis.getBuffer(uuid);
			if (!data) {
				return null;
			}
			return deserialize(data);
		} catch (e) {
			this.logger.error(`deseralize`, '');
			console.error(e);
			return null;
		}
	}

	private async find(user_uuid: string, domain: string, options: OAuthAutorizeOptions) {
		const all = await this.redis.keys('*');

		for (const key of all) {
			const found = (await this.get(key)) as OAuthSession | null;
			if (!found || !found.user_uuid) {
				continue;
			}

			if (
				found.user_uuid === user_uuid &&
				found.domain === domain &&
				(found.options.add_account === options.add_account ||
					found.options.refresh_account === options.refresh_account)
			) {
				return found;
			}
		}
		return null;
	}

	private async createPartial(domain: string): Promise<OAuthSessionPartial | null> {
		const session: OAuthSessionPartial = {
			token_uuid: randomUUID(),
			domain,
		};

		while (!!(await this.deseralize(session.token_uuid))) {
			session.token_uuid = randomUUID();
		}

		const result = await this.seralize(session);
		if (!result) {
			return null;
		}

		return session;
	}

	//#endregion

	/**
	 * Service
	 */
	//#region

	async get(uuid: string): Promise<OAuthSessionPartial | OAuthSession | null> {
		return this.deseralize(uuid);
	}

	async create(
		user_uuid: string | null,
		domain: string,
		options?: OAuthAutorizeOptions,
	): Promise<OAuthSessionPartial | OAuthSession | null> {
		if (user_uuid === null) {
			return this.createPartial(domain);
		}

		const session: OAuthSession = {
			token_uuid: randomUUID(),
			user_uuid,
			account_uuid: options?.account_uuid,
			domain,
			options: options!,
		};

		const exist = await this.find(user_uuid, domain, options!);
		if (exist) {
			return exist;
		}

		while (!!(await this.deseralize(session.token_uuid))) {
			session.token_uuid = randomUUID();
		}

		const result = await this.seralize(session);
		if (!result) {
			return null;
		}

		return session;
	}

	async delete(uuid: string) {
		try {
			return !!(await this.redis.del(uuid));
		} catch (e) {
			this.logger.error(`delete`, '');
			console.error(e);
			return false;
		}
	}
	//#endregion
}
