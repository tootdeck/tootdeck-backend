import { BadRequestException, Inject, Injectable, forwardRef } from '@nestjs/common';
import { Redis } from 'ioredis';

import { JWESessionService } from './jwe.session.service';
import { WsService } from '../../../websocket/services/ws.service';
import { DBUserSessionService } from '../../../database/services/database.user.session.service';
import { DBUserService } from '../../../database/services/database.user.service';

import { UserSessionStore } from '../../../database/entities/user.session.entity';

import { Logger } from '../../../utils/logger';
import { RedisDB, RedisNamespace } from '../../../app/constants/redis';

import { Fingerprint } from '../../types/fingerprint.types';
import { JWEType } from '../../types/jwe.types';
import { ResponseType, WebsocketAPI } from '../../../websocket/types';
import { RedisService } from '@liaoliaots/nestjs-redis';

@Injectable()
export class OpenSessionService {
	private readonly logger = new Logger(OpenSessionService.name);
	private readonly redis_subscription: Redis;

	constructor(
		@Inject(forwardRef(() => JWESessionService))
		private readonly JWESessionService: JWESessionService,
		private readonly WsService: WsService,
		private readonly DBUserService: DBUserService,
		private readonly DBUserSessionService: DBUserSessionService,
		RedisService: RedisService,
	) {
		this.redis_subscription = RedisService.getOrThrow(RedisNamespace.OpenSessionSubscribe);

		if (process.env['NODE_ENV'] !== 'test') {
			this.redis_subscription.subscribe(`__keyevent@${RedisDB.RefreshToken}__:expired`);
			this.redis_subscription.on('message', (_: string, key: string) => {
				console.log('garbage collector trigger called');
				this.revokeTokenByUUID(key);
			});
		}
	}

	/**
	 * Service
	 */
	//#region

	async get(user_uuid: string): Promise<Array<UserSessionStore> | null> {
		const user = await this.DBUserService.getWithSessions(user_uuid);
		if (!user) {
			return null;
		}

		return user.session;
	}

	private async getBySessionUUID(user_uuid: string, session_uuid: string) {
		const user = await this.DBUserService.getWithSessions(user_uuid, session_uuid);
		if (!user) {
			return false;
		}

		const session = user.session[0];
		if (!session) {
			return false;
		}

		return session;
	}

	async create(
		user_uuid: string,
		token_uuid: string,
		fingerprint: Fingerprint,
	): Promise<UserSessionStore | null> {
		return this.DBUserService.createSession(user_uuid, token_uuid, fingerprint);
	}

	/**
	 * Update an open session with a new token UUID
	 */
	async updateToken(old_token_uuid: string, new_token_uuid: string) {
		return this.DBUserSessionService.updateToken(old_token_uuid, new_token_uuid);
	}

	async revokeTokenBySessionUUID(
		user_uuid: string,
		session_uuid: string,
	): Promise<boolean | null> {
		const session = await this.getBySessionUUID(user_uuid, session_uuid);
		if (!session || !session.token_uuid) {
			return false;
		}

		return this.revokeToken(user_uuid, session.token_uuid);
	}

	async revokeTokenByUUID(token_uuid: string): Promise<boolean | null> {
		return this.DBUserSessionService.revokeToken(token_uuid);
	}

	/**
	 * @throws BadRequestException
	 */
	async revokeToken(user_uuid: string, token_uuid: string): Promise<boolean | null> {
		if (!user_uuid || !token_uuid) {
			return null;
		}

		const refresh_session = await this.JWESessionService.get(JWEType.Refresh, token_uuid);
		if (!refresh_session) {
			// This should already be garbage collected
			return this.revokeTokenByUUID(token_uuid);
		}

		// check if the user match
		if (refresh_session.user_uuid !== user_uuid) {
			this.logger.warn('revokeToken', "User doesn't match");
			throw new BadRequestException();
		}

		await Promise.all([
			this.JWESessionService.delete(JWEType.Access, refresh_session?.linked_to_token_uuid),
			this.JWESessionService.delete(JWEType.Refresh, token_uuid),
		]);

		this.WsService.sendByUserUUID(user_uuid, {
			type: ResponseType.API,
			data: WebsocketAPI.ResponseData.Refresh,
		});

		return this.revokeTokenByUUID(token_uuid);
	}

	async delete(user_uuid: string, session_uuid: string): Promise<boolean | null> {
		const revoke = await this.revokeTokenBySessionUUID(user_uuid, session_uuid);
		if (!revoke) {
			return false;
		}

		return this.DBUserService.deleteSessions(user_uuid, [session_uuid]);
	}

	//#endregion
}
