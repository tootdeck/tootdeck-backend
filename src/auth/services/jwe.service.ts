import { Injectable, UnauthorizedException, InternalServerErrorException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { randomUUID } from 'crypto';

import { JWESessionService } from './sessions/jwe.session.service';

import { Logger } from '../../utils/logger';

import { secretConfig } from '../../app/configs/secret.config';
import { JWETime } from '../config';

import { CreatedJWE, JWEType } from '../types/jwe.types';
import { Fingerprint } from '../types/fingerprint.types';
import { JWESession } from '../types/sessions.types';

@Injectable()
export class JWEService {
	private readonly logger = new Logger(JWEService.name);
	private readonly secret: string;

	constructor(
		private readonly JwtService: JwtService,
		private readonly JWESessionService: JWESessionService,
	) {
		this.secret = secretConfig();
		if (!this.secret) {
			this.logger.error('constructor', 'Unable to read secret');
			process.exit(1);
		}
		this.logger.log('constructor', 'Secret loaded');
	}

	/**
	 * Utils
	 */
	//#region

	private async accessToken(token_uuid: string): Promise<string> {
		return this.JwtService.signAsync(
			{ u: token_uuid },
			{
				algorithm: 'HS512',
				expiresIn: JWETime.Access(),
				secret: this.secret,
			},
		);
	}

	private async refreshToken(token_uuid: string): Promise<string> {
		return this.JwtService.signAsync(
			{ u: token_uuid },
			{
				algorithm: 'HS512',
				expiresIn: JWETime.Refresh(),
				secret: this.secret,
			},
		);
	}

	private async generateTokens(access_uuid: string, refresh_uuid: string) {
		return Promise.all([this.accessToken(access_uuid), this.refreshToken(refresh_uuid)]);
	}

	/**
	 * Generate `access` and `refresh` tokens UUIDs
	 */
	private generateTokenUUIDs(): [string, string] {
		return [randomUUID(), randomUUID()];
	}

	/**
	 * Append JWTs in previously created redis entries
	 */
	private async createRedisEntries(
		user_uuid: string,
		access: { uuid: string; token: string },
		refresh: { uuid: string; token: string },
		fingerprint: Fingerprint,
		refreshing: boolean,
		token_uuid?: string,
	) {
		const access_session = this.JWESessionService.createAccess(
			user_uuid,
			access.uuid,
			access.token,
			refresh.uuid,
		);
		const refresh_session = this.JWESessionService.createRefresh(
			user_uuid,
			refresh.uuid,
			refresh.token,
			access.uuid,
			fingerprint,
			refreshing,
			token_uuid,
		);

		return Promise.all([access_session, refresh_session]);
	}

	//#endregion

	/**
	 * Service
	 */
	//#region

	/**
	 * @throws UnauthorizedException
	 * @throws InternalServerErrorException
	 */
	async validate(type: JWEType, raw_token: string): Promise<JWESession> {
		const jwt = await this.JwtService.verifyAsync<{ u: string }>(raw_token, {
			algorithms: ['HS512'],
			secret: this.secret,
		}).catch((e) => {
			this.logger.warn('validate', 'Invalid JWT: signature verification failed');
			throw new UnauthorizedException();
		});

		const token_uuid = jwt.u;

		const session = await this.JWESessionService.get(type, token_uuid);
		if (!session || session.token !== raw_token) {
			this.logger.warn('validate', 'Invalid JWT: no session');
			throw new UnauthorizedException();
		}

		return session;
	}

	/**
	 * - Generate tokens UUIDs
	 * - Generate JWT
	 * - Create a new open session in redis
	 *
	 * @returns `access` and `refresh` tokens UUIDs and JWTs
	 * @throws InternalServerErrorException
	 */
	async create(
		user_uuid: string,
		fingerprint: Fingerprint,
		refreshing: boolean,
		token_uuid?: string,
	): Promise<CreatedJWE> {
		if (!user_uuid || !fingerprint || (refreshing && !token_uuid)) {
			throw new InternalServerErrorException();
		}

		const [access_uuid, refresh_uuid] = this.generateTokenUUIDs();
		const [access_token, refresh_token] = await this.generateTokens(access_uuid, refresh_uuid);
		if (!access_token || !refresh_token) {
			this.logger.error('create', 'Failed to sign JWTs');
			throw new InternalServerErrorException();
		}

		const [access_entry, refresh_entry] = await this.createRedisEntries(
			user_uuid,
			{ uuid: access_uuid, token: access_token },
			{ uuid: refresh_uuid, token: refresh_token },
			fingerprint,
			refreshing,
			token_uuid,
		);
		if (!access_entry || !refresh_entry) {
			this.logger.error('create', 'Unable to create redis entries');
			await Promise.all([
				this.JWESessionService.delete(JWEType.Access, access_uuid),
				this.JWESessionService.delete(JWEType.Refresh, refresh_uuid),
			]);
			throw new InternalServerErrorException();
		}

		return {
			access: { token_uuid: access_uuid, token: access_token },
			refresh: { token_uuid: refresh_uuid, token: refresh_token },
		};
	}

	/**
	 * @throws InternalServerErrorException
	 */
	async refresh(session: JWESession, fingerprint: Fingerprint): Promise<CreatedJWE> {
		const tokens = await this.create(session.user_uuid, fingerprint, true, session.token_uuid);

		await Promise.all([
			this.JWESessionService.delete(JWEType.Access, session.linked_to_token_uuid),
			this.JWESessionService.delete(JWEType.Refresh, session.token_uuid),
		]);

		return tokens;
	}

	async delete(session: JWESession): Promise<void> {
		await Promise.all([
			this.JWESessionService.delete(JWEType.Access, session.linked_to_token_uuid),
			this.JWESessionService.delete(JWEType.Refresh, session.token_uuid, true),
		]);
	}

	//#endregion
}
