import { Test, TestingModule } from '@nestjs/testing';
import { JwtService } from '@nestjs/jwt';

import { FullAppModule } from '../../app/app.module';

import { JWEService } from './jwe.service';
import { BrowserFingerprintService } from './utils/fingerprint.service';
import { DBApplicationService } from '../../database/services/database.application.service';
import { DBAccountService } from '../../database/services/database.account.service';
import { DBUserService } from '../../database/services/database.user.service';

import { UserStore } from '../../database/entities/user.entity';
import { AccountStore } from '../../database/entities/account.entity';

import { Fingerprint } from '../types/fingerprint.types';
import { CreatedJWE, JWEType } from '../types/jwe.types';
import { JWESession } from '../types/sessions.types';
import { InstanceType } from '../../database/types';

describe('JWEService', () => {
	let Application: DBApplicationService;
	let Account: DBAccountService;
	let User: DBUserService;
	let JWE: JWEService;
	let Jwt: JwtService;
	let BrowserFingerprint: BrowserFingerprintService;
	let fingerprint: Fingerprint;

	const application_domain = 'example.com';
	const application_key = 't52jyR5LOeqAK4PhXqI7rktO7CZaD6OPaRohyFVrgzI=';
	const application_secret = 'qlUrKaKNhIuUZrt1pxvGrOnxNdrMDyqPp+qkvhn4Fzw=';

	const user_agent =
		'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/125.0.0.0 Safari/537.36';
	const ip = '95.186.43.213';

	const account_username = 'tootdeck';
	const account_domain = application_domain;
	const account_token = 'xOOnkfBjAtRqtAmqWFNZEhDsjetgaBIFccIg1EPLqdM=';

	let user: UserStore;
	let main: AccountStore;

	let tokens: CreatedJWE;
	let session: JWESession;

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule(FullAppModule).compile();

		Application = app.get<DBApplicationService>(DBApplicationService);
		Account = app.get<DBAccountService>(DBAccountService);
		User = app.get<DBUserService>(DBUserService);
		JWE = app.get<JWEService>(JWEService);
		Jwt = app.get<JwtService>(JwtService);
		BrowserFingerprint = app.get<BrowserFingerprintService>(BrowserFingerprintService);
		fingerprint = BrowserFingerprint.get(user_agent, ip);

		/**
		 * Create a valid application
		 */
		await Application.create(
			application_domain,
			InstanceType.Mastodon,
			application_key,
			application_secret,
		);

		/**
		 * Create a valid main account
		 */
		main = await Account.create(account_username, account_domain, account_token).then(
			(entity) => entity!,
		);

		/**
		 * Create a valid user
		 */
		user = await User.create(main).then((entity) => entity!);
	});

	describe('create', () => {
		test('Create a new session', async () => {
			await JWE.create(user.uuid, fingerprint, false).then((result) => {
				// @ts-ignore
				tokens = result;

				expect(result).toBeTruthy();
			});
		});

		test('Create an invalid session', async () => {
			expect(() => JWE.create(undefined as any, fingerprint, false)).rejects.toThrow();
			expect(() => JWE.create(user.uuid, undefined as any, false)).rejects.toThrow();
		});

		test('Refresh a valid session', async () => {
			await JWE.create(user.uuid, fingerprint, true, tokens.refresh.token_uuid).then(
				(result) => {
					// @ts-ignore
					tokens = result;

					expect(result).toBeTruthy();
				},
			);
		});

		test('Refresh an invalid session', async () => {
			expect(() =>
				JWE.create(undefined as any, fingerprint, true, tokens.refresh.token_uuid),
			).rejects.toThrow();
			expect(() =>
				JWE.create(user.uuid, undefined as any, true, tokens.refresh.token_uuid),
			).rejects.toThrow();
			expect(() =>
				JWE.create(user.uuid, fingerprint, true, undefined as any),
			).rejects.toThrow();
		});
	});

	describe('validate', () => {
		test('Validate a valid access token', async () => {
			await JWE.validate(JWEType.Access, tokens.access.token).then((result) => {
				expect(result).toBeTruthy();
			});
		});

		test('Validate an invalid access token', async () => {
			expect(() => JWE.validate(undefined as any, tokens.access.token)).rejects.toThrow();
			expect(() => JWE.validate(JWEType.Access, undefined as any)).rejects.toThrow();
			expect(() => JWE.validate(JWEType.Access, tokens.access.token + 1)).rejects.toThrow();
		});

		test('Validate a valid access refresh', async () => {
			await JWE.validate(JWEType.Refresh, tokens.refresh.token).then((result) => {
				// @ts-ignore
				session = result;

				expect(result).toBeTruthy();
			});
		});

		test('Validate an invalid access refresh', async () => {
			expect(() => JWE.validate(undefined as any, tokens.refresh.token)).rejects.toThrow();
			expect(() => JWE.validate(JWEType.Refresh, undefined as any)).rejects.toThrow();
			expect(() => JWE.validate(JWEType.Refresh, tokens.refresh.token + 1)).rejects.toThrow();
		});
	});

	describe('refresh', () => {
		test('Refresh a valid session', async () => {
			await JWE.refresh(session, fingerprint).then((result) => {
				expect(result).toBeTruthy();
			});
		});

		test('Refresh an valid session', async () => {
			expect(() => JWE.refresh(undefined as any, fingerprint)).rejects.toThrow();
			expect(() => JWE.refresh(session, undefined as any)).rejects.toThrow();
		});
	});

	describe('delete', () => {
		test('Delete a valid session', async () => {
			await JWE.delete(session).then((result) => {
				expect(result).toBe(undefined);
			});
		});

		test('Delete an invalid session', async () => {
			expect(() => JWE.delete(undefined as any)).rejects.toThrow();
		});
	});

	afterAll(async () => {
		await User.getWithSessions(user.uuid)
			.then((result) =>
				User.deleteSessions(user.uuid, result?.session.map((s) => s.uuid) ?? []).catch(
					() => {},
				),
			)
			.catch(() => {});
		await User.delete(main).catch(() => {});
		await Account.delete(account_username, account_domain).catch(() => {});
		await Application.delete(application_domain).catch(() => {});
	});
});
