import {
	BadRequestException,
	Inject,
	Injectable,
	InternalServerErrorException,
	ServiceUnavailableException,
	UnauthorizedException,
	forwardRef,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { HttpService } from '@nestjs/axios';
import { AxiosError } from 'axios';

import MegalodonGenerator from '../../lib/megalodon/megalodon';
import OAuth from '../../lib/megalodon/oauth';

import { OAuthSessionService } from './sessions/oauth.session.service';
import { DBApplicationService } from '../../database/services/database.application.service';
import { DBUserService } from '../../database/services/database.user.service';
import { DBAccountService } from '../../database/services/database.account.service';
import { UserCacheService } from '../../user/services/user.cache.service';
import { InstanceCacheService } from '../../app/services/instanceCache.service';

import { ApplicationStore } from '../../database/entities/application.entity';
import { AccountStore } from '../../database/entities/account.entity';

import { Logger } from '../../utils/logger';
import { findAccountInUser } from '../../utils/findAccountInUser';

import { JWETime } from '../config';
import { secretConfig } from '../../app/configs/secret.config';

import { InstanceType } from '../../database/types';
import { ApiResponseError } from '../types/api.types';
import {
	OAuthAppVerify,
	OAuthAutorize,
	OAuthAutorizeOptionsAddAccount,
	OAuthAutorizeOptionsRefreshAccount,
	OAuthSessionToken,
	OAuthTokenResponse,
	OAuthVerify,
} from '../types/oauth.types';
import { OAuthSession, OAuthSessionPartial } from '../types/sessions.types';

@Injectable()
export class OAuthService {
	private readonly logger = new Logger(OAuthService.name);
	private readonly secret: string;

	private readonly DOMAIN: string;
	private readonly REDIRECT: string;

	constructor(
		configService: ConfigService,
		private readonly JwtService: JwtService,
		@Inject(forwardRef(() => DBApplicationService))
		private readonly DBApplicationService: DBApplicationService,
		@Inject(forwardRef(() => DBAccountService))
		private readonly DBAccountService: DBAccountService,
		private readonly DBUserService: DBUserService,
		private readonly OAuthSessionService: OAuthSessionService,
		private readonly UserCacheService: UserCacheService,
		private readonly HttpService: HttpService,
		private readonly InstanceCacheService: InstanceCacheService,
	) {
		this.secret = secretConfig();
		if (!this.secret) {
			this.logger.error('constructor', 'Unable to read secret');
			process.exit(1);
		}
		this.logger.log('constructor', 'Secret loaded');

		this.DOMAIN = configService.get<string>('DOMAIN') ?? '';
		this.REDIRECT = `https://${this.DOMAIN}/api/oauth/authorize/redirect`;
	}

	/**
	 * Utils
	 */
	//#region

	private async signOAuthToken(token_uuid: string): Promise<string> {
		return this.JwtService.signAsync(
			{ s: token_uuid },
			{
				algorithm: 'HS512',
				expiresIn: JWETime.OAuth(),
				secret: this.secret,
			},
		);
	}

	/**
	 * @throws InternalServerErrorException
	 */
	private async getAppToken(
		application: ApplicationStore,
	): Promise<ApplicationStore | null | undefined> {
		const token = await this.HttpService.axiosRef
			.request({
				url: 'https://' + application.domain + '/oauth/token',
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				data: {
					grant_type: 'client_credentials',
					client_id: application.key,
					client_secret: application.secret,
					redirect_uri: 'urn:ietf:wg:oauth:2.0:oob',
				},
			})
			.then((r) => r.data.access_token)
			.catch((e) => {
				this.logger.error('getAppToken', `${e}`);
				// console.log(e);

				return null;
			});

		if (!token) {
			this.logger.error(
				'getAppToken',
				`Unable to fetch application token for ${application.domain}`,
			);
			return undefined;
		}

		return this.DBApplicationService.updateToken(application, token);
	}

	private async verifAppFallback(application: ApplicationStore): Promise<OAuthAppVerify> {
		return this.HttpService.axiosRef
			.request({
				url: 'https://' + application.domain + '/api/v1/accounts',
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${application.token}`,
				},
			})
			.then((r) => {
				throw r;
			})
			.catch((e: AxiosError) => {
				if (!e.response) {
					this.logger.error(
						'verifAppFallback',
						`Unable to verify app for ${application.domain} ${e}`,
					);
					return OAuthAppVerify.Error;
				}

				const error_status = e.response.status;
				switch (error_status) {
					case 401:
						this.logger.error(
							'verifAppFallback',
							`Unable to verify app for ${application.domain} ${e}`,
						);
						return OAuthAppVerify.Failed;
					case 400:
						return OAuthAppVerify.Success;
					default:
						this.logger.error(
							'verifAppFallback',
							`Unable to verify app for ${application.domain} ${e}`,
						);
						return OAuthAppVerify.Error;
				}
			});
	}

	/**
	 * @throws InternalServerErrorException
	 */
	private async verifyApp(application: ApplicationStore): Promise<OAuthAppVerify> {
		// if (!application.token) {
		const app_token = await this.getAppToken(application);
		switch (app_token) {
			case null:
				return OAuthAppVerify.Error;
			case undefined:
				return OAuthAppVerify.Failed;
			default:
				return OAuthAppVerify.Success;
		}
		// }

		// return MegalodonGenerator(application.type, 'https://' + application.domain, {
		// 	accessToken: application.token,
		// 	axios: this.HttpService.axiosRef,
		// })
		// 	.verifyAppCredentials()
		// 	.then((r) => {
		// 		console.log(r);
		// 		return r.status === 200 ? OAuthAppVerify.Success : OAuthAppVerify.Failed;
		// 	})
		// 	.catch((e: AxiosError) => {
		// 		this.logger.error('verifyApp', `Unable to verify app for ${application.domain}`);
		// 		console.error(e);

		// 		if (!e.response) {
		// 			return OAuthAppVerify.Error;
		// 		}

		// 		const error_status = e.response.status;
		// 		const error_text = (e.response.data as any).error;
		// 		if (
		// 			e instanceof NoImplementedError || // Misskey
		// 			(error_status === 401 && error_text === 'The access token is invalid') || // Mastodon
		// 			(error_status === 400 && error_text?.detail === 'Internal server error') // Pleroma
		// 		) {
		// 			return OAuthAppVerify.Failed;
		// 		}

		// 		if (error_status === 404) {
		// 			return this.verifAppFallback(application);
		// 		}

		// 		return OAuthAppVerify.Error;
		// 	});
	}

	private redirect(entry: ApplicationStore, domain: string) {
		return `https://${domain}/oauth/authorize?redirect_uri=https://${this.DOMAIN}/api/oauth/authorize/redirect&response_type=code&client_id=${entry.key}&scope=read%20write%20follow&force_login=true`;
	}

	/**
	 * @throws InternalServerErrorException
	 */
	private async createOAuthSession(user_uuid: null, domain: string): Promise<string>;
	private async createOAuthSession(
		user_uuid: string,
		domain: string,
		options: OAuthAutorizeOptionsAddAccount | OAuthAutorizeOptionsRefreshAccount,
	): Promise<string>;
	private async createOAuthSession(
		user_uuid: string | null,
		domain: string,
		options?: OAuthAutorizeOptionsAddAccount | OAuthAutorizeOptionsRefreshAccount,
	): Promise<string> {
		const session = await this.OAuthSessionService.create(user_uuid, domain, options);
		if (!session) {
			this.logger.error('createOAuthSession', `Failed to create session.`);
			throw new InternalServerErrorException();
		}

		const token = await this.signOAuthToken(session.token_uuid);
		if (!token) {
			this.logger.error('createOAuthSession', `Failed to create token.`);
			throw new InternalServerErrorException();
		}

		return token;
	}

	/**
	 * @throws BadRequestException
	 * @throws InternalServerErrorException
	 */
	private async generateApplication(domain: string): Promise<ApplicationStore | null>;
	private async generateApplication(
		domain: string,
		update: true,
	): Promise<ApplicationStore | null>;
	private async generateApplication(
		domain: string,
		update: boolean = false,
	): Promise<ApplicationStore | null> {
		const sns = await this.InstanceCacheService.get(domain).then((sns) => {
			if (!sns) {
				this.logger.error('generateApplication', `Unable to determine sns for ${domain}`);
			}

			return sns;
		});
		if (!sns || sns === InstanceType.Friendica || sns === InstanceType.Misskey) {
			throw new BadRequestException(ApiResponseError.InvalidInstance);
		}

		const app_data = await MegalodonGenerator(sns, 'https://' + domain, {
			axios: this.HttpService.axiosRef,
		})
			.registerApp('TootDeck', {
				redirect_uris: this.REDIRECT,
				scopes: ['read', 'write', 'follow'],
			})
			.catch((e) => {
				this.logger.error(
					'generateApplication',
					`Unable to register app for ${domain} ${e}`,
				);
				console.error(e);

				return null;
			});

		const uri = app_data?.url;
		if (!uri) {
			throw new BadRequestException(ApiResponseError.InvalidInstanceResponse);
		}

		this.logger.verbose('generateApplication', `Created app for ${domain}`);

		if (update) {
			// Application removed from instance
			const updated = await this.DBApplicationService.updateCredentials(
				domain,
				app_data.client_id,
				app_data.client_secret,
			);
			if (!updated) {
				this.logger.error('generateApplication', `Failed to update ${domain} secrets`);
				throw new InternalServerErrorException();
			}

			return this.getAppToken(updated).then((r) => {
				if (r === undefined) {
					throw new InternalServerErrorException();
				}

				return r;
			});
		}

		const store = await this.DBApplicationService.create(
			domain,
			sns as InstanceType,
			app_data.client_id,
			app_data.client_secret,
		);
		if (!store) {
			this.logger.error('generateApplication', `Failed to store ${domain} secrets`);
			throw new InternalServerErrorException();
		}

		return this.getAppToken(store).then((r) => {
			if (r === undefined) {
				throw new InternalServerErrorException();
			}

			return r;
		});
	}

	//#endregion

	/**
	 * Service
	 */
	//#region

	async validate(raw_token: string): Promise<OAuthSessionPartial | OAuthSession> {
		const jwt = await this.JwtService.verifyAsync<{ s: string }>(raw_token, {
			algorithms: ['HS512'],
			secret: this.secret,
		}).catch((e) => {
			this.logger.warn('validate', 'Invalid JWT: signature verification failed');
			throw new UnauthorizedException();
		});

		const token_uuid = jwt.s;

		const session = await this.OAuthSessionService.get(token_uuid);
		if (!session) {
			this.logger.verbose('validate', 'Invalid JWT: no session');
			throw new UnauthorizedException();
		}

		if (process.env['NODE_ENV'] !== 'test') {
			const del = await this.OAuthSessionService.delete(token_uuid);
			if (!del) {
				this.logger.verbose('validate', 'Invalid delete');
				throw new InternalServerErrorException();
			}
		}

		return session;
	}

	/**
	 * @throws BadRequestException
	 * @throws InternalServerErrorException
	 * @throws ServiceUnavailableException
	 */
	async authorizeURL(domain: string, create_session: false): Promise<string>;
	async authorizeURL(domain: string, create_session: true): Promise<OAuthAutorize>;
	async authorizeURL(domain: string, create_session: boolean): Promise<OAuthAutorize | string> {
		let application = await this.DBApplicationService.get(domain);
		if (!application) {
			application = await this.generateApplication(domain);
			if (!application) {
				throw new InternalServerErrorException();
			}
		}

		if (
			application.type === InstanceType.Misskey ||
			application.type === InstanceType.Friendica
		) {
			throw new ServiceUnavailableException(ApiResponseError.UnsupportedInstance);
		}

		const verif = await this.verifyApp(application);
		switch (verif) {
			case OAuthAppVerify.Error:
				throw new BadRequestException(ApiResponseError.InvalidInstanceResponse);
			case OAuthAppVerify.Failed:
				application = await this.generateApplication(domain, true);
				if (!application) {
					throw new InternalServerErrorException();
				}
		}

		const redirect_uri = this.redirect(application, domain);
		if (create_session) {
			const session_token = await this.createOAuthSession(null, domain);
			return {
				token: session_token,
				uri: redirect_uri,
			};
		}

		return redirect_uri;
	}

	/**
	 * @throws BadRequestException
	 * @throws InternalServerErrorException
	 * @throws ServiceUnavailableException
	 */
	async authorizeAdd(user_uuid: string, domain: string): Promise<OAuthAutorize> {
		// Add secondary account
		const user = await this.DBUserService.getByUUID(user_uuid);
		if (!user) {
			this.logger.error('authorize', `Unable to find user for ${user_uuid}.`);
			throw new InternalServerErrorException();
		}

		const redirect_uri = await this.authorizeURL(domain, false);
		const token = await this.createOAuthSession(user_uuid, domain, {
			add_account: true,
		});

		return {
			token,
			uri: redirect_uri,
		};
	}

	/**
	 * @throws InternalServerErrorException
	 * @throws BadRequestException
	 * @throws ServiceUnavailableException
	 */
	async authorizeRefesh(
		user_uuid: string,
		username: string,
		domain: string,
	): Promise<OAuthAutorize> {
		const user = await this.DBUserService.getByUUID(user_uuid);
		if (!user) {
			this.logger.error(
				'authorizeRefesh',
				`Unable to get account to refresh token for ${username}@${domain}`,
			);
			throw new InternalServerErrorException();
		}

		const account = findAccountInUser(user, { username, domain });
		if (!account) {
			this.logger.error(
				'authorizeRefesh',
				`Account ${username}@${domain} is not linked to this user`,
			);
			throw new BadRequestException();
		}

		const redirect_uri = await this.authorizeURL(domain, false);
		const token = await this.createOAuthSession(user_uuid, domain, {
			refresh_account: true,
			account_uuid: account.uuid,
		});

		return {
			token,
			uri: redirect_uri,
		};
	}

	/**
	 * @throws InternalServerErrorException
	 * @throws ServiceUnavailableException
	 * @throws BadRequestException
	 */
	async getToken(
		session: OAuthSessionPartial | OAuthSession,
		code: string,
	): Promise<OAuthTokenResponse<OAuthSessionToken | string>> {
		const application = await this.DBApplicationService.get(session.domain);
		if (!application) {
			this.logger.error('getToken', `Unable to find application for ${session.domain}.`);
			throw new InternalServerErrorException();
		}

		const token_response: OAuthTokenResponse<OAuth.TokenData | string> =
			await MegalodonGenerator(application.type, 'https://' + application.domain, {
				axios: this.HttpService.axiosRef,
			})
				.fetchAccessToken(application.key, application.secret, code, this.REDIRECT)
				.then((r) => ({ status: OAuthVerify.Ok, value: r }))
				.catch(async (e: AxiosError) => {
					this.logger.error(`getToken`, '');
					console.error(e);

					throw new BadRequestException(ApiResponseError.InvalidInstanceResponse);
				});
		if (token_response.status !== OAuthVerify.Ok) {
			return token_response as OAuthTokenResponse<string>;
		}

		const token = token_response.value as OAuth.TokenData;

		const updated = await this.DBApplicationService.updateLastUsage(application);
		if (!updated) {
			this.logger.warn('getToken', `Unable to update access time for ${application.domain}`);
		}

		// Get user info
		const entity_user = await MegalodonGenerator(
			application.type,
			'https://' + application.domain,
			{ accessToken: token.access_token, axios: this.HttpService.axiosRef },
		)
			.verifyAccountCredentials()
			.then((r) => (r ? r.data : null))
			.catch((e) => {
				this.logger.error(`getToken`, '');
				console.error(e);
				throw new BadRequestException(ApiResponseError.InvalidInstanceResponse);
			});
		if (!entity_user) {
			this.logger.error('getToken', 'Failed to retrieve token.');
			throw new InternalServerErrorException();
		}

		const account = await this.DBAccountService.get(entity_user.username, application.domain);
		const session_account_uuid = (session as OAuthSession).account_uuid;
		// Wrong account
		if (session_account_uuid && session_account_uuid !== account?.uuid) {
			return { status: OAuthVerify.Failed, value: '' };
		}

		// Update access token for account if empty
		if (account && !account.token) {
			const account_update = await this.DBAccountService.updateToken(
				entity_user.username,
				application.domain,
				token.access_token,
			);
			if (!account_update) {
				this.logger.error(
					'getToken',
					`Failed to set access token for ${account.username}@${account.application.domain}`,
				);
				throw new InternalServerErrorException();
			}

			// Purge all user cache associated with this account
			await this.UserCacheService.purgeAccount(account);
		}

		return {
			status: OAuthVerify.Ok,
			value: {
				token: token.access_token,
				username: entity_user.username,
				domain: application.domain,
			},
		};
	}

	async revokeToken(account: AccountStore): Promise<boolean> {
		const application = await this.DBApplicationService.get(account.application.domain);
		if (!application) {
			return false;
		}

		const revoked = await MegalodonGenerator(
			application.type,
			'https://' + application.domain,
			{
				accessToken: account.token,
				handle: account.username + '@' + account.application.domain,
				axios: this.HttpService.axiosRef,
			},
		)
			.revokeToken(application.key, application.secret, account.token)
			.catch((e: Error) => {
				this.logger.error(
					`revokeToken`,
					`Unable to revoke token for ${account.username}@${account.application.domain}`,
				);
				console.error(e);
				return null;
			});
		if (!revoked) {
			return false;
		}

		return revoked.status === 200;
	}
	//#endregion
}
