import { Test, TestingModule } from '@nestjs/testing';

import { BrowserFingerprintService } from './fingerprint.service';

describe('BrowserFingerprintService', () => {
	let service: BrowserFingerprintService;

	const test_user_agent =
		'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36';
	const test_ip = '192.168.1.1';

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule({
			providers: [BrowserFingerprintService],
		}).compile();

		service = app.get<BrowserFingerprintService>(BrowserFingerprintService);
	});

	describe('get', () => {
		it('generating valid entry, should return entry', () => {
			expect(service.get(test_user_agent, test_ip)).toMatchObject({
				browser: {
					name: 'Chrome',
					version: 112,
				},
				os: { name: 'Linux', version: undefined },
				device: { vendor: undefined, model: undefined, type: undefined },
				cpu: { architecture: 'amd64' },
				ip: test_ip,
			});
		});

		it('generating invalid entry, should throw', () => {
			let has_throw = false;
			try {
				service.get('', test_ip);
			} catch (e) {
				has_throw = true;
			}

			expect(has_throw).toBe(true);
		});

		it('generating invalid entry, should throw', () => {
			let has_throw = false;
			try {
				service.get(test_user_agent, '');
			} catch (e) {
				has_throw = true;
			}

			expect(has_throw).toBe(true);
		});
	});
});
