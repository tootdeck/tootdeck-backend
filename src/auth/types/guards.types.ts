import { FastifyRequest } from 'fastify';

import { Fingerprint } from './fingerprint.types';
import { UserDeleteSession, JWESession, OAuthSession, OAuthSessionPartial } from './sessions.types';

export interface RequestAccessGuard extends FastifyRequest {
	access_session: JWESession;
}

export interface RequestRefreshGuard extends FastifyRequest {
	refresh_session: JWESession;
	fingerprint: Fingerprint;
}

export interface RequestOAuthGuard extends FastifyRequest {
	oauth_session: OAuthSessionPartial | OAuthSession;
	fingerprint: Fingerprint;
}

export interface RequestAccountDeleteGuard extends RequestAccessGuard {
	delete_session: UserDeleteSession;
}
