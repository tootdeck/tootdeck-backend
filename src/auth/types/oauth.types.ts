export interface OAuthSessionToken {
	token: string;
	username: string;
	domain: string;
}

export interface OAuthAutorize {
	token: string;
	uri: string;
}

export interface OAuthAutorizeOptions {
	account_uuid?: string;
	add_account?: boolean;
	refresh_account?: boolean;
}

export interface OAuthAutorizeOptionsAddAccount extends OAuthAutorizeOptions {
	add_account: boolean;
}

export interface OAuthAutorizeOptionsRefreshAccount extends OAuthAutorizeOptions {
	account_uuid: string;
	refresh_account: boolean;
}

export enum OAuthAppVerify {
	Error = -2,
	NotFound = -1,
	Failed = 0,
	Success = 1,
}

export enum OAuthVerify {
	InvalidApp = -2,
	Unsupported = -1,
	Failed = 0,
	Ok = 1,
}

export interface OAuthTokenResponse<T> {
	status: OAuthVerify;
	value: T;
}
