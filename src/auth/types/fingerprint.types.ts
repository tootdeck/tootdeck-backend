export interface Fingerprint {
	browser: {
		name: string | undefined;
		version: number;
	};
	os: { name: string | undefined; version: string | undefined };
	device: { vendor: string | undefined; model: string | undefined; type: string | undefined };
	cpu: { architecture: string | undefined };
	ip: string | undefined;
}
