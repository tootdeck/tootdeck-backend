export enum JWEType {
	Access = 0,
	Refresh = 1,
	OAuth = 2,
	AccountDelete = 3,
}

export type CreatedJWE = {
	access: { token_uuid: string; token: string };
	refresh: { token_uuid: string; token: string };
};
