import { OAuthAutorizeOptions } from './oauth.types';

export interface OAuthSessionPartial {
	token_uuid: string;
	domain: string;
}

export interface OAuthSession extends OAuthSessionPartial {
	token_uuid: string;
	user_uuid: string;
	account_uuid: string | undefined;
	options: OAuthAutorizeOptions;
}

export interface JWESession {
	token_uuid: string;
	user_uuid: string;
	token: string;
	linked_to_token_uuid: string;
}

export interface OpenSession {
	token_uuid: string;
	browser: {
		name: string;
		version: number;
	};
	os: string;
	created_at: Date;
	updated_at: Date;
}

export type OpenSessions = OpenSession[];

export interface UserDeleteSession {
	token_uuid: string;
	user_uuid: string;
	code: string;
	token: string;
}
