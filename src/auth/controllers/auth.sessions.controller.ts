import {
	BadRequestException,
	Controller,
	Delete,
	Get,
	HttpCode,
	InternalServerErrorException,
	Param,
	Request,
	UseGuards,
} from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { isUUID } from 'class-validator';

import { OpenSessionService } from '../services/sessions/open.session.service';

import { AccessGuard } from '../guards/access.guard';

import { OpenSessionResponse } from '../properties/auth.sessions.get.property';

import { RequestAccessGuard } from '../types/guards.types';

@ApiTags('Auth · Sessions')
@Controller('auth/sessions')
export class AuthSessionsController {
	constructor(private readonly OpenSessionService: OpenSessionService) {}

	@UseGuards(AccessGuard)
	@ApiResponse({ status: 200, type: OpenSessionResponse, isArray: true })
	@ApiResponse({ status: 401 })
	@ApiResponse({ status: 500 })
	@HttpCode(200)
	@Get()
	async get(@Request() request: RequestAccessGuard) {
		const user_uuid = request.access_session.user_uuid;
		const token_uuid = request.access_session.linked_to_token_uuid;

		const open_session = (await this.OpenSessionService.get(user_uuid)) || [];

		return open_session
			.map((x) => {
				const { token_uuid: _, uuid, ...filtered } = x;

				const revoked = !x.token_uuid;

				if (x.token_uuid === token_uuid) {
					return { current: true, revoked, uuid, ...filtered };
				}
				return { current: false, revoked, uuid, ...filtered };
			})
			.sort((a, b) => {
				// Sort by state
				if (a.current && !b.current) {
					return -1;
				}
				if (!a.current && b.current) {
					return 1;
				}

				// Sort by date
				if (a.created_at > b.created_at) {
					return -1;
				}
				if (a.created_at < b.created_at) {
					return 1;
				}

				return 0;
			});
	}

	@UseGuards(AccessGuard)
	@ApiResponse({ status: 200 })
	@ApiResponse({ status: 400 })
	@ApiResponse({ status: 401 })
	@HttpCode(200)
	@Delete('revoke/:session_uuid')
	async revoke(
		@Request() request: RequestAccessGuard,
		@Param('session_uuid') session_uuid: string,
	) {
		if (!session_uuid || !isUUID(session_uuid)) {
			throw new BadRequestException();
		}

		const user_uuid = request.access_session.user_uuid;

		await this.OpenSessionService.revokeTokenBySessionUUID(user_uuid, session_uuid).then(
			(r) => {
				if (r === null) {
					throw new InternalServerErrorException();
				}

				if (r === false) {
					throw new BadRequestException();
				}
			},
		);
	}

	@UseGuards(AccessGuard)
	@ApiResponse({ status: 200 })
	@ApiResponse({ status: 400 })
	@ApiResponse({ status: 401 })
	@HttpCode(200)
	@Delete('delete/:session_uuid')
	async delete(
		@Request() request: RequestAccessGuard,
		@Param('session_uuid') session_uuid: string,
	) {
		if (!session_uuid || !isUUID(session_uuid)) {
			throw new BadRequestException();
		}

		const user_uuid = request.access_session.user_uuid;

		await this.OpenSessionService.delete(user_uuid, session_uuid).then((r) => {
			if (r === null) {
				throw new InternalServerErrorException();
			}

			if (r === false) {
				throw new BadRequestException();
			}
		});
	}
}
