import {
	BadRequestException,
	Controller,
	Get,
	HttpCode,
	Query,
	Request,
	Response,
	UseGuards,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { FastifyReply } from 'fastify';

import { OAuthService } from '../services/oauth.service';
import { CookieService } from '../services/utils/cookie.service';
import { UserService } from '../../user/services/user.service';
import { JWEService } from '../services/jwe.service';

import { Logger } from '../../utils/logger';
import { parseHandle } from '../../utils/parseHandle';
import { isURL } from '../../utils/isURL';

import { AccessGuard } from '../guards/access.guard';
import { OAuthGuard } from '../guards/oauth.guard';

import { ApiResponseError } from '../types/api.types';
import { JWEType } from '../types/jwe.types';
import { RequestAccessGuard, RequestOAuthGuard } from '../types/guards.types';
import { OAuthSessionToken, OAuthVerify } from '../types/oauth.types';
import { OAuthSession } from '../types/sessions.types';

@ApiTags('Auth · OAuth')
@Controller('oauth')
export class OAuthController {
	private readonly logger = new Logger(OAuthController.name);
	private readonly DOMAIN: string;

	constructor(
		private readonly configService: ConfigService,
		private readonly OAuthService: OAuthService,
		private readonly UserService: UserService,
		private readonly JWEService: JWEService,
		private readonly CookieService: CookieService,
	) {
		this.DOMAIN = this.configService.get<string>('DOMAIN') ?? '';
	}

	private getDomain(url: string) {
		return new URL(url.includes('http') ? url : 'http://' + url).hostname;
	}

	@ApiResponse({ status: 200, description: 'Redirect uri', type: String })
	@ApiResponse({ status: 400.1, description: ApiResponseError.InvalidDomain })
	@ApiResponse({ status: 400.2, description: ApiResponseError.InvalidInstance })
	@ApiResponse({ status: 400.3, description: ApiResponseError.InvalidInstanceResponse })
	@ApiResponse({ status: 500, description: ApiResponseError.GenerateRedirectFail })
	@ApiResponse({ status: 503, description: ApiResponseError.UnsupportedInstance })
	@HttpCode(200)
	@Get('authorize')
	async authorize(
		@Response({ passthrough: true }) response: FastifyReply,
		@Query('domain') domain: string,
	): Promise<string> {
		if (!isURL(domain, process.env['NODE_ENV'] !== 'test')) {
			throw new BadRequestException(ApiResponseError.InvalidDomain);
		}
		const authorize = await this.OAuthService.authorizeURL(this.getDomain(domain), true);

		this.CookieService.create(response, JWEType.OAuth, authorize.token);

		return authorize.uri;
	}

	@UseGuards(AccessGuard)
	@ApiResponse({ status: 200, description: 'Redirect uri', type: String })
	@ApiResponse({ status: 400.1, description: ApiResponseError.InvalidDomain })
	@ApiResponse({ status: 400.2, description: ApiResponseError.InvalidInstance })
	@ApiResponse({ status: 400.3, description: ApiResponseError.InvalidInstanceResponse })
	@ApiResponse({ status: 401 })
	@ApiResponse({ status: 500, description: ApiResponseError.GenerateRedirectFail })
	@ApiResponse({ status: 503, description: ApiResponseError.UnsupportedInstance })
	@HttpCode(200)
	@Get('authorize/add')
	async authorizeAdd(
		@Request() request: RequestAccessGuard,
		@Response({ passthrough: true }) response: FastifyReply,
		@Query('domain') domain: string,
	): Promise<string> {
		if (!isURL(domain, process.env['NODE_ENV'] === 'test')) {
			throw new BadRequestException(ApiResponseError.InvalidDomain);
		}

		const authorize = await this.OAuthService.authorizeAdd(
			request.access_session.user_uuid,
			this.getDomain(domain),
		);

		this.CookieService.create(response, JWEType.OAuth, authorize.token);

		return authorize.uri;
	}

	@UseGuards(AccessGuard)
	@ApiResponse({ status: 200, description: 'Redirect uri', type: String })
	@ApiResponse({ status: 400.1, description: ApiResponseError.InvalidDomain })
	@ApiResponse({ status: 400.2, description: ApiResponseError.InvalidInstance })
	@ApiResponse({ status: 400.3, description: ApiResponseError.InvalidInstanceResponse })
	@ApiResponse({ status: 401 })
	@ApiResponse({ status: 500, description: ApiResponseError.GenerateRedirectFail })
	@ApiResponse({ status: 503, description: ApiResponseError.UnsupportedInstance })
	@HttpCode(200)
	@Get('authorize/refresh')
	async authorizeRefresh(
		@Request() request: RequestAccessGuard,
		@Response({ passthrough: true }) response: FastifyReply,
		@Query('handle') handle: string,
	): Promise<string> {
		const { username, domain } = parseHandle(handle);

		const authorize = await this.OAuthService.authorizeRefesh(
			request.access_session.user_uuid,
			username,
			domain,
		);

		this.CookieService.create(response, JWEType.OAuth, authorize.token);

		return authorize.uri;
	}

	@UseGuards(OAuthGuard)
	@ApiResponse({ status: 302, description: 'Authenticated' })
	@ApiResponse({ status: 400 })
	@ApiResponse({ status: 500 })
	@HttpCode(302)
	@Get('authorize/redirect')
	async redirect(
		@Request() request: RequestOAuthGuard,
		@Response({ passthrough: true }) response: FastifyReply,
		@Query('code') code: string,
		@Query('error') error: string,
	) {
		if (error) {
			return response
				.status(302)
				.redirect(
					`https://${this.DOMAIN}/?message_type=0&message="Authorization hasn't been granted."`,
				);
		}

		const session = request.oauth_session;
		const fingerprint = request.fingerprint;

		if (!code) {
			this.logger.warn('redirect', 'No code');
			throw new BadRequestException();
		}

		// Get token
		const token_response = await this.OAuthService.getToken(session, code);
		switch (token_response.status) {
			// Misskey or other unsupported instance
			case OAuthVerify.Unsupported:
				return response
					.status(302)
					.redirect(
						`https://${this.DOMAIN}/?message_type=0&message="Unsupported instance type."`,
					);
			// Wrong account
			case OAuthVerify.Failed:
				return response
					.status(302)
					.redirect(
						`https://${this.DOMAIN}/?message_type=0&message="You just logged with a different account than the one you want to refresh on ${session.domain}."`,
					);
			case OAuthVerify.InvalidApp:
				return response.status(302).redirect(token_response.value as string);
		}

		const token = token_response.value as OAuthSessionToken;

		// Remove oauth token
		this.CookieService.delete(response, JWEType.OAuth);

		const full_session = session as OAuthSession;
		if (full_session.options) {
			// Refresh token for account
			if (full_session.options.refresh_account) {
				await this.UserService.refresh(token, full_session.account_uuid!);
			}
			// Add account to user
			else if (full_session.options.add_account) {
				await this.UserService.addAccount(token, full_session.user_uuid);
			}

			return response.status(302).redirect(`https://${this.DOMAIN}/`);
		}

		// Create user if not existing and then log user
		const user_uuid = await this.UserService.login(token);

		// Get tokens
		const { access, refresh } = await this.JWEService.create(user_uuid, fingerprint, false);

		// Set tokens
		this.CookieService.create(response, JWEType.Access, access.token);
		this.CookieService.create(response, JWEType.Refresh, refresh.token);

		return response.status(302).redirect(`https://${this.DOMAIN}/`);
	}
}
