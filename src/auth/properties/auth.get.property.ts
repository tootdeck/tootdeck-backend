import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

class AccountResponse {
	@ApiProperty()
	username: string;

	@ApiProperty()
	domain: string;

	@ApiProperty()
	type: string;

	@ApiProperty()
	valid: boolean;
}

export class AuthGetResponse {
	@ApiPropertyOptional()
	admin: boolean;

	@ApiProperty({ type: AccountResponse })
	main: AccountResponse;

	@ApiProperty({ type: AccountResponse, isArray: true })
	secondary: AccountResponse[];
}
