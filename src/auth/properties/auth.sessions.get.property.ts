import { ApiProperty } from '@nestjs/swagger';

class OpenSessionBrowser {
	@ApiProperty()
	name: string;

	@ApiProperty()
	version: number;
}

export class OpenSessionResponse {
	@ApiProperty()
	uuid: string;

	@ApiProperty()
	current: boolean;

	@ApiProperty()
	revoked: boolean;

	@ApiProperty({ type: OpenSessionBrowser })
	browser: OpenSessionBrowser;

	@ApiProperty()
	os: string;

	@ApiProperty()
	created_at: Date;

	@ApiProperty()
	updated_at: Date;
}
