import axios, { AxiosResponse, AxiosRequestConfig, Axios } from 'axios';
const objectAssignDeep = require('object-assign-deep');

import Response from './response';
import { RequestCanceledError } from './cancel';
import { NO_REDIRECT, DEFAULT_SCOPE, DEFAULT_UA } from './default';

export interface OptionalAxiosClient {
	accessToken?: string;
	axios?: Axios;
	handle?: string;
	userAgent?: string;
	cache?: string;
}

export class AxiosClient {
	static DEFAULT_SCOPE = DEFAULT_SCOPE;
	static NO_REDIRECT = NO_REDIRECT;

	public accessToken: string | null = null;
	public userAgent: string = DEFAULT_UA;
	public handle: string | null = null;
	public cache: string | null = null;

	private readonly axios: Axios = axios;
	private readonly abortController: AbortController = new AbortController();

	/**
	 * @param baseUrl hostname or base URL
	 * @param accessToken access token from OAuth2 authorization
	 * @param userAgent UserAgent is specified in header on request.
	 * @param proxyConfig Proxy setting, or set false if don't use proxy.
	 */
	constructor(
		private readonly baseUrl: string,
		optional?: OptionalAxiosClient,
	) {
		if (optional?.accessToken) {
			this.accessToken = optional.accessToken;
		}

		if (optional?.axios) {
			this.axios = optional.axios;
		}

		if (optional?.userAgent) {
			this.userAgent = optional.userAgent;
		}

		if (optional?.userAgent) {
			this.userAgent = optional.userAgent;
		}

		if (optional?.handle) {
			this.handle = optional.handle;
		}

		if (optional?.cache) {
			this.cache = optional.cache;
		}

		axios.defaults.signal = this.abortController.signal;
	}

	private appendHeaders(options: AxiosRequestConfig) {
		if (this.accessToken) {
			options = objectAssignDeep({}, options, {
				headers: {
					Authorization: `Bearer ${this.accessToken}`,
				},
			});
		}

		if (this.cache) {
			options = objectAssignDeep({}, options, {
				headers: {
					['cache-control']: this.cache,
					['pragma']: this.cache,
					['expires']: 0,
				},
			});
		}

		if (this.handle) {
			options = objectAssignDeep({}, options, {
				addPrefix: this.handle,
			});
		}

		return options;
	}

	private onResponse<T>(resp: AxiosResponse<T>): any {
		if (!resp.status) {
			throw resp;
		}

		const res: Response<T> = {
			data: resp.data,
			status: resp.status,
			statusText: resp.statusText,
			headers: resp.headers,
		};
		return res;
	}

	private onError(err: Error): any {
		if (axios.isCancel(err)) {
			throw new RequestCanceledError(err.message);
		} else {
			throw err;
		}
	}

	/**
	 * Cancel all requests in this instance.
	 * @returns void
	 */
	public cancel(): void {
		return this.abortController.abort();
	}

	/**
	 * GET request to mastodon REST API.
	 * @param path relative path from baseUrl
	 * @param params Query parameters
	 * @param headers Request header object
	 */
	public async get<T>(
		path: string,
		params = {},
		headers: { [key: string]: string } = {},
		pathIsFullyQualified: boolean = false,
	): Promise<Response<T>> {
		let options: AxiosRequestConfig = {
			params: params,
			headers: headers,
		};

		options = this.appendHeaders(options);

		return this.axios
			.get<T>((pathIsFullyQualified ? '' : this.baseUrl) + path, options)
			.then(this.onResponse.bind(this))
			.catch(this.onError.bind(this));
	}

	/**
	 * PUT request to mastodon REST API.
	 * @param path relative path from baseUrl
	 * @param params Form data. If you want to post file, please use FormData()
	 * @param headers Request header object
	 */
	public async put<T>(
		path: string,
		params = {},
		headers: { [key: string]: string } = {},
	): Promise<Response<T>> {
		let options: AxiosRequestConfig = {
			headers: headers,
			maxContentLength: Infinity,
			maxBodyLength: Infinity,
		};

		options = this.appendHeaders(options);

		return this.axios
			.put<T>(this.baseUrl + path, params, options)
			.then(this.onResponse.bind(this))
			.catch(this.onError.bind(this));
	}

	/**
	 * PUT request to mastodon REST API for multipart.
	 * @param path relative path from baseUrl
	 * @param params Form data. If you want to post file, please use FormData()
	 * @param headers Request header object
	 */
	public async putForm<T>(
		path: string,
		params = {},
		headers: { [key: string]: string } = {},
	): Promise<Response<T>> {
		let options: AxiosRequestConfig = {
			headers: headers,
			maxContentLength: Infinity,
			maxBodyLength: Infinity,
		};

		options = this.appendHeaders(options);

		return this.axios
			.putForm<T>(this.baseUrl + path, params, options)
			.then(this.onResponse.bind(this))
			.catch(this.onError.bind(this));
	}

	/**
	 * PATCH request to mastodon REST API.
	 * @param path relative path from baseUrl
	 * @param params Form data. If you want to post file, please use FormData()
	 * @param headers Request header object
	 */
	public async patch<T>(
		path: string,
		params = {},
		headers: { [key: string]: string } = {},
	): Promise<Response<T>> {
		let options: AxiosRequestConfig = {
			headers: headers,
			maxContentLength: Infinity,
			maxBodyLength: Infinity,
		};

		options = this.appendHeaders(options);

		return this.axios
			.patch<T>(this.baseUrl + path, params, options)
			.then(this.onResponse.bind(this))
			.catch(this.onError.bind(this));
	}

	/**
	 * PATCH request to mastodon REST API for multipart.
	 * @param path relative path from baseUrl
	 * @param params Form data. If you want to post file, please use FormData()
	 * @param headers Request header object
	 */
	public async patchForm<T>(
		path: string,
		params = {},
		headers: { [key: string]: string } = {},
	): Promise<Response<T>> {
		let options: AxiosRequestConfig = {
			headers: headers,
			maxContentLength: Infinity,
			maxBodyLength: Infinity,
		};

		options = this.appendHeaders(options);

		return this.axios
			.patchForm<T>(this.baseUrl + path, params, options)
			.then(this.onResponse.bind(this))
			.catch(this.onError.bind(this));
	}

	/**
	 * POST request to mastodon REST API.
	 * @param path relative path from baseUrl
	 * @param params Form data
	 * @param headers Request header object
	 */
	public async post<T>(
		path: string,
		params = {},
		headers: { [key: string]: string } = {},
	): Promise<Response<T>> {
		let options: AxiosRequestConfig = {
			headers: headers,
			maxContentLength: Infinity,
			maxBodyLength: Infinity,
		};

		options = this.appendHeaders(options);

		return this.axios
			.post<T>(this.baseUrl + path, params, options)
			.then(this.onResponse.bind(this))
			.catch(this.onError.bind(this));
	}

	/**
	 * POST request to mastodon REST API for multipart.
	 * @param path relative path from baseUrl
	 * @param params Form data
	 * @param headers Request header object
	 */
	public async postForm<T>(
		path: string,
		params = {},
		headers: { [key: string]: string } = {},
	): Promise<Response<T>> {
		let options: AxiosRequestConfig = {
			headers: headers,
			maxContentLength: Infinity,
			maxBodyLength: Infinity,
		};

		options = this.appendHeaders(options);

		return this.axios
			.postForm<T>(this.baseUrl + path, params, options)
			.then(this.onResponse.bind(this))
			.catch(this.onError.bind(this));
	}

	/**
	 * DELETE request to mastodon REST API.
	 * @param path relative path from baseUrl
	 * @param params Form data
	 * @param headers Request header object
	 */
	public async del<T>(
		path: string,
		params = {},
		headers: { [key: string]: string } = {},
	): Promise<Response<T>> {
		let options: AxiosRequestConfig = {
			data: params,
			headers: headers,
			maxContentLength: Infinity,
			maxBodyLength: Infinity,
		};

		options = this.appendHeaders(options);

		return this.axios
			.delete(this.baseUrl + path, options)
			.then(this.onResponse.bind(this))
			.catch(this.onError.bind(this));
	}
}
