import { ConsoleLogger } from '@nestjs/common';
import { ClientOptions, Data, WebSocket as WS } from 'ws';
import { EventEmitter } from 'events';
import { Dayjs } from 'dayjs';
const dayjs = require('dayjs');

import { Entity } from './entities';

export interface Optional {
	list: string;
	tag: string;
}

export interface WebsocketCallbacks {
	// New status
	onUpdate?: (data: { stream: Array<string>; content: Entity.Status } & Optional) => void;
	// New notification
	onNotification?: (
		data: {
			stream: Array<string>;
			content: Entity.Notification;
		} & Optional,
	) => void;
	// Deleted status
	onDelete?: (id: string) => void;
	// New message in conversation
	onConversation?: (
		data: {
			stream: Array<string>;
			content: Entity.Conversation;
		} & Optional,
	) => void;
	// Edited status
	onStatusUpdate?: (
		data: {
			stream: Array<string>;
			content: Entity.Status;
		} & Optional,
	) => void;
	onError?: (error: Error) => void;
	onHeartbeat?: () => void;
}

export abstract class WebSocketInterface extends EventEmitter {
	public stream: string;
	public params: string | null;
	public headers: { [key: string]: string };
	protected _reconnectInterval: number = 3000;
	protected _reconnectMaxAttempts: number = Infinity;
	protected _reconnectCurrentAttempts: number = 0;
	protected _connectionClosed: boolean = false;
	protected _client: WS | null = null;
	protected _pongReceivedTimestamp: Dayjs = dayjs();
	protected _heartbeatInterval: number = 40000;
	protected _pongWaiting: boolean = false;
	protected callbacks: WebsocketCallbacks;

	/**
	 * @param url Full url of websocket: e.g. https://pleroma.io/api/v1/streaming
	 * @param stream Stream name, please refer: https://git.pleroma.social/pleroma/pleroma/blob/develop/lib/pleroma/web/mastodon_api/mastodon_socket.ex#L19-28
	 * @param accessToken The access token.
	 * @param userAgent The specified User Agent.
	 */
	constructor(
		public url: string,
		protected readonly _accessToken: string,
		protected readonly logger: ConsoleLogger,
		userAgent?: string,
	) {
		super();
		if (userAgent) {
			this.headers = {
				'User-Agent': userAgent,
			};
		}
	}

	/**
	 * Start websocket connection.
	 */
	public start() {
		this._connectionClosed = false;
		this._resetRetryParams();
		this._startWebSocketConnection();
	}

	/**
	 * Reset connection and start new websocket connection.
	 */
	private _startWebSocketConnection() {
		this._resetConnection();
		this._client = this._connect(this.url, this.params, this._accessToken, this.headers);
		if (!this._client) {
			setTimeout(
				() =>
					(this._client = this._connect(
						this.url,
						this.params,
						this._accessToken,
						this.headers,
					)),
				this._reconnectInterval,
			);
			return;
		}
		this._bindSocket(this._client);
	}

	/**
	 * Stop current connection.
	 */
	public stop() {
		this._connectionClosed = true;
		this._resetConnection();
		this._resetRetryParams();
	}

	/**
	 * Clean up current connection, and listeners.
	 */
	private _resetConnection() {
		if (this._client) {
			this._client.close(1000);
			this._client.removeAllListeners();
			this._client = null;
		}
	}

	/**
	 * Resets the parameters used in reconnect.
	 */
	private _resetRetryParams() {
		this._reconnectCurrentAttempts = 0;
	}

	/**
	 * Reconnects to the same endpoint.
	 */
	private _reconnect() {
		setTimeout(() => {
			// Skip reconnect when client is closed or when it's connecting.
			// https://github.com/websockets/ws/blob/7.2.1/lib/websocket.js#L365
			if (!this._client || (this._client && this._client.readyState === WS.CONNECTING)) {
				return;
			}

			if (this._reconnectCurrentAttempts < this._reconnectMaxAttempts) {
				this._reconnectCurrentAttempts++;
				this._clearBinding();
				if (this._client) {
					// In reconnect, we want to close the connection immediately,
					// because recoonect is necessary when some problems occur.
					this._client.terminate();
				}
				// Call connect methods
				this.logger.log('_reconnect', 'Reconnecting');
				this._client = this._connect(
					this.url,
					this.params,
					this._accessToken,
					this.headers,
				);
				this._bindSocket(this._client!);
			}
		}, this._reconnectInterval);
	}

	/**
	 * @param url Base url of streaming endpoint.
	 * @param stream The specified stream name.
	 * @param accessToken Access token.
	 * @param headers The specified headers.
	 * @return A WebSocket instance.
	 */
	private _connect(
		url: string,
		params: string | null,
		accessToken: string,
		headers: { [key: string]: string },
	): WS | null {
		const parameter: Array<string> = [];

		if (params) {
			parameter.push(params);
		}

		if (accessToken !== null) {
			parameter.push(`access_token=${accessToken}`);
		}
		const requestURL: string = `${url}/?${parameter.join('&')}`;
		let options: ClientOptions = {
			headers: headers,
		};

		try {
			return new WS(requestURL, options);
		} catch (e) {
			return null;
		}
	}

	/**
	 * Clear binding event for web socket client.
	 */
	private _clearBinding() {
		if (this._client) {
			this._client.removeAllListeners('close');
			this._client.removeAllListeners('pong');
			this._client.removeAllListeners('open');
			this._client.removeAllListeners('message');
			this._client.removeAllListeners('error');
		}
	}

	/**
	 * Bind event for web socket client.
	 * @param client A WebSocket instance.
	 */
	private _bindSocket(client: WS) {
		client.on('close', (code: number, _reason: Buffer) => {
			// Refer the code: https://tools.ietf.org/html/rfc6455#section-7.4
			if (code === 1000) {
				this.emit('close', {});
			} else {
				this.logger.log('_bindSocket', `Closed connection with ${code}`);
				// If already called close method, it does not retry.
				if (!this._connectionClosed) {
					this._reconnect();
				}
			}
		});

		client.on('pong', () => {
			this._pongWaiting = false;
			this.emit('pong', {});
			this._pongReceivedTimestamp = dayjs();

			// It is required to anonymous function since get this scope in checkAlive.
			setTimeout(
				() => this._checkAlive(this._pongReceivedTimestamp),
				this._heartbeatInterval,
			);
		});

		client.on('open', () => {
			this.logger.log('_bindSocket', 'Connected');

			this.emit('connect', {});

			// Call first ping event.
			setTimeout(() => {
				client.ping('');
			}, 10000);
		});

		client.on('message', (data: Data, isBinary: boolean) => {
			this.parse(data, isBinary);
		});

		client.on('error', (err: Error) => {
			this.emit('error', err);
		});
	}

	/**
	 * Call ping and wait to pong.
	 */
	private _checkAlive(timestamp: Dayjs) {
		const now: Dayjs = dayjs();
		// Block multiple calling, if multiple pong event occur.
		// It the duration is less than interval, through ping.
		if (now.diff(timestamp) > this._heartbeatInterval - 1000 && !this._connectionClosed) {
			// Skip ping when client is connecting.
			// https://github.com/websockets/ws/blob/7.2.1/lib/websocket.js#L289
			if (this._client && this._client.readyState !== WS.CONNECTING) {
				this._pongWaiting = true;
				this._client.ping('');

				setTimeout(() => {
					if (this._pongWaiting) {
						this._pongWaiting = false;
						this._reconnect();
					}
				}, 10000);
			}
		}
	}

	send(data: string) {
		if (this._client?.readyState === this._client?.OPEN) {
			this._client?.send(data);
		}
	}

	setCallbacks(callbacks: WebsocketCallbacks) {
		this.callbacks = callbacks;
	}

	/**
	 * Set up parser when receive message.
	 */
	protected abstract parse(data: Data, isBinary: boolean): void;
}
