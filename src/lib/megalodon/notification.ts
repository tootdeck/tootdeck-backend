export class UnknownNotificationTypeError extends Error {
	constructor() {
		super();
		Object.setPrototypeOf(this, UnknownNotificationTypeError.prototype);
	}
}
