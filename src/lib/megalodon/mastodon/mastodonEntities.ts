export namespace MastodonEntity {
	export interface Account {
		acct: string;
		avatar_static: string;
		avatar: string;
		bot: boolean;
		created_at: string;
		discoverable?: boolean | undefined;
		display_name: string;
		emojis: Array<Emoji>;
		fields: Array<Field>;
		followers_count: number;
		following_count: number;
		group: boolean | null;
		header_static: string;
		header: string;
		id: string;
		limited: boolean | null;
		locked: boolean;
		moved: Account | null;
		mute_expires_at?: string | undefined;
		noindex: boolean | null;
		note: string;
		role?: Role | undefined;
		source?: Source | undefined;
		statuses_count: number;
		suspended: boolean | null;
		url: string;
		username: string;
	}

	export interface Activity {
		logins: string;
		registrations: string;
		statuses: string;
		week: string;
	}

	export interface Announcement {
		all_day: boolean;
		content: string;
		emojis: Array<Emoji>;
		ends_at: string | null;
		id: string;
		mentions: Array<AnnouncementAccount>;
		published_at: string;
		published: boolean;
		reactions: Array<AnnouncementReaction>;
		read: boolean | null;
		starts_at: string | null;
		statuses: Array<AnnouncementStatus>;
		tags: Array<StatusTag>;
		updated_at: string;
	}

	export interface AnnouncementAccount {
		acct: string;
		id: string;
		url: string;
		username: string;
	}

	export interface AnnouncementStatus {
		id: string;
		url: string;
	}

	export interface AnnouncementReaction {
		count: number;
		me: boolean | null;
		name: string;
		static_url: string | null;
		url: string | null;
	}

	export interface Application {
		name: string;
		vapid_key: string | null;
		website: string | null;
	}

	export interface Sub {
		// For Image, Gifv, and Video
		aspect?: number | undefined;
		height?: number | undefined;
		size?: string | undefined;
		width?: number | undefined;

		// For Gifv and Video
		frame_rate?: string | undefined;

		// For Audio, Gifv, and Video
		bitrate?: number | undefined;
		duration?: number | undefined;
	}

	export interface Focus {
		x: number;
		y: number;
	}

	export interface Meta {
		aspect?: number | undefined;
		audio_bitrate?: string | undefined;
		audio_channel?: string | undefined;
		audio_encode?: string | undefined;
		duration?: number | undefined;
		focus?: Focus | undefined;
		fps?: number | undefined;
		height?: number | undefined;
		length?: string | undefined;
		original?: Sub | undefined;
		size?: string | undefined;
		small?: Sub | undefined;
		width?: number | undefined;
	}

	export interface Attachment {
		blurhash: string | null;
		description: string | null;
		id: string;
		meta: Meta | null;
		preview_url: string | null;
		remote_url: string | null;
		text_url: string | null;
		type: AttachmentType;
		url: string;
	}

	export interface AsyncAttachment {
		blurhash: string | null;
		description: string | null;
		id: string;
		meta: Meta | null;
		preview_url: string;
		remote_url: string | null;
		text_url: string | null;
		type: AttachmentType;
		url: string | null;
	}

	export enum AttachmentType {
		Unknown = 'unknown',
		Image = 'image',
		Gifv = 'gifv',
		Video = 'video',
		Audio = 'audio',
	}

	export interface AttachmentAttributes {
		description: string;
		focus: string;
		id: string;
	}

	export interface Card {
		author_name: string;
		author_url: string;
		blurhash: string | null;
		description: string;
		embed_url: string;
		height: number;
		html: string;
		image: string | null;
		provider_name: string;
		provider_url: string;
		title: string;
		type: CardType;
		url: string;
		width: number;
	}

	export enum CardType {
		Link = 'link',
		Photo = 'photo',
		Video = 'video',
		Rich = 'rich',
	}

	export interface Context {
		ancestors: Array<Status>;
		descendants: Array<Status>;
	}

	export interface Conversation {
		accounts: Array<Account>;
		id: string;
		last_status: Status | null;
		unread: boolean;
	}

	export interface Emoji {
		category?: string | undefined;
		shortcode: string;
		static_url: string;
		url: string;
		visible_in_picker: boolean;
	}

	export interface FeaturedTag {
		id: string;
		last_status_at: string;
		name: string;
		statuses_count: number;
	}

	export interface Field {
		name: string;
		value: string;
		verified_at: string | null;
	}

	export interface Filter {
		context: Array<string>;
		expires_at: string | null;
		id: string;
		irreversible: boolean;
		phrase: string;
		whole_word: boolean;
	}

	export interface History {
		accounts: number;
		day: string;
		uses: number;
	}

	export interface IdentityProof {
		profile_url: string;
		proof_url: string;
		provider_username: string;
		provider: string;
		updated_at: string;
	}

	export interface Instance {
		approval_required: boolean;
		configuration: {
			media_attachments: {
				supported_mime_types: Array<string>;
				image_size_limit: number;
				image_matrix_limit: number;
				video_size_limit: number;
				video_frame_limit: number;
				video_matrix_limit: number;
			};
			polls: {
				allow_media?: boolean | undefined;
				max_options: number;
				max_characters_per_option: number;
				min_expiration: number;
				max_expiration: number;
			};
			statuses: {
				max_characters: number;
				max_media_attachments: number;
				characters_reserved_per_url: number;
			};
		};
		contact_account: Account;
		description: string;
		email: string;
		invites_enabled: boolean;
		languages: Array<string>;
		max_toot_chars?: number | undefined;
		registrations: boolean;
		rules: Array<InstanceRule>;
		stats: Stats;
		thumbnail: string | null;
		title: string;
		uri: string;
		urls: URLs;
		version: string;
	}

	export interface InstanceRule {
		id: string;
		text: string;
	}

	export interface List {
		id: string;
		replies_policy: RepliesPolicy;
		exclusive: boolean;
		title: string;
	}

	export enum RepliesPolicy {
		Followed = 'followed',
		List = 'list',
		None = 'none',
	}
	export interface Marker {
		home: {
			last_read_id: string;
			updated_at: string;
			version: number;
		};

		notifications: {
			last_read_id: string;
			updated_at: string;
			version: number;
		};
	}

	export interface Mention {
		acct: string;
		id: string;
		url: string;
		username: string;
	}

	export interface Notification {
		account: Account;
		created_at: string;
		id: string;
		status?: Status | undefined;
		type: NotificationType;
	}

	export enum NotificationType {
		AdminReport = 'admin.report',
		AdminSignup = 'admin.sign_up',
		Favourite = 'favourite',
		Follow = 'follow',
		FollowRequest = 'follow_request',
		Mention = 'mention',
		Poll = 'poll',
		Reaction = 'reaction',
		Reblog = 'reblog',
		Status = 'status',
		Update = 'update',
	}

	export interface PollOption {
		title: string;
		votes_count: number | null;
	}

	export interface Poll {
		expired: boolean;
		expires_at: string | null;
		id: string;
		multiple: boolean;
		options: Array<PollOption>;
		voted: boolean;
		votes_count: number;
	}

	export interface Preferences {
		'posting:default:language': string | null;
		'posting:default:sensitive': boolean;
		'posting:default:visibility': Visibility;
		'reading:expand:media': PreferencesMedia;
		'reading:expand:spoilers': boolean;
	}

	export enum Visibility {
		Public = 'public',
		Unlisted = 'unlisted',
		Private = 'private',
		Direct = 'direct',
	}

	export enum PreferencesMedia {
		Default = 'default',
		ShowAll = 'show_all',
		HideAll = 'hide_all',
	}

	export interface Alerts {
		favourite: boolean;
		follow: boolean;
		mention: boolean;
		poll: boolean;
		reblog: boolean;
	}

	export interface PushSubscription {
		alerts: Alerts;
		endpoint: string;
		id: string;
		server_key: string;
	}

	export interface Reaction {
		count: number;
		me?: boolean | undefined;
		name: string;
		static_url: string;
		url: string;
	}

	export interface Relationship {
		blocked_by: boolean;
		blocking: boolean;
		domain_blocking: boolean;
		endorsed: boolean;
		followed_by: boolean;
		following: boolean;
		id: string;
		languages: Array<string>;
		muting_notifications: boolean;
		muting: boolean;
		note: string;
		notifying: boolean;
		requested: boolean;
		requested_by: boolean;
		showing_reblogs: boolean;
	}

	export interface Report {
		action_taken_at: string | null;
		action_taken: boolean;
		category: Category;
		comment: string;
		forwarded: boolean;
		id: string;
		rule_ids: Array<string> | null;
		status_ids: Array<string> | null;
		target_account: Account;
	}

	export enum Category {
		Spam = 'spam',
		Violation = 'violation',
		Other = 'other',
	}

	export interface Results {
		accounts: Array<Account>;
		hashtags: Array<Tag>;
		statuses: Array<Status>;
	}

	export interface Role {
		name: string;
	}

	export interface ScheduledStatus {
		id: string;
		media_attachments: Array<Attachment>;
		params: StatusParams;
		scheduled_at: string;
	}

	export interface Source {
		fields: Array<Field>;
		language: string | null;
		note: string;
		privacy: string | null;
		sensitive: boolean | null;
	}

	export interface Stats {
		domain_count: number;
		status_count: number;
		user_count: number;
	}

	export interface StatusEdit {
		account: Account;
		content: string;
		created_at: string;
		emojis: Emoji[];
		media_attachments: Array<Attachment>;
		poll: Poll | null;
		sensitive: boolean;
		spoiler_text: string;
	}

	export interface StatusParams {
		application_id: number;
		in_reply_to_id: string | null;
		media_ids: Array<string> | null;
		scheduled_at: string | null;
		sensitive: boolean | null;
		spoiler_text: string | null;
		text: string;
		visibility: Visibility | null;
	}

	export interface StatusSource {
		id: string;
		spoiler_text: string;
		text: string;
	}

	export interface Status {
		account: Account;
		application: Application | null;
		bookmarked?: boolean | undefined;
		card: Card | null;
		content: string;
		created_at: string;
		edited_at: string | null;
		emojis: Emoji[];
		favourited: boolean | null;
		favourites_count: number;
		id: string;
		in_reply_to_account_id: string | null;
		in_reply_to_id: string | null;
		language: string | null;
		media_attachments: Array<Attachment>;
		mentions: Array<Mention>;
		muted: boolean | null;
		pinned: boolean | null;
		poll: Poll | null;
		reblog: Status | null;
		reblogged: boolean | null;
		reblogs_count: number;
		replies_count: number;
		sensitive: boolean;
		spoiler_text: string;
		tags: Array<StatusTag>;
		uri: string;
		url: string;
		visibility: Visibility;
		// Chuckya
		reactions?: Array<Reaction> | undefined;
		// These parameters are unique parameters in fedibird.com for quote.
		quote_id?: string | undefined;
		quote: Status | null;
	}

	export interface StatusTag {
		name: string;
		url: string;
	}

	export interface Tag {
		following?: boolean | undefined;
		history: Array<History>;
		name: string;
		url: string;
	}

	export interface Token {
		access_token: string;
		created_at: number;
		scope: string;
		token_type: string;
	}

	export interface URLs {
		streaming_api: string;
	}
}
