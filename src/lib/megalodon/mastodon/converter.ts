import { MastodonEntity } from './mastodonEntities';
import { Entity as MegalodonEntity } from '../entities';
import { UnknownNotificationTypeError } from '../notification';

export namespace Converter {
	export function encodeNotificationType(
		t: MegalodonEntity.NotificationType,
	): MastodonEntity.NotificationType | UnknownNotificationTypeError {
		switch (t) {
			case MegalodonEntity.NotificationType.AdminReport:
				return MastodonEntity.NotificationType.AdminReport;
			case MegalodonEntity.NotificationType.AdminSignup:
				return MastodonEntity.NotificationType.AdminSignup;
			case MegalodonEntity.NotificationType.Favourite:
				return MastodonEntity.NotificationType.Favourite;
			case MegalodonEntity.NotificationType.Follow:
				return MastodonEntity.NotificationType.Follow;
			case MegalodonEntity.NotificationType.FollowRequest:
				return MastodonEntity.NotificationType.FollowRequest;
			case MegalodonEntity.NotificationType.Mention:
				return MastodonEntity.NotificationType.Mention;
			case MegalodonEntity.NotificationType.PollExpired:
				return MastodonEntity.NotificationType.Poll;
			case MegalodonEntity.NotificationType.Reblog:
				return MastodonEntity.NotificationType.Reblog;
			case MegalodonEntity.NotificationType.Status:
				return MastodonEntity.NotificationType.Status;
			case MegalodonEntity.NotificationType.Update:
				return MastodonEntity.NotificationType.Update;
			default:
				return new UnknownNotificationTypeError();
		}
	}

	export function decodeNotificationType(
		t: MastodonEntity.NotificationType,
	): MegalodonEntity.NotificationType | UnknownNotificationTypeError {
		switch (t) {
			case MastodonEntity.NotificationType.AdminReport:
				return MegalodonEntity.NotificationType.AdminReport;
			case MastodonEntity.NotificationType.AdminSignup:
				return MegalodonEntity.NotificationType.AdminSignup;
			case MastodonEntity.NotificationType.Favourite:
				return MegalodonEntity.NotificationType.Favourite;
			case MastodonEntity.NotificationType.Follow:
				return MegalodonEntity.NotificationType.Follow;
			case MastodonEntity.NotificationType.FollowRequest:
				return MegalodonEntity.NotificationType.FollowRequest;
			case MastodonEntity.NotificationType.Mention:
				return MegalodonEntity.NotificationType.Mention;
			case MastodonEntity.NotificationType.Poll:
				return MegalodonEntity.NotificationType.PollExpired;
			case MastodonEntity.NotificationType.Reaction:
				return MegalodonEntity.NotificationType.Reaction;
			case MastodonEntity.NotificationType.Reblog:
				return MegalodonEntity.NotificationType.Reblog;
			case MastodonEntity.NotificationType.Status:
				return MegalodonEntity.NotificationType.Status;
			case MastodonEntity.NotificationType.Update:
				return MegalodonEntity.NotificationType.Update;
			default:
				return new UnknownNotificationTypeError();
		}
	}

	export function account(a: MastodonEntity.Account): MegalodonEntity.Account {
		return a;
	}

	export function activity(a: MastodonEntity.Activity): MegalodonEntity.Activity {
		return a;
	}

	export function announcement(a: MastodonEntity.Announcement): MegalodonEntity.Announcement {
		return a;
	}

	export function application(a: MastodonEntity.Application): MegalodonEntity.Application {
		return a;
	}

	export function attachment(a: MastodonEntity.Attachment): MegalodonEntity.Attachment {
		return a;
	}

	export function async_attachment(
		a: MastodonEntity.Attachment | MastodonEntity.AsyncAttachment,
	): MegalodonEntity.Attachment | MegalodonEntity.AsyncAttachment {
		if (a.url) {
			return {
				blurhash: a.blurhash,
				description: a.description,
				id: a.id,
				meta: a.meta,
				preview_url: a.preview_url,
				remote_url: a.remote_url,
				text_url: a.text_url,
				type: a.type,
				url: a.url!,
			} as MegalodonEntity.Attachment;
		} else {
			return a as MegalodonEntity.AsyncAttachment;
		}
	}

	export function card(c: MastodonEntity.Card): MegalodonEntity.Card {
		return c;
	}

	export function context(c: MastodonEntity.Context): MegalodonEntity.Context {
		return {
			ancestors: Array.isArray(c.ancestors) ? c.ancestors.map((a) => status(a)) : [],
			descendants: Array.isArray(c.descendants) ? c.descendants.map((d) => status(d)) : [],
		};
	}

	export function conversation(c: MastodonEntity.Conversation): MegalodonEntity.Conversation {
		return {
			accounts: Array.isArray(c.accounts) ? c.accounts.map((a) => account(a)) : [],
			id: c.id,
			last_status: c.last_status ? status(c.last_status) : null,
			unread: c.unread,
		};
	}

	export function emoji(e: MastodonEntity.Emoji): MegalodonEntity.Emoji {
		return e;
	}

	export function featured_tag(e: MastodonEntity.FeaturedTag): MegalodonEntity.FeaturedTag {
		return e;
	}

	export function field(f: MastodonEntity.Field): MegalodonEntity.Field {
		return f;
	}

	export function filter(f: MastodonEntity.Filter): MegalodonEntity.Filter {
		return f;
	}

	export function history(h: MastodonEntity.History): MegalodonEntity.History {
		return h;
	}

	export function identity_proof(i: MastodonEntity.IdentityProof): MegalodonEntity.IdentityProof {
		return i;
	}

	export function instance(i: MastodonEntity.Instance): MegalodonEntity.Instance {
		return i;
	}

	export function list(l: MastodonEntity.List): MegalodonEntity.List {
		return l;
	}

	export function marker(
		m: MastodonEntity.Marker | Record<never, never>,
	): MegalodonEntity.Marker | Record<never, never> {
		return m;
	}

	export function mention(m: MastodonEntity.Mention): MegalodonEntity.Mention {
		return m;
	}

	export function notification(
		n: MastodonEntity.Notification,
	): MegalodonEntity.Notification | UnknownNotificationTypeError {
		const notificationType = decodeNotificationType(n.type);
		if (notificationType instanceof UnknownNotificationTypeError) {
			return notificationType;
		}

		if (n.status) {
			return {
				account: account(n.account),
				created_at: n.created_at,
				id: n.id,
				status: status(n.status),
				type: notificationType,
			};
		} else {
			return {
				account: account(n.account),
				created_at: n.created_at,
				id: n.id,
				type: notificationType,
			};
		}
	}

	export function poll(p: MastodonEntity.Poll): MegalodonEntity.Poll {
		return p;
	}

	export function poll_option(p: MastodonEntity.PollOption): MegalodonEntity.PollOption {
		return p;
	}

	export function preferences(p: MastodonEntity.Preferences): MegalodonEntity.Preferences {
		return p;
	}

	export function push_subscription(
		p: MastodonEntity.PushSubscription,
	): MegalodonEntity.PushSubscription {
		return p;
	}

	export function relationship(r: MastodonEntity.Relationship): MegalodonEntity.Relationship {
		return r;
	}

	export function report(r: MastodonEntity.Report): MegalodonEntity.Report {
		return r;
	}

	export function results(r: MastodonEntity.Results): MegalodonEntity.Results {
		return {
			accounts: Array.isArray(r.accounts) ? r.accounts.map((a) => account(a)) : [],
			hashtags: Array.isArray(r.hashtags) ? r.hashtags.map((h) => tag(h)) : [],
			statuses: Array.isArray(r.statuses) ? r.statuses.map((s) => status(s)) : [],
		};
	}

	export function scheduled_status(
		s: MastodonEntity.ScheduledStatus,
	): MegalodonEntity.ScheduledStatus {
		return s;
	}

	export function source(s: MastodonEntity.Source): MegalodonEntity.Source {
		return s;
	}

	export function stats(s: MastodonEntity.Stats): MegalodonEntity.Stats {
		return s;
	}

	export function reaction(r: MastodonEntity.Reaction): MegalodonEntity.Reaction {
		return {
			accounts: [],
			count: r.count,
			me: r.me ?? false,
			name: r.name,
			static_url: r.static_url,
			url: r.url,
		};
	}

	export function status(s: MastodonEntity.Status): MegalodonEntity.Status {
		return {
			account: account(s.account),
			application: s.application ? application(s.application) : null,
			bookmarked: s.bookmarked ? s.bookmarked : false,
			card: s.card ? card(s.card) : null,
			content: s.content,
			created_at: s.created_at,
			edited_at: s.edited_at ?? null,
			emoji_reactions: Array.isArray(s.reactions) ? s.reactions.map((r) => reaction(r)) : [], // Chuckya only
			emojis: Array.isArray(s.emojis) ? s.emojis.map((e) => emoji(e)) : [],
			favourited: s.favourited,
			favourites_count: s.favourites_count,
			id: s.id,
			in_reply_to_account_id: s.in_reply_to_account_id,
			in_reply_to_id: s.in_reply_to_id,
			language: s.language,
			media_attachments: Array.isArray(s.media_attachments)
				? s.media_attachments.map((m) => attachment(m))
				: [],
			mentions: Array.isArray(s.mentions) ? s.mentions.map((m) => mention(m)) : [],
			muted: s.muted,
			pinned: s.pinned,
			plain_content: null,
			poll: s.poll ? poll(s.poll) : null,
			quote: s.quote !== undefined && s.quote !== null, // Now quote is supported only fedibird.com.
			reblog: s.reblog ? status(s.reblog) : null,
			reblogged: s.reblogged,
			reblogs_count: s.reblogs_count,
			replies_count: s.replies_count,
			sensitive: s.sensitive,
			spoiler_text: s.spoiler_text,
			tags: s.tags,
			uri: s.uri,
			url: s.url,
			visibility: s.visibility,
		};
	}

	export function status_params(s: MastodonEntity.StatusParams): MegalodonEntity.StatusParams {
		return s;
	}

	export function status_source(s: MastodonEntity.StatusSource): MegalodonEntity.StatusSource {
		return s;
	}

	export function status_edit(s: MastodonEntity.StatusEdit): MegalodonEntity.StatusEdit {
		return {
			account: account(s.account),
			content: s.content,
			created_at: s.created_at,
			emojis: Array.isArray(s.emojis) ? s.emojis.map((e) => emoji(e)) : [],
			media_attachments: Array.isArray(s.media_attachments)
				? s.media_attachments.map((m) => attachment(m))
				: [],
			poll: s.poll ? poll(s.poll) : null,
			sensitive: s.sensitive,
			spoiler_text: s.spoiler_text,
		};
	}

	export function tag(t: MastodonEntity.Tag): MegalodonEntity.Tag {
		return t;
	}

	export function token(t: MastodonEntity.Token): MegalodonEntity.Token {
		return t;
	}

	export function urls(u: MastodonEntity.URLs): MegalodonEntity.URLs {
		return u;
	}
}

export default Converter;
