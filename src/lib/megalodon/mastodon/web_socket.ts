import { ConsoleLogger } from '@nestjs/common';
import { Data } from 'ws';

import { UnknownNotificationTypeError } from '../notification';
import { WebSocketInterface } from '../web_socket';

import Converter from './converter';

/**
 * WebSocket
 * Pleroma is not support streaming. It is support websocket instead of streaming.
 * So this class connect to Phoenix websocket for Pleroma.
 */
export default class WebSocket extends WebSocketInterface {
	constructor(url: string, accessToken: string, logger: ConsoleLogger, userAgent?: string) {
		super(url, accessToken, logger, userAgent);
	}

	parse(data: Data, isBinary: boolean): void {
		const message = isBinary ? data : data.toString();
		if (typeof message !== 'string' || message === '') {
			this.callbacks.onHeartbeat?.();
			return;
		}

		let event: string = '';
		let payload: string = '';
		let stream: Array<any> = [];
		let optional: any = {};
		let mes: any = {};
		try {
			const obj = JSON.parse(message);
			event = obj.event;
			payload = obj.payload;
			stream = obj.stream;
			optional = {
				list: obj.list,
				tag: obj.tag,
			};
			mes = JSON.parse(payload);
		} catch (err) {
			// delete event does not have json object
			if (event !== 'delete') {
				const error = new Error(
					`Error parsing websocket reply: ${message}, error message: ${err}`,
				);
				if (!this.callbacks.onError) {
					throw error;
				}

				this.callbacks.onError?.(error);
				return;
			}
		}

		switch (event) {
			case 'update':
				this.callbacks.onUpdate?.({ stream, content: Converter.status(mes), ...optional });
				break;
			case 'notification':
				const n = Converter.notification(mes);
				if (n instanceof UnknownNotificationTypeError) {
					try {
						this.logger.warn(
							'_setupParser',
							`Unknown notification event has received: ${JSON.stringify(mes)}`,
						);
					} catch (_) {
						this.logger.warn(
							'_setupParser',
							`Unknown notification event has received: ${mes}`,
						);
					}
					return;
				}
				this.callbacks.onNotification?.({ stream, content: n, ...optional });
				break;
			case 'delete':
				this.callbacks.onDelete?.(payload);
				break;
			case 'conversation':
				this.callbacks.onConversation?.({
					stream,
					content: Converter.conversation(mes),
					...optional,
				});
				break;
			case 'status.update':
				this.callbacks.onStatusUpdate?.({
					stream,
					content: Converter.status(mes),
					...optional,
				});
				break;
			default:
				this.callbacks.onError?.(new Error(`Unknown event has received: ${message}`));
		}
	}
}
