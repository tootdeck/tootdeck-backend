import axios, { Axios, AxiosRequestConfig } from 'axios';

const NODEINFO_10 = 'http://nodeinfo.diaspora.software/ns/schema/1.0';
const NODEINFO_20 = 'http://nodeinfo.diaspora.software/ns/schema/2.0';
const NODEINFO_21 = 'http://nodeinfo.diaspora.software/ns/schema/2.1';

type Links = {
	links: Array<Link>;
};

type Link = {
	href: string;
	rel: string;
};

type Nodeinfo10 = {
	software: Software;
	metadata: Metadata;
};

type Nodeinfo20 = {
	software: Software;
	metadata: Metadata;
};

type Nodeinfo21 = {
	software: Software;
	metadata: Metadata;
};

type Software = {
	name: string;
};

type Metadata = {
	upstream?: {
		name: string;
	};
};

type Nodeinfo = Nodeinfo10 | Nodeinfo20 | Nodeinfo21;

type ValidInstanceType = 'pleroma' | 'mastodon' | 'misskey' | 'friendica';

function getSoftwareName(data: Nodeinfo10 | Nodeinfo20 | Nodeinfo21): ValidInstanceType | string {
	switch (data.software.name) {
		case 'pleroma':
		case 'akkoma':
			return 'pleroma';
		case 'mastodon':
		case 'hometown':
		case 'wildebeest':
		case 'gotosocial':
			return 'mastodon';
		case 'misskey':
		case 'firefish':
		case 'calckey':
		case 'sharkey':
		case 'foundkey':
		case 'iceshrimp':
			return 'misskey';
		case 'friendica':
			return 'friendica';
		default:
			return data.metadata?.upstream?.name && data.metadata.upstream.name === 'mastodon'
				? 'mastodon'
				: data.software.name;
	}
}

function handleAggregateError(e: Error) {
	if (e.name === 'AggregateError') {
		return ((e as any).errors as Array<Error>).map((err) => `${err}`).join('\n');
	}

	return `${e}`;
}

/**
 * Detect SNS type.
 * Now support Mastodon, Pleroma and Pixelfed. Throws an error when no known platform can be detected.
 *
 * @param url Base URL of SNS.
 * @return `pleroma` | `mastodon` | `misskey` | `friendica`
 * @return `null` if it couldn't find the SNS
 * @return `undefined` if an error happened
 */
export async function detector(
	url: string,
	axiosRef: Axios,
): Promise<{
	success: false;
	value: string;
}>;
export async function detector(
	url: string,
	axiosRef: Axios,
): Promise<{
	success: true;
	value: ValidInstanceType;
}>;
export async function detector(
	url: string,
	axiosRef: Axios = axios,
): Promise<{
	success: boolean;
	value: string | ValidInstanceType;
}> {
	let options: AxiosRequestConfig = {
		timeout: 20000,
		// @ts-ignore
		addPrefix: 'detector',
	};

	const nodeinfo_url = url + '/.well-known/nodeinfo';
	const nodeinfo_data: {
		success: boolean;
		value: Links | string;
	} = await axiosRef
		.get(nodeinfo_url, options)
		.then((r) => ({ success: true, value: r.data }))
		.catch((e) => ({ success: false, value: handleAggregateError(e) }));
	if (!nodeinfo_data.success) {
		return {
			success: false,
			value: `[1] Could not find nodeinfo\n${nodeinfo_data.value}\n${nodeinfo_url}`,
		};
	}

	const link = (nodeinfo_data.value as Links)?.links?.find(
		(l) => l.rel === NODEINFO_10 || l.rel === NODEINFO_20 || l.rel === NODEINFO_21,
	);
	if (!link) {
		return {
			success: false,
			value: `[2] Could not find nodeinfo\n${nodeinfo_url}`,
		};
	}

	if (![NODEINFO_10, NODEINFO_20, NODEINFO_21].includes(link.rel)) {
		return {
			success: false,
			value: `[3] Invalid nodeinfo\n${url}`,
		};
	}

	/**
	 * {
	 * 		success: true,
	 * 		value: Nodeinfo10 | Nodeinfo20 | Nodeinfo21
	 * }
	 *
	 * {
	 * 		success: false,
	 * 		value: string
	 * }
	 */
	const res: {
		success: boolean;
		value: Nodeinfo | string;
	} = await axiosRef
		.get(link.href, options)
		.then((r) => ({ success: true, value: r.data }))
		.catch((e) => ({
			success: false,
			value: handleAggregateError(e),
		}));
	if (!res.success) {
		return res as {
			success: false;
			value: string;
		};
	}

	const software_name = getSoftwareName(res.value as Nodeinfo);
	if (!['pleroma', 'mastodon', 'misskey', 'friendica'].includes(software_name)) {
		return {
			success: false,
			value: `Unknown SNS\n${software_name}`,
		};
	}

	return {
		success: true,
		value: software_name,
	};
}
