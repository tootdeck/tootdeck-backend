import { PleromaEntity } from './pleromaEntities';
import { Entity as MegalodonEntity } from '../entities';
import { UnknownNotificationTypeError } from '../notification';

export namespace Converter {
	export function decodeNotificationType(
		t: PleromaEntity.NotificationType,
	): MegalodonEntity.NotificationType | UnknownNotificationTypeError {
		switch (t) {
			case PleromaEntity.NotificationType.Favourite:
				return MegalodonEntity.NotificationType.Favourite;
			case PleromaEntity.NotificationType.Follow:
				return MegalodonEntity.NotificationType.Follow;
			case PleromaEntity.NotificationType.FollowRequest:
				return MegalodonEntity.NotificationType.FollowRequest;
			case PleromaEntity.NotificationType.Mention:
				return MegalodonEntity.NotificationType.Mention;
			case PleromaEntity.NotificationType.Move:
				return MegalodonEntity.NotificationType.Move;
			case PleromaEntity.NotificationType.PleromaEmojiReaction:
				return MegalodonEntity.NotificationType.EmojiReaction;
			case PleromaEntity.NotificationType.Poll:
				return MegalodonEntity.NotificationType.PollExpired;
			case PleromaEntity.NotificationType.Reblog:
				return MegalodonEntity.NotificationType.Reblog;
			case PleromaEntity.NotificationType.Update:
				return MegalodonEntity.NotificationType.Update;
			default:
				return new UnknownNotificationTypeError();
		}
	}

	export function encodeNotificationType(
		t: MegalodonEntity.NotificationType,
	): PleromaEntity.NotificationType | UnknownNotificationTypeError {
		switch (t) {
			case MegalodonEntity.NotificationType.EmojiReaction:
				return PleromaEntity.NotificationType.PleromaEmojiReaction;
			case MegalodonEntity.NotificationType.Favourite:
				return PleromaEntity.NotificationType.Favourite;
			case MegalodonEntity.NotificationType.Follow:
				return PleromaEntity.NotificationType.Follow;
			case MegalodonEntity.NotificationType.FollowRequest:
				return PleromaEntity.NotificationType.FollowRequest;
			case MegalodonEntity.NotificationType.Mention:
				return PleromaEntity.NotificationType.Mention;
			case MegalodonEntity.NotificationType.Move:
				return PleromaEntity.NotificationType.Move;
			case MegalodonEntity.NotificationType.PollExpired:
				return PleromaEntity.NotificationType.Poll;
			case MegalodonEntity.NotificationType.Reblog:
				return PleromaEntity.NotificationType.Reblog;
			case MegalodonEntity.NotificationType.Update:
				return PleromaEntity.NotificationType.Update;
			default:
				return new UnknownNotificationTypeError();
		}
	}

	export function account(a: PleromaEntity.Account): MegalodonEntity.Account {
		return {
			acct: a.acct,
			avatar_static: a.avatar_static,
			avatar: a.avatar,
			bot: a.bot,
			created_at: a.created_at,
			discoverable: a.discoverable,
			display_name: a.display_name,
			emojis: a.emojis.map((e) => emoji(e)),
			fields: a.fields,
			followers_count: a.followers_count,
			following_count: a.following_count,
			group: null,
			header_static: a.header_static,
			header: a.header,
			id: a.id,
			limited: a.limited,
			locked: a.locked,
			moved: a.moved ? account(a.moved) : null,
			noindex: a.noindex,
			note: a.note,
			source: a.source,
			statuses_count: a.statuses_count,
			suspended: a.suspended,
			url: a.url,
			username: a.username,
			mute_expires_at: undefined,
			role: undefined,
		};
	}

	export function activity(a: PleromaEntity.Activity): MegalodonEntity.Activity {
		return a;
	}

	export function announcement(a: PleromaEntity.Announcement): MegalodonEntity.Announcement {
		return {
			all_day: a.all_day,
			content: a.content,
			emojis: a.emojis,
			ends_at: a.ends_at,
			id: a.id,
			mentions: a.mentions,
			published_at: a.published_at,
			published: a.published,
			reactions: a.reactions,
			read: null,
			starts_at: a.starts_at,
			statuses: a.statuses,
			tags: a.tags,
			updated_at: a.updated_at,
		};
	}
	export function application(a: PleromaEntity.Application): MegalodonEntity.Application {
		return a;
	}

	export function attachment(a: PleromaEntity.Attachment): MegalodonEntity.Attachment {
		return a;
	}

	export function async_attachment(
		a: MegalodonEntity.Attachment | MegalodonEntity.AsyncAttachment,
	): MegalodonEntity.Attachment | MegalodonEntity.AsyncAttachment {
		if (a.url) {
			return {
				blurhash: a.blurhash,
				description: a.description,
				id: a.id,
				meta: a.meta,
				preview_url: a.preview_url,
				remote_url: a.remote_url,
				text_url: a.text_url,
				type: a.type,
				url: a.url!,
			} as MegalodonEntity.Attachment;
		} else {
			return a as MegalodonEntity.AsyncAttachment;
		}
	}

	export function card(c: PleromaEntity.Card): MegalodonEntity.Card {
		return {
			author_name: null,
			author_url: null,
			blurhash: null,
			description: c.description,
			embed_url: null,
			height: null,
			html: null,
			image: c.image,
			provider_name: c.provider_name,
			provider_url: c.provider_url,
			title: c.title,
			type: c.type,
			url: c.url,
			width: null,
		};
	}

	export function context(c: PleromaEntity.Context): MegalodonEntity.Context {
		return {
			ancestors: Array.isArray(c.ancestors) ? c.ancestors.map((a) => status(a)) : [],
			descendants: Array.isArray(c.descendants) ? c.descendants.map((d) => status(d)) : [],
		};
	}

	export function conversation(c: PleromaEntity.Conversation): MegalodonEntity.Conversation {
		return {
			accounts: Array.isArray(c.accounts) ? c.accounts.map((a) => account(a)) : [],
			id: c.id,
			last_status: c.last_status ? status(c.last_status) : null,
			unread: c.unread,
		};
	}

	export function emoji(e: PleromaEntity.Emoji): MegalodonEntity.Emoji {
		return {
			shortcode: e.shortcode,
			static_url: e.static_url,
			url: e.url,
			visible_in_picker: e.visible_in_picker,
		};
	}

	export function featured_tag(f: PleromaEntity.FeaturedTag): MegalodonEntity.FeaturedTag {
		return f;
	}

	export function field(f: PleromaEntity.Field): MegalodonEntity.Field {
		return f;
	}

	export function filter(f: PleromaEntity.Filter): MegalodonEntity.Filter {
		return f;
	}

	export function history(h: PleromaEntity.History): MegalodonEntity.History {
		return h;
	}

	export function identity_proof(i: PleromaEntity.IdentityProof): MegalodonEntity.IdentityProof {
		return i;
	}

	export function instance(i: PleromaEntity.Instance): MegalodonEntity.Instance {
		return {
			approval_required: i.approval_required,
			configuration: {
				polls: {
					max_characters_per_option: i.poll_limits.max_option_chars,
					max_expiration: i.poll_limits.max_expiration,
					max_options: i.poll_limits.max_options,
					min_expiration: i.poll_limits.min_expiration,
				},
				statuses: {
					characters_reserved_per_url: undefined,
					max_characters: i.max_toot_chars,
					max_media_attachments: i.max_media_attachments,
				},
				features: i.pleroma.metadata.features,
			},
			contact_account: undefined,
			description: i.description,
			email: i.email,
			invites_enabled: undefined,
			languages: i.languages,
			registrations: i.registrations,
			rules: undefined,
			stats: stats(i.stats),
			thumbnail: i.thumbnail,
			title: i.title,
			uri: i.uri,
			urls: urls(i.urls),
			version: i.version,
		};
	}

	export function list(l: PleromaEntity.List): MegalodonEntity.List {
		return {
			id: l.id,
			replies_policy: null,
			exclusive: false,
			title: l.title,
		};
	}

	export function marker(
		m: PleromaEntity.Marker | Record<never, never>,
	): MegalodonEntity.Marker | Record<never, never> {
		if ((m as any).notifications) {
			const mm = m as PleromaEntity.Marker;
			return {
				notifications: {
					last_read_id: mm.notifications.last_read_id,
					unread_count: mm.notifications.pleroma.unread_count,
					updated_at: mm.notifications.updated_at,
					version: mm.notifications.version,
				},
			};
		} else {
			return {};
		}
	}

	export function mention(m: PleromaEntity.Mention): MegalodonEntity.Mention {
		return m;
	}

	export function notification(
		n: PleromaEntity.Notification,
	): MegalodonEntity.Notification | UnknownNotificationTypeError {
		const notificationType = decodeNotificationType(n.type);
		if (notificationType instanceof UnknownNotificationTypeError) {
			return notificationType;
		}

		if (n.status && n.emoji) {
			return {
				account: account(n.account),
				created_at: n.created_at,
				emoji: n.emoji,
				id: n.id,
				status: status(n.status),
				type: notificationType,
			};
		} else if (n.status) {
			return {
				account: account(n.account),
				created_at: n.created_at,
				id: n.id,
				status: status(n.status),
				type: notificationType,
			};
		} else if (n.target) {
			return {
				account: account(n.account),
				created_at: n.created_at,
				id: n.id,
				target: account(n.target),
				type: notificationType,
			};
		} else {
			return {
				account: account(n.account),
				created_at: n.created_at,
				id: n.id,
				type: notificationType,
			};
		}
	}
	export function poll(p: PleromaEntity.Poll): MegalodonEntity.Poll {
		return p;
	}

	export function pollOption(p: PleromaEntity.PollOption): MegalodonEntity.PollOption {
		return p;
	}

	export function preferences(p: PleromaEntity.Preferences): MegalodonEntity.Preferences {
		return p;
	}

	export function push_subscription(
		p: PleromaEntity.PushSubscription,
	): MegalodonEntity.PushSubscription {
		return p;
	}

	export function reaction(r: PleromaEntity.Reaction): MegalodonEntity.Reaction {
		const p: MegalodonEntity.Reaction = {
			accounts: undefined,
			static_url: undefined,
			url: r.url,
			count: r.count,
			me: r.me,
			name: r.name,
		};

		if (r.accounts) {
			return Object.assign({}, p, {
				accounts: r.accounts.map((a) => account(a)),
			});
		}

		return p;
	}

	export function relationship(r: PleromaEntity.Relationship): MegalodonEntity.Relationship {
		return {
			blocked_by: r.blocked_by,
			blocking: r.blocking,
			domain_blocking: r.domain_blocking,
			endorsed: r.endorsed,
			followed_by: r.followed_by,
			following: r.following,
			id: r.id,
			muting_notifications: r.muting_notifications,
			muting: r.muting,
			note: r.note,
			notifying: r.notifying,
			requested: r.requested,
			requested_by: null,
			showing_reblogs: r.showing_reblogs,
		};
	}

	export function report(r: PleromaEntity.Report): MegalodonEntity.Report {
		return {
			action_taken_at: null,
			action_taken: r.action_taken,
			category: null,
			comment: null,
			forwarded: null,
			id: r.id,
			rule_ids: null,
			status_ids: null,
			target_account: null,
		};
	}

	export function results(r: PleromaEntity.Results): MegalodonEntity.Results {
		return {
			accounts: Array.isArray(r.accounts) ? r.accounts.map((a) => account(a)) : [],
			hashtags: Array.isArray(r.hashtags) ? r.hashtags.map((h) => tag(h)) : [],
			statuses: Array.isArray(r.statuses) ? r.statuses.map((s) => status(s)) : [],
		};
	}

	export function scheduled_status(
		s: PleromaEntity.ScheduledStatus,
	): MegalodonEntity.ScheduledStatus {
		return {
			id: s.id,
			media_attachments: Array.isArray(s.media_attachments)
				? s.media_attachments.map((m) => attachment(m))
				: null,
			params: status_params(s.params),
			scheduled_at: s.scheduled_at,
		};
	}

	export function source(s: PleromaEntity.Source): MegalodonEntity.Source {
		return s;
	}

	export function stats(s: PleromaEntity.Stats): MegalodonEntity.Stats {
		return s;
	}

	export function status(s: PleromaEntity.Status): MegalodonEntity.Status {
		return {
			account: account(s.account),
			application: s.application ? application(s.application) : null,
			bookmarked: s.bookmarked ? s.bookmarked : false,
			card: s.card ? card(s.card) : null,
			content: s.content,
			created_at: s.created_at,
			edited_at: s.edited_at ?? null,
			emoji_reactions: Array.isArray(s.pleroma.emoji_reactions)
				? s.pleroma.emoji_reactions.map((r) => reaction(r))
				: [],
			emojis: Array.isArray(s.emojis) ? s.emojis.map((e) => emoji(e)) : [],
			favourited: s.favourited,
			favourites_count: s.favourites_count,
			id: s.id,
			in_reply_to_account_id: s.in_reply_to_account_id,
			in_reply_to_id: s.in_reply_to_id,
			language: s.language,
			media_attachments: Array.isArray(s.media_attachments)
				? s.media_attachments.map((m) => attachment(m))
				: [],
			mentions: Array.isArray(s.mentions) ? s.mentions.map((m) => mention(m)) : [],
			muted: s.muted,
			pinned: s.pinned,
			plain_content: s.pleroma.content?.['text/plain']
				? s.pleroma.content['text/plain']
				: null,
			poll: s.poll ? poll(s.poll) : null,
			quote: s.reblog !== null && s.reblog.content !== s.content,
			reblog: s.reblog ? status(s.reblog) : null,
			reblogged: s.reblogged,
			reblogs_count: s.reblogs_count,
			replies_count: s.replies_count,
			sensitive: s.sensitive,
			spoiler_text: s.spoiler_text,
			tags: s.tags,
			uri: s.uri,
			url: s.url,
			visibility: s.visibility,
		};
	}

	export function status_params(s: PleromaEntity.StatusParams): MegalodonEntity.StatusParams {
		return {
			application_id: null,
			in_reply_to_id: s.in_reply_to_id,
			media_ids: Array.isArray(s.media_ids) ? s.media_ids : null,
			scheduled_at: s.scheduled_at,
			sensitive: s.sensitive,
			spoiler_text: s.spoiler_text,
			text: s.text,
			visibility: s.visibility,
		};
	}

	export function status_source(s: PleromaEntity.StatusSource): MegalodonEntity.StatusSource {
		return s;
	}

	export function status_edit(s: PleromaEntity.StatusEdit): MegalodonEntity.StatusEdit {
		return {
			account: account(s.account),
			content: s.content,
			created_at: s.created_at,
			emojis: Array.isArray(s.emojis) ? s.emojis.map((e) => emoji(e)) : [],
			media_attachments: Array.isArray(s.media_attachments)
				? s.media_attachments.map((m) => attachment(m))
				: [],
			poll: s.poll ? poll(s.poll) : null,
			sensitive: s.sensitive,
			spoiler_text: s.spoiler_text,
		};
	}

	export function tag(t: PleromaEntity.Tag): MegalodonEntity.Tag {
		return t;
	}

	export function token(t: PleromaEntity.Token): MegalodonEntity.Token {
		return t;
	}

	export function urls(u: PleromaEntity.URLs): MegalodonEntity.URLs {
		return u;
	}
}

export default Converter;
