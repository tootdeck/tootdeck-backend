import {
	CallHandler,
	ExecutionContext,
	Inject,
	Injectable,
	NestInterceptor,
	forwardRef,
} from '@nestjs/common';
import { Redis } from 'ioredis';
import { RedisService } from '@liaoliaots/nestjs-redis';
import { FastifyReply } from 'fastify';
import { map } from 'rxjs';
import * as sanitizeHtml from 'sanitize-html';

import { Entity } from '../../lib/megalodon/entities';

import { AvailableJobs } from '../../worker/controllers/jobs.controller';
import { OptionalFeaturesService } from '../../app/services/utils/optionalFeatures.service';
import { WsService } from '../../websocket/services/ws.service';
import { ClusterSchedulerService } from '../../app/services/clusterScheduler.service';

import { Logger } from '../../utils/logger';

import { RedisNamespace } from '../../app/constants/redis';

import { RequestMirror } from '../types/guards.types';
import { ResponseType, WebsocketAPI } from '../../websocket/types';

@Injectable()
export class InjectorInterceptor<T> implements NestInterceptor<T, FastifyReply> {
	private readonly logger = new Logger(InjectorInterceptor.name);
	private readonly redis_excluded_domain: Redis;

	constructor(
		private readonly OptionalFeaturesService: OptionalFeaturesService,
		@Inject(forwardRef(() => WsService))
		private readonly WsService: WsService,
		private readonly ClusterSchedulerService: ClusterSchedulerService,
		RedisService: RedisService,
	) {
		this.redis_excluded_domain = RedisService.getOrThrow(RedisNamespace.ExcludedDomains);
	}

	/**
	 * Statuses interactions
	 */
	// #region

	async getInteractions(
		user_uuid: string,
		handle: string,
		disabled: boolean,
		entity: Entity.Status,
	) {
		if (disabled) {
			return;
		}

		// Status not public, skipping it
		if (entity.visibility === 'direct' || entity.visibility === 'private') {
			// this.logger.verbose('getInteractions', 'Skipped (not public)');
			return;
		}

		// Status just created, skipping it
		const created_at = new Date(entity.created_at).valueOf();
		const now = new Date().valueOf();
		if (created_at + 40000 - now > 0) {
			// this.logger.verbose('getInteractions', 'Skipped (just created)');
			return;
		}

		let href: string | undefined = entity.uri ?? entity.url;
		if (!href) {
			return;
		}

		// Same instance, storing it
		const { hostname } = new URL(entity.account.url);
		const handle_hostname = handle.split('@')[1];
		if (hostname === handle_hostname) {
			this.ClusterSchedulerService.sendJob(handle, AvailableJobs.InteractionsStore, {
				entity,
			});
			return;
		}

		const excluded = await this.redis_excluded_domain.get(hostname);
		if (excluded) {
			return;
		}

		this.ClusterSchedulerService.sendJob(
			handle,
			AvailableJobs.InteractionsFetch,
			{ entity, url: href },
			(interactions) => {
				if (!interactions) {
					return;
				}

				this.WsService.sendByUserUUID(user_uuid, {
					type: ResponseType.API,
					data: WebsocketAPI.ResponseData.Interactions,
					handle,
					// @ts-ignore
					interactions: { ...interactions, id: entity.id },
				});
			},
		);
	}

	// #endregion

	/**
	 * Parser
	 */
	// #region

	forAccount(entity: Entity.Account) {
		entity.note = sanitizeHtml(entity.note, {
			allowedAttributes: false,
		});

		entity.fields.forEach((field) => ({
			name: field.name,
			value: sanitizeHtml(field.value, {
				allowedAttributes: false,
			}),
			verified_at: field.verified_at,
		}));
	}

	forAccounts(entities: Entity.Account[]) {
		entities.forEach((entity) => this.forAccount(entity));
	}

	forReaction(entity: Entity.Reaction) {
		if (!entity.accounts?.length) {
			return;
		}

		this.forAccounts(entity.accounts);
	}

	forReactions(entities: Entity.Reaction[]) {
		if (!entities?.length) {
			return;
		}

		entities.forEach((entity) => this.forReaction(entity));
	}

	forStatus(
		user_uuid: string,
		handle: string,
		disable_cache: boolean,
		entity: Entity.Status | undefined,
	) {
		if (!entity) {
			return;
		}

		if (entity.reblog) {
			this.forStatus(user_uuid, handle, disable_cache, entity.reblog);
			return;
		}

		entity.content = sanitizeHtml(entity.content, {
			allowedTags: sanitizeHtml.defaults.allowedTags.concat(['del']),
			allowedAttributes: false,
			transformTags: {
				del: 's',
			},
		});

		this.forAccount(entity.account);
		this.forReactions(entity.emoji_reactions);
		this.getInteractions(user_uuid, handle, disable_cache, entity);
	}

	forStatuses(
		user_uuid: string,
		handle: string,
		disable_cache: boolean,
		entities: Entity.Status[],
	) {
		entities.forEach((entity) => this.forStatus(user_uuid, handle, disable_cache, entity));
	}

	forConversation(
		user_uuid: string,
		handle: string,
		disable_cache: boolean,
		entity: Entity.Conversation,
	) {
		this.forAccounts(entity.accounts);
		if (entity.last_status) {
			this.forStatus(user_uuid, handle, disable_cache, entity.last_status);
		}
	}

	forNotification(
		user_uuid: string,
		handle: string,
		disable_cache: boolean,
		entity: Entity.Notification,
	) {
		this.forAccount(entity.account);
		this.forStatus(user_uuid, handle, disable_cache, entity.status);
	}

	forInstance(entity: Entity.Instance) {
		if (!entity.contact_account) {
			return;
		}

		this.forAccount(entity.contact_account);
	}

	forReport(entity: Entity.Report) {
		if (!entity.target_account) {
			return;
		}

		this.forAccount(entity.target_account);
	}

	forResults(user_uuid: string, handle: string, disable_cache: boolean, entity: Entity.Results) {
		this.forAccounts(entity.accounts);
		this.forStatuses(user_uuid, handle, disable_cache, entity.statuses);
	}

	// #endregion

	private dispatchEntity(user_uuid: string, handle: string, disable_cache: boolean, entity: any) {
		if ((entity as Entity.Account)?.acct) {
			this.forAccount(entity);
		} else if ((entity as Entity.Status)?.account) {
			if ((entity as Entity.Notification).type) {
				this.forNotification(user_uuid, handle, disable_cache, entity);
			} else {
				this.forStatus(user_uuid, handle, disable_cache, entity);
			}
		} else if (
			(entity as Entity.Results).accounts &&
			(entity as Entity.Results).hashtags &&
			(entity as Entity.Results).statuses
		) {
			this.forResults(user_uuid, handle, disable_cache, entity);
		} else if ((entity as Entity.Conversation)?.accounts) {
			this.forConversation(user_uuid, handle, disable_cache, entity);
		} else if ((entity as Entity.Instance)?.contact_account) {
			this.forInstance(entity);
		} else if ((entity as Entity.Report)?.target_account) {
			this.forReport(entity);
		}
	}

	private interceptorInject(user_uuid: string, handle: string, data: any) {
		if (!data?.data || typeof data.data === 'string') {
			return data;
		}

		const entities = data.data as
			| Entity.Account[]
			| Entity.Status[]
			| Entity.Conversation[]
			| Entity.Notification[]
			| Entity.Account
			| Entity.Status
			| Entity.Conversation
			| Entity.Notification
			| Entity.Instance
			| Entity.Report
			| Entity.Results;

		const disable_cache = !this.OptionalFeaturesService.CACHE_STATUS_INTERACTIONS;

		if (Array.isArray(entities)) {
			entities.forEach((entity) =>
				this.dispatchEntity(user_uuid, handle, disable_cache, entity),
			);
		} else {
			this.dispatchEntity(user_uuid, handle, disable_cache, entities);
		}

		try {
			// await this.interaction_queue.process();
		} catch (e) {
			this.logger.error('interceptorInjectCache', 'This should not happend');
			console.error(e);
		}

		return data;
	}

	websocketInject(user_uuid: string, handle: string, throttled: boolean, data: any) {
		if (!data?.data || !data?.data.content || typeof data.data === 'string') {
			return data;
		}

		const entity = data.data.content as
			| Entity.Status
			| Entity.Conversation
			| Entity.Notification;

		const disable_cache = throttled || !this.OptionalFeaturesService.CACHE_STATUS_INTERACTIONS;

		this.dispatchEntity(user_uuid, handle, disable_cache, entity);

		try {
			// await this.interaction_queue.process();
		} catch (e) {
			this.logger.error('websocketInjectCache', 'This should not happend');
			console.error(e);
		}

		return data;
	}

	intercept(ctx: ExecutionContext, next: CallHandler) {
		const request = ctx.switchToHttp().getRequest() as RequestMirror;
		const handle = request.headers['handle'] as string; // Already checked in MirrorGuard
		if (!handle) {
			return next.handle();
		}

		const user_uuid = request.access_session.user_uuid;

		return next.handle().pipe(map((data) => this.interceptorInject(user_uuid, handle, data)));
	}
}
