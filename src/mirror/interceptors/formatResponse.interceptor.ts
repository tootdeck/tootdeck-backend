import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { FastifyReply } from 'fastify/types/reply';
import { Response } from 'express';
import { Observable, map } from 'rxjs';

import MegalodonResponse from '../../lib/megalodon/response';

@Injectable()
export class FormatResponseInterceptor<T> implements NestInterceptor<T, Response<T>> {
	intercept(context: ExecutionContext, next: CallHandler): Observable<Response<T>> {
		return next.handle().pipe(
			map((data) => {
				if (!data?.data) {
					return data;
				}

				const response = context.switchToHttp().getResponse() as FastifyReply;

				const megalodon_response = data as MegalodonResponse;

				const rate_limit = {
					limit: megalodon_response.headers['x-ratelimit-limit'],
					remaining: megalodon_response.headers['x-ratelimit-remaining'],
					reset: megalodon_response.headers['x-ratelimit-reset'],
				};

				const request_id = megalodon_response.headers['x-request-id'];
				const runtime = megalodon_response.headers['x-runtime'];

				const link = megalodon_response.headers['link'];

				if (rate_limit.limit) {
					response.header('X-RateLimit-limit', rate_limit.limit);
					response.header('X-RateLimit-remaining', rate_limit.remaining);
					response.header('X-RateLimit-reset', rate_limit.reset);
				}

				if (request_id) {
					response.header('X-Request-id', request_id);
				}

				if (runtime) {
					response.header('X-Instance-time', +runtime * 1000);
				}

				if (link) {
					response.header('link', link);
				}

				return megalodon_response;
			}),
		);
	}
}
