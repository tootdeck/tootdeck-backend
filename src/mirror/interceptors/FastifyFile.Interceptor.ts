import { CallHandler, ExecutionContext, NestInterceptor, Type } from '@nestjs/common';
import { Observable } from 'rxjs';
import FastifyMulter from 'fastify-multer';

export function FastifyFileInterceptor(fieldName: string): Type<NestInterceptor> {
	class MixinInterceptor implements NestInterceptor {
		private readonly multer = FastifyMulter();

		async intercept(context: ExecutionContext, next: CallHandler): Promise<Observable<any>> {
			const ctx = context.switchToHttp();

			await new Promise<void>((resolve, reject) =>
				// @ts-ignore
				this.multer.single(fieldName)(ctx.getRequest(), ctx.getResponse(), (error: any) =>
					error ? reject(error) : resolve(),
				),
			);

			return next.handle();
		}
	}

	return MixinInterceptor as Type<NestInterceptor>;
}
