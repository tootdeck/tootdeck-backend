import { Injectable } from '@nestjs/common';
import { Redis } from 'ioredis';
import { RedisService } from '@liaoliaots/nestjs-redis';

import { RedisNamespace } from '../../app/constants/redis';

type Domain = string;
type QueueID = string;
type PromiseCallback<T> = (data: T) => void;
type QueuedPromise<T> = () => Promise<T>;
type QueueValue<T> = { fetcher: QueuedPromise<T>; setters: Array<PromiseCallback<T>> };
type PromiseMap<T> = Map<QueueID, QueueValue<T>>;

@Injectable()
export class InjectorQueue {
	private readonly redis_excluded_domain: Redis;

	private readonly _queue = new Map<Domain, PromiseMap<any>>();
	private _processing: boolean = false;
	private _waiting_queue: Array<(v: any) => void> = [];
	private _waiting_process_queue: Array<(v: any) => void> = [];

	constructor(RedisService: RedisService) {
		this.redis_excluded_domain = RedisService.getOrThrow(RedisNamespace.ExcludedDomains);
	}

	/**
	 * Add to queue
	 */
	async add<T>(key: string, promise: QueuedPromise<void>): Promise<void>;
	async add<T>(
		key: string,
		fetcher: QueuedPromise<T>,
		setters: PromiseCallback<T>,
	): Promise<void>;
	async add<T>(
		key: string,
		fetcher: QueuedPromise<T>,
		setters?: PromiseCallback<T>,
	): Promise<void> {
		if (this._processing) {
			await new Promise((resolve) => {
				this._waiting_queue.push(resolve);
			});
		}

		const { hostname } = new URL(key);
		const excluded = await this.redis_excluded_domain.get(hostname);
		if (excluded) {
			return;
		}

		const inner = this._queue.get(hostname);
		let value = inner?.get(key);
		if (value) {
			if (setters) {
				value.setters.push(setters);
			}

			return;
		}

		value = { fetcher: fetcher, setters: setters ? [setters] : [] };
		if (!inner) {
			const inner_map = new Map([[key, value]]);
			this._queue.set(hostname, inner_map);
		} else {
			inner.set(key, value);
		}
	}

	/**
	 * Iterates on promises map and execute them
	 */
	private async processPromiseMap(promise_map: PromiseMap<any>): Promise<Array<void>> {
		const values = promise_map.entries();

		let iter: [string, QueueValue<any>];
		const promises: Array<Promise<void>> = [];
		while ((iter = values.next().value)) {
			const [_, value] = iter;

			promises.push(
				value
					.fetcher()
					.then((data) => {
						value.setters.forEach((callback) => callback(data));
					})
					.catch((e) => console.error(e)),
			);
		}

		return Promise.all(promises);
	}

	/**
	 * Process queue, getting promises map and execute them
	 */
	async process(): Promise<void> {
		if (this._processing) {
			await new Promise((resolve) => {
				this._waiting_process_queue.push(resolve);
			});
		}

		this._processing = true;

		const values = this._queue.entries();

		let iter: [string, PromiseMap<any>];
		const promises: Array<Promise<Array<void>>> = [];
		while ((iter = values.next().value)) {
			const [domain, promise_map] = iter;

			promises.push(this.processPromiseMap(promise_map));

			this._queue.delete(domain);
		}

		await Promise.all(promises);

		// Process all pending additions
		this._waiting_queue.forEach((promise) => promise(true));
		this._waiting_queue = [];
		this._processing = false;

		// Process one pending queue
		this._waiting_process_queue.splice(0, 1)[0]?.(true);
	}
}
