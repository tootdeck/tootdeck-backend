import {
	BadRequestException,
	Body,
	Controller,
	Delete,
	Get,
	Post,
	Query,
	Request,
	UseFilters,
	UseGuards,
	UseInterceptors,
} from '@nestjs/common';
import { ApiBody, ApiQuery, ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';

import { AccessGuard } from '../../../auth/guards/access.guard';
import { MirrorGuard } from '../../guards/mirror.guard';

import { AxiosErrorFilter } from '../../filters/AxiosError.filter';

import { FormatResponseInterceptor } from '../../interceptors/formatResponse.interceptor';
import { InjectorInterceptor } from '../../interceptors/injector.interceptor';

import { validateBody } from '../../utils/validateBody';

import { Body as FormBody } from '../../properties/body.property';

import { RequestMirror } from '../../types/guards.types';

@ApiTags('Mirror · Accounts · Domain blocks')
@ApiSecurity('InstanceHandle')
@UseGuards(AccessGuard, MirrorGuard)
@UseInterceptors(FormatResponseInterceptor, InjectorInterceptor)
@UseFilters(AxiosErrorFilter)
@Controller('/v1/domain_blocks')
export class MirrorAccountsDomainBlocksController {
	/**
	 * GET /api/v1/domain_blocks
	 * https://docs.joinmastodon.org/methods/domain_blocks/#get
	 *
	 * @query `limit`
	 * @query `max_id`
	 * @query `min_id`
	 * @returns `Array<String>`
	 */
	@ApiQuery({
		name: 'limit',
		required: false,
		description:
			'Maximum number of results to return. Defaults to 20 statuses. Max 40 statuses.',
		type: Number,
	})
	@ApiQuery({
		name: 'max_id',
		required: false,
		description: 'Return results older than ID.',
		type: String,
	})
	@ApiQuery({
		name: 'min_id',
		required: false,
		description: 'Return results immediately newer than ID.',
		type: String,
	})
	@ApiResponse({
		status: 200,
		description: 'View domains the user has blocked.',
		type: String,
		isArray: true,
	})
	@Get()
	async get(@Request() request: RequestMirror, @Query() query: any) {
		const options = {
			limit: query.limit,
			max_id: query.max_id,
			min_id: query.min_id,
		};

		return request.instance.getDomainBlocks(options);
	}

	/**
	 * POST /api/v1/domain_blocks
	 * https://docs.joinmastodon.org/methods/domain_blocks/#block
	 *
	 * @body `FormBody.DomainBlock`
	 * @returns `{}`
	 */
	@ApiBody({
		type: FormBody.DomainBlock,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Block a domain.',
		type: String,
		isArray: true,
	})
	@ApiResponse({
		status: 400,
		description: 'Empty body.',
	})
	@ApiResponse({
		status: 422.1,
		description: "Validation failed: Domain can't be blank",
	})
	@ApiResponse({
		status: 422.2,
		description: 'Validation failed: Domain is not a valid domain name',
	})
	@Post()
	async block(@Request() request: RequestMirror, @Body() body: FormBody.DomainBlock) {
		if (!validateBody(body) || !body.domain) {
			throw new BadRequestException();
		}

		return request.instance.blockDomain(body.domain);
	}

	/**
	 * DELETE /api/v1/domain_blocks
	 * https://docs.joinmastodon.org/methods/domain_blocks/#unblock
	 *
	 * @body `FormBody.DomainBlock`
	 * @returns `{}`
	 */
	@ApiBody({
		type: FormBody.DomainBlock,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Block a domain.',
		type: String,
		isArray: true,
	})
	@ApiResponse({
		status: 400,
		description: 'Empty body.',
	})
	@ApiResponse({
		status: 422.1,
		description: "Validation failed: Domain can't be blank",
	})
	@ApiResponse({
		status: 422.2,
		description: 'Validation failed: Domain is not a valid domain name',
	})
	@Delete()
	async unblock(@Request() request: RequestMirror, @Body() body: FormBody.DomainBlock) {
		if (!validateBody(body) || !body.domain) {
			throw new BadRequestException();
		}

		return request.instance.unblockDomain(body.domain);
	}
}
