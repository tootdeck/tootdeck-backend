import {
	BadRequestException,
	Body,
	Controller,
	Delete,
	Get,
	Param,
	Post,
	Put,
	Request,
	UseFilters,
	UseGuards,
	UseInterceptors,
} from '@nestjs/common';
import { ApiBody, ApiParam, ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';

import { AccessGuard } from '../../../auth/guards/access.guard';
import { MirrorGuard } from '../../guards/mirror.guard';

import { AxiosErrorFilter } from '../../filters/AxiosError.filter';

import { FormatResponseInterceptor } from '../../interceptors/formatResponse.interceptor';
import { InjectorInterceptor } from '../../interceptors/injector.interceptor';

import { validateBody } from '../../utils/validateBody';

import { Entity } from '../../properties/entities.property';
import { Body as FormBody } from '../../properties/body.property';

import { RequestMirror } from '../../types/guards.types';

@ApiTags('Mirror · Accounts · Filters')
@ApiSecurity('InstanceHandle')
@UseGuards(AccessGuard, MirrorGuard)
@UseInterceptors(FormatResponseInterceptor, InjectorInterceptor)
@UseFilters(AxiosErrorFilter)
@Controller('/v1/filters')
export class MirrorAccountsFiltersController {
	/**
	 * GET /api/v1/filters
	 * https://docs.joinmastodon.org/methods/filters/#get-v1
	 *
	 * @returns `Array<Entity.Filter>`
	 */
	@ApiResponse({
		status: 200,
		description: 'View your filters.',
		type: Entity.Filter,
		isArray: true,
	})
	@Get()
	async getAll(@Request() request: RequestMirror) {
		return request.instance.getFilters();
	}

	/**
	 * GET /api/v1/filters/:id
	 * https://docs.joinmastodon.org/methods/filters/#get-one-v1
	 *
	 * @param `id`
	 * @returns `Entity.Filter`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'View a single filter.',
		type: Entity.Filter,
	})
	@Get(':id')
	async getOne(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.getFilter(id);
	}

	/**
	 * POST /api/v1/filters
	 * https://docs.joinmastodon.org/methods/filters/#update-v1
	 *
	 * @body `FormBody.Filter`
	 * @returns `Entity.Filter`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiBody({
		type: FormBody.Filter,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Create a filter.',
		type: Entity.Filter,
	})
	@ApiResponse({
		status: 400,
		description: 'Empty body.',
	})
	@ApiResponse({
		status: 422.1,
		description: "Validation failed: Phrase can't be blank.",
	})
	@ApiResponse({
		status: 422.2,
		description:
			"Validation failed: Context can't be blank, Context None or invalid context supplied.",
	})
	@Post()
	async create(@Request() request: RequestMirror, @Body() body: FormBody.Filter) {
		if (!validateBody(body) || !body.phrase || !body.context?.length) {
			throw new BadRequestException();
		}

		const options = {
			irreversible: body.irreversible,
			whole_word: body.whole_word,
			expires_in: body.expires_in,
		} as any;

		return request.instance.createFilter(body.phrase, body.context, options);
	}

	/**
	 * PUT /api/v1/filters/:id
	 * https://docs.joinmastodon.org/methods/filters/#update-v1
	 *
	 * @param `id`
	 * @body `FormBody.Filter`
	 * @returns `Entity.Filter`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiBody({
		type: FormBody.Filter,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Replaces a filter’s parameters in-place.',
		type: Entity.Filter,
	})
	@ApiResponse({
		status: 400.1,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 400.2,
		description: 'Empty body.',
	})
	@ApiResponse({
		status: 422.1,
		description: "Validation failed: Phrase can't be blank.",
	})
	@ApiResponse({
		status: 422.2,
		description:
			"Validation failed: Context can't be blank, Context None or invalid context supplied.",
	})
	@Put()
	async update(
		@Request() request: RequestMirror,
		@Param('id') id: string,
		@Body() body: FormBody.Filter,
	) {
		if (!id || !validateBody(body) || !body.phrase || !body.context?.length) {
			throw new BadRequestException();
		}

		const options = {
			irreversible: body.irreversible,
			whole_word: body.whole_word,
			expires_in: body.expires_in,
		} as any;

		return request.instance.updateFilter(id, body.phrase, body.context, options);
	}

	/**
	 * DELETE /api/v1/filters/:id
	 * https://docs.joinmastodon.org/methods/filters/#delete-v1
	 *
	 * @param `id`
	 * @returns `Entity.Filter`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Remove a filter.',
		type: Entity.Filter,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@Delete()
	async delete(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.deleteFilter(id);
	}
}
