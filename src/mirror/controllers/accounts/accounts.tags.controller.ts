import {
	BadRequestException,
	Controller,
	Get,
	Param,
	Post,
	Request,
	UseFilters,
	UseGuards,
	UseInterceptors,
} from '@nestjs/common';
import { ApiParam, ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';

import { AccessGuard } from '../../../auth/guards/access.guard';
import { MirrorGuard } from '../../guards/mirror.guard';

import { AxiosErrorFilter } from '../../filters/AxiosError.filter';

import { FormatResponseInterceptor } from '../../interceptors/formatResponse.interceptor';
import { InjectorInterceptor } from '../../interceptors/injector.interceptor';

import { Entity } from '../../properties/entities.property';

import { RequestMirror } from '../../types/guards.types';

@ApiTags('Mirror · Accounts · Tags')
@ApiSecurity('InstanceHandle')
@UseGuards(AccessGuard, MirrorGuard)
@UseInterceptors(FormatResponseInterceptor, InjectorInterceptor)
@UseFilters(AxiosErrorFilter)
@Controller('/v1/tags')
export class MirrorAccountsTagsController {
	/**
	 * GET /api/v1/tags/:id
	 * https://docs.joinmastodon.org/methods/tags/#get
	 *
	 * @param `id`
	 * @returns `Entity.Tag`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Statuses the user has bookmarked.',
		type: Entity.Tag,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@Get(':id')
	async get(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.getTag(id);
	}

	/**
	 * POST /api/v1/tags/:id/follow
	 * https://docs.joinmastodon.org/methods/tags/#follow
	 *
	 * @param `id`
	 * @returns `Entity.Tag`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Statuses the user has bookmarked.',
		type: Entity.Tag,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@ApiResponse({
		status: 422,
		description: 'Duplicate record.',
	})
	@Post(':id/follow')
	async follow(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.followTag(id);
	}

	/**
	 * POST /api/v1/tags/:id/unfollow
	 * https://docs.joinmastodon.org/methods/tags/#unfollow
	 *
	 * @param `id`
	 * @returns `Entity.Tag`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Statuses the user has bookmarked.',
		type: Entity.Tag,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@Post(':id/unfollow')
	async unfollow(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.unfollowTag(id);
	}
}
