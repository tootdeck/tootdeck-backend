import {
	BadRequestException,
	Controller,
	Get,
	Param,
	Post,
	Query,
	Request,
	UseFilters,
	UseGuards,
	UseInterceptors,
} from '@nestjs/common';
import { ApiParam, ApiQuery, ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';

import { AccessGuard } from '../../../auth/guards/access.guard';
import { MirrorGuard } from '../../guards/mirror.guard';

import { AxiosErrorFilter } from '../../filters/AxiosError.filter';

import { FormatResponseInterceptor } from '../../interceptors/formatResponse.interceptor';
import { InjectorInterceptor } from '../../interceptors/injector.interceptor';

import { Entity } from '../../properties/entities.property';

import { RequestMirror } from '../../types/guards.types';

@ApiTags('Mirror · Accounts · Follow Requests ')
@ApiSecurity('InstanceHandle')
@UseGuards(AccessGuard, MirrorGuard)
@UseInterceptors(FormatResponseInterceptor, InjectorInterceptor)
@UseFilters(AxiosErrorFilter)
@Controller('/v1/follow_requests')
export class MirrorAccountsFollowRequestController {
	/**
	 * GET /api/v1/follow_requests
	 * https://docs.joinmastodon.org/methods/follow_requests/#get
	 *
	 * @query `limit`
	 * @returns `Array<Entity.Account>`
	 */
	@ApiQuery({
		name: 'limit',
		description:
			'Maximum number of results to return. Defaults to 40 accounts. Max 80 accounts',
		type: Number,
	})
	@ApiResponse({
		status: 200,
		description: 'View pending follow requests.',
		type: Entity.Account,
		isArray: true,
	})
	@ApiResponse({
		status: 400,
		description: 'Query parameter "limit" should be a number.',
	})
	@Get(':id')
	async get(@Request() request: RequestMirror, @Query('limit') query_limit: string) {
		const limit = +query_limit;
		if (isNaN(limit)) {
			throw new BadRequestException();
		}

		return request.instance.getFollowRequests(limit);
	}

	/**
	 * POST /api/v1/follow_requests/:account_id/authorize
	 * https://docs.joinmastodon.org/methods/follow_requests/#accept
	 *
	 * @param `id`
	 * @returns `Entity.Relationship`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Accept follow request.',
		type: Entity.Relationship,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 400,
		description: 'Record not found.',
	})
	@Post(':id/authorize')
	async authorize(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.acceptFollowRequest(id);
	}

	/**
	 * POST /api/v1/follow_requests/:account_id/reject
	 * https://docs.joinmastodon.org/methods/follow_requests/#reject
	 *
	 * @param `id`
	 * @returns `Entity.Relationship`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Reject follow request.',
		type: Entity.Relationship,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@Post(':id/reject')
	async reject(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.rejectFollowRequest(id);
	}
}
