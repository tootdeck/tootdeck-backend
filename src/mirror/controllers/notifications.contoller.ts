import {
	BadRequestException,
	Controller,
	Get,
	Param,
	Post,
	Query,
	Request,
	UseFilters,
	UseGuards,
	UseInterceptors,
} from '@nestjs/common';
import { ApiParam, ApiQuery, ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';

import { Entity } from '../../lib/megalodon/entities';

import { AccessGuard } from '../../auth/guards/access.guard';
import { MirrorGuard } from '../guards/mirror.guard';

import { AxiosErrorFilter } from '../filters/AxiosError.filter';

import { FormatResponseInterceptor } from '../interceptors/formatResponse.interceptor';
import { InjectorInterceptor } from '../interceptors/injector.interceptor';

import { Entity as PropertyEntity } from '../properties/entities.property';

import { RequestMirror } from '../types/guards.types';

@ApiTags('Mirror · Notifications')
@ApiSecurity('InstanceHandle')
@UseGuards(AccessGuard, MirrorGuard)
@UseInterceptors(FormatResponseInterceptor, InjectorInterceptor)
@UseFilters(AxiosErrorFilter)
@Controller('/v1/notifications')
export class MirrorNotificationsController {
	/**
	 * GET /api/v1/notifications
	 * https://docs.joinmastodon.org/methods/notifications/#get
	 *
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @query `min_id`
	 * @query `types`
	 * @query `exclude_types`
	 * @query `account_id`
	 * @returns `Array<Entity.Notification>`
	 */
	@ApiQuery({
		name: 'limit',
		required: false,
		description:
			'Maximum number of results to return. Defaults to 20 statuses. Max 40 statuses.',
		type: Number,
	})
	@ApiQuery({
		name: 'max_id',
		required: false,
		description: 'Return results older than ID.',
		type: String,
	})
	@ApiQuery({
		name: 'since_id',
		required: false,
		description: 'Return results newer than ID.',
		type: String,
	})
	@ApiQuery({
		name: 'min_id',
		required: false,
		description: 'Return results immediately newer than ID.',
		type: String,
	})
	@ApiQuery({
		name: 'types',
		required: false,
		description: 'Types to include in the result.',
		type: String,
		isArray: true,
	})
	@ApiQuery({
		name: 'exclude_types',
		required: false,
		description: 'Types to exclude from the results.',
		type: String,
		isArray: true,
	})
	@ApiQuery({
		name: 'account_id',
		required: false,
		description: 'Return only notifications received from the specified account.',
		type: String,
	})
	@ApiResponse({
		status: 200,
		description: 'Get saved timeline positions.',
		type: PropertyEntity.Notification,
		isArray: true,
	})
	@Get()
	async all(@Request() request: RequestMirror, @Query() query: any) {
		const options: {
			limit?: number;
			max_id?: string;
			since_id?: string;
			min_id?: string;
			types?: Array<Entity.NotificationType>;
			exclude_types?: Array<Entity.NotificationType>;
			account_id?: string;
		} = {
			limit: query.limit,
			max_id: query.max_id,
			since_id: query.since_id,
			min_id: query.min_id,
			types: [],
			exclude_types: [],
			account_id: query.account_id,
		};

		const types = query['types[]'] as
			| Entity.NotificationType
			| Array<Entity.NotificationType>
			| undefined;
		if (types) {
			options.types = Array.isArray(types) ? types : [types];
		}

		const exclude_types = query['exclude_types[]'] as
			| Entity.NotificationType
			| Array<Entity.NotificationType>
			| undefined;
		if (exclude_types) {
			options.exclude_types = Array.isArray(exclude_types) ? exclude_types : [exclude_types];
		}

		return request.instance.getNotifications(options);
	}

	/**
	 * GET /api/v1/notifications/:id
	 * https://docs.joinmastodon.org/methods/notifications/#get-one
	 *
	 * @param `id`
	 * @returns `Entity.Notification`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'View information about a notification with a given ID.',
		type: PropertyEntity.Notification,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing IDs in path.',
	})
	@Get(':id')
	async get(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.getNotification(id);
	}

	/**
	 * POST /api/v1/notifications/clear
	 * https://docs.joinmastodon.org/methods/notifications/#clear
	 *
	 * @returns `{}`
	 */
	@ApiResponse({
		status: 200,
		description: 'Clear all notifications from the server.',
	})
	@Post('clear')
	async clear(@Request() request: RequestMirror) {
		return request.instance.dismissNotifications();
	}

	/**
	 * POST /api/v1/notifications/:id/dismiss
	 * https://docs.joinmastodon.org/methods/notifications/#dismiss'
	 *
	 * @param `id`
	 * @returns `{}`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Clear all notifications from the server.',
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@Post(':id/dismiss')
	async dismiss(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.dismissNotification(id);
	}
}
