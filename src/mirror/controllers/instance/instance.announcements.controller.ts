import {
	BadRequestException,
	Controller,
	Delete,
	Get,
	Param,
	Put,
	Request,
	UseFilters,
	UseGuards,
	UseInterceptors,
} from '@nestjs/common';
import { ApiParam, ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';

import { AccessGuard } from '../../../auth/guards/access.guard';
import { MirrorGuard } from '../../guards/mirror.guard';

import { AxiosErrorFilter } from '../../filters/AxiosError.filter';

import { FormatResponseInterceptor } from '../../interceptors/formatResponse.interceptor';
import { InjectorInterceptor } from '../../interceptors/injector.interceptor';

import { Entity } from '../../properties/entities.property';

import { RequestMirror } from '../../types/guards.types';

@ApiTags('Mirror · Instance · Announcements')
@ApiSecurity('InstanceHandle')
@UseGuards(AccessGuard, MirrorGuard)
@UseInterceptors(FormatResponseInterceptor, InjectorInterceptor)
@UseFilters(AxiosErrorFilter)
@Controller('/v1/announcements')
export class MirrorInstanceAnnouncementsController {
	/**
	 * GET /api/v1/announcements
	 * https://docs.joinmastodon.org/methods/announcements/#get
	 *
	 * @returns `Array<Entity.Announcement>`
	 */
	@ApiResponse({
		status: 200,
		description: 'See all currently active announcements set by admins.',
		type: Entity.Announcement,
		isArray: true,
	})
	@Get()
	async get(@Request() request: RequestMirror) {
		return request.instance.getInstanceAnnouncements();
	}

	/**
	 * PUT /api/v1/announcements/:id/reactions/:name
	 * https://docs.joinmastodon.org/methods/announcements/#put-reactions
	 *
	 * @param `id`
	 * @param `name`
	 * @returns `{}`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiParam({
		name: 'name',
		description: 'Unicode emoji, or the shortcode of a custom emoji',
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'React to an announcement with an emoji.',
	})
	@ApiResponse({
		status: 400,
		description: 'Missing path arguments.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@ApiResponse({
		status: 422,
		description: 'Validation failed: Name is not a recognized emoji.',
	})
	@Put(':id/reactions/:name')
	async put(
		@Request() request: RequestMirror,
		@Param('id') id: string,
		@Param('name') name: string,
	) {
		if (!name || !id) {
			throw new BadRequestException();
		}

		return request.instance.createEmojiReaction(id, name);
	}

	/**
	 * DELETE /api/v1/announcements/:id/reactions/:name
	 * https://docs.joinmastodon.org/methods/announcements/#delete-reactions
	 *
	 * @param `id`
	 * @param `name`
	 * @returns `{}`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiParam({
		name: 'name',
		description: 'Unicode emoji, or the shortcode of a custom emoji',
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Undo a react emoji to an announcement.',
	})
	@ApiResponse({
		status: 400,
		description: 'Missing path arguments.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@ApiResponse({
		status: 422,
		description: 'Validation failed: Name is not a recognized emoji.',
	})
	@Delete(':id/reactions/:name')
	async delete(
		@Request() request: RequestMirror,
		@Param('id') id: string,
		@Param('name') name: string,
	) {
		if (!name || !id) {
			throw new BadRequestException();
		}

		return request.instance.deleteEmojiReaction(id, name);
	}
}
