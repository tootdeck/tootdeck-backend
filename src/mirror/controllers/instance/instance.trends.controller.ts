import { Controller, Get, Request, UseFilters, UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';

import { AccessGuard } from '../../../auth/guards/access.guard';
import { MirrorGuard } from '../../guards/mirror.guard';

import { AxiosErrorFilter } from '../../filters/AxiosError.filter';

import { FormatResponseInterceptor } from '../../interceptors/formatResponse.interceptor';
import { InjectorInterceptor } from '../../interceptors/injector.interceptor';

import { Entity } from '../../properties/entities.property';

import { RequestMirror } from '../../types/guards.types';

@ApiTags('Mirror · Instance · Trends')
@ApiSecurity('InstanceHandle')
@UseGuards(AccessGuard, MirrorGuard)
@UseInterceptors(FormatResponseInterceptor, InjectorInterceptor)
@UseFilters(AxiosErrorFilter)
@Controller('/v1/trends')
export class MirrorInstanceTrendsController {
	/**
	 * GET /api/v1/trends
	 * https://docs.joinmastodon.org/methods/trends/#tags
	 *
	 * @deprecated Deprecated endpoint but Megalodon use it
	 * @returns `Array<Entity.Tag>`
	 */
	@ApiResponse({
		status: 200,
		description: 'Tags that are being used more frequently within the past week.',
		type: Entity.Tag,
		isArray: true,
	})
	@Get(['', 'tags'])
	async trends(@Request() request: RequestMirror) {
		return request.instance.getInstanceTrends();
	}

	/**
	 * GET /api/v1/trends/statuses
	 * https://docs.joinmastodon.org/methods/trends/#statuses
	 */
	// @ApiResponse({
	// 	status: 200,
	// 	description: 'Statuses that have been interacted with more than others.',
	// })
	// @Get('statuses')
	// async statuses(@Request() request: RequestMirror) {}

	/**
	 * GET /api/v1/trends/links
	 * https://docs.joinmastodon.org/methods/trends/#links
	 */
	// @ApiResponse({
	// 	status: 200,
	// 	description: 'Statuses that have been interacted with more than others.',
	// })
	// @Get('links')
	// async links(@Request() request: RequestMirror) {}
}
