import {
	BadRequestException,
	Controller,
	Get,
	Query,
	Request,
	UseFilters,
	UseGuards,
	UseInterceptors,
} from '@nestjs/common';
import { ApiQuery, ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';

import { AccessGuard } from '../../auth/guards/access.guard';
import { MirrorGuard } from '../guards/mirror.guard';

import { AxiosErrorFilter } from '../filters/AxiosError.filter';

import { FormatResponseInterceptor } from '../interceptors/formatResponse.interceptor';
import { InjectorInterceptor } from '../interceptors/injector.interceptor';

import { Entity } from '../properties/entities.property';

import { RequestMirror } from '../types/guards.types';

@ApiTags('Mirror · Search')
@ApiSecurity('InstanceHandle')
@UseGuards(AccessGuard, MirrorGuard)
@UseInterceptors(FormatResponseInterceptor, InjectorInterceptor)
@UseFilters(AxiosErrorFilter)
@Controller()
export class MirrorSearchController {
	/**
	 * GET /api/v2/search
	 * https://docs.joinmastodon.org/methods/search/#v2
	 *
	 * @query `q`
	 * @query `types`
	 * @query `max_id`
	 * @query `min_id`
	 * @query `limit`
	 * @returns `Entity.Results`
	 */
	@ApiQuery({
		name: 'q',
		required: true,
		description: 'The search query.',
		type: String,
	})
	@ApiQuery({
		name: 'types',
		required: false,
		description: 'Specify whether to search for only accounts, hashtags, statuses.',
		type: String,
	})
	@ApiQuery({
		name: 'resolve',
		required: false,
		description: 'Attempt WebFinger lookup? Defaults to false.',
		type: String,
	})
	@ApiQuery({
		name: 'following',
		required: false,
		description: 'Only include accounts that the user is following? Defaults to false.',
		type: String,
	})
	@ApiQuery({
		name: 'account_id',
		required: false,
		description: 'If provided, will only return statuses authored by this account.',
		type: String,
	})
	@ApiQuery({
		name: 'exclude_unreviewed',
		required: false,
		description:
			'Filter out unreviewed tags? Defaults to false. Use true when trying to find trending tags.',
		type: Boolean,
	})
	@ApiQuery({
		name: 'max_id',
		required: false,
		description: 'Return results older than ID.',
		type: String,
	})
	@ApiQuery({
		name: 'min_id',
		required: false,
		description: 'Return results immediately newer than ID.',
		type: String,
	})
	@ApiQuery({
		name: 'limit',
		required: false,
		description:
			'Maximum number of results to return. Defaults to 20 statuses. Max 40 statuses.',
		type: Number,
	})
	@ApiQuery({
		name: 'offset',
		required: false,
		description: 'Skip the first n results.',
		type: Number,
	})
	@ApiResponse({
		status: 200,
		description: 'Perform a search.',
		type: Entity.Results,
	})
	@ApiResponse({
		status: 400.1,
		description: 'Missing query.',
	})
	@ApiResponse({
		status: 400.2,
		description: 'Invalid query type, only accounts, hashtags and statuses are supported.',
	})
	@Get('v2/search')
	async all(@Request() request: RequestMirror, @Query() query: any) {
		const q = query.q;
		const type = query.type;

		if (!q) {
			throw new BadRequestException('Missing query.');
		}

		if (type && !['accounts', 'hashtags', 'statuses'].includes(type)) {
			throw new BadRequestException(
				'Invalid query type, only accounts, hashtags and statuses are supported.',
			);
		}

		const options = {
			limit: query.limit,
			max_id: query.max_id,
			min_id: query.min_id,
			resolve: query.resolve,
			offset: query.offset,
			following: query.following,
			account_id: query.account_id,
			exclude_unreviewed: query.exclude_unreviewed,
		};

		return request.instance.search(q, type, options);
	}
}
