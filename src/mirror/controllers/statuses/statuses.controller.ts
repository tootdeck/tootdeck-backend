import {
	BadRequestException,
	Body,
	Controller,
	Delete,
	Get,
	Param,
	Post,
	Put,
	Request,
	UseFilters,
	UseGuards,
	UseInterceptors,
} from '@nestjs/common';
import { ApiBody, ApiParam, ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';

import { AccessGuard } from '../../../auth/guards/access.guard';
import { MirrorGuard } from '../../guards/mirror.guard';

import { AxiosErrorFilter } from '../../filters/AxiosError.filter';

import { FormatResponseInterceptor } from '../../interceptors/formatResponse.interceptor';
import { InjectorInterceptor } from '../../interceptors/injector.interceptor';

import { validateBody } from '../../utils/validateBody';

import { Entity as MastodonEntity } from '../../../lib/megalodon/entities';
import { Entity } from '../../properties/entities.property';
import { Body as FormBody } from '../../properties/body.property';

import { RequestMirror } from '../../types/guards.types';

@ApiTags('Mirror · Statuses')
@ApiSecurity('InstanceHandle')
@UseGuards(AccessGuard, MirrorGuard)
@UseInterceptors(FormatResponseInterceptor, InjectorInterceptor)
@UseFilters(AxiosErrorFilter)
@Controller('/v1/statuses')
export class MirrorStatusesController {
	/**
	 * GET /api/v1/statuses/:id
	 * https://docs.joinmastodon.org/methods/statuses/#get
	 *
	 * @param `id`
	 * @returns `Entity.Status`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Obtain information about a status.',
		type: Entity.Status,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@Get(':id')
	async get(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.getStatus(id);
	}

	/**
	 * GET /api/v1/statuses/:id/context
	 * https://docs.joinmastodon.org/methods/statuses/#context
	 *
	 * @param `id`
	 * @returns `Entity.Context`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'View statuses above and below this status in the thread.',
		type: Entity.Context,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@Get(':id/context')
	async context(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.getStatusContext(id);
	}

	/**
	 * GET /api/v1/statuses/:id/reblogged_by
	 * https://docs.joinmastodon.org/methods/statuses/#reblogged_by
	 *
	 * @param `id`
	 * @returns `Array<Entity.Account>`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'View who boosted a given status.',
		type: Entity.Account,
		isArray: true,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@Get(':id/reblogged_by')
	async reblogged_by(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.getStatusRebloggedBy(id);
	}

	/**
	 * GET /api/v1/statuses/:id/favourited_by
	 * https://docs.joinmastodon.org/methods/statuses/#favourited_by
	 *
	 * @param `id`
	 * @returns `Array<Entity.Account>`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'View who favourited a given status.',
		type: Entity.Account,
		isArray: true,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@Get(':id/favourited_by')
	async favourited_by(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.getStatusFavouritedBy(id);
	}

	/**
	 * GET /api/v1/statuses/:id/history
	 * https://docs.joinmastodon.org/methods/statuses/#history
	 *
	 * @param `id`
	 * @returns `Entity.StatusEdit`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description:
			'Get all known versions of a status, including the initial and current states.',
		type: Entity.Status,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@Get(':id/history')
	async getHistory(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.getHistory(id);
	}

	/**
	 * GET /api/v1/statuses/:id/source
	 * https://docs.joinmastodon.org/methods/statuses/#source
	 *
	 * @param `id`
	 * @returns `Entity.StatusSource`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Obtain the source properties for a status so that it can be edited.',
		type: Entity.StatusSource,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@Get(':id/source')
	async source(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.getStatusSource(id);
	}

	/**
	 * POST /api/v1/statuses
	 * https://docs.joinmastodon.org/methods/statuses/#create
	 *
	 * @body `FormBody.Status`
	 * @returns `Entity.Status`
	 * @returns `Entity.ScheduledStatus`
	 */
	@ApiBody({
		type: FormBody.Status,
		required: true,
		examples: {
			['Create a status']: {
				value: {
					status: 'Test',
					scheduled_at: null,
				},
			},
			['Create a scheduled status']: {
				value: {
					status: 'Scheduled test',
					scheduled_at: new Date(),
				},
			},
		},
	})
	@ApiResponse({
		status: 200.1,
		description: 'Publish a status with the given parameters.',
		type: Entity.Status,
	})
	@ApiResponse({
		status: 200.2,
		description: 'Publish a scheduled status with the given parameters.',
		type: Entity.ScheduledStatus,
	})
	@ApiResponse({
		status: 400,
		description: 'Empty body.',
	})
	@ApiResponse({
		status: 422,
		description: "Validation failed: Text can't be blank",
	})
	@Post()
	async post(@Request() request: RequestMirror, @Body() body: FormBody.Status) {
		if (!body.status && !validateBody(body)) {
			throw new BadRequestException();
		}

		const { status: text, ...filtered } = body;
		const options = {
			media_ids: filtered.media_ids,
			in_reply_to_id: filtered.in_reply_to_id,
			sensitive: filtered.sensitive,
			spoiler_text: filtered.spoiler_text,
			visibility: filtered.visibility,
			scheduled_at: filtered.scheduled_at,
			language: filtered.language,
			quote_id: filtered.quote_id,
		} as any;

		let poll: any;
		if (filtered.poll) {
			poll = {
				options: filtered.poll?.options,
				expires_in: filtered.poll?.expires_in,
				multiple: filtered.poll?.multiple,
				hide_totals: filtered.poll?.hide_totals,
			};
		}

		return request.instance.postStatus(text, { ...options, poll });
	}

	/**
	 * POST /api/v1/statuses/:id/translate
	 * https://docs.joinmastodon.org/methods/statuses/#translate
	 *
	 * @param `id`
	 * @returns `Entity`
	 */
	// @ApiParam({
	// 	name: 'id',
	// 	type: String,
	// 	required: true,
	// })
	// @ApiResponse({
	// 	status: 200,
	// 	description: 'Translate the status content into some language.',
	// 	type: Entity.StatusSource,
	// })
	// @ApiResponse({
	// 	status: 400,
	// 	description: 'Missing ID in path.',
	// })
	// @ApiResponse({
	// 	status: 404,
	// 	description: 'Record not found.',
	// })
	// @Get(':id/translate')
	// async translate(@Request() request: RequestMirror, @Param('id') id: string) {
	// 	if (!id) {
	// 		throw new BadRequestException();
	// 	}
	// }

	/**
	 * POST /api/v1/statuses/:id/favourite
	 * https://docs.joinmastodon.org/methods/statuses/#favourite
	 *
	 * @param `id`
	 * @returns `Entity.Status`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Add a status to your favourites list.',
		type: Entity.Status,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@Post(':id/favourite')
	async favourite(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.favouriteStatus(id);
	}

	/**
	 * POST /api/v1/statuses/:id/unfavourite
	 * https://docs.joinmastodon.org/methods/statuses/#unfavourite
	 *
	 * @param `id`
	 * @returns `Entity.Status`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Remove a status from your favourites list.',
		type: Entity.Status,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@Post(':id/unfavourite')
	async unfavourite(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.unfavouriteStatus(id);
	}

	/**
	 * POST /api/v1/statuses/:id/reblog
	 * https://docs.joinmastodon.org/methods/statuses/#reblog
	 *
	 * @param `id`
	 * @returns `Entity.Status`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiBody({
		type: FormBody.Reblog,
		required: false,
	})
	@ApiResponse({
		status: 200,
		description: 'Reshare a status on your own profile.',
		type: Entity.Status,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@Post(':id/reblog')
	async reblog(
		@Request() request: RequestMirror,
		@Param('id') id: string,
		@Body('visibility')
		visibility?: Exclude<MastodonEntity.Visibility, MastodonEntity.Visibility.Direct>,
	) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.reblogStatus(id, visibility);
	}

	/**
	 * POST /api/v1/statuses/:id/unreblog
	 * https://docs.joinmastodon.org/methods/statuses/#unreblog
	 *
	 * @param `id`
	 * @returns `Entity.Status`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Undo a reshare of a status.',
		type: Entity.Status,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@Post(':id/unreblog')
	async unreblog(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.unreblogStatus(id);
	}

	/**
	 * POST /api/v1/statuses/:id/bookmark
	 * https://docs.joinmastodon.org/methods/statuses/#bookmark
	 *
	 * @param `id`
	 * @returns `Entity.Status`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Privately bookmark a status.',
		type: Entity.Status,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@Post(':id/bookmark')
	async bookmark(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.bookmarkStatus(id);
	}

	/**
	 * POST /api/v1/statuses/:id/unbookmark
	 * https://docs.joinmastodon.org/methods/statuses/#unbookmark
	 *
	 * @param `id`
	 * @returns `Entity.Status`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Remove a status from your private bookmarks.',
		type: Entity.Status,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@Post(':id/unbookmark')
	async unbookmark(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.unbookmarkStatus(id);
	}

	/**
	 * POST /api/v1/statuses/:id/mute
	 * https://docs.joinmastodon.org/methods/statuses/#mute
	 *
	 * @param `id`
	 * @returns `Entity.Status`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description:
			'Do not receive notifications for the thread that this status is part of. Must be a thread in which you are a participant.',
		type: Entity.Status,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@Post(':id/mute')
	async mute(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.muteStatus(id);
	}

	/**
	 * POST /api/v1/statuses/:id/unmute
	 * https://docs.joinmastodon.org/methods/statuses/#unmute
	 *
	 * @param `id`
	 * @returns `Entity.Status`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description:
			'Start receiving notifications again for the thread that this status is part of.',
		type: Entity.Status,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@Post(':id/unmute')
	async unmute(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.unmuteStatus(id);
	}

	/**
	 * POST /api/v1/statuses/:id/pin
	 * https://docs.joinmastodon.org/methods/statuses/#pin
	 *
	 * @param `id`
	 * @returns `Entity.Status`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Feature one of your own public statuses at the top of your profile.',
		type: Entity.Status,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@ApiResponse({
		status: 422,
		description: "Validation failed: Someone else's post cannot be pinned",
	})
	@Post(':id/pin')
	async pin(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.pinStatus(id);
	}

	/**
	 * POST /api/v1/statuses/:id/unpin
	 * https://docs.joinmastodon.org/methods/statuses/#unpin
	 *
	 * @param `id`
	 * @returns `Entity.Status`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Unfeature a status from the top of your profile.',
		type: Entity.Status,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@Post(':id/unpin')
	async unpin(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.unpinStatus(id);
	}

	/**
	 * DELETE /api/v1/statuses/:id
	 * https://docs.joinmastodon.org/methods/statuses/#delete
	 *
	 * @param `id`
	 * @returns `Entity.Status`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Delete one of your own statuses.',
		type: Entity.Status,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@Delete(':id')
	async delete(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.deleteStatus(id);
	}

	/**
	 * PUT /api/v1/statuses/:id
	 * https://docs.joinmastodon.org/methods/statuses/#edit
	 *
	 * @param `id`
	 * @body `FormBody.Status`
	 * @returns `Entity.Status`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiBody({
		type: FormBody.Status,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description:
			'Edit a given status to change its text, sensitivity, media attachments, or poll. Note that editing a poll’s options will reset the votes.',
		type: Entity.Status,
	})
	@ApiResponse({
		status: 400.1,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 400.2,
		description: 'Empty body.',
	})
	@ApiResponse({
		status: 400.3,
		description: "Status text can't be blank.",
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@ApiResponse({
		status: 422,
		description: "Validation failed: Text can't be blank",
	})
	@Put(':id')
	async edit(
		@Request() request: RequestMirror,
		@Param('id') id: string,
		@Body() body: FormBody.Status,
	) {
		if (!id || !validateBody(body)) {
			throw new BadRequestException();
		}

		const options = {
			status: body.status,
			spoiler_text: body.spoiler_text,
			sensitive: body.sensitive,
			media_attributes: body.media_attributes,
			media_ids: body.media_ids,
			language: body.language,
			// scheduled_at: body.scheduled_at,
			// quote_id: body.quote_id,
		} as any;

		let poll: any;
		if (body.poll) {
			poll = {
				options: body.poll?.options,
				expires_in: body.poll?.expires_in,
				multiple: body.poll?.multiple,
				hide_totals: body.poll?.hide_totals,
			};
		}

		return request.instance.editStatus(id, { ...options, poll });
	}

	/**
	 * POST /api/v1/statuses/:id/react/:name
	 *
	 * @param `id`
	 * @param `name`
	 * @returns `Entity.Status`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiParam({
		name: 'name',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Add an emoji reaction to the targeted status.',
		type: Entity.Status,
	})
	@ApiResponse({
		status: 400.1,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 400.2,
		description: 'Missing name in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@Post(':id/react/:name')
	async react(
		@Request() request: RequestMirror,
		@Param('id') id: string,
		@Param('name') name: string,
	) {
		if (!id || !name) {
			throw new BadRequestException();
		}

		return request.instance.createEmojiReaction(id, name);
	}

	/**
	 * POST /api/v1/statuses/:id/unreact/:name
	 *
	 * @param `id`
	 * @param `name`
	 * @returns `Entity.Status`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiParam({
		name: 'name',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Remove an emoji reaction of the targeted status.',
		type: Entity.Status,
	})
	@ApiResponse({
		status: 400.1,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 400.2,
		description: 'Missing name in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@ApiResponse({
		status: 422,
		description: 'Validation failed: reaction limit reached for this status.',
	})
	@Post(':id/unreact/:name')
	async unreact(
		@Request() request: RequestMirror,
		@Param('id') id: string,
		@Param('name') name: string,
	) {
		if (!id || !name) {
			throw new BadRequestException();
		}

		return request.instance.deleteEmojiReaction(id, name);
	}
}
