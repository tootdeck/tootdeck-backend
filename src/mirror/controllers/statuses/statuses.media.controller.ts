import {
	BadRequestException,
	Body,
	Controller,
	Get,
	InternalServerErrorException,
	Param,
	Post,
	Put,
	Request,
	UploadedFile,
	UseFilters,
	UseGuards,
	UseInterceptors,
} from '@nestjs/common';
import { ApiBody, ApiConsumes, ApiParam, ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';

import Mastodon from '../../../lib/megalodon/mastodon/mastodon';
import Pleroma from '../../../lib/megalodon/pleroma/pleroma';

import { AccessGuard } from '../../../auth/guards/access.guard';
import { MirrorGuard } from '../../guards/mirror.guard';

import { AxiosErrorFilter } from '../../filters/AxiosError.filter';

import { FormatResponseInterceptor } from '../../interceptors/formatResponse.interceptor';
import { InjectorInterceptor } from '../../interceptors/injector.interceptor';
import { FastifyFileInterceptor } from '../../interceptors/FastifyFile.Interceptor';

import { detectInterfaceInstance, mastodonUpload, pleromaUpload } from '../../utils/uploadMedia';
import { validateBody } from '../../utils/validateBody';

import { Entity } from '../../properties/entities.property';
import { FileBody, Body as FormBody } from '../../properties/body.property';

import { RequestMirror } from '../../types/guards.types';
import { InstanceType } from '../../../database/types';

@ApiTags('Mirror · Statuses · Media')
@ApiSecurity('InstanceHandle')
@UseGuards(AccessGuard, MirrorGuard)
@UseInterceptors(FormatResponseInterceptor, InjectorInterceptor)
@UseFilters(AxiosErrorFilter)
@Controller()
export class MirrorStatusesMediaController {
	/**
	 * POST /api/v2/media
	 * https://docs.joinmastodon.org/methods/media/#v2
	 *
	 * @body `FileBody`
	 * @file 'file
	 * @returns `Entity.Attachment`
	 * @returns `Entity.AsyncAttachment`
	 */
	@ApiConsumes('multipart/form-data')
	@ApiBody(FileBody)
	@ApiResponse({
		status: 200,
		description:
			'Creates a media attachment to be used with a new status. The full sized media will be processed asynchronously in the background for large uploads.',
		type: Entity.Attachment,
	})
	@ApiResponse({
		status: 202,
		description:
			'MediaAttachment was created successfully, but the full-size file is still processing.',
		type: Entity.AsyncAttachment,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing file in body.',
	})
	@ApiResponse({
		status: 422,
		description: 'Validation failed: File content type is invalid, File is invalid.',
	})
	@ApiResponse({
		status: 500,
		description: 'Error processing thumbnail for uploaded media.',
	})
	@UseInterceptors(FastifyFileInterceptor('file'))
	@Post('/v2/media')
	async upload(
		@Request() request: RequestMirror,
		@Body() body: FormBody.UploadMedia,
		@UploadedFile('file') file: Express.Multer.File,
	) {
		if (!file) {
			throw new BadRequestException();
		}

		const options = {
			description: body?.description,
			focus: body?.focus,
		} as any;

		switch (detectInterfaceInstance(request.instance)) {
			case InstanceType.Mastodon:
				return mastodonUpload((request.instance as Mastodon).client, file, options);
			case InstanceType.Pleroma:
				return pleromaUpload((request.instance as Pleroma).client, file, options);
			// case InstanceType.Misskey:
			// return misskeyUpload((request.instance as Misskey).client, file, options);
			default:
				throw new InternalServerErrorException();
		}
	}

	/**
	 * GET /api/v1/media/:id
	 * https://docs.joinmastodon.org/methods/media/#get
	 *
	 * @param `id`
	 * @returns `Entity.Attachment`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description:
			'Get a media attachment, before it is attached to a status and posted, but after it is accepted for processing. Use this method to check that the full-sized media has finished processing.',
		type: Entity.Attachment,
	})
	@ApiResponse({
		status: 206,
		description: 'The media attachment is still being processed.',
	})
	@ApiResponse({
		status: 400,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@ApiResponse({
		status: 422,
		description: 'Validation failed: File content type is invalid, File is invalid.',
	})
	@Get('/v1/media/:id')
	async get(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.getMedia(id);
	}

	/**
	 * PUT /api/v1/media/:id
	 * https://docs.joinmastodon.org/methods/media/#update
	 *
	 * @param `id`
	 * @body `FormBody.UpdateMedia`
	 * @returns `Entity.Attachment`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiBody({
		type: FormBody.UpdateMedia,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description:
			'Get a media attachment, before it is attached to a status and posted, but after it is accepted for processing. Use this method to check that the full-sized media has finished processing.',
		type: Entity.Attachment,
	})
	@ApiResponse({
		status: 206,
		description: 'The media attachment is still being processed.',
	})
	@ApiResponse({
		status: 400.1,
		description: 'Missing ID in path.',
	})
	@ApiResponse({
		status: 400.2,
		description: 'Empty body.',
	})
	@ApiResponse({
		status: 404,
		description: 'Record not found.',
	})
	@ApiResponse({
		status: 422,
		description: 'Validation failed: File content type is invalid, File is invalid.',
	})
	@Put('/v1/media/:id')
	async edit(
		@Request() request: RequestMirror,
		@Param('id') id: string,
		@Body() body: FormBody.UpdateMedia,
	) {
		if (!id || !validateBody(body)) {
			throw new BadRequestException();
		}

		const options = {
			description: body.description,
			focus: body.focus,
			is_sensitive: body.is_sensitive,
		} as any;

		return request.instance.updateMedia(id, options);
	}
}
