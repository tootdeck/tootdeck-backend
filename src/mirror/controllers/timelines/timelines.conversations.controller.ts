import {
	BadRequestException,
	Controller,
	Delete,
	Get,
	Param,
	Query,
	Request,
	UseFilters,
	UseGuards,
	UseInterceptors,
} from '@nestjs/common';
import { ApiParam, ApiQuery, ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';

import { AccessGuard } from '../../../auth/guards/access.guard';
import { MirrorGuard } from '../../guards/mirror.guard';

import { AxiosErrorFilter } from '../../filters/AxiosError.filter';

import { FormatResponseInterceptor } from '../../interceptors/formatResponse.interceptor';
import { InjectorInterceptor } from '../../interceptors/injector.interceptor';

import { Entity } from '../../properties/entities.property';

import { RequestMirror } from '../../types/guards.types';

@ApiTags('Mirror · Timelines · Conversations')
@ApiSecurity('InstanceHandle')
@UseGuards(AccessGuard, MirrorGuard)
@UseInterceptors(FormatResponseInterceptor, InjectorInterceptor)
@UseFilters(AxiosErrorFilter)
@Controller('/v1/conversations')
export class MirrorTimelinesConversationsController {
	/**
	 * GET /api/v1/conversations
	 * https://docs.joinmastodon.org/methods/conversations/#get
	 *
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @query `min_id`
	 * @returns `Array<Entity.Conversation>`
	 */
	@ApiQuery({
		name: 'limit',
		required: false,
		description:
			'Maximum number of results to return. Defaults to 20 statuses. Max 40 statuses.',
		type: Number,
	})
	@ApiQuery({
		name: 'max_id',
		required: false,
		description: 'Return results older than ID.',
		type: String,
	})
	@ApiQuery({
		name: 'since_id',
		required: false,
		description: 'Return results newer than ID.',
		type: String,
	})
	@ApiQuery({
		name: 'min_id',
		required: false,
		description: 'Return results immediately newer than ID.',
		type: String,
	})
	@ApiResponse({
		status: 200,
		description: 'View all conversations.',
		type: Entity.Conversation,
		isArray: true,
	})
	@Get()
	async all(@Request() request: RequestMirror, @Query() query: any) {
		const options = {
			limit: query.limit,
			max_id: query.max_id,
			since_id: query.since_id,
			min_id: query.min_id,
		};
		return request.instance.getConversationTimeline(options);
	}

	/**
	 * DELETE /api/v1/conversations/:id
	 * https://docs.joinmastodon.org/methods/conversations/#delete
	 *
	 * @params `id`
	 * @returns `{}`
	 */
	@ApiParam({
		name: 'id',
		type: String,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'View all conversations.',
	})
	@ApiResponse({
		status: 400,
		description: 'Missing id in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'The conversation does not exist, or is not owned by you.',
	})
	@Delete(':id')
	async delete(@Request() request: RequestMirror, @Param('id') id: string) {
		if (!id) {
			throw new BadRequestException();
		}

		return request.instance.deleteConversation(id);
	}
}
