import {
	BadRequestException,
	Controller,
	Get,
	Param,
	Query,
	Request,
	UseFilters,
	UseGuards,
	UseInterceptors,
} from '@nestjs/common';
import { ApiParam, ApiQuery, ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';

import { AccessGuard } from '../../../auth/guards/access.guard';
import { MirrorGuard } from '../../guards/mirror.guard';

import { AxiosErrorFilter } from '../../filters/AxiosError.filter';

import { FormatResponseInterceptor } from '../../interceptors/formatResponse.interceptor';
import { InjectorInterceptor } from '../../interceptors/injector.interceptor';

import { Entity } from '../../properties/entities.property';

import { RequestMirror } from '../../types/guards.types';

@ApiTags('Mirror · Timelines')
@ApiSecurity('InstanceHandle')
@UseGuards(AccessGuard, MirrorGuard)
@UseInterceptors(FormatResponseInterceptor, InjectorInterceptor)
@UseFilters(AxiosErrorFilter)
@Controller('/v1/timelines')
export class MirrorTimelinesController {
	/**
	 * GET /api/v1/timelines/public
	 * https://docs.joinmastodon.org/methods/timelines/#public
	 *
	 * @query `local`
	 * @query `remote`
	 * @query `only_media`
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @query `min_id`
	 * @returns `Array<Entity.Status>`
	 */
	@ApiQuery({
		name: 'local',
		required: false,
		description: 'Show only local statuses.',
		type: Boolean,
	})
	@ApiQuery({
		name: 'only_media',
		required: false,
		description: 'Show only statuses with media attached.',
		type: Boolean,
	})
	@ApiQuery({
		name: 'limit',
		required: false,
		description:
			'Maximum number of results to return. Defaults to 20 statuses. Max 40 statuses.',
		type: Number,
	})
	@ApiQuery({
		name: 'max_id',
		required: false,
		description: 'Return results older than ID.',
		type: String,
	})
	@ApiQuery({
		name: 'since_id',
		required: false,
		description: 'Return results newer than ID.',
		type: String,
	})
	@ApiQuery({
		name: 'min_id',
		required: false,
		description: 'Return results immediately newer than ID.',
		type: String,
	})
	@ApiResponse({
		status: 200,
		description: 'View statuses from public timeline.',
		type: Entity.Status,
		isArray: true,
	})
	@Get('public')
	async public(@Request() request: RequestMirror, @Query() query: any) {
		const options = {
			only_media: query.only_media,
			limit: query.limit,
			max_id: query.max_id,
			since_id: query.since_id,
			min_id: query.min_id,
		};
		if (query.local === 'true') {
			return request.instance.getLocalTimeline(options);
		}
		return request.instance.getPublicTimeline(options);
	}

	/**
	 * GET /api/v1/timelines/home
	 * https://docs.joinmastodon.org/methods/timelines/#home
	 *
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @query `min_id`
	 * @returns `Array<Entity.Status>`
	 */
	@ApiQuery({
		name: 'limit',
		required: false,
		description:
			'Maximum number of results to return. Defaults to 20 statuses. Max 40 statuses.',
		type: Number,
	})
	@ApiQuery({
		name: 'max_id',
		required: false,
		description: 'Return results older than ID.',
		type: String,
	})
	@ApiQuery({
		name: 'since_id',
		required: false,
		description: 'Return results newer than ID.',
		type: String,
	})
	@ApiQuery({
		name: 'min_id',
		required: false,
		description: 'Return results immediately newer than ID.',
		type: String,
	})
	@ApiResponse({
		status: 200,
		description: 'View statuses from home timeline.',
		type: Entity.Status,
		isArray: true,
	})
	@Get('home')
	async home(@Request() request: RequestMirror, @Query() query: any) {
		const options = {
			local: query.local,
			limit: query.limit,
			max_id: query.max_id,
			since_id: query.since_id,
			min_id: query.min_id,
		};
		return request.instance.getHomeTimeline(options);
	}

	/**
	 * GET /api/v1/timelines/tag/:hashtag
	 * https://docs.joinmastodon.org/methods/timelines/#tag
	 *
	 * @param `hashtag`
	 * @query `local`
	 * @query `only_media`
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @query `min_id`
	 * @returns `Array<Entity.Status>`
	 */
	@ApiParam({
		name: 'hashtag',
		description: 'The name of the hashtag.',
		required: true,
		type: String,
	})
	@ApiQuery({
		name: 'local',
		required: false,
		description: 'Show only local statuses.',
		type: Boolean,
	})
	@ApiQuery({
		name: 'only_media',
		required: false,
		description: 'Show only statuses with media attached.',
		type: Boolean,
	})
	@ApiQuery({
		name: 'limit',
		required: false,
		description:
			'Maximum number of results to return. Defaults to 20 statuses. Max 40 statuses.',
		type: Number,
	})
	@ApiQuery({
		name: 'max_id',
		required: false,
		description: 'Return results older than ID.',
		type: String,
	})
	@ApiQuery({
		name: 'since_id',
		required: false,
		description: 'Return results newer than ID.',
		type: String,
	})
	@ApiQuery({
		name: 'min_id',
		required: false,
		description: 'Return results immediately newer than ID.',
		type: String,
	})
	@ApiResponse({
		status: 200,
		description: 'View public statuses containing the given hashtag.',
		type: Entity.Status,
		isArray: true,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing hashtag in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'Hashtag does not exist.',
	})
	@Get('tag/:hashtag')
	async tag(
		@Request() request: RequestMirror,
		@Param('hashtag') hashtag: string,
		@Query() query: any,
	) {
		if (!hashtag) {
			throw new BadRequestException();
		}

		const options = {
			local: query.local,
			only_media: query.only_media,
			limit: query.limit,
			max_id: query.max_id,
			since_id: query.since_id,
			min_id: query.min_id,
		};
		return request.instance.getTagTimeline(hashtag, options);
	}

	/**
	 * GET /api/v1/timelines/list/:list_id
	 * https://docs.joinmastodon.org/methods/timelines/#list
	 *
	 * @param `list_id`
	 * @query `limit`
	 * @query `max_id`
	 * @query `since_id`
	 * @query `min_id`
	 * @returns `Array<Entity.Status>`
	 */
	@ApiParam({
		name: 'list_id',
		description: 'Local ID of the List in the database.',
		required: true,
		type: String,
	})
	@ApiQuery({
		name: 'limit',
		required: false,
		description:
			'Maximum number of results to return. Defaults to 20 statuses. Max 40 statuses.',
		type: Number,
	})
	@ApiQuery({
		name: 'max_id',
		required: false,
		description: 'Return results older than ID.',
		type: String,
	})
	@ApiQuery({
		name: 'since_id',
		required: false,
		description: 'Return results newer than ID.',
		type: String,
	})
	@ApiQuery({
		name: 'min_id',
		required: false,
		description: 'Return results immediately newer than ID.',
		type: String,
	})
	@ApiResponse({
		status: 200,
		description: 'View statuses in the given list timeline.',
		type: Entity.Status,
		isArray: true,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing list_id in path.',
	})
	@ApiResponse({
		status: 404,
		description: 'List is not owned by you or does not exist.',
	})
	@Get('list/:list_id')
	async list(
		@Request() request: RequestMirror,
		@Param('list_id') list_id: string,
		@Query() query: any,
	) {
		if (!list_id) {
			throw new BadRequestException();
		}

		const options = {
			limit: query.limit,
			max_id: query.max_id,
			since_id: query.since_id,
			min_id: query.min_id,
		};
		return request.instance.getListTimeline(list_id, options);
	}
}
