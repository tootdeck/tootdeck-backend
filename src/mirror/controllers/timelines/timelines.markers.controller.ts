import {
	BadRequestException,
	Body,
	Controller,
	Get,
	Post,
	Query,
	Request,
	UseFilters,
	UseGuards,
	UseInterceptors,
} from '@nestjs/common';
import { ApiBody, ApiQuery, ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';

import { AccessGuard } from '../../../auth/guards/access.guard';
import { MirrorGuard } from '../../guards/mirror.guard';

import { AxiosErrorFilter } from '../../filters/AxiosError.filter';

import { FormatResponseInterceptor } from '../../interceptors/formatResponse.interceptor';
import { InjectorInterceptor } from '../../interceptors/injector.interceptor';

import { validateBody } from '../../utils/validateBody';

import { Entity } from '../../properties/entities.property';
import { Body as FormBody } from '../../properties/body.property';

import { RequestMirror } from '../../types/guards.types';

@ApiTags('Mirror · Timelines · Markers')
@ApiSecurity('InstanceHandle')
@UseGuards(AccessGuard, MirrorGuard)
@UseInterceptors(FormatResponseInterceptor, InjectorInterceptor)
@UseFilters(AxiosErrorFilter)
@Controller('/v1/markers')
export class MirrorTimelinesMarkersController {
	/**
	 * GET /api/v1/markers
	 * https://docs.joinmastodon.org/methods/markers/#get
	 *
	 * @query `timeline`
	 * @returns `Entity.Marker`
	 */
	@ApiQuery({
		name: 'timeline',
		required: true,
		description:
			'Specify the timeline(s) for which markers should be fetched. Possible values: home, notifications.',
		type: String,
	})
	@ApiResponse({
		status: 200,
		description: 'Get saved timeline positions.',
		type: Entity.Marker,
	})
	@ApiResponse({
		status: 400,
		description: 'Timeline not specified.',
	})
	@Get()
	async get(@Request() request: RequestMirror, @Query('timeline') timeline: string[]) {
		if (!timeline || !(timeline instanceof Array) || !timeline.length) {
			throw new BadRequestException();
		}

		return request.instance.getMarkers(timeline);
	}

	/**
	 * POST /api/v1/markers
	 * https://docs.joinmastodon.org/methods/markers/#create
	 *
	 * @body `Body.Marker`
	 * @returns `Entity.Marker`
	 */
	@ApiBody({
		type: FormBody.Marker,
		required: true,
	})
	@ApiResponse({
		status: 200,
		description: 'Save your position in a timeline.',
		type: Entity.Marker,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing body.',
	})
	@ApiResponse({
		status: 409,
		description: 'If object is stale while being updated, an error will occur.',
	})
	@Post()
	async save(@Request() request: RequestMirror, @Body() body: FormBody.Marker) {
		if (!validateBody(body) || !body.home?.last_read_id || !body.notifications?.last_read_id) {
			throw new BadRequestException();
		}

		const options = {
			home: undefined,
			notifications: undefined,
		} as any;

		if (body.home) {
			options.home = { last_read_id: body.home.last_read_id };
		}

		if (body.notifications) {
			options.notifications = { last_read_id: body.notifications.last_read_id };
		}

		return request.instance.saveMarkers(options);
	}
}
