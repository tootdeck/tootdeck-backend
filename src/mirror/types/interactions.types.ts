export interface CachedStatusInteractionsResponse {
	replies_count: number;
	reblogs_count: number;
	favourites_count: number;
}
