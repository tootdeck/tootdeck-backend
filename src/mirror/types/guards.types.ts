import { MegalodonInterface } from '../../lib/megalodon/megalodon';

import { RequestAccessGuard } from '../../auth/types/guards.types';
import { AccountCache } from '../../user/types/cache.types';

export interface RequestMirror extends RequestAccessGuard {
	account: AccountCache;
	instance: MegalodonInterface;
}
