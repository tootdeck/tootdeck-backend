import {
	CanActivate,
	ExecutionContext,
	ForbiddenException,
	Injectable,
	InternalServerErrorException,
} from '@nestjs/common';
import { HttpService } from '@nestjs/axios';

import MegalodonGenerator from '../../lib/megalodon/megalodon';

import { UserCacheService } from '../../user/services/user.cache.service';

import { Logger } from '../../utils/logger';
import { findAccountInUser } from '../../utils/findAccountInUser';
import { parseHandle } from '../../utils/parseHandle';

import { RequestAccessGuard } from '../../auth/types/guards.types';
import { InstanceHandle } from '../../utils/handle.types';
import { RequestMirror } from '../types/guards.types';

@Injectable()
export class MirrorGuard implements CanActivate {
	private readonly logger = new Logger(MirrorGuard.name);

	constructor(
		private readonly UserCacheService: UserCacheService,
		private readonly HttpService: HttpService,
	) {}

	/**
	 * @throws UnauthorizedException
	 * @throws InternalServerErrorException
	 */
	async canActivate(context: ExecutionContext): Promise<boolean> {
		const request = context.switchToHttp().getRequest() as RequestAccessGuard;

		if (!request.access_session) {
			this.logger.error(null, 'Guard is called without AccessGuard');
			throw new InternalServerErrorException();
		}

		const raw_handle = request.headers['handle'] as string;
		let handle: InstanceHandle;
		try {
			handle = parseHandle(raw_handle);
		} catch (e) {
			this.logger.verbose(null, 'Failed to parse handle ' + raw_handle);
			throw new ForbiddenException();
		}

		const user_uuid = request.access_session.user_uuid;
		const user = await this.UserCacheService.get(user_uuid);
		if (!user) {
			this.logger.error(null, 'Failed to get user from cache');
			throw new InternalServerErrorException();
		}

		const account = findAccountInUser(user, handle);
		if (!account) {
			this.logger.verbose(
				null,
				`User ${user.main.username}@${user.main.application.domain} isn't linked to ${raw_handle}`,
			);
			throw new ForbiddenException();
		}

		const cache_control = request.headers['cache-control'];
		const cache = cache_control === 'no-cache' ? cache_control : '';

		const instance = MegalodonGenerator(
			account.application.type,
			'https://' + account.application.domain,
			{
				accessToken: account.token,
				handle: account.username + '@' + account.application.domain,
				axios: this.HttpService.axiosRef,
				cache,
			},
		);

		(request as RequestMirror)['account'] = account;
		(request as RequestMirror)['instance'] = instance;

		return true;
	}
}
