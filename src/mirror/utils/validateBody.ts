export const validateBody = (body: Object) =>
	body && typeof body === 'object' && Object.keys(body).length;
