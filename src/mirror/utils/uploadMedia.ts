import { MegalodonInterface } from '../../lib/megalodon/megalodon';
import { AxiosClient } from '../../lib/megalodon/axios_client';
import Mastodon from '../../lib/megalodon/mastodon/mastodon';
import MastodonConverter from '../../lib/megalodon/mastodon/converter';
import Pleroma from '../../lib/megalodon/pleroma/pleroma';
import PleromaConverter from '../../lib/megalodon/mastodon/converter';
import { MastodonEntity } from '../../lib/megalodon/mastodon/mastodonEntities';

import { InstanceType } from '../../database/types';

interface UploadOptional {
	description?: string;
	focus?: string;
}

export function detectInterfaceInstance(megalodon: MegalodonInterface): InstanceType | null {
	if (megalodon instanceof Mastodon) return InstanceType.Mastodon;
	if (megalodon instanceof Pleroma) return InstanceType.Pleroma;
	return null;
}

/**
 * This reimplements the uploadMedia method from https://github.com/h3poteto/megalodon/blob/v6.1.0/megalodon/src/mastodon.ts
 * To support creation of a formData from a buffer
 */
export async function mastodonUpload(
	client: AxiosClient,
	file: Express.Multer.File,
	options: UploadOptional,
) {
	const form = new FormData();
	form.set('file', new Blob([file.buffer]), file.filename);

	if (options) {
		if (options.description) {
			form.set('description', options.description);
		}
		if (options.focus) {
			form.set('focus', options.focus);
		}
	}

	return client.postForm<MastodonEntity.AsyncAttachment>('/api/v2/media', form).then((res) => {
		return Object.assign(res, {
			data: MastodonConverter.async_attachment(res.data),
		});
	});
}

/**
 * This reimplements the uploadMedia method from https://github.com/h3poteto/megalodon/blob/v6.1.0/megalodon/src/pleroma.ts
 * To support creation of a formData from a buffer
 */
export async function pleromaUpload(
	client: AxiosClient,
	file: Express.Multer.File,
	options: UploadOptional,
) {
	const form = new FormData();
	form.set('file', new Blob([file.buffer]), file.filename);

	if (options) {
		if (options.description) {
			form.set('description', options.description);
		}
		if (options.focus) {
			form.set('focus', options.focus);
		}
	}

	return client.postForm<MastodonEntity.AsyncAttachment>('/api/v2/media', form).then((res) => {
		return Object.assign(res, {
			data: PleromaConverter.async_attachment(res.data),
		});
	});
}

/**
 * This reimplements the uploadMedia method from https://github.com/h3poteto/megalodon/blob/v6.1.0/megalodon/src/misskey.ts
 * To support creation of a formData from a buffer
 */
// export async function misskeyUpload(
// 	client: MisskeyAPI.Interface,
// 	file: Express.Multer.File,
// 	_: UploadOptional,
// ) {
// 	const form = new FormData();
// 	form.set('file', new Blob([file.buffer]), file.filename);

// 	return client
// 		.post<MisskeyAPI.Entity.File>('/api/drive/files/create', form)
// 		.then((res) => ({ ...res, data: MisskeyAPI.Converter.file(res.data) }));
// }
