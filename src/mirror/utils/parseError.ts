export enum ErrorActions {
	Invalid = -1,
	Dead = 0,
	Retry = 1,
}
export function parseError(e: any) {
	// Awful workaround because thrown error are standard only if converted to json and i'm tired to fight with it
	const error = JSON.parse(JSON.stringify(e));

	switch (error.status) {
		case 403:
		case 503:
			return ErrorActions.Invalid;
		case 400:
		case 404:
		case 422:
			return ErrorActions.Dead;
		case 401:
		case 429:
		case 500:
			return ErrorActions.Retry;
	}

	console.warn('parseError', '----- Unhandled error');
	console.log(error.status, error.code);
	console.log(JSON.stringify(error));

	return ErrorActions.Retry;
}
