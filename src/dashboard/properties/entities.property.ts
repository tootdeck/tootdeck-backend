import { ApiProperty } from '@nestjs/swagger';

import { ApiResponseTime } from '../../app/properties/times.property';

/**
 * Workers stats
 */
// #region

export class Worker {
	@ApiProperty()
	uuid: string;

	@ApiProperty()
	started_at: Date;

	@ApiProperty()
	memory: number;

	@ApiProperty({ type: ApiResponseTime })
	times: ApiResponseTime;

	@ApiProperty()
	concurrency: number;
}

export class Jobs {
	@ApiProperty()
	processed: number;

	@ApiProperty()
	failed: number;
}

export class DashboardStats {
	@ApiProperty({ type: Jobs })
	jobs: Jobs;

	@ApiProperty()
	requests_made: number;

	@ApiProperty()
	queue_size: number;

	@ApiProperty()
	max_concurrency: number;

	@ApiProperty({ isArray: true, type: Worker })
	workers: Array<Worker>;
}

// #endregion

/**
 * Charts
 */
// #region
export class DashboardCharts {
	@ApiProperty()
	jobs_processed: number;

	@ApiProperty()
	jobs_failed: number;

	@ApiProperty()
	requests_made: number;

	@ApiProperty()
	worker_queue_max: number;

	@ApiProperty()
	worker_memory_max: number;

	@ApiProperty()
	worker_job_time_min: number;

	@ApiProperty()
	worker_job_time_max: number;

	@ApiProperty()
	worker_job_time_average: number;

	@ApiProperty()
	cache_size_max: number;

	@ApiProperty()
	cache_size_dead: number;

	@ApiProperty()
	created_at: Date;

	@ApiProperty()
	merge_type: number;
}

// #endregion

/**
 * Users stats
 */
// #region

export class AccountStats {
	@ApiProperty()
	handle: string;

	@ApiProperty()
	jobs_count: number;

	@ApiProperty()
	status: boolean;
}

export class UserStats {
	@ApiProperty({ type: AccountStats })
	main: AccountStats;

	@ApiProperty({ type: AccountStats, isArray: true })
	secondary: Array<Omit<AccountStats, 'status'>>;
}

export class SocketStats {
	@ApiProperty()
	total: number;

	@ApiProperty()
	instances: number;

	@ApiProperty()
	frontend: number;

	@ApiProperty()
	dashboard: number;
}

export class DashboardUserStats {
	@ApiProperty({ type: SocketStats })
	sockets: SocketStats;

	@ApiProperty({ type: UserStats })
	users: UserStats;
}

// #endregion

/**
 * Redis
 */
// #region

export class DashboardRedis {
	@ApiProperty()
	key: string;
}

export class DashboardRedisTTL extends DashboardRedis {
	@ApiProperty()
	ttl: number;
}

// #endregion
