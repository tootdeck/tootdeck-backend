import { ApiProperty } from '@nestjs/swagger';

export class FormRedisDelete {
	@ApiProperty({ type: String, isArray: true })
	keys: Array<string>;
}
