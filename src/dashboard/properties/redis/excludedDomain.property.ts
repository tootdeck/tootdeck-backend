import { ApiProperty } from '@nestjs/swagger';

import { DashboardRedisTTL } from '../entities.property';

import { RedisState } from '../../../app/types';

export class RedisExcludedDomain {
	@ApiProperty({ enum: RedisState })
	state: RedisState.Retry | RedisState.Dead;

	@ApiProperty()
	value: string;
}

export class DashboardRedisExcludedDomains extends DashboardRedisTTL {
	@ApiProperty({ type: RedisExcludedDomain })
	value: RedisExcludedDomain;
}
