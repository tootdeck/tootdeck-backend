import { ApiProperty } from '@nestjs/swagger';

import { DashboardRedis } from '../entities.property';

import { RedisState } from '../../../app/types';

export enum TwitterMediaType {
	Image = 'image',
	Gif = 'gif',
	Video = 'video',
}

export class TwitterMediaSize {
	@ApiProperty()
	height: number;

	@ApiProperty()
	width: number;
}

export class TwitterMedia {
	@ApiProperty()
	altText: string | null;

	@ApiProperty()
	duration_millis: number;

	@ApiProperty({ type: TwitterMediaSize })
	size: TwitterMediaSize;

	@ApiProperty()
	thumbnail_url: string;

	@ApiProperty({ enum: TwitterMediaType })
	type: TwitterMediaType;

	@ApiProperty()
	url: string;
}

export class TwitterResponse {
	@ApiProperty()
	allSameType: boolean;

	@ApiProperty()
	combinedMediaUrl: string | null;

	@ApiProperty()
	communityNote: string | null;

	@ApiProperty()
	conversationID: string;

	@ApiProperty()
	date: string;

	@ApiProperty()
	date_epoch: number;

	@ApiProperty()
	hasMedia: boolean;

	@ApiProperty({ type: String, isArray: true })
	hashtags: Array<string>;

	@ApiProperty()
	likes: number;

	@ApiProperty({ type: String, isArray: true })
	mediaURLs: Array<string>;

	@ApiProperty({ type: TwitterMedia, isArray: true })
	media_extended: Array<TwitterMedia>;

	@ApiProperty()
	pollData: string | null;

	@ApiProperty()
	possibly_sensitive: boolean;

	@ApiProperty()
	qrt: string | null;

	@ApiProperty()
	qrtURL: string | null;

	@ApiProperty()
	replies: number;

	@ApiProperty()
	retweets: number;

	@ApiProperty()
	text: string;

	@ApiProperty()
	tweetID: string;

	@ApiProperty()
	tweetURL: string;

	@ApiProperty()
	user_name: string;

	@ApiProperty()
	user_profile_image_url: string;

	@ApiProperty()
	user_screen_name: string;

	/**
	 * {
	 *	"allSameType": true,
	 *	"combinedMediaUrl": null,
	 *	"communityNote": null,
	 *	"conversationID": "1792184009755390314",
	 *	"date": "Sun May 19 13:22:19 +0000 2024",
	 *	"date_epoch": 1716124939,
	 *	"hasMedia": true,
	 *	"hashtags": [
	 *	  "persona3"
	 *	],
	 *	"likes": 1300,
	 *	"mediaURLs": [
	 *	  "https://pbs.twimg.com/media/GN8d2ByacAADfbK.jpg"
	 *	],
	 *	"media_extended": [
	 *	  {
	 *		"altText": null,
	 *		"size": {
	 *		  "height": 2048,
	 *		  "width": 2048
	 *		},
	 *		"thumbnail_url": "https://pbs.twimg.com/media/GN8d2ByacAADfbK.jpg",
	 *		"type": "image",
	 *		"url": "https://pbs.twimg.com/media/GN8d2ByacAADfbK.jpg"
	 *	  }
	 *	],
	 *	"pollData": null,
	 *	"possibly_sensitive": false,
	 *	"qrt": null,
	 *	"qrtURL": null,
	 *	"replies": 13,
	 *	"retweets": 361,
	 *	"text": "Absolution\n#persona3 https://t.co/hLtspz2HgE",
	 *	"tweetID": "1792184009755390314",
	 *	"tweetURL": "https://twitter.com/KickThat_Can/status/1792184009755390314",
	 *	"user_name": "🌕",
	 *	"user_profile_image_url": "https://pbs.twimg.com/profile_images/1748698804688437249/tjSIeVa__normal.jpg",
	 *	"user_screen_name": "KickThat_Can"
	 *  }
	 */
}

export class RedisTwitterEmbedOK {
	@ApiProperty({ enum: RedisState })
	state: RedisState.Ok;

	@ApiProperty({ type: TwitterResponse })
	value: TwitterResponse;
}

export class RedisTwitterEmbedDead {
	@ApiProperty({ enum: RedisState })
	state: RedisState.Dead;

	@ApiProperty()
	value: string;
}

export class DashboardRedisTwitterEmbed extends DashboardRedis {
	@ApiProperty({ type: RedisTwitterEmbedOK })
	value: RedisTwitterEmbedOK | RedisTwitterEmbedDead;
}
