import { ApiProperty } from '@nestjs/swagger';

import { DashboardRedis } from '../entities.property';

import { RedisState } from '../../../app/types';

export class RedisInstanceCache {
	@ApiProperty({ enum: RedisState })
	state: RedisState.Ok | RedisState.Dead;

	@ApiProperty()
	value: string;
}

export class DashboardRedisInstancesTypes extends DashboardRedis {
	@ApiProperty({ type: RedisInstanceCache })
	value: RedisInstanceCache;
}
