import { ApiProperty } from '@nestjs/swagger';

import { DashboardRedisTTL } from '../entities.property';

import { RedisState } from '../../../app/types';

export class EmojiReaction {
	@ApiProperty()
	count: number;

	@ApiProperty()
	name: string;

	@ApiProperty()
	url: string | undefined;

	@ApiProperty()
	fallback: string | undefined;

	@ApiProperty()
	static_url: string | undefined;
}

export class BasicInteraction {
	@ApiProperty()
	replies_count: number;

	@ApiProperty()
	reblogs_count: number;

	@ApiProperty()
	favourites_count: number;
}

export class Interactions extends BasicInteraction {
	@ApiProperty({ type: EmojiReaction, isArray: true })
	emoji_reactions: Array<EmojiReaction>;

	@ApiProperty()
	application: string | null;
}

export class RedisInteraction {
	@ApiProperty({ enum: RedisState })
	state: RedisState;

	@ApiProperty({ type: Interactions })
	value: Interactions | string;
}

export class DashboardRedisInteractions extends DashboardRedisTTL {
	@ApiProperty({ type: RedisInteraction })
	value: RedisInteraction;
}
