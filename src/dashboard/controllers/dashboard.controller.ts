import {
	BadRequestException,
	Body,
	Controller,
	Get,
	InternalServerErrorException,
	Post,
	Query,
	UseGuards,
} from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

import { ClusterSchedulerService } from '../../app/services/clusterScheduler.service';
import { DBWorkerStatsService } from '../../database/services/database.worker.stats.service';
import { WsService } from '../../websocket/services/ws.service';
import { WsStatsService } from '../../websocket/services/ws.stats.service';
import { DBUserService } from '../../database/services/database.user.service';

import { AccessGuard } from '../../auth/guards/access.guard';
import { AdminGuard } from '../admin.guard';

import {
	DashboardCharts,
	DashboardStats,
	DashboardUserStats,
	UserStats,
} from '../properties/entities.property';

@ApiTags('Dashboard')
@UseGuards(AccessGuard)
@Controller('dashboard')
export class DashboardController {
	constructor(
		private readonly ClusterSchedulerService: ClusterSchedulerService,
		private readonly DBWorkerStatsService: DBWorkerStatsService,
		private readonly DBUserService: DBUserService,
		private readonly WsService: WsService,
		private readonly WsStatsService: WsStatsService,
	) {}

	@ApiResponse({ status: 200, description: 'Dashboard data', type: DashboardStats })
	@Get('stats')
	dashboard() {
		return this.ClusterSchedulerService.getStats();
	}

	@ApiResponse({
		status: 200,
		description: 'Dashboard charts data',
		type: DashboardCharts,
		isArray: true,
	})
	@Get('charts')
	async charts(@Query('limit') limit: number) {
		if (!limit) {
			throw new BadRequestException();
		}

		return this.DBWorkerStatsService.get(limit);
	}

	@ApiResponse({
		status: 200,
		description: 'Dashboard users and sockets stats',
		type: DashboardUserStats,
		isArray: true,
	})
	@UseGuards(AdminGuard)
	@Get('users_stats')
	async usersStats() {
		const users = await this.DBUserService.getAllPartial();
		if (!users) {
			throw new InternalServerErrorException();
		}

		const accounts_stats = this.ClusterSchedulerService.getAccountsStats();
		const connected = this.WsService.getOnlineUsers();
		const sockets = this.WsStatsService.get();

		const stats: Array<UserStats> = [];

		for (const user of users) {
			const main_handle = user.main.username + '@' + user.main.domain;
			const main_connected = connected.find((x) => x.main === main_handle);
			const main_jobs_count = accounts_stats.find(([handle]) => handle === main_handle)?.[1];

			const main_entry = {
				handle: main_handle,
				jobs_count: main_jobs_count ?? 0,
				status: !!main_connected,
			};

			const secondary_entries = [];

			for (const secondary of user.secondary) {
				const secondary_handle = secondary.username + '@' + secondary.domain;
				const secondary_jobs_count = accounts_stats.find(
					([handle]) => handle === secondary_handle,
				)?.[1];

				const entry = {
					handle: secondary_handle,
					jobs_count: secondary_jobs_count ?? 0,
				};
				secondary_entries.push(entry);
			}

			stats.push({ main: main_entry, secondary: secondary_entries });
		}

		return {
			sockets,
			users: stats,
		};
	}

	@ApiResponse({
		status: 200,
		description: 'Message successfully sent',
	})
	@ApiResponse({
		status: 400,
		description: 'Missing message',
	})
	@UseGuards(AdminGuard)
	@Post('broadcast_message')
	broadcastMessage(@Body('message') message: string) {
		if (!message) {
			throw new BadRequestException();
		}
		this.WsService.broadcastMessage(message);
	}
}
