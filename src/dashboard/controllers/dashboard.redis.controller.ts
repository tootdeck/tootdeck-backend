import {
	BadRequestException,
	Body,
	Controller,
	Delete,
	Get,
	Put,
	Response,
	UseGuards,
} from '@nestjs/common';
import { ApiBody, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { FastifyReply } from 'fastify';
import { Redis } from 'ioredis';
import { deserialize } from 'v8';

import { FailedDomainsService } from '../../app/services/failedDomain.service';

import { AccessGuard } from '../../auth/guards/access.guard';
import { AdminGuard } from '../admin.guard';

import { RedisNamespace } from '../../app/constants/redis';

import { FormRedisDelete } from '../properties/body.property';
import { DashboardRedisInteractions } from '../properties/redis/interactions.property';
import { DashboardRedisExcludedDomains } from '../properties/redis/excludedDomain.property';
import { DashboardRedisInstancesTypes } from '../properties/redis/instancesTypes.property';
import { DashboardRedisTwitterEmbed } from '../properties/redis/twitter.property';

import { RedisInteraction, RedisState } from '../../app/types';
import { RedisService } from '@liaoliaots/nestjs-redis';

@ApiTags('Dashboard · Redis')
@UseGuards(AccessGuard, AdminGuard)
@Controller('dashboard/redis')
export class DashboardRedisController {
	private readonly redis_interactions: Redis;
	private readonly redis_instances: Redis;
	private readonly redis_excluded_domain: Redis;
	private readonly redis_twitter_embed: Redis;

	constructor(
		private readonly FailedDomainsService: FailedDomainsService,
		RedisService: RedisService,
	) {
		this.redis_interactions = RedisService.getOrThrow(RedisNamespace.StatusesInteractions);
		this.redis_instances = RedisService.getOrThrow(RedisNamespace.InstanceCache);
		this.redis_excluded_domain = RedisService.getOrThrow(RedisNamespace.ExcludedDomains);
		this.redis_twitter_embed = RedisService.getOrThrow(RedisNamespace.TwitterEmbed);
	}

	@ApiResponse({
		status: 200,
		description: 'All cached redis interactions',
		type: DashboardRedisInteractions,
		isArray: true,
	})
	@Get('interactions')
	async getInteractions() {
		/**
		 * Array<entries keys>
		 */
		const all = await this.redis_interactions.keys('*');

		/**
		 * entries
		 * Array<{ keys, values, ttls }>
		 */
		const entries = await Promise.all(
			all.map((e) =>
				this.redis_interactions
					.pipeline()
					.getBuffer(e)
					.ttl(e)
					.exec()
					.then((result) => {
						if (!result) {
							return;
						}

						const value = result[0]![1] as Uint8Array;
						const ttl = result[1]![1] as number;

						return { key: e, value: value ? deserialize(value) : value, ttl };
					}),
			),
		);

		return entries;
	}

	@ApiBody({
		type: FormRedisDelete,
		required: true,
	})
	@ApiResponse({ status: 200, description: 'Keys successfully deleted' })
	@Delete('interactions')
	async delInteractions(
		@Response({ passthrough: true }) res: FastifyReply,
		@Body('keys') keys: Array<string>,
	) {
		if (!keys || !keys.length) {
			throw new BadRequestException();
		}

		const r = await this.redis_interactions.del(keys).then((r) => r === 1);

		res.code(r ? 200 : 404);
	}

	@ApiOperation({ summary: 'Force remove cached interactions from excluded domain' })
	@ApiResponse({ status: 200, description: 'Keys successfully deleted', type: Number })
	@Put('interactions/clean')
	async cleanInteractions(): Promise<number> {
		const excluded = await this.FailedDomainsService.getExcludedDomains();
		/**
		 * InteractionsService.removeRedisLeftovers
		 */
		return Promise.all(
			excluded.map(async (domain) =>
				this.redis_interactions.keys(`https://${domain}/*`).then(async (keys) =>
					Promise.all(
						keys.map(async (key) => {
							const value = await this.redis_interactions
								.getBuffer(key)
								.then((r) => (r ? (deserialize(r) as RedisInteraction) : null));
							return !value || value.state !== RedisState.Ok ? key : undefined;
						}),
					),
				),
			),
		).then(async (keys_to_del) => {
			const keys = keys_to_del.flat().filter((x) => x) as Array<string>;
			if (!keys.length) {
				return 0;
			}
			return this.redis_interactions.del(keys);
		});
	}

	@ApiResponse({
		status: 200,
		description: 'All redis excluded domains',
		type: DashboardRedisExcludedDomains,
		isArray: true,
	})
	@Get('excluded_domains')
	async getExcludedDomains() {
		/**
		 * Array<entries keys>
		 */
		const all = await this.redis_excluded_domain.keys('*');

		/**
		 * entries
		 * Array<{ keys, values, ttls }>
		 */
		const entries = await Promise.all(
			all.map((e) =>
				this.redis_excluded_domain
					.pipeline()
					.getBuffer(e)
					.ttl(e)
					.exec()
					.then((result) => {
						if (!result) {
							return;
						}

						const value = result[0]![1] as Uint8Array;
						const ttl = result[1]![1] as number;

						return { key: e, value: value ? deserialize(value) : value, ttl };
					}),
			),
		);

		return entries;
	}

	@ApiBody({
		type: FormRedisDelete,
		required: true,
	})
	@ApiResponse({ status: 200, description: 'Keys successfully deleted' })
	@Delete('excluded_domains')
	async delExcludedDomains(
		@Response({ passthrough: true }) res: FastifyReply,
		@Body('keys') keys: Array<string>,
	) {
		if (!keys || !keys.length) {
			throw new BadRequestException();
		}

		const r = await this.redis_excluded_domain.del(keys).then((r) => r === 1);

		res.code(r ? 200 : 404);
	}

	@ApiResponse({
		status: 200,
		description: 'All cached redis instances types',
		type: DashboardRedisInstancesTypes,
		isArray: true,
	})
	@Get('instances')
	async getInstances() {
		/**
		 * Array<entries keys>
		 */
		const all = await this.redis_instances.keys('*');

		/**
		 * entries
		 * Array<{ keys, values }>
		 */
		const entries = await Promise.all(
			all.map((e) =>
				this.redis_instances.getBuffer(e).then((value) => {
					return { key: e, value: value ? deserialize(value) : value };
				}),
			),
		);

		return entries;
	}

	@ApiBody({
		type: FormRedisDelete,
		required: true,
	})
	@ApiResponse({ status: 200, description: 'Keys successfully deleted' })
	@Delete('instances')
	async delInstances(
		@Response({ passthrough: true }) res: FastifyReply,
		@Body('keys') keys: Array<string>,
	) {
		if (!keys || !keys.length) {
			throw new BadRequestException();
		}

		const r = await this.redis_instances.del(keys).then((r) => r === 1);

		res.code(r ? 200 : 404);
	}

	@ApiResponse({
		status: 200,
		description: 'All redis cached twitter embed',
		type: DashboardRedisTwitterEmbed,
		isArray: true,
	})
	@Get('twitter')
	async getTwitterEmbed() {
		/**
		 * Array<entries keys>
		 */
		const all = await this.redis_twitter_embed.keys('*');

		/**
		 * entries
		 * Array<{ keys, values }>
		 */
		const entries = await Promise.all(
			all.map((e) =>
				this.redis_twitter_embed.getBuffer(e).then((value) => {
					if (!value) {
						return;
					}

					return { key: e, value: value ? deserialize(value) : value };
				}),
			),
		);

		return entries;
	}

	@ApiBody({
		type: FormRedisDelete,
		required: true,
	})
	@ApiResponse({ status: 200, description: 'Keys successfully deleted' })
	@Delete('twitter')
	async delTwitterEmbed(
		@Response({ passthrough: true }) res: FastifyReply,
		@Body('keys') keys: Array<string>,
	) {
		if (!keys || !keys.length) {
			throw new BadRequestException();
		}

		const r = await this.redis_twitter_embed.del(keys).then((r) => r === 1);

		res.code(r ? 200 : 404);
	}
}
