import {
	CanActivate,
	ExecutionContext,
	Injectable,
	InternalServerErrorException,
	NotFoundException,
} from '@nestjs/common';

import { UserCacheService } from '../user/services/user.cache.service';
import { AppService } from '../app/services/app.service';

import { Logger } from '../utils/logger';

import { RequestAccessGuard } from '../auth/types/guards.types';

@Injectable()
export class AdminGuard implements CanActivate {
	private readonly logger = new Logger(AdminGuard.name);

	constructor(
		private readonly UserCacheService: UserCacheService,
		private readonly AppService: AppService,
	) {}

	/**
	 * @throws UnauthorizedException
	 * @throws InternalServerErrorException
	 */
	async canActivate(context: ExecutionContext): Promise<boolean> {
		const request = context.switchToHttp().getRequest() as RequestAccessGuard;

		if (!request.access_session) {
			this.logger.error(null, 'Guard is called without AccessGuard');
			throw new InternalServerErrorException();
		}

		const user_uuid = request.access_session.user_uuid;
		const user = await this.UserCacheService.get(user_uuid);
		if (!user) {
			this.logger.error(null, 'Failed to get user from cache');
			throw new InternalServerErrorException();
		}

		const handle = user.main.username + '@' + user.main.application.domain;
		if (!this.AppService.isAdmin(handle)) {
			throw new NotFoundException();
		}

		return true;
	}
}
