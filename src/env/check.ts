import ValidateEnv from './validate';

export default function () {
	const env = new ValidateEnv();

	env.check('DOMAIN', 'string');
	env.check('PSQL_HOST', 'string');
	env.check('PSQL_PORT', 0);
	env.check('PSQL_USERNAME', 'string');
	env.check('PSQL_PASSWORD', 'string');
	env.check('PSQL_DATABASE', 'string');
	env.check('REDIS_HOST', 'string');
	env.check('REDIS_PORT', 0);
	env.check('MAIN_PROCESS_HOST', 'string');
	env.check('MAIN_PROCESS_PORT', 0);
	env.check('WORKER_PORT', 0);

	if (env.getErrorState()) {
		env.printErrorMessage();
		process.exit(1);
	}
	return true;
}
