export interface InstanceHandle {
	username: string;
	domain: string;
}
