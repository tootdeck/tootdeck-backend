import { InstanceType } from '../database/types';
import { getHandleFromURL } from './getHandleFromURL';
import { InstanceHandle } from './handle.types';

describe('getHandleFromURL', () => {
	const valid = [
		{ username: '0x4d696b75', domain: 'akko.wtf' },
		{ username: '6VCR', domain: 'mastodon.art' },
		{ username: 'acegikmo', domain: 'mastodon.social' },
		{ username: 'aelys', domain: 'eldritch.cafe' },
		{ username: 'AlexWinter', domain: 'masto.ai' },
		{ username: 'alice', domain: 'mstdn.shalyu.run' },
		{ username: 'alkihis', domain: 'mastodon.social' },
		{ username: 'amd64sucks', domain: 'wetdry.world' },
		{ username: 'azusa', domain: 'donphan.social' },
		{ username: 'bansheaft', domain: 'mastodon.social' },
		{ username: 'beaufdelphine', domain: 'rivals.space' },
		{ username: 'bstndn', domain: 'mastodon.social' },
		{ username: 'cafard', domain: 'mstdn.shalyu.run' },
		{ username: 'Cappuccino35', domain: 'eldritch.cafe' },
		{ username: 'chilledtentacle', domain: 'wetdry.world' },
		{ username: 'ChoGath', domain: 'mstdn.shalyu.run' },
		{ username: 'citronsaley', domain: 'mstdn.shalyu.run' },
		{ username: 'Do', domain: 'eldritch.cafe' },
		{ username: 'dotty', domain: 'infosec.exchange' },
		{ username: 'ebibiglass', domain: 'mstdn.social' },
		{ username: 'Ecledia', domain: 'eldritch.cafe' },
		{ username: 'Elarcis', domain: 'mamot.fr' },
		{ username: 'ElWaddlzDoo', domain: 'mstdn.shalyu.run' },
		{ username: 'Eramdam', domain: 'octodon.social' },
		{ username: 'esm', domain: 'wetdry.world' },
		{ username: 'esorette', domain: 'rivals.space' },
		{ username: 'Florimelles', domain: 'mastodon.social' },
		{ username: 'Florimelles', domain: 'piaille.fr' },
		{ username: 'Gargron', domain: 'mastodon.social' },
		{ username: 'Goodly', domain: 'mastodon.gamedev.place' },
		{ username: 'guillaumesiron', domain: 'mastodon.social' },
		{ username: 'gwenouille', domain: 'morgane.io' },
		{ username: 'Impessa', domain: 'morgane.io' },
		{ username: 'ipg', domain: 'wetdry.world' },
		{ username: 'jill', domain: 'va-11-hall-a.cafe' },
		{ username: 'jorun', domain: 'meta.jorun.dev' },
		{ username: 'kahianyaaa', domain: 'eldritch.cafe' },
		{ username: 'kata444444', domain: 'wetdry.world' },
		{ username: 'kenygia', domain: 'mastodon.social' },
		{ username: 'KillerQueen', domain: 'im-in.space' },
		{ username: 'Kurt', domain: 'catcatnya.com' },
		{ username: 'lea', domain: 'mstdn.shalyu.run' },
		{ username: 'lezareuhh', domain: 'rivals.space' },
		{ username: 'malwaretech', domain: 'infosec.exchange' },
		{ username: 'manovi', domain: 'mstdn.shalyu.run' },
		{ username: 'mariocbt', domain: 'wetdry.world' },
		{ username: 'marioProvence', domain: 'piaille.fr' },
		{ username: 'marmelade', domain: 'piaille.fr' },
		{ username: 'megalodon', domain: 'floss.social' },
		{ username: 'meufplouf', domain: 'mstdn.shalyu.run' },
		{ username: 'mieru', domain: 'mstdn.shalyu.run' },
		{ username: 'miraty', domain: 'plero.antopie.org' },
		{ username: 'Morgane', domain: 'morgane.io' },
		{ username: 'MrAntoineDaniel', domain: 'mastodon.xyz' },
		{ username: 'Nellen', domain: 'mastodon.social' },
		{ username: 'Nellen', domain: 'mstdn.shalyu.run' },
		{ username: 'Nemo', domain: 'shelter.moe' },
		{ username: 'niavlys', domain: 'mstdn.shalyu.run' },
		{ username: 'NiDeDroite', domain: 'piaille.fr' },
		{ username: 'nonamenosocks', domain: 'mastodon.art' },
		{ username: 'noodledie', domain: 'mstdn.shalyu.run' },
		{ username: 'plumie', domain: 'eldritch.cafe' },
		{ username: 'Rasp', domain: 'raru.re' },
		{ username: 'requiemforahoe', domain: 'mstdn.shalyu.run' },
		{ username: 'Richiesque', domain: 'mastodon.social' },
		{ username: 'RisenValkyrie', domain: 'bingus.bloodshed.party' },
		{ username: 'Saigyouji', domain: 'shelter.moe' },
		{ username: 'SatanicCunni', domain: 'mastodon.social' },
		{ username: 'Saturda', domain: 'aleph.land' },
		{ username: 'serval', domain: 'shelter.moe' },
		{ username: 'Sine', domain: 'wetdry.world' },
		{ username: 'smaugleplot', domain: 'mstdn.shalyu.run' },
		{ username: 'snflwrfld', domain: 'rivals.space' },
		{ username: 'SwiftOnSecurity', domain: 'infosec.exchange' },
		{ username: 'sylvqin', domain: 'mastodon.social' },
		{ username: 'tgv', domain: 'mstdn.shalyu.run' },
		{ username: 'thememesniper', domain: 'wetdry.world' },
		{ username: 'thomas', domain: 'mastodon.xyz' },
		{ username: 'TootDeck', domain: 'mstdn.shalyu.run' },
		{ username: 'Usul2000', domain: 'piaille.fr' },
		{ username: 'vuktur', domain: 'fosstodon.org' },
		{ username: 'zaski', domain: 'piaille' },
	];

	function formatURLFor(handle: InstanceHandle, type: InstanceType) {
		const base_url = 'https://' + handle.domain + '/';

		switch (type) {
			case InstanceType.Mastodon:
				return [
					base_url + '@' + handle.username,
					base_url + '@' + handle.username + '@' + handle.domain,
					base_url + 'users/' + handle.username,
				];
			case InstanceType.Misskey:
				return [
					base_url + '@' + handle.username,
					base_url + '@' + handle.username + '@' + handle.domain,
				];
			case InstanceType.Pleroma:
				return [base_url + handle.username, base_url + 'users/' + handle.username];
			case InstanceType.Friendica:
				return [base_url + 'profile/' + handle.username];
		}
	}

	test('Valid handle', () => {
		valid.forEach((handle) => {
			for (const type of Object.values(InstanceType)) {
				const urls = formatURLFor(handle, type);

				for (const url of urls) {
					expect(getHandleFromURL(url)).toMatchObject(handle);
				}
			}
		});
	});
});
