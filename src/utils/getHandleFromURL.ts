import { InstanceHandle } from './handle.types';

export function getHandleFromURL(href: string): InstanceHandle {
	const url = new URL(href);

	let pathname = url.pathname;
	if (
		// Pleroma
		pathname.includes('users') ||
		// friendica
		pathname.includes('profile')
	) {
		pathname = pathname.split('/')[2]!;
	}

	// Misskey
	const tmp = pathname.split('@');
	if (pathname.includes(url.host) || tmp.length === 3) {
		return {
			username: tmp[1]!,
			domain: tmp[2]!,
		};
	}

	const username = pathname.replace('@', '').replace('/', '');

	return {
		username,
		domain: url.hostname,
	};
}
