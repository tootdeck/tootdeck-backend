import { Test, TestingModule } from '@nestjs/testing';

import { DBApplicationService } from '../database/services/database.application.service';
import { DBAccountService } from '../database/services/database.account.service';
import { DBUserService } from '../database/services/database.user.service';

import { AccountStore } from '../database/entities/account.entity';
import { UserStore } from '../database/entities/user.entity';

import { parseHandle } from './parseHandle';
import { findAccountInUser } from './findAccountInUser';

import { InstanceType } from '../database/types';
import { FullAppModule } from '../app/app.module';

describe('findAccountInUser', () => {
	let ApplicationDB: DBApplicationService;
	let AccountDB: DBAccountService;
	let UserDB: DBUserService;
	let user_1: UserStore;
	let user_2: UserStore;
	let user_3: UserStore;

	let account_1: AccountStore;
	let account_2: AccountStore;
	let account_3: AccountStore;
	let account_4: AccountStore;
	let account_5: AccountStore;

	const test_domain_1 = 'AccountService1'.toLocaleLowerCase();
	const test_username_1 = 'AccountService1'.toLocaleLowerCase();
	const test_username_2 = 'AccountService2'.toLocaleLowerCase();

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule(FullAppModule).compile();

		ApplicationDB = app.get(DBApplicationService);
		AccountDB = app.get(DBAccountService);
		UserDB = app.get(DBUserService);

		await ApplicationDB.create(test_domain_1, InstanceType.Mastodon, 'key', 'secret');

		account_1 = (await AccountDB.create(test_username_1, test_domain_1, 'token'))!;
		account_2 = (await AccountDB.create(test_username_2, test_domain_1, 'token'))!;
		account_3 = (await AccountDB.create('test_username_3', test_domain_1, 'token'))!;
		account_4 = (await AccountDB.create('test_username_4', test_domain_1, 'token'))!;
		account_5 = (await AccountDB.create('test_username_5', test_domain_1, 'token'))!;
		await UserDB.create(account_1!);
		await UserDB.addAccount(account_1!, account_2!);
		await UserDB.addAccount(account_1!, account_3!);
		await UserDB.addAccount(account_1!, account_4!);
		user_1 = (await UserDB.addAccount(account_1!, account_5!))!;
		user_2 = (await UserDB.create(account_2!))!;
		user_3 = (await UserDB.addAccount(account_2!, account_4!))!;
	});

	test('Valid', () => {
		expect(
			findAccountInUser(user_1, parseHandle(`@${test_username_1}@${test_domain_1}`)),
		).toMatchObject(account_1);
		expect(
			findAccountInUser(user_1, parseHandle(`@${test_username_2}@${test_domain_1}`)),
		).toMatchObject(account_2);
		expect(
			findAccountInUser(user_1, parseHandle(`@test_username_3@${test_domain_1}`)),
		).toMatchObject(account_3);
		expect(
			findAccountInUser(user_1, parseHandle(`@test_username_4@${test_domain_1}`)),
		).toMatchObject(account_4);
		expect(
			findAccountInUser(user_1, parseHandle(`@test_username_5@${test_domain_1}`)),
		).toMatchObject(account_5);
		expect(
			findAccountInUser(user_2, parseHandle(`@${test_username_2}@${test_domain_1}`)),
		).toMatchObject(account_2);
	});

	test('Invalid', () => {
		expect(findAccountInUser(user_2, parseHandle(`@${test_username_1}@${test_domain_1}`))).toBe(
			null,
		);
		expect(findAccountInUser(user_2, parseHandle(`@test_username_3@${test_domain_1}`))).toBe(
			null,
		);
		expect(findAccountInUser(user_2, parseHandle(`@test_username_4@${test_domain_1}`))).toBe(
			null,
		);
		expect(findAccountInUser(user_3, parseHandle(`@test_username_5@${test_domain_1}`))).toBe(
			null,
		);
		expect(findAccountInUser(user_2, undefined as any)).toBe(null);
	});

	afterAll(async () => {
		await UserDB.delete(account_1);
		await UserDB.delete(account_2);
		await AccountDB.delete(test_username_1, test_domain_1);
		await AccountDB.delete(test_username_2, test_domain_1);
		await AccountDB.delete('test_username_3', test_domain_1);
		await AccountDB.delete('test_username_4', test_domain_1);
		await AccountDB.delete('test_username_5', test_domain_1);
		await ApplicationDB.delete(test_domain_1);
	});
});
