import { parseHandle } from './parseHandle';

describe('Parse handle', () => {
	const TryCatch = (handle: string) => {
		let result: any;
		let has_throw = false;

		try {
			result = parseHandle(handle);
		} catch (e) {
			has_throw = true;
		}

		return {
			result,
			has_throw,
		};
	};

	const valid_handle = [
		['@test@localhost', 'test', 'localhost'],
		['test@localhost', 'test', 'localhost'],
		['@valid.valid@valid.valid', 'valid.valid', 'valid.valid'],
		['valid.valid@valid.valid', 'valid.valid', 'valid.valid'],
	];

	const invalid_handle = [
		'',
		'@@',
		'localhost',
		'@valid@invalid@',
		'@@invalid@valid',
		'@valid@inva..lid',
		'@valid@https://test',
		'@valid@invalid/invalid',
	];

	test('Valid handle', async () => {
		valid_handle.forEach(([handle, username, domain]) => {
			const { result, has_throw } = TryCatch(handle!);

			expect(result).toMatchObject({
				username,
				domain,
			});
			expect(has_throw).toBe(false);
		});
	});

	test('Invalid handle', async () => {
		invalid_handle.forEach((handle) => {
			const { has_throw } = TryCatch(handle);

			expect(has_throw).toBe(true);
		});
	});
});
