import { ConsoleLogger } from '@nestjs/common';

import colors from './colors';

export class Logger extends ConsoleLogger {
	private readonly disabled = process.env['NODE_ENV'] === 'test';

	override log(fn_name: string | null, message: any, ...optionalParams: any[]): void {
		if (this.disabled) {
			return;
		}
		if (fn_name) {
			return super.log(
				`${colors.pink}[${fn_name}]${colors.green} ` + message,
				...optionalParams,
			);
		}
		super.log(message, ...optionalParams);
	}

	override error(fn_name: string | null, message: any, ...optionalParams: any[]): void {
		if (this.disabled) {
			return;
		}
		if (fn_name) {
			return super.error(
				`${colors.pink}[${fn_name}]${colors.red} ` + message,
				...optionalParams,
			);
		}
		super.error(message, ...optionalParams);
	}

	override warn(fn_name: string | null, message: any, ...optionalParams: any[]): void {
		if (this.disabled) {
			return;
		}
		if (fn_name) {
			return super.warn(
				`${colors.pink}[${fn_name}]${colors.yellow} ` + message,
				...optionalParams,
			);
		}
		super.warn(message, ...optionalParams);
	}

	override debug(fn_name: string | null, message: any, ...optionalParams: any[]): void {
		if (this.disabled) {
			return;
		}
		if (fn_name) {
			return super.debug(
				`${colors.pink}[${fn_name}]${colors.pink} ` + message,
				...optionalParams,
			);
		}
		super.debug(message, ...optionalParams);
	}

	override verbose(fn_name: string | null, message: any, ...optionalParams: any[]): void {
		if (this.disabled) {
			return;
		}
		if (fn_name) {
			return super.verbose(
				`${colors.pink}[${fn_name}]${colors.cyan} ` + message,
				...optionalParams,
			);
		}
		super.verbose(message, ...optionalParams);
	}
}
