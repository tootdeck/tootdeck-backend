import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { readFileSync } from 'fs';

import { Logger } from '../../utils/logger';

@Injectable()
export class AppService {
	private readonly logger = new Logger(AppService.name);
	private MAIN_THREAD: boolean = false;
	private readonly DOMAIN: string;
	private readonly VERSION: string;
	private readonly ADMIN_HANDLE: Array<string> =
		this.ConfigService.get<string>('ADMIN_HANDLE')?.split(',') ?? [];

	constructor(private readonly ConfigService: ConfigService) {
		const domain = this.ConfigService.get<string>('DOMAIN');
		if (!domain) {
			this.logger.error('constructor', `No domain in environment variables.`);
			process.exit(1);
		}
		this.DOMAIN = domain!;

		try {
			this.VERSION = readFileSync('version', { encoding: 'utf8' });
			this.VERSION = this.VERSION.trim();
		} catch (e) {
			this.logger.error('constructor', 'Unable to read version file');
			console.log(e);
		}
	}

	isAdmin(handle: string): boolean {
		if (!handle) {
			return false;
		}

		return this.ADMIN_HANDLE.includes(handle);
	}

	getDomain(): string {
		return this.DOMAIN;
	}

	getVersion(): string {
		return this.VERSION;
	}

	setMainThread(): void {
		this.MAIN_THREAD = true;
	}

	isMainThread(): boolean {
		return this.MAIN_THREAD;
	}
}
