import { ConfigModule, ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { HttpModule } from '@nestjs/axios';
import { Redis } from 'ioredis';

import { InstanceCacheService } from './instanceCache.service';

import { RedisDB } from '../constants/redis';
import { RedisConfig } from '../configs/redis.config';

import { InstanceType } from '../../database/types';

describe('ConfigService', () => {
	let configService: ConfigService;
	let redis: Redis;
	let InstanceCache: InstanceCacheService;

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule({
			imports: [ConfigModule, RedisConfig([RedisDB.InstanceCache]), HttpModule],
			providers: [InstanceCacheService],
		}).compile();

		InstanceCache = app.get(InstanceCacheService);
		configService = app.get(ConfigService);
		redis = new Redis({
			db: RedisDB.InstanceCache,
			host: configService.get<string>('REDIS_HOST')!,
			port: configService.get<number>('REDIS_PORT')!,
		});
	});

	test('Fetch one instance', async () => {
		await InstanceCache.get('mastodon.social').then((result) => {
			expect(result).toBe(InstanceType.Mastodon);
		});
	});

	test('Get instance from cache', async () => {
		await InstanceCache.get('mastodon.social').then((result) => {
			expect(result).toBe(InstanceType.Mastodon);
		});
	});

	test('Fetch one invalid instance', async () => {
		await InstanceCache.get('example.com').then((result) => {
			expect(result).toBeNull();
		});
	});

	test('Get invalid instance from cache', async () => {
		await InstanceCache.get('example.com').then((result) => {
			expect(result).toBeNull();
		});
	});

	afterAll(async () => {
		await redis.keys('*').then((keys) => redis.del(keys));
	});
});
