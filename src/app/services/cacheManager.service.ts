import { Injectable } from '@nestjs/common';

export interface Cache<T> {
	cached: boolean;
	promise: Promise<T>;
}

@Injectable()
export class CacheManager {
	private readonly requests_cache = new Map<string, Promise<any>>();

	get<T>(request_id: string): Cache<T> | undefined {
		const cache = this.requests_cache.get(request_id);
		if (!cache) {
			return undefined;
		}

		return { cached: true, promise: cache };
	}

	await<T>(request_id: string, request: () => Promise<T>, cache_time: number = 3000): Cache<T> {
		const cache = this.get<T>(request_id);
		if (cache) {
			return cache;
		}

		const promise = request();

		this.requests_cache.set(request_id, promise);
		setTimeout(() => {
			this.requests_cache.delete(request_id);
		}, cache_time);

		return { cached: false, promise };
	}

	cancel(request_id: string) {
		this.requests_cache.delete(request_id);
	}
}
