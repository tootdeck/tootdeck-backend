import { Test, TestingModule } from '@nestjs/testing';
import { HttpModule } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';

import { AxiosCache } from './axiosCache.service';
import { AxiosProxy } from './axiosProxy.service';
import { AppService } from './app.service';

describe('AxiosProxy', () => {
	let cache: AxiosCache;

	function sleep(ms: number) {
		return new Promise((r) => setTimeout(r, ms));
	}

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule({
			imports: [HttpModule, ConfigModule],
			providers: [AxiosProxy, AxiosCache, AppService],
		}).compile();

		app.get(AxiosProxy).onModuleInit();

		cache = app.get(AxiosCache);
	});

	test('Make a request', async () => {
		const { cached, promise } = cache.request({
			url: 'https://mastodon.social/about',
		});
		promise.catch(() => {});

		expect(cached).toBe(false);
	});

	test('Make a request already in cache', async () => {
		const { cached, promise } = cache.request({
			url: 'https://mastodon.social/about',
		});

		expect(cached).toBe(true);
	});

	test('Make a request mixed cache', async () => {
		const { cached, promise } = cache.request(
			{
				url: 'https://mastodon.social/aboutt',
			},
			100,
		);
		promise.catch(() => {});

		expect(cached).toBe(false);

		await sleep(200);

		const { cached: c, promise: p } = cache.request(
			{
				url: 'https://mastodon.social/aboutt',
			},
			100,
		);
		p.catch(() => {});

		expect(c).toBe(false);
	});

	test('Make a request with no cache', async () => {
		const { cached, promise } = cache.request(
			{
				url: 'https://mastodon.social/aboutt',
			},
			0,
		);
		promise.catch(() => {});

		expect(cached).toBe(false);
	});
});
