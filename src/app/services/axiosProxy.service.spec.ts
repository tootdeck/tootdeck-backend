import { Test, TestingModule } from '@nestjs/testing';
import { HttpModule, HttpService } from '@nestjs/axios';
import { AxiosInstance } from 'axios';
import { ConfigModule } from '@nestjs/config';

import { AxiosProxy } from './axiosProxy.service';
import { AppService } from './app.service';

describe('AxiosCache', () => {
	let axios: AxiosInstance;

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule({
			imports: [HttpModule, ConfigModule],
			providers: [AxiosProxy, AppService],
		}).compile();

		app.get(AxiosProxy).onModuleInit();
		app.get(AxiosProxy).setWorker();

		axios = app.get(HttpService).axiosRef;
	});

	test('Test proxy', async () => {
		await axios
			.request<string>({
				url: 'https://mastodon.social/about',
			})
			.then((result) => {
				expect(result.data).toBeTruthy();
			});
		expect(() =>
			axios.request({
				url: 'https://mastodon.social/aboutt',
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				data: {
					und: 'und',
				},
			}),
		).rejects.toThrow();
		expect(() =>
			axios.request({
				url: 'https://mastodon.social/aboutt',
				timeout: 1,
			}),
		).rejects.toThrow();
	});
});
