import { Injectable, OnModuleInit } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { AxiosError, InternalAxiosRequestConfig } from 'axios';

import { AppService } from './app.service';

import colors from '../../utils/colors';
import { Logger } from '../../utils/logger';

@Injectable()
export class AxiosProxy implements OnModuleInit {
	private readonly logger = new Logger(AxiosProxy.name);
	private isWorker: boolean = false;

	constructor(
		private readonly HttpService: HttpService,
		private readonly AppService: AppService,
	) {}

	private objectToString(object: Object) {
		if (!object) {
			return '';
		}

		const entries = Object.entries(object);
		if (!entries.length) {
			return '';
		}

		return entries.map(([key, value]) => `${key}:"${value}"`).join(',');
	}

	private getPrefix(config: InternalAxiosRequestConfig<any>) {
		// @ts-ignore
		const prefix = config['addPrefix'] ?? '';
		return prefix ? `${colors.cyan}[${prefix}]${colors.end} ` : '';
	}

	private onError(error: AxiosError) {
		const config = error.config;
		const response = error.response;
		if (!config || !response) {
			this.logger.error('onError', error);
			console.error(error);

			throw error;
		}

		let params = this.objectToString(config.params);
		const prefix = this.getPrefix(config);
		if (config.url?.includes('/api/notes/show')) {
			// @ts-ignore
			params += ' ' + config['body_string'];
		}
		// @ts-ignore
		const response_time = new Date().valueOf() - config['request_time'];

		this.logger.verbose(
			'onError',
			`${prefix}${colors.yellow}[HTTP ${response.status} ${
				!isNaN(response_time) ? response_time + 'ms ' : ''
			}=> ${response.statusText || 'No status text'}] ${
				config.method?.toUpperCase?.() ?? ''
			} ${config.url} ${params}`,
		);

		throw error;
	}

	onModuleInit(): any {
		const { axiosRef: axios } = this.HttpService;

		axios.interceptors.request.use((config) => {
			const version = this.AppService.getVersion() ?? '';
			const ua = `TootDeck${this.isWorker ? '-Worker' : ''}${version ? '/' + version : ''}`;
			config.headers.setUserAgent(ua);

			let params = this.objectToString(config.params);
			const prefix = this.getPrefix(config);
			if (config.url?.includes('/api/notes/show')) {
				// @ts-ignore
				config['body_string'] = this.objectToString(config.data);
				// @ts-ignore
				params += ' ' + config['body_string'];
			}
			// @ts-ignore
			config['request_time'] = new Date().valueOf();

			this.logger.verbose(
				null,
				`${prefix}${colors.green}${config.method?.toUpperCase?.() ?? ''} ${
					config.url
				} ${params}`,
			);

			return config;
		}, this.onError.bind(this));

		axios.interceptors.response.use((response) => {
			const config = response.config;

			let params = this.objectToString(config.params);
			const prefix = this.getPrefix(config);
			if (config.url?.includes('/api/notes/show')) {
				// @ts-ignore
				params += ' ' + config['body_string'];
			}
			// @ts-ignore
			const response_time = new Date().valueOf() - config['request_time'];

			this.logger.verbose(
				null,
				`${prefix}${colors.pink}[HTTP ${response.status} ${response_time}ms] ${
					config.method?.toUpperCase?.() ?? ''
				} ${config.url} ${params}`,
			);

			return response;
		}, this.onError.bind(this));
	}

	setWorker() {
		this.isWorker = true;
	}
}
