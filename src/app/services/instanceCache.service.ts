import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { Redis } from 'ioredis';
import { RedisService } from '@liaoliaots/nestjs-redis';
import { deserialize, serialize } from 'v8';

import { detector } from '../../lib/megalodon/detector';

import { RedisNamespace } from '../constants/redis';

import { InstanceType } from '../../database/types';
import { RedisInstanceCache, RedisState } from '../types';

@Injectable()
export class InstanceCacheService {
	private readonly Redis: Redis;

	constructor(
		private readonly HttpService: HttpService,
		RedisService: RedisService,
	) {
		this.Redis = RedisService.getOrThrow(RedisNamespace.InstanceCache);
	}

	async get(hostname: string): Promise<InstanceType | null> {
		const cache = await this.Redis.getBuffer(hostname).then<RedisInstanceCache | null>(
			(buff) => (buff ? deserialize(buff) : buff),
		);
		if (cache) {
			switch (cache.state) {
				case RedisState.Ok:
					return cache.value as InstanceType;
				case RedisState.Dead:
					return null;
			}
		}

		const type = await detector('https://' + hostname, this.HttpService.axiosRef);
		if (!type.success) {
			const value: RedisInstanceCache = {
				state: RedisState.Dead,
				value: type.value,
			};
			await this.Redis.set(hostname, serialize(value));
			return null;
		}

		const value: RedisInstanceCache = {
			state: RedisState.Ok,
			value: type.value,
		};
		await this.Redis.set(hostname, serialize(value));

		return type.value as InstanceType;
	}
}
