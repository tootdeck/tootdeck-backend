import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class OptionalFeaturesService {
	readonly CACHE_STATUS_INTERACTIONS: boolean;
	readonly CACHE_STATUS_REACTIONS: boolean;
	readonly CACHE_TWITTER_EMBED: boolean;
	readonly CHANGELOG: boolean;

	// prettier-ignore
	constructor(private readonly configService: ConfigService) {
		this.CACHE_STATUS_INTERACTIONS = !!this.configService.get<boolean>('CACHE_STATUS_INTERACTIONS');
		this.CACHE_STATUS_REACTIONS = !!this.configService.get<boolean>('CACHE_STATUS_REACTIONS');
		this.CACHE_TWITTER_EMBED = !!this.configService.get<boolean>('CACHE_TWITTER_EMBED');

		this.CHANGELOG = !!this.configService.get<boolean>('CHANGELOG')
	}

	get(): {
		CACHE_STATUS_INTERACTIONS: boolean;
		CACHE_STATUS_REACTIONS: boolean;
		CACHE_TWITTER_EMBED: boolean;
		CHANGELOG: boolean;
	} {
		return {
			CACHE_STATUS_INTERACTIONS: this.CACHE_STATUS_INTERACTIONS,
			CACHE_STATUS_REACTIONS: this.CACHE_STATUS_REACTIONS,
			CACHE_TWITTER_EMBED: this.CACHE_TWITTER_EMBED,
			CHANGELOG: this.CHANGELOG,
		};
	}
}
