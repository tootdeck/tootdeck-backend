import { Test, TestingModule } from '@nestjs/testing';
import { ConfigModule } from '@nestjs/config';

import { TimesService } from './times.service';

describe('TimesService', () => {
	let times: TimesService;

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule({
			imports: [ConfigModule],
			providers: [TimesService],
		}).compile();

		times = app.get(TimesService);
	});

	test('getTimes', async () => {
		expect(times.getTimes()).toMatchObject({
			count: 0,
			average: 0,
			last: 0,
		});

		const len = 11;
		for (let i = 0; i < len; i++) {
			times.addTime(i * 10);
		}

		expect(times.getTimes()).toMatchObject({
			count: len,
			average: 50,
			last: 100,
		});

		for (let i = len; i < 1001; i++) {
			times.addTime(i * 10);
		}

		expect(times.getTimes()).toMatchObject({
			count: 1001,
			average: 5005,
			last: 10000,
		});
	});
});
