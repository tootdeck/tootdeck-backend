import { Injectable } from '@nestjs/common';

export interface ResponseTime {
	count: number;
	average: number;
	last: number;
}

@Injectable()
export class TimesService {
	private times: number[] = [];
	private nbr: number = 0;

	private calcAverage() {
		const len = this.times.length || 1;
		const total = len > 1 ? this.times.reduce((a, b) => a + b) : this.times[0] ?? 0;

		return total / len;
	}

	addTime(time: number) {
		this.times.unshift(time);

		if (this.times.length > 1000) {
			this.times.pop();
		}

		this.nbr++;
	}

	getTimes(): ResponseTime {
		return {
			count: this.nbr,
			average: this.calcAverage(),
			last: this.times[0] ?? 0,
		};
	}
}
