import { Test, TestingModule } from '@nestjs/testing';
import { ConfigModule, ConfigService } from '@nestjs/config';

import { OptionalFeaturesService } from './optionalFeatures.service';

describe('OptionalFeaturesService', () => {
	let config: ConfigService;

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule({
			imports: [ConfigModule],
			providers: [OptionalFeaturesService],
		}).compile();

		config = app.get(ConfigService);
	});

	test('Get full config', async () => {
		// @ts-ignore
		process.env['CACHE_STATUS_INTERACTIONS'] = true;
		// @ts-ignore
		process.env['CACHE_STATUS_REACTIONS'] = true;
		// @ts-ignore
		process.env['CACHE_TWITTER_EMBED'] = true;
		// @ts-ignore
		process.env['CHANGELOG'] = true;

		expect(new OptionalFeaturesService(config).get()).toMatchObject({
			CACHE_STATUS_INTERACTIONS: true,
			CACHE_STATUS_REACTIONS: true,
			CACHE_TWITTER_EMBED: true,
			CHANGELOG: true,
		});
	});

	test('Get empty config', async () => {
		// @ts-ignore
		process.env['CACHE_STATUS_INTERACTIONS'] = '';
		// @ts-ignore
		process.env['CACHE_STATUS_REACTIONS'] = '';
		// @ts-ignore
		process.env['CACHE_TWITTER_EMBED'] = '';
		// @ts-ignore
		process.env['CHANGELOG'] = '';

		expect(new OptionalFeaturesService(config).get()).toMatchObject({
			CACHE_STATUS_INTERACTIONS: false,
			CACHE_STATUS_REACTIONS: false,
			CACHE_TWITTER_EMBED: false,
			CHANGELOG: false,
		});
	});
});
