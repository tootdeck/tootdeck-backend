import { Test, TestingModule } from '@nestjs/testing';
import { ConfigModule } from '@nestjs/config';

import { AppService } from './app.service';

describe('AxiosProxy', () => {
	let application: AppService;

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule({
			imports: [ConfigModule],
			providers: [AppService],
		}).compile();

		application = app.get(AppService);
	});

	test('isAdmin', async () => {
		expect(application.isAdmin('tootdeck@tootdeck.org')).toBe(false);
		expect(application.isAdmin('')).toBe(false);
	});

	test('getDomain', async () => {
		expect(application.getDomain()).toBe('localhost');
	});

	test('getVersion', async () => {
		expect(application.getVersion()).toBeTruthy();
	});

	test('isMainThread', async () => {
		application.setMainThread();
		expect(application.isMainThread()).toBe(true);
	});
});
