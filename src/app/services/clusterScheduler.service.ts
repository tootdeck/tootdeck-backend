import { Injectable, ServiceUnavailableException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { RedisService } from '@liaoliaots/nestjs-redis';
import { ClientProxy, ClientProxyFactory, Transport } from '@nestjs/microservices';
import { Cron, CronExpression, Interval } from '@nestjs/schedule';
import { Redis } from 'ioredis';
import { timeout } from 'rxjs';
import { Axios } from 'axios';

import {
	AvailableJobs,
	EmojiGetStaticCallbackData,
	EmojiGetStaticJobData,
	InteractionsFetchCallbackData,
	InteractionsFetchJobData,
	InteractionsStoreJobData,
	TwitterEmbedJobData,
} from '../../worker/controllers/jobs.controller';

import { WsDashboardService } from '../../websocket/services/ws.dashboard.service';
import { WsStatsService } from '../../websocket/services/ws.stats.service';
import { DBWorkerStatsService } from '../../database/services/database.worker.stats.service';

import { WorkerStats as DBWorkerStats } from '../../database/entities/worker.stats.entity';

import { RedisDB, RedisNamespace } from '../constants/redis';

import { Logger } from '../../utils/logger';

import {
	DashboardStats,
	Worker as DashboardWorker,
} from '../../dashboard/properties/entities.property';

import { WebsocketDashboard } from '../../websocket/types';
import { Workers } from '../../worker/types';
import { UUID } from 'crypto';

interface Worker {
	uuid: UUID;
	ip: string;
	ping: number;
	concurrency: number;
	client: ClientProxy;
	stats: Workers.Stats;
	/**
	 * When the main thread restart but not the worker,
	 * the worker `requests_made` is not reset and cause
	 * it's already added value to be added twice.
	 *
	 * This value is used as a fallback in case the http method `resetWorkerRequestsCount` fail.
	 */
	requests_made_offset: number;
}

type JobCallback = (data: any) => void;

interface ParsedJob {
	job: AvailableJobs;
	data: any;
	callbacks: Array<JobCallback>;
}

@Injectable()
export class ClusterSchedulerService {
	private readonly logger = new Logger(ClusterSchedulerService.name);
	private readonly redis_interactions: Redis;

	private readonly WORKER_PORT = this.ConfigService.getOrThrow<number>('WORKER_PORT');
	private readonly workers: Array<Worker> = [];
	private readonly waiting_jobs_queue: Map<string, ParsedJob> = new Map();
	private readonly Axios: Axios = new Axios({
		method: 'GET',
		transformResponse: (res, headers) => {
			if ((headers['content-type'] as string)?.includes('application/json')) {
				return JSON.parse(res);
			}

			return res;
		},
	});

	private readonly accounts_stats = new Map<string, number>();
	private DBstats: DBWorkerStats;
	private stats = {
		jobs: {
			processed: 0,
			failed: 0,
		},
		requests_made: 0,
		worker: {
			queue_max: 0,
			memory_max: 0,
			jobs: {
				time_min: Infinity,
				time_max: 0,
				time_average: 0,
			},
		},
	};
	private workers_last_times: Array<number> = [];
	private max_concurrency: number = 5;

	constructor(
		private readonly ConfigService: ConfigService,
		private readonly WsDashboardService: WsDashboardService,
		private readonly WsStatsService: WsStatsService,
		private readonly DBWorkerStatsService: DBWorkerStatsService,
		RedisService: RedisService,
	) {
		this.redis_interactions = RedisService.getOrThrow(RedisNamespace.StatusesInteractions);
		this.initStats();
	}

	/**
	 * Init
	 */
	// #region

	private async connectError(data: Workers.Register, retry_count: number, error?: any) {
		if (retry_count > 3) {
			const { uuid, ip } = data;

			this.logger.error('register', `Unable to connect to worker ${uuid} ${ip}`);
			if (error) {
				this.logger.error('register', `${uuid}: ${error}`, error);
			}
			return;
		}

		setTimeout(() => this.register(data, ++retry_count), 2000);
	}

	async register(data: Workers.Register, retry_count: number = 0) {
		if (process.env['NODE_ENV'] === 'test') {
			return;
		}

		const { uuid, ip } = data;

		const client = ClientProxyFactory.create({
			transport: Transport.TCP,
			options: {
				host: ip,
				port: this.WORKER_PORT,
			},
		});

		try {
			await client.connect();

			const worker: Worker = {
				uuid,
				ip,
				ping: Date.now(),
				concurrency: 0,
				client,
				requests_made_offset: 0,
				stats: {
					uuid,
					memory: 0,
					started_at: new Date(),
					requests_made: 0,
					times: {
						average: 0,
						count: 0,
						last: 0,
					},
				},
			};

			/**
			 * When the main thread restart but not the worker,
			 * the worker `requests_made` need to be reset to not cause
			 * it's already added value to be added twice.
			 */
			await this.resetWorkerRequestsCount(worker);

			this.workers.push(worker);

			this.logger.log('register', `Added worker ${uuid} ${ip}`);
		} catch (e) {
			this.connectError(data, retry_count, e);
		}
	}

	async unregister(uuid: UUID, e?: any) {
		const index = this.workers.findIndex((worker) => worker.uuid === uuid);
		if (index === -1) {
			return;
		}

		const worker = this.workers[index]!;
		this.workers.splice(index, 1);
		worker.client.close();

		if (e) {
			this.logger.error(
				'unregister',
				`Unable to comunicate with the worker ${uuid} ${worker.ip}`,
				e,
			);
		} else {
			this.logger.log('unregister', `Removed worker ${uuid} ${worker.ip}`);
		}
	}

	// #endregion

	/**
	 * Lifecylce
	 */
	// #region

	@Interval(15000)
	private async healthcheck() {
		for (let i = 0; i < this.workers.length; i++) {
			const worker = this.workers[i]!;

			const diff = Date.now() - worker.ping;
			if (diff > 25000) {
				this.logger.warn(
					'healthcheck',
					`No health update since ${diff}ms from worker ${worker.uuid} ${worker.ip}, removed`,
				);
				worker.client.close();
				this.workers.splice(i, 1);
			}

			// this.logger.log('healthcheck', `OK (UUID=${uuid} IP=${worker.ip})`);
		}
	}

	async updateHealth(data: Workers.Register) {
		const worker = this.workers.find((worker) => worker.uuid === data.uuid);
		if (!worker) {
			this.register(data);
			return;
		}

		worker.ping = Date.now();
	}

	// #endregion

	/**
	 * Stats
	 */
	// #region

	private async initStats(): Promise<void> {
		const latest = await this.DBWorkerStatsService.getLast();
		if (!latest) {
			this.DBstats = {
				jobs_failed: 0,
				jobs_processed: 0,
				requests_made: 0,
				//
				worker_job_time_average: 0,
				worker_job_time_max: 0,
				worker_job_time_min: 0,
				worker_memory_max: 0,
				worker_queue_max: 0,
				cache_size_max: 0,
				cache_size_dead: 0,
				//
				merge_type: 0,
				created_at: new Date(),
				uuid: '',
			};
			return;
		}

		this.DBstats = latest;

		this.stats = {
			jobs: {
				processed: latest.jobs_processed,
				failed: latest.jobs_failed,
			},
			requests_made: latest.requests_made,
			worker: {
				queue_max: 0,
				memory_max: 0,
				jobs: {
					time_min: 0,
					time_max: 0,
					time_average: 0,
				},
			},
		};
	}

	private async resetWorkerRequestsCount(worker: Worker, retrying: boolean = false) {
		return this.Axios.post('http://' + worker.ip + ':3000/reset_requests_count')
			.then(() => {
				this.logger.log(
					'resetWorkerRequestsCount',
					`Successfully reset worker ${worker.uuid} ${worker.ip} requests count`,
				);
			})
			.catch((e) => {
				if (!retrying) {
					return new Promise((resolve, reject) => {
						setTimeout(
							() =>
								this.resetWorkerRequestsCount(worker, true)
									.then(resolve)
									.catch(reject),
							2000,
						);
					});
				}
				this.logger.error(
					'resetWorkerRequestsCount',
					`Unable to reset worker ${worker.uuid} ${worker.ip} requests count`,
				);
				throw e;
			});
	}

	getStats(): DashboardStats {
		const workers: Array<DashboardWorker> = this.workers.map((worker) => {
			const { requests_made, ...data } = worker.stats;

			return { ...data, concurrency: worker.concurrency };
		});

		return {
			jobs: {
				processed: this.stats.jobs.processed,
				failed: this.stats.jobs.failed,
			},
			requests_made: this.stats.requests_made,
			queue_size: this.waiting_jobs_queue.size,
			max_concurrency: this.max_concurrency,
			workers,
		};
	}

	getAccountsStats() {
		return Array.from(this.accounts_stats);
	}

	private updateAccountStats(handle: string) {
		let stats = this.accounts_stats.get(handle);
		if (stats === undefined) {
			stats = 0;
		}

		this.accounts_stats.set(handle, ++stats);

		this.delayWebsocketSend({
			type: WebsocketDashboard.ResponseType.User,
			data: {
				sockets: this.WsStatsService.get(),
				account: {
					handle,
					jobs_count: stats,
				},
			},
		});
	}

	private calcAverageWorkerTimes() {
		const average =
			this.workers_last_times.length > 1
				? Math.round(
						this.workers_last_times.reduce((previous, current) => previous + current) /
							this.workers_last_times.length,
					)
				: (this.workers_last_times[0] ?? 0);

		return average;
	}

	private calcRequestsMade() {
		const requests_made =
			this.workers.length > 1
				? this.workers
						.map((w) => ({
							value: w.stats.requests_made,
							offset: w.requests_made_offset,
						}))
						.reduce((previous, current) => ({
							value: previous.value + (current.value - current.offset),
							offset: 0,
						})).value
				: (this.workers[0]?.stats.requests_made ?? 0);
		return this.DBstats.requests_made + requests_made;
	}

	private calcWorkersMemoryMax() {
		return this.workers.length > 1
			? this.workers
					.map((w) => w.stats.memory)
					.reduce((previous, current) => Math.max(previous, current))
			: (this.workers[0]?.stats.memory ?? 0);
	}

	private updateWorkerStats(worker: Worker, stats?: Workers.Stats): void {
		if (stats) {
			worker.stats = stats;

			const last = stats.times.last;
			this.workers_last_times.unshift(last);

			this.stats.worker.jobs.time_min = Math.min(
				this.stats.worker.jobs.time_min,
				last || Infinity,
			);
			this.stats.worker.jobs.time_max = Math.max(this.stats.worker.jobs.time_max, last);
			this.stats.worker.jobs.time_average = this.calcAverageWorkerTimes();

			this.stats.requests_made = this.calcRequestsMade();
			this.stats.worker.memory_max = this.calcWorkersMemoryMax();
		}

		const { requests_made, ...worker_stats } = worker.stats;

		this.delayWebsocketSend({
			type: WebsocketDashboard.ResponseType.Data,
			data: {
				jobs: { processed: this.stats.jobs.processed, failed: this.stats.jobs.failed },
				max_concurrency: this.max_concurrency,
				requests_made: this.stats.requests_made,
				queue_size: this.waiting_jobs_queue.size,
				worker: { ...worker_stats, concurrency: worker.concurrency },
			},
		});
	}

	private websocket_delay: NodeJS.Timeout | null = null;
	private websocket_data: { [key: string]: WebsocketDashboard.Data | WebsocketDashboard.User } =
		{};

	private delayWebsocketSend(data: WebsocketDashboard.AvailableResponse) {
		if (this.websocket_delay) {
			clearTimeout(this.websocket_delay);
		}

		switch (data.type) {
			case WebsocketDashboard.ResponseType.Data:
				this.websocket_data[`worker=${data.data.worker.uuid}`] = data;
				break;
			case WebsocketDashboard.ResponseType.User:
				this.websocket_data[`handle=${data.data.account.handle}`] = data;
				break;
		}

		this.websocket_delay = setTimeout(() => {
			Object.values(this.websocket_data).forEach((data) =>
				this.WsDashboardService.send(data),
			);

			this.websocket_data = {};
		}, 500);
	}

	private async redisStats(): Promise<{ keys: number; dead_keys: number }> {
		/**
		 * # Keyspace
		 * db0:keys=1,expires=1,avg_ttl=1197806
		 * db1:keys=1,expires=1,avg_ttl=172287226
		 * db3:keys=1,expires=0,avg_ttl=0
		 * db5:keys=1,expires=1,avg_ttl=2152500
		 * db7:keys=146,expires=141,avg_ttl=49012786
		 * db8:keys=9,expires=0,avg_ttl=0
		 * db9:keys=1,expires=0,avg_ttl=0
		 * db10:keys=50,expires=0,avg_ttl=0
		 */
		const keyspace_info = await this.redis_interactions.info('Keyspace');
		if (!keyspace_info) {
			return { keys: 0, dead_keys: 0 };
		}

		/**
		 * db7:keys=146,expires=141,avg_ttl=49012786
		 */
		const db = 'db' + RedisDB.StatusesInteractions;
		const array = keyspace_info.trim().split('\n');
		const raw_interactions_info = array.find((str) => str.includes(db))!;
		/**
		 * keys=146
		 * expires=141
		 * avg_ttl=49012786
		 */
		// Empty DB
		if (!raw_interactions_info) {
			return { keys: 0, dead_keys: 0 };
		}

		const interactions_info = raw_interactions_info.slice(db.length + 1).split(',');

		const keys_info = +interactions_info.find((str) => str.includes('keys'))!.split('=')[1]!;
		const expires_info = +interactions_info
			.find((str) => str.includes('expires'))!
			.split('=')[1]!;
		// const avg_ttl_info = interactions_info.find(str => str.includes("avg_ttl"))!.split('=')[1]!

		return { keys: keys_info, dead_keys: keys_info - expires_info };
	}

	@Cron(CronExpression.EVERY_MINUTE, {
		disabled: process.env['NODE_ENV'] === 'test',
	})
	private async saveWorkerStats() {
		const memory_max =
			this.stats.worker.memory_max === 0
				? this.calcWorkersMemoryMax()
				: this.stats.worker.memory_max;
		const time_min =
			this.stats.worker.jobs.time_min === Infinity ? 0 : this.stats.worker.jobs.time_min;

		const { keys, dead_keys } = await this.redisStats();

		this.DBWorkerStatsService.create({
			jobs_processed: this.stats.jobs.processed,
			jobs_failed: this.stats.jobs.failed,
			requests_made: this.stats.requests_made,
			worker_queue_max: this.stats.worker.queue_max,
			worker_memory_max: memory_max,
			worker_job_time_min: time_min,
			worker_job_time_max: this.stats.worker.jobs.time_max,
			worker_job_time_average: this.stats.worker.jobs.time_average,
			cache_size_max: keys,
			cache_size_dead: dead_keys,
		});

		this.workers_last_times = [];
		this.stats.worker.queue_max = 0;
		this.stats.worker.memory_max = 0;
		this.stats.worker.jobs.time_min = Infinity;
		this.stats.worker.jobs.time_max = 0;
		this.stats.worker.jobs.time_average = 0;
	}

	// #endregion

	/**
	 * Jobs
	 */
	// #region

	hasWorker() {
		return this.workers.length !== 0;
	}

	/**
	 * Find an available worker
	 */
	private findWorker(): Worker | undefined {
		const sorted = this.workers.sort((a, b) => a.concurrency - b.concurrency);
		const worker = sorted[0]!;
		if (worker.concurrency >= this.max_concurrency) {
			return undefined;
		}

		return worker;
	}

	private createJob(worker: Worker, args: ParsedJob) {
		worker.concurrency++;

		worker.client
			.send(args.job, args.data)
			.pipe(timeout({ each: 60000 }))
			.subscribe({
				next: (x: Workers.JobResult<any>) => {
					if (x === undefined) {
						this.stats.jobs.failed++;
					}

					worker.concurrency--;
					this.updateWorkerStats(worker, x.stats);

					args.callbacks.forEach((callback) => callback(x.result));
				},
				error: (e) => {
					this.updateWorkerStats(worker);

					const error = JSON.parse(JSON.stringify(e)) as {
						errno: number;
						code: string;
						syscall: string;
						address: string;
						port: number;
					};
					this.logger.error('sendJob', '');
					console.error(e);

					//  Worker went offline
					if (error?.syscall === 'connect') {
						this.unregister(worker.uuid);

						this.logger.log('sendJob', 'Rescheduling job');

						args.callbacks.forEach((callback) =>
							this.queueJob(args.job, args.data, callback),
						);

						return;
					}

					worker.concurrency--;
					this.stats.jobs.failed++;

					args.callbacks.forEach((callback) => callback(undefined));
				},
				complete: () => {
					this.stats.jobs.processed++;
				},
			});

		this.updateWorkerStats(worker);
	}

	private queue_processing: boolean = false;

	@Interval(500)
	private async process_queue() {
		if (this.queue_processing || !this.waiting_jobs_queue.size) {
			return;
		}

		this.queue_processing = true;

		this.stats.worker.queue_max = Math.max(
			this.stats.worker.queue_max,
			this.waiting_jobs_queue.size,
		);

		const parsedJobs = Array.from(this.waiting_jobs_queue).map((data) => {
			const cast = data as [string, ParsedJob & { callback_count: number }];
			cast[1].callback_count = data[1].callbacks.length;
			return cast;
		});

		while (parsedJobs.length) {
			let worker = this.findWorker()!;
			if (!worker) {
				this.queue_processing = false;
				return;
			}

			const [key, job] = parsedJobs.pop()!;

			this.createJob(worker, job);

			const current = this.waiting_jobs_queue.get(key);
			if (current?.callbacks.length === job.callback_count) {
				this.waiting_jobs_queue.delete(key);
			} else if (current) {
				current.callbacks = current.callbacks.filter(
					(curr) => !job.callbacks.find((exec) => Object.is(exec, curr)),
				);
			}
		}

		this.queue_processing = false;
	}

	private queueJob(job: AvailableJobs, data: any, callback: (data: any) => void) {
		const key = JSON.stringify([job, data]);
		const found = this.waiting_jobs_queue.get(key);
		if (found) {
			found.callbacks.push(callback);
		} else {
			const entry: ParsedJob = {
				job,
				data,
				callbacks: [callback],
			};
			this.waiting_jobs_queue.set(key, entry);
		}
	}

	/**
	 * Send job to an available worker and return a promise when the job is finished
	 */
	async awaitJob<T>(
		handle: string,
		job: AvailableJobs.InteractionsFetch,
		data: InteractionsFetchJobData,
	): Promise<T>;
	async awaitJob<T>(
		handle: string,
		job: AvailableJobs.InteractionsStore,
		data: InteractionsStoreJobData,
	): Promise<T>;
	async awaitJob<T>(
		handle: string,
		job: AvailableJobs.EmojiGetStatic,
		data: EmojiGetStaticJobData,
	): Promise<T>;
	async awaitJob<T>(
		handle: string,
		job: AvailableJobs.TwitterEmbed,
		data: TwitterEmbedJobData,
	): Promise<T>;
	async awaitJob<T>(handle: string, job: AvailableJobs, data: any): Promise<T> {
		if (!this.workers.length) {
			throw new ServiceUnavailableException();
		}

		return new Promise<T>((resolve) => {
			this.sendJob(handle, job, data, (result) => resolve(result ?? undefined));
		});
	}

	/**
	 * Send job to an available worker
	 */
	sendJob(
		handle: string,
		job: AvailableJobs.InteractionsFetch,
		data: InteractionsFetchJobData,
		callback: (data: InteractionsFetchCallbackData) => void,
	): void;
	sendJob(
		handle: string,
		job: AvailableJobs.InteractionsStore,
		data: InteractionsStoreJobData,
	): void;
	sendJob(
		handle: string,
		job: AvailableJobs.EmojiGetStatic,
		data: EmojiGetStaticJobData,
		callback: (data: EmojiGetStaticCallbackData) => void,
	): void;
	sendJob(handle: string, job: AvailableJobs, data: any, callback: (data: any) => void): void;
	sendJob(
		handle: string,
		job: AvailableJobs,
		data: any,
		callback: (data: any) => void = () => {},
	): void {
		if (!this.workers.length) {
			return;
		}

		this.updateAccountStats(handle);

		this.queueJob(job, data, callback);
	}

	// #endregion
}
