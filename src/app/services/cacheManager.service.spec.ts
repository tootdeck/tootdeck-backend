import { Test, TestingModule } from '@nestjs/testing';

import { CacheManager } from './cacheManager.service';

describe('CacheManager', () => {
	let cache: CacheManager;

	function sleep(ms: number) {
		return new Promise((r) => setTimeout(r, ms));
	}

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule({
			providers: [CacheManager],
		}).compile();

		cache = app.get(CacheManager);
	});

	test('Cache a request', async () => {
		const { cached, promise } = cache.await('id:1', () => sleep(3000));

		expect(cached).toBe(false);
	});

	test('Cache a request already in cache', async () => {
		const { cached, promise } = cache.await('id:1', () => sleep(3000));

		expect(cached).toBe(true);
	});

	test('Get a cached request', async () => {
		const result = cache.get('id:1');

		expect(result?.cached).toBe(true);
	});

	test('Get a invalid cached request', async () => {
		const result = cache.get('id:2');

		expect(result).toBeUndefined();
	});

	test('Cancel a cached request', async () => {
		cache.cancel('id:1');
		const result = cache.get('id:1');

		expect(result).toBeUndefined();
	});

	test('Cancel a cached request', async () => {
		const { cached, promise } = cache.await('id:2', () => sleep(100), 100);

		expect(cached).toBe(false);

		await sleep(200);

		const result = cache.get('id:2');

		expect(result).toBeUndefined();
	});
});
