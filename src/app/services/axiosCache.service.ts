import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { AxiosRequestConfig, AxiosResponse } from 'axios';

import { Cache, CacheManager } from './cacheManager.service';

@Injectable()
export class AxiosCache {
	private readonly RequestsManager = new CacheManager();

	constructor(private readonly HttpService: HttpService) {}

	request<T = any>(config: AxiosRequestConfig, timeout: number = 3000): Cache<AxiosResponse<T>> {
		if (timeout === 0) {
			return { cached: false, promise: this.HttpService.axiosRef(config) };
		}

		const { url, method, headers, data, params } = config;
		const key = JSON.stringify({
			url,
			method,
			headers,
			data,
			params,
		});

		config.timeout = timeout;

		return this.RequestsManager.await(key, () => this.HttpService.axiosRef(config), timeout);
	}
}
