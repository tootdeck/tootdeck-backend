import { Injectable } from '@nestjs/common';
import { Redis } from 'ioredis';
import { RedisService } from '@liaoliaots/nestjs-redis';
import { serialize } from 'v8';

import { RedisNamespace } from '../constants/redis';

import { RedisExcludedDomain } from '../../dashboard/properties/redis/excludedDomain.property';

import { RedisExpiry, RedisExpiryToken, RedisState } from '../types';

@Injectable()
export class FailedDomainsService {
	private readonly redis_failed_domain: Redis;
	private readonly redis_excluded_domains: Redis;

	constructor(RedisService: RedisService) {
		this.redis_failed_domain = RedisService.getOrThrow(RedisNamespace.FailedDomain);
		this.redis_excluded_domains = RedisService.getOrThrow(RedisNamespace.ExcludedDomains);
	}

	private async set(hostname: string, count: number) {
		return this.redis_failed_domain.set(hostname, count).then(() => count);
	}

	private async get(hostname: string) {
		const count = await this.redis_failed_domain.get(hostname);
		if (!count) {
			return this.set(hostname, 1);
		}

		return +count;
	}

	async getExcludedDomains() {
		return this.redis_excluded_domains.keys('*');
	}

	async setExcludedDomain(hostname: string, reason: RedisExcludedDomain, expiry?: RedisExpiry) {
		if (expiry?.token === RedisExpiryToken.NX) {
			return this.redis_excluded_domains.set(
				hostname,
				serialize(reason),
				// @ts-ignore
				expiry.token,
			);
		}

		if (expiry) {
			return this.redis_excluded_domains.set(
				hostname,
				serialize(reason),
				// @ts-ignore
				expiry.token,
				expiry.time,
			);
		}

		return this.redis_excluded_domains.set(hostname, serialize(reason));
	}

	/**
	 * @returns `true` if the domain is excluded
	 */
	async excludeDomain(hostname: string, reason?: string): Promise<boolean> {
		const count = await this.get(hostname);
		if (count < 10) {
			await this.set(hostname, count + 1);
			return false;
		}

		if (count > 20) {
			await this.setExcludedDomain(hostname, {
				state: RedisState.Dead,
				value: reason ?? `Failed to many times`,
			});
			return true;
		}

		const hours = (count - 10 + 1) * 6;
		const new_ex = hours * 60 * 60;

		await this.setExcludedDomain(
			hostname,
			{
				state: RedisState.Retry,
				value: reason ?? `Excluded for ${hours} hours\nFailed to many times`,
			},
			{
				token: RedisExpiryToken.EX,
				time: new_ex,
			},
		);
		await this.set(hostname, count + 1);

		return true;
	}
}
