import {
	BadRequestException,
	Body,
	Controller,
	InternalServerErrorException,
	Param,
	Post,
	Request,
	Response,
	ServiceUnavailableException,
	UseGuards,
} from '@nestjs/common';
import { ApiBody, ApiParam, ApiResponse, ApiTags } from '@nestjs/swagger';
import { FastifyReply } from 'fastify';

import {
	AvailableJobs,
	EmojiGetStaticCallbackData,
	TwitterEmbedCallbackData,
} from '../../worker/controllers/jobs.controller';

import { ClusterSchedulerService } from '../services/clusterScheduler.service';
import { UserCacheService } from '../../user/services/user.cache.service';
import { OptionalFeaturesService } from '../services/utils/optionalFeatures.service';

import { AccessGuard } from '../../auth/guards/access.guard';

import { Logger } from '../../utils/logger';

import { Body as FormBody } from '../../mirror/properties/body.property';
import { TwitterEmebedFrom } from '../properties/twitter.property';
import { TwitterResponse } from '../../dashboard/properties/redis/twitter.property';

import { RequestAccessGuard } from '../../auth/types/guards.types';
import { RedisState } from '../types';

@ApiTags('Worker')
@UseGuards(AccessGuard)
@Controller('worker')
export class WorkerController {
	private readonly logger = new Logger(WorkerController.name);

	constructor(
		private readonly ClusterSchedulerService: ClusterSchedulerService,
		private readonly UserCacheService: UserCacheService,
		private readonly OptionalFeaturesService: OptionalFeaturesService,
	) {}

	private async getHandle(req: RequestAccessGuard): Promise<string> {
		const user_uuid = req.access_session.user_uuid;
		const user = await this.UserCacheService.get(user_uuid);
		if (!user) {
			this.logger.error(null, 'Failed to get user from cache');
			throw new InternalServerErrorException();
		}

		return user.main.username + '@' + user.main.application.domain;
	}

	@ApiParam({
		name: 'identifier',
		type: String,
		required: true,
	})
	@ApiBody({
		type: FormBody.StaticEmoji,
		required: false,
	})
	@ApiResponse({
		status: 200,
		description: 'URL or local file name.',
		type: String,
	})
	@ApiResponse({
		status: 400,
		description: 'Missing or invalid identifier',
	})
	@ApiResponse({
		status: 503,
		description: 'No started worker',
	})
	@Post('static_emoji/:identifier')
	async static_emoji(
		@Request() req: RequestAccessGuard,
		@Response({ passthrough: true }) res: FastifyReply,
		@Param('identifier') identifier: string,
		@Body('cdn') cdn?: string,
	) {
		const check = identifier.split('@');
		if (!check[0] || !check[1]) {
			throw new BadRequestException();
		}

		const handle = await this.getHandle(req);

		return this.ClusterSchedulerService.awaitJob<EmojiGetStaticCallbackData>(
			handle,
			AvailableJobs.EmojiGetStatic,
			{
				identifier,
				cdn,
			},
		).then((r) => {
			if (!r) {
				res.code(500);
				return;
			}

			res.code(200);

			return r;
		});
	}

	@ApiBody({
		type: TwitterEmebedFrom,
		required: false,
	})
	@ApiResponse({
		status: 200,
		description: 'Twitter embed json',
		type: TwitterResponse,
	})
	@ApiResponse({
		status: 204,
		description: 'Twitter embed not available',
	})
	@ApiResponse({
		status: 400,
		description: 'Missing or wrong url',
	})
	@ApiResponse({
		status: 503.1,
		description: 'Feature disabled',
	})
	@ApiResponse({
		status: 503.2,
		description: 'No started worker',
	})
	@Post('twitter_embed')
	async twitter_embed(
		@Request() req: RequestAccessGuard,
		@Response({ passthrough: true }) res: FastifyReply,
		@Body('url') url: string,
	) {
		if (!this.OptionalFeaturesService.CACHE_TWITTER_EMBED) {
			throw new ServiceUnavailableException();
		}

		const sanitizeLink = (href: string): string | undefined => {
			if (!href) {
				return undefined;
			}

			const { hostname, origin, pathname } = new URL(href);

			return [
				'twitter.com',
				'vxtwitter.com',
				'fxtwitter.com',
				'twittpr.com',
				'x.com',
				'fixupx.com',
				'xcancel.com',
			].includes(hostname) && pathname.includes('status')
				? origin + pathname
				: undefined;
		};

		const sanitized_url = sanitizeLink(url);
		if (!sanitized_url) {
			throw new BadRequestException();
		}

		const handle = await this.getHandle(req);

		return this.ClusterSchedulerService.awaitJob<TwitterEmbedCallbackData>(
			handle,
			AvailableJobs.TwitterEmbed,
			{
				url: sanitized_url,
			},
		).then((r) => {
			if (!r) {
				res.code(500);
				return;
			}

			if (r.state === RedisState.Dead) {
				res.code(204);
				return;
			}

			res.code(200);

			return r.value;
		});
	}
}
