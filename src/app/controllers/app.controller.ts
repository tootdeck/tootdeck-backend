import { Controller, Get, NotFoundException, UseGuards } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

import { TimesService } from '../services/utils/times.service';
import { OptionalFeaturesService } from '../services/utils/optionalFeatures.service';
import { ClusterSchedulerService } from '../services/clusterScheduler.service';
import { AppService } from '../services/app.service';

import { AccessGuard } from '../../auth/guards/access.guard';

import { ApiResponseTime } from '../properties/times.property';
import { ApiOptionalFeatures } from '../properties/optionalFeatures.property';

@ApiTags('Other')
@Controller()
export class AppController {
	constructor(
		private readonly TimesService: TimesService,
		private readonly OptionalFeaturesService: OptionalFeaturesService,
		private readonly AppService: AppService,
		private readonly ClusterSchedulerService: ClusterSchedulerService,
	) {}

	@Get('ping')
	ping(): string {
		return 'PONG';
	}

	@Get('version')
	version(): string {
		const version = this.AppService.getVersion();
		if (!version) {
			throw new NotFoundException();
		}
		return version;
	}

	@UseGuards(AccessGuard)
	@ApiResponse({ status: 200, description: 'API response time', type: ApiResponseTime })
	@Get('stats')
	stats() {
		return this.TimesService.getTimes();
	}

	@UseGuards(AccessGuard)
	@ApiResponse({
		status: 200,
		description: 'Show avaliable optional features',
		type: ApiOptionalFeatures,
	})
	@Get('optional_features')
	features() {
		const optional = this.OptionalFeaturesService.get();

		return {
			...optional,
			WORKER: this.ClusterSchedulerService.hasWorker(),
		};
	}
}
