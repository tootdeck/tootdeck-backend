import { Controller } from '@nestjs/common';
import { EventPattern } from '@nestjs/microservices';
import { UUID } from 'crypto';

import { ClusterSchedulerService } from '../services/clusterScheduler.service';

import { Workers } from '../../worker/types';

@Controller()
export class WorkerLifecycleController {
	constructor(private readonly ClusterSchedulerService: ClusterSchedulerService) {}

	@EventPattern(Workers.Event.Register)
	register(data: Workers.Register) {
		this.ClusterSchedulerService.register(data);
	}

	@EventPattern(Workers.Event.Unregister)
	unregister(uuid: UUID) {
		this.ClusterSchedulerService.unregister(uuid);
	}

	@EventPattern(Workers.Event.Healthcheck)
	updateHealth(data: Workers.Register) {
		this.ClusterSchedulerService.updateHealth(data);
	}
}
