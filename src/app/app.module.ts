import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { HttpModule } from '@nestjs/axios';
import { ScheduleModule } from '@nestjs/schedule';

import { ResponseTimeMiddleware } from './middlewares/times.middleware';

import { InjectorInterceptor } from '../mirror/interceptors/injector.interceptor';

import { AppController } from './controllers/app.controller';
import { AuthController } from '../auth/controllers/auth.controller';
import { OAuthController } from '../auth/controllers/oauth.controller';
import { AuthSessionsController } from '../auth/controllers/auth.sessions.controller';
import { UserController } from '../user/user.controller';
import { DashboardController } from '../dashboard/controllers/dashboard.controller';
import { DashboardRedisController } from '../dashboard/controllers/dashboard.redis.controller';
import { MirrorAccountsController } from '../mirror/controllers/accounts/accounts.controller';
import { MirrorAccountsOthersController } from '../mirror/controllers/accounts/accounts.others.controller';
import { MirrorAccountsTagsController } from '../mirror/controllers/accounts/accounts.tags.controller';
import { MirrorAccountsDomainBlocksController } from '../mirror/controllers/accounts/accounts.domainblocks.controller';
import { MirrorAccountsFiltersController } from '../mirror/controllers/accounts/accounts.filters.controller';
import { MirrorAccountsFollowRequestController } from '../mirror/controllers/accounts/accounts.follow_requests.controller';
import { MirrorTimelinesController } from '../mirror/controllers/timelines/timelines.controller';
import { MirrorTimelinesConversationsController } from '../mirror/controllers/timelines/timelines.conversations.controller';
import { MirrorTimelinesMarkersController } from '../mirror/controllers/timelines/timelines.markers.controller';
import { MirrorNotificationsController } from '../mirror/controllers/notifications.contoller';
import { MirrorInstanceController } from '../mirror/controllers/instance/instance.controller';
import { MirrorInstanceTrendsController } from '../mirror/controllers/instance/instance.trends.controller';
import { MirrorInstanceDirectoryController } from '../mirror/controllers/instance/instance.directory.controller';
import { MirrorInstanceEmojisController } from '../mirror/controllers/instance/instance.emojis.controller';
import { MirrorInstanceAnnouncementsController } from '../mirror/controllers/instance/instance.announcements.controller';
import { MirrorStatusesController } from '../mirror/controllers/statuses/statuses.controller';
import { MirrorStatusesMediaController } from '../mirror/controllers/statuses/statuses.media.controller';
import { MirrorStatusesPollsController } from '../mirror/controllers/statuses/statuses.poll.controller';
import { MirrorStatusesScheduledController } from '../mirror/controllers/statuses/statuses.scheduled.controller';
import { MirrorTimelinesListsController } from '../mirror/controllers/timelines/timelines.lists.controller';
import { MirrorSearchController } from '../mirror/controllers/search.contoller';
import { WorkerController } from './controllers/worker.controller';
import { WorkerLifecycleController } from './controllers/workerLifecycle.controller';

import { AppService } from './services/app.service';
import { AxiosProxy } from './services/axiosProxy.service';
import { AxiosCache } from './services/axiosCache.service';
import { CacheManager } from './services/cacheManager.service';
import { OptionalFeaturesService } from './services/utils/optionalFeatures.service';
import { TimesService } from './services/utils/times.service';
import { DBApplicationService } from '../database/services/database.application.service';
import { DBAccountService } from '../database/services/database.account.service';
import { DBUserService } from '../database/services/database.user.service';
import { DBUserConfigService } from '../database/services/database.user.config.service';
import { DBUserSessionService } from '../database/services/database.user.session.service';
import { DBEmojiCacheService } from '../database/services/database.emoji.cache.service';
import { DBEmojiFetchService } from '../database/services/database.emoji.fetch.service';
import { DBWorkerStatsService } from '../database/services/database.worker.stats.service';
import { OAuthService } from '../auth/services/oauth.service';
import { UserDeleteService } from '../user/services/user.delete.service';
import { JWEService } from '../auth/services/jwe.service';
import { OAuthSessionService } from '../auth/services/sessions/oauth.session.service';
import { JWESessionService } from '../auth/services/sessions/jwe.session.service';
import { OpenSessionService } from '../auth/services/sessions/open.session.service';
import { UserDeleteSessionService } from '../auth/services/sessions/user.delete.session.service';
import { CookieService } from '../auth/services/utils/cookie.service';
import { BrowserFingerprintService } from '../auth/services/utils/fingerprint.service';
import { UserService } from '../user/services/user.service';
import { UserCacheService } from '../user/services/user.cache.service';
import { WsService } from '../websocket/services/ws.service';
import { WsDashboardService } from '../websocket/services/ws.dashboard.service';
import { WsStatsService } from '../websocket/services/ws.stats.service';
import { ClusterSchedulerService } from './services/clusterScheduler.service';
import { InstanceCacheService } from './services/instanceCache.service';
import { FailedDomainsService } from './services/failedDomain.service';

import { InjectorQueue } from '../mirror/class/InjectorQueue';

import { WsGateway } from '../websocket/ws.gateway';

import { RedisConfig } from './configs/redis.config';
import { typeormConfig } from './configs/typeorm.config';

export const FullAppModule = {
	imports: [
		ConfigModule.forRoot({ isGlobal: true }),
		ScheduleModule.forRoot(),
		JwtModule,
		HttpModule,
		RedisConfig(),
		...typeormConfig(),
	],
	controllers: [
		AppController,
		OAuthController,
		AuthController,
		AuthSessionsController,
		UserController,
		DashboardController,
		DashboardRedisController,
		MirrorAccountsController,
		MirrorAccountsOthersController,
		MirrorAccountsTagsController,
		MirrorAccountsDomainBlocksController,
		MirrorAccountsFiltersController,
		MirrorAccountsFollowRequestController,
		MirrorTimelinesController,
		MirrorTimelinesConversationsController,
		MirrorTimelinesListsController,
		MirrorTimelinesMarkersController,
		MirrorNotificationsController,
		MirrorInstanceController,
		MirrorInstanceTrendsController,
		MirrorInstanceDirectoryController,
		MirrorInstanceEmojisController,
		MirrorInstanceAnnouncementsController,
		MirrorStatusesController,
		MirrorStatusesScheduledController,
		MirrorStatusesMediaController,
		MirrorStatusesPollsController,
		MirrorSearchController,
		WorkerController,
		WorkerLifecycleController,
	],
	providers: [
		AxiosProxy,
		AxiosCache,
		CacheManager,
		InstanceCacheService,
		AppService,
		OptionalFeaturesService,
		TimesService,
		FailedDomainsService,
		DBApplicationService,
		DBAccountService,
		DBUserService,
		DBUserSessionService,
		DBUserConfigService,
		DBEmojiCacheService,
		DBEmojiFetchService,
		DBWorkerStatsService,
		OAuthService,
		OAuthSessionService,
		UserService,
		CookieService,
		BrowserFingerprintService,
		JWESessionService,
		JWEService,
		OpenSessionService,
		UserDeleteSessionService,
		UserDeleteService,
		UserCacheService,
		InjectorInterceptor,
		InjectorQueue,
		WsGateway,
		WsService,
		WsDashboardService,
		WsStatsService,
		ClusterSchedulerService,
	],
};
@Module(FullAppModule)
export class AppModule implements NestModule {
	constructor(private readonly AppService: AppService) {
		this.AppService.setMainThread();
	}

	configure(consumer: MiddlewareConsumer) {
		consumer
			.apply(ResponseTimeMiddleware)
			.exclude('ping', 'stats', 'dashboard', 'favicon-32x32.png')
			.forRoutes('*all');
	}
}
