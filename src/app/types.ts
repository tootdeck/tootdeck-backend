import { Interactions } from '../dashboard/properties/redis/interactions.property';

/**
 * Cache
 */
// #region

export enum RedisState {
	Ok = 'OK',
	Retry = 'RETRY',
	Dead = 'DEAD',
}

export interface RedisExcludedDomain {
	state: RedisState.Retry | RedisState.Dead;
	value: string;
}

export interface RedisInstanceCache {
	state: RedisState.Ok | RedisState.Dead;
	value: string;
}

export interface RedisInteractionTemplate {
	state: RedisState;
	value: Interactions | string;
}

export interface RedisInteractionRetry extends RedisInteractionTemplate {
	state: RedisState.Retry;
	value: string;
}

export interface RedisInteractionDead extends RedisInteractionTemplate {
	state: RedisState.Dead;
	value: string;
}

export interface RedisInteractionOK extends RedisInteractionTemplate {
	state: RedisState.Ok;
	value: Interactions;
}

export type RedisInteraction = RedisInteractionRetry | RedisInteractionDead | RedisInteractionOK;

// #endregion

/**
 * Expiry
 */
// #region

export enum RedisExpiryToken {
	/**
	 * `EX` seconds
	 */
	EX = 'EX',
	/**
	 * `PX` milliseconds
	 */
	PX = 'PX',
	/**
	 * `EXAT` unix time seconds
	 */
	EXAT = 'EXAT',
	/**
	 * `PXAT` unix time milliseconds
	 */
	PXAT = 'PXAT',
	/**
	 * `NX` SET if Not eXists
	 */
	NX = 'NX',
}

export interface RedisExpiry {
	token: RedisExpiryToken;
	time?: number;
}

// #endregion
