import { readFileSync } from 'fs';

export function secretConfig() {
	const filename = process.env['NODE_ENV'] === 'test' ? 'test.pem' : 'secret.pem';
	try {
		return readFileSync('crypto/' + filename, { encoding: 'utf8' });
	} catch (e) {
		console.error(e);
		process.exit(1);
	}
}
