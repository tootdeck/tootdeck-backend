import { RedisModule, RedisModuleOptions } from '@liaoliaots/nestjs-redis';
import { ConfigModule, ConfigService } from '@nestjs/config';

import { RedisDB, RedisNamespace } from '../constants/redis';

export function RedisConfig(db?: RedisDB[]) {
	let config: Array<{
		namespace: RedisNamespace;
		db: RedisDB;
	}> = [];

	if (db) {
		for (const index of db) {
			switch (index) {
				case RedisDB.AccessToken:
					config.push({
						namespace: RedisNamespace.AccessToken,
						db: RedisDB.AccessToken,
					});
					break;
				case RedisDB.RefreshToken:
					config.push(
						{
							namespace: RedisNamespace.RefreshToken,
							db: RedisDB.RefreshToken,
						},
						{
							namespace: RedisNamespace.OpenSessionSubscribe,
							db: RedisDB.RefreshToken,
						},
					);
					break;
				case RedisDB.OAuthToken:
					config.push({
						namespace: RedisNamespace.OAuthToken,
						db: RedisDB.OAuthToken,
					});
					break;
				case RedisDB.UserDelete:
					config.push({
						namespace: RedisNamespace.UserDelete,
						db: RedisDB.UserDelete,
					});
					break;
				case RedisDB.UserCache:
					config.push({
						namespace: RedisNamespace.UserCache,
						db: RedisDB.UserCache,
					});
					break;
				case RedisDB.StatusesInteractions:
					config.push({
						namespace: RedisNamespace.StatusesInteractions,
						db: RedisDB.StatusesInteractions,
					});
					break;
				case RedisDB.ExcludedDomains:
					config.push({
						namespace: RedisNamespace.ExcludedDomains,
						db: RedisDB.ExcludedDomains,
					});
					break;
				case RedisDB.InstanceCache:
					config.push({
						namespace: RedisNamespace.InstanceCache,
						db: RedisDB.InstanceCache,
					});
					break;
				case RedisDB.TwitterEmbed:
					config.push({
						namespace: RedisNamespace.TwitterEmbed,
						db: RedisDB.TwitterEmbed,
					});
					break;
				case RedisDB.FailedDomain:
					config.push({
						namespace: RedisNamespace.FailedDomain,
						db: RedisDB.FailedDomain,
					});
					break;
			}
		}
	} else {
		config = [
			{
				namespace: RedisNamespace.AccessToken,
				db: RedisDB.AccessToken,
			},
			{
				namespace: RedisNamespace.RefreshToken,
				db: RedisDB.RefreshToken,
			},
			{
				namespace: RedisNamespace.OAuthToken,
				db: RedisDB.OAuthToken,
			},
			{
				namespace: RedisNamespace.OpenSessionSubscribe,
				db: RedisDB.RefreshToken,
			},
			{
				namespace: RedisNamespace.UserDelete,
				db: RedisDB.UserDelete,
			},
			{
				namespace: RedisNamespace.UserCache,
				db: RedisDB.UserCache,
			},
			{
				namespace: RedisNamespace.StatusesInteractions,
				db: RedisDB.StatusesInteractions,
			},
			{
				namespace: RedisNamespace.ExcludedDomains,
				db: RedisDB.ExcludedDomains,
			},
			{
				namespace: RedisNamespace.InstanceCache,
				db: RedisDB.InstanceCache,
			},
			{
				namespace: RedisNamespace.TwitterEmbed,
				db: RedisDB.TwitterEmbed,
			},
			{
				namespace: RedisNamespace.FailedDomain,
				db: RedisDB.FailedDomain,
			},
		];
	}

	return RedisModule.forRootAsync(
		{
			imports: [ConfigModule],
			inject: [ConfigService],
			// @ts-ignore
			useFactory: (configService: ConfigService): RedisModuleOptions => ({
				commonOptions: {
					host: configService.getOrThrow<string>('REDIS_HOST')!,
					port: configService.getOrThrow<number>('REDIS_PORT')!,
				},
				config,
				readyLog: true,
				errorLog: true,
			}),
		},
		true,
	);
}
