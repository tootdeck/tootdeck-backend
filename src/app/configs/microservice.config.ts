import { ConfigService } from '@nestjs/config';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';

export function MAIN_PROCESS(configService: ConfigService): MicroserviceOptions {
	return {
		transport: Transport.TCP,
		options: {
			host: '0.0.0.0',
			port: configService.getOrThrow<number>('MAIN_PROCESS_PORT'),
		},
	};
}
