import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { EntityClassOrSchema } from '@nestjs/typeorm/dist/interfaces/entity-class-or-schema.type';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';

import { AccountStore } from '../../database/entities/account.entity';
import { ApplicationStore } from '../../database/entities/application.entity';
import { UserStore } from '../../database/entities/user.entity';
import { ConfigStore } from '../../database/entities/config.entity';
import { UserSessionStore } from '../../database/entities/user.session.entity';
import { EmojiCache } from '../../database/entities/emoji.cache.entity';
import { EmojiFetch } from '../../database/entities/emoji.fetch.entity';
import { WorkerStats } from '../../database/entities/worker.stats.entity';

export function typeormConfig(entities?: EntityClassOrSchema[]) {
	const features = [
		ApplicationStore,
		AccountStore,
		UserStore,
		UserSessionStore,
		ConfigStore,
		EmojiCache,
		EmojiFetch,
		WorkerStats,
	];

	return [
		TypeOrmModule.forRootAsync({
			imports: [ConfigModule],
			inject: [ConfigService],
			useFactory: (configService: ConfigService): TypeOrmModuleOptions => ({
				type: 'postgres',
				host: configService.getOrThrow<string>('PSQL_HOST')!,
				port: configService.getOrThrow<number>('PSQL_PORT')!,
				username: configService.getOrThrow<string>('PSQL_USERNAME')!,
				password: configService.getOrThrow<string>('PSQL_PASSWORD')!,
				database: configService.getOrThrow<string>('PSQL_DATABASE')!,
				entities: ['dist/**/*.entity{.ts,.js}'],
				namingStrategy: new SnakeNamingStrategy(),
				autoLoadEntities: true,
				synchronize: false,
				logging: false,
			}),
		}),
		TypeOrmModule.forFeature(entities ?? features),
	];
}
