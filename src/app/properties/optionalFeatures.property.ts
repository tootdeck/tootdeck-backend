import { ApiProperty } from '@nestjs/swagger';

export class ApiOptionalFeatures {
	@ApiProperty()
	CACHE_STATUS_INTERACTIONS: boolean;

	@ApiProperty()
	CACHE_STATUS_REACTIONS: boolean;

	@ApiProperty()
	CACHE_TWITTER_EMBED: boolean;

	@ApiProperty()
	WORKER: boolean;

	@ApiProperty()
	CHANGELOG: boolean;
}
