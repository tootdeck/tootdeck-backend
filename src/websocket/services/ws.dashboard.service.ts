import { Injectable } from '@nestjs/common';

import { WsServiceInterface } from './ws.service.interface';

import { AppService } from '../../app/services/app.service';

import { Logger } from '../../utils/logger';

import { UserCache } from '../../user/types/cache.types';
import { WebSocketUser, WebsocketDashboard } from '../types';

@Injectable()
export class WsDashboardService extends WsServiceInterface {
	private readonly logger = new Logger(WsDashboardService.name);
	private readonly clients = new Map<string, Array<WebSocketUser>>();

	constructor(private readonly AppService: AppService) {
		super();
	}

	/**
	 * Client store
	 */
	// #region

	protected saveClient(user_uuid: string, frontend: WebSocketUser) {
		const map = this.clients.get(user_uuid);
		if (!map) {
			this.clients.set(user_uuid, [frontend]);
		} else {
			map.push(frontend);
		}

		this.logger.verbose('saveClient', this.clients.size + ' currently connected users.');
	}

	protected removeClient(user_uuid: string, client: WebSocketUser) {
		const map = this.clients.get(user_uuid);
		const index = map?.findIndex((frontend) => Object.is(frontend, client));
		if (!map || index === undefined || index === -1) {
			this.logger.warn('removeClient', user_uuid + ' no found, this should not happen.');
			return;
		}

		const removed = map.splice(index, 1)[0]!;
		removed.removeAllListeners();

		if (!map.length) {
			this.clients.delete(user_uuid);
		}

		this.logger.verbose('removeClient', this.clients.size + ' currently connected users.');
	}

	// #endregion

	/**
	 * Send
	 */
	// #region

	private canSend(handle: string, type: WebsocketDashboard.ResponseType) {
		if (type === WebsocketDashboard.ResponseType.User) {
			return this.AppService.isAdmin(handle);
		}

		return true;
	}

	send(data: WebsocketDashboard.AvailableResponse): void {
		Array.from(this.clients).forEach(([_, clients]) =>
			clients.forEach((frontend) => {
				if (this.canSend(frontend.handle, data.type)) {
					frontend.send(JSON.stringify(data));
				}
			}),
		);
	}

	// #endregion

	/**
	 * handleConnect
	 */
	// #region

	async handleConnect(user: UserCache, frontend: WebSocketUser) {
		// const accounts = [user.main, ...user.secondary];

		this.saveClient(user.uuid, frontend);
	}

	// #endregion

	/**
	 * handleDisconnect
	 */
	// #region

	handleDisconnect(frontend: WebSocketUser) {
		if (!frontend.user_uuid) {
			this.logger.debug('handleDisconnect', 'Disconnected unauthenticated client');
			return;
		}

		this.removeClient(frontend.user_uuid, frontend);
	}

	// #endregion

	getSocketCount() {
		return this.clients.size;
	}
}
