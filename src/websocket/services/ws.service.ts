import { Inject, Injectable, forwardRef } from '@nestjs/common';
import { randomUUID } from 'crypto';
import { WebSocket } from 'ws';

import { OptionalFeaturesService } from '../../app/services/utils/optionalFeatures.service';
import { DBApplicationService } from '../../database/services/database.application.service';
import { WsDashboardService } from './ws.dashboard.service';
import { WsStatsService } from './ws.stats.service';

import { InjectorInterceptor } from '../../mirror/interceptors/injector.interceptor';

import { WsServiceInterface } from './ws.service.interface';

import MegalodonGenerator, { WebsocketPath } from '../../lib/megalodon/megalodon';
import { WebSocketInterface, WebsocketCallbacks } from '../../lib/megalodon/web_socket';

import { Logger } from '../../utils/logger';
import { parseHandle } from '../../utils/parseHandle';

import { UserCache } from '../../user/types/cache.types';
import { InstanceType } from '../../database/types';
import {
	AvaliableSubscription,
	CommandType,
	ConnectedClient,
	InstanceSubscription,
	OptionalParams,
	RemoteInstanceClient,
	ResponseType,
	SubscribeEvent,
	WebSocketUser,
	WebsocketAPI,
	WebsocketAvailableResponse,
	WebsocketDashboard,
	WebsocketMirror,
} from '../types';

@Injectable()
export class WsService extends WsServiceInterface {
	private readonly logger = new Logger(WsService.name);
	private readonly clients = new Map<string, ConnectedClient[]>();
	private readonly misskeyTranslate = {
		subscribe: 'connect',
		unsubscribe: 'disconnect',
		user: 'homeTimeline',
		'user:notification': 'main',
		'public:local': 'localTimeline',
		public: 'globalTimeline',
		list: 'userList',
		hashtag: '', // Unsupported
		direct: '', // Unsupported
	};

	constructor(
		private readonly OptionalFeaturesService: OptionalFeaturesService,
		@Inject(forwardRef(() => DBApplicationService))
		private readonly DBApplicationService: DBApplicationService,
		@Inject(forwardRef(() => InjectorInterceptor<any>))
		private readonly InjectCache: InjectorInterceptor<any>,
		private readonly WsDashboardService: WsDashboardService,
		private readonly WsStatsService: WsStatsService,
	) {
		super();
		if (this.OptionalFeaturesService.CACHE_STATUS_INTERACTIONS) {
			this._interval_count = this.setThrottleInterval();
		}
	}

	/**
	 * Throttle
	 * When throttled, response injections are disabled
	 */
	// #region

	private readonly _count = new Map<string, number>();
	private readonly _throttle_reset = new Map<string, NodeJS.Timeout | null>();
	private readonly _interval_count: NodeJS.Timer;

	private setThrottleInterval() {
		return setInterval(() => {
			const values = this._count.entries();
			let iter: [string, number];
			while ((iter = values.next().value)) {
				const [key, value] = iter;

				let timeout = this._throttle_reset.get(key) ?? null;
				if (value > 20) {
					if (timeout) {
						clearTimeout(timeout);
					}

					if (!timeout) {
						this.logger.debug(key, 'throttled');
					}

					timeout = setTimeout(() => {
						this._throttle_reset.set(key, null);
						this.logger.debug(key, 'throttle reset');
					}, 60000);
				}

				this._count.set(key, 0);
				this._throttle_reset.set(key, timeout);
			}
		}, 10000);
	}

	/**
	 * @returns `true` when throttled
	 */
	private throttleSentStatuses(handle: string, data: WebsocketMirror.Data<any>) {
		const subscription = [handle, ...data.stream].join(':');
		const count = this._count.get(subscription);
		if (count !== undefined) {
			this._count.set(subscription, count + 1);
			return this._throttle_reset.get(subscription) !== null;
		}

		this._count.set(subscription, 1);
		return false;
	}

	// #endregion

	/**
	 * StreamingURL
	 */
	// #region

	private async fetchStreamingURL(
		domain: string,
		force: boolean = false,
	): Promise<WebsocketPath | null> {
		const application = await this.DBApplicationService.get(domain);
		if (!application) {
			return null;
		}

		if (!force && application.streaming_url) {
			return application.streaming_url as WebsocketPath;
		}

		const instance = await MegalodonGenerator(application.type, 'https://' + application.domain)
			.getInstance()
			.catch((e) => {
				this.logger.error(
					'FetchStreamingURL',
					`Unable to get instance info for ${domain}.`,
				);
				console.error(e);
				return null;
			});
		const streaming_url = instance?.data.urls?.streaming_api;
		if (!instance || !streaming_url) {
			return null;
		}

		const update = await this.DBApplicationService.updateStreamingURL(
			application,
			streaming_url,
		);
		if (!update) {
			return null;
		}

		return streaming_url as WebsocketPath;
	}

	private async getStreamingURL(
		frontend: WebSocket,
		domain: string,
		force: boolean = false,
	): Promise<WebsocketPath | null> {
		const streaming_url = await this.fetchStreamingURL(domain, force);
		if (!streaming_url) {
			this.logger.error('handleConnect', `Failed to get streaming url for ${domain}.`);
			const response: WebsocketAPI.Response = {
				type: ResponseType.API,
				data: WebsocketAPI.ResponseData.InternalServerError, //TODO
			};
			frontend.send(JSON.stringify(response));
			frontend.close();
			return null;
		}

		return streaming_url;
	}

	// #endregion

	/**
	 * Client store
	 */
	// #region

	protected saveClient(user_uuid: string, client: ConnectedClient) {
		const map = this.clients.get(user_uuid);
		if (!map) {
			this.clients.set(user_uuid, [client]);
		} else {
			map.push(client);
		}

		this.logger.verbose('saveClient', this.clients.size + ' currently connected users.');
	}

	protected removeClient(user_uuid: string, client: WebSocketUser) {
		const map = this.clients.get(user_uuid);
		const index = map?.findIndex((x) => Object.is(x.frontend, client));
		if (!map || index === undefined || index === -1) {
			this.logger.warn('removeClient', user_uuid + ' no found, this should not happen.');
			return;
		}

		const removed = map.splice(index, 1)[0]!;
		removed.frontend.removeAllListeners();
		removed.remote.forEach((client) => {
			client.socket.stop();
			client.socket.removeAllListeners();
		});

		if (!map.length) {
			this.clients.delete(user_uuid);
		}

		this.logger.verbose('removeClient', this.clients.size + ' currently connected users.');
	}

	// #endregion

	/**
	 * Subscriptions
	 */
	// #region

	private findSubscription(
		x: InstanceSubscription,
		subscription_name: AvaliableSubscription,
		params?: OptionalParams,
	) {
		if (params?.list_id) {
			return x.name === subscription_name && x.list_id === params?.list_id;
		}

		if (params?.hashtag) {
			return x.name === subscription_name && x.hashtag === params?.hashtag;
		}

		return x.name === subscription_name;
	}

	private formatSubscription(
		command: CommandType,
		type: InstanceType,
		subscription: InstanceSubscription,
	) {
		switch (type) {
			case InstanceType.Mastodon:
			case InstanceType.Pleroma:
				return JSON.stringify({
					type: command,
					stream: subscription.name,
					list: subscription.list_id, // Optional
					tag: subscription.hashtag, // Optional
				});
			case InstanceType.Misskey:
				return JSON.stringify({
					type: this.misskeyTranslate[command],
					body: {
						channel: this.misskeyTranslate[subscription.name],
						id: subscription.uuid,
						params: {
							listId: subscription.list_id, // Optional
						},
					},
				});
			case InstanceType.Friendica:
				return '';
		}
	}

	private subscribe(
		remote: RemoteInstanceClient,
		subscription_name: AvaliableSubscription,
		params?: OptionalParams,
	) {
		// Unsupported
		if (
			remote.type === InstanceType.Misskey &&
			(subscription_name === AvaliableSubscription.Direct || params?.hashtag)
		) {
			return;
		}

		const subscription: InstanceSubscription = {
			uuid: randomUUID(),
			name: subscription_name,
			list_id: params?.list_id,
			hashtag: params?.hashtag,
		};

		if (remote.subscriptions.find((x) => this.findSubscription(x, subscription_name, params))) {
			return;
		}

		// If instance is Mastodon API complient, stream: "user" already includes notifications, skipping it
		if (
			remote.type !== InstanceType.Misskey &&
			subscription_name === AvaliableSubscription.Notification &&
			remote.subscriptions.find((x) => x.name === AvaliableSubscription.User)
		) {
			return;
		}

		remote.subscriptions.push(subscription);

		const data = this.formatSubscription(CommandType.Subscribe, remote.type, subscription);

		remote.socket.send(data);
	}

	private unsubscribe(
		remote: RemoteInstanceClient,
		subscription_name: AvaliableSubscription,
		params?: OptionalParams,
	) {
		const subscription = remote.subscriptions.find((x) =>
			this.findSubscription(x, subscription_name, params),
		);
		if (!subscription) {
			return;
		}

		remote.subscriptions = remote.subscriptions.filter(
			(x) => !this.findSubscription(x, subscription_name, params),
		);

		const data = this.formatSubscription(CommandType.Unsubscribe, remote.type, subscription);
		remote.socket.send(data);
	}

	private handleSubscription(entry: ConnectedClient, event: SubscribeEvent) {
		if (!event.handle || !event.type || !event.stream) {
			return;
		}
		if (event.stream === 'list' && !event.list) {
			return;
		}

		const handle = parseHandle(event.handle);
		const instance = entry.remote.find(
			(x) => x.username === handle.username && x.domain === handle.domain,
		);
		if (!instance) {
			return;
		}

		switch (event.type) {
			case CommandType.Subscribe:
				this.subscribe(instance, event.stream, { list_id: event.list, hashtag: event.tag });
				break;
			case CommandType.Unsubscribe:
				this.unsubscribe(instance, event.stream, {
					list_id: event.list,
					hashtag: event.tag,
				});
				break;
		}
	}

	// #endregion

	/**
	 * Send
	 */
	// #region

	sendByApp(domain: string, data: WebsocketAPI.Response) {
		this.clients.forEach((user) => {
			user.forEach((client) => {
				const found = client.remote.find((x) => x.domain === domain);
				if (found) {
					this.send(client.frontend, data);
				}
			});
		});
	}

	sendByUserUUID(user_uuid: string, data: WebsocketAPI.Response): void {
		const clients = this.clients.get(user_uuid);
		if (!clients) {
			return;
		}

		clients.forEach((client) => {
			client.frontend.send(JSON.stringify(data));
		});
	}

	send(frontend: WebSocketUser, data: WebsocketAvailableResponse): void {
		if (data.type !== ResponseType.API && typeof data.data !== 'string') {
			const throttled = this.throttleSentStatuses(data.handle, data.data);
			const mirror_response = data as WebsocketMirror.Response;
			data = this.InjectCache.websocketInject(
				frontend.user_uuid,
				mirror_response.handle,
				throttled,
				data,
			);
		}

		frontend.send(JSON.stringify(data));
		return;
	}

	// #endregion

	/**
	 * handleConnect
	 */
	// #region

	private handleMisskeyData(entry: RemoteInstanceClient, data: any) {
		if (entry.type !== InstanceType.Misskey) {
			return data;
		}

		const misskey_data = data as {
			id: string;
			content: any;
		};

		const subscription = entry.subscriptions.find((x) => x.uuid === misskey_data.id);
		if (!subscription) {
			this.logger.error(
				'handleMisskeyData',
				`Couldn't translate UUID (${misskey_data.id}) to Mastodon API subscription.`,
			);
			return;
		}

		if (subscription.name === AvaliableSubscription.List) {
			return {
				stream: [subscription.name],
				content: misskey_data.content,
				list: subscription.list_id,
			};
		}

		return {
			stream: [subscription.name],
			content: misskey_data.content,
		};
	}

	private getCallbacks(
		frontend: WebSocketUser,
		entry: RemoteInstanceClient,
		handle: string,
		error_handle: (error: any) => Promise<void>,
	): WebsocketCallbacks {
		return {
			onUpdate: (data) => {
				this.send(frontend, {
					type: ResponseType.Update,
					handle,
					data: this.handleMisskeyData(entry, data),
				});
			},
			onNotification: (data) => {
				this.send(frontend, {
					type: ResponseType.Notification,
					handle,
					data: this.handleMisskeyData(entry, data),
				});
			},
			onDelete: (data) => {
				this.send(frontend, {
					type: ResponseType.Delete,
					handle,
					data,
				});
			},
			onConversation: (data) => {
				this.send(frontend, {
					type: ResponseType.Conversation,
					handle,
					data: this.handleMisskeyData(entry, data),
				});
			},
			onStatusUpdate: (data) => {
				this.send(frontend, {
					type: ResponseType.Status_update,
					handle,
					data: this.handleMisskeyData(entry, data),
				});
			},
			onError: error_handle,
		};
	}

	private bindListeners(
		frontend: WebSocketUser,
		remote: WebSocketInterface,
		entry: RemoteInstanceClient,
		handle: string,
		error_handle: (error: any) => Promise<void>,
	) {
		remote.setCallbacks(this.getCallbacks(frontend, entry, handle, error_handle));

		// Error
		remote.on('error', error_handle);

		// Reconnected
		let initialized: boolean = false;
		remote.on('connect', () => {
			if (initialized) {
				this.send(frontend, {
					type: ResponseType.API,
					data: WebsocketAPI.ResponseData.Reconnected,
					handle,
				});
			}

			entry.subscriptions.forEach((subscription) => {
				remote.send(
					this.formatSubscription(CommandType.Subscribe, entry.type, subscription),
				);
			});

			initialized = true;
		});
	}

	async handleConnect(user: UserCache, frontend: WebSocketUser) {
		const accounts = [user.main, ...user.secondary];
		const remote: RemoteInstanceClient[] = [];

		for (const account of accounts) {
			const handle = account.username + '@' + account.application.domain;

			const streaming_url = await this.getStreamingURL(frontend, account.application.domain);
			if (!streaming_url) {
				continue;
			}

			let remote_socket = MegalodonGenerator(account.application.type, streaming_url, {
				accessToken: account.token,
			}).socket(streaming_url, new Logger(`Websocket-${account.application.type} ${handle}`));

			const remote_entry: RemoteInstanceClient = {
				subscriptions: [],
				username: account.username,
				domain: account.application.domain,
				type: account.application.type,
				socket: remote_socket,
			};

			let retries: number = 0;

			const error_handler = async (error: any) => {
				retries++;

				const message = error.message as string;
				if (message.includes('301')) {
					remote_socket.stop();
					remote_socket.removeAllListeners();

					const streaming_url = await this.getStreamingURL(
						frontend,
						account.application.domain,
						true,
					);
					if (!streaming_url) {
						this.send(frontend, {
							type: ResponseType.API,
							data: WebsocketAPI.ResponseData.InternalServerError, //TODO
						});
						return;
					}

					const updated_remote_entry = remote.find((x) => Object.is(x, remote_entry));
					if (!updated_remote_entry) {
						// Frontend closed
						return;
					}

					const new_remote_socket = MegalodonGenerator(
						account.application.type,
						streaming_url,
						{ accessToken: account.token },
					).socket(
						streaming_url,
						new Logger(`Websocket-${account.application.type} ${handle}`),
					);

					updated_remote_entry.socket = new_remote_socket;

					this.bindListeners(
						frontend,
						new_remote_socket,
						updated_remote_entry,
						handle,
						async (_) => {
							remote_socket.stop();
							remote_socket.removeAllListeners();

							this.logger.error(handle, 'Unable to connect.');

							this.send(frontend, {
								type: ResponseType.API,
								data: WebsocketAPI.ResponseData.InternalServerError, //TODO
							});
						},
					);

					retries = 0;

					this.logger.verbose(handle, 'Invalid streaming url, recovered.');
					return;
				}

				this.logger.error(`Handled error ${handle}`, error);

				if (retries >= 5) {
					this.logger.error(handle, 'Unable to connect.');
					remote_socket.stop();
					remote_socket.removeAllListeners();

					this.send(frontend, {
						type: ResponseType.API,
						data: WebsocketAPI.ResponseData.CouldNotConnect,
						handle,
					});
				}
			};

			this.bindListeners(frontend, remote_socket, remote_entry, handle, error_handler);

			remote.push(remote_entry);
		}

		const entry: ConnectedClient = {
			frontend: frontend as WebSocketUser,
			remote,
		};

		this.saveClient(user.uuid, entry);

		frontend.addEventListener('message', (data) => {
			try {
				const event = JSON.parse(data.data as string);
				this.handleSubscription(entry, event);
			} catch (e) {}
		});

		this.send(frontend, {
			type: ResponseType.API,
			data: WebsocketAPI.ResponseData.AwaitingSubscription,
		});

		this.WsDashboardService.send({
			type: WebsocketDashboard.ResponseType.Status,
			data: {
				sockets: this.WsStatsService.get(),
				account: {
					handle: frontend.handle,
					status: true,
				},
			},
		});
	}

	// #endregion

	/**
	 * handleDisconnect
	 */
	// #region

	handleDisconnect(frontend: WebSocketUser) {
		if (!frontend.user_uuid) {
			this.logger.debug('handleDisconnect', 'Disconnected unauthenticated client');
			return;
		}

		this.removeClient(frontend.user_uuid, frontend);

		this.WsDashboardService.send({
			type: WebsocketDashboard.ResponseType.Status,
			data: {
				sockets: this.WsStatsService.get(),
				account: {
					handle: frontend.handle,
					status: false,
				},
			},
		});
	}

	// #endregion

	getSocketCount() {
		const total = Array.from(this.clients)
			.map(([_, user]) =>
				user.map((connected) => connected.remote.length).reduce((a, b) => a + b, 0),
			)
			.reduce((a, b) => a + b, 0);

		return this.clients.size + total;
	}

	getOnlineUsers() {
		return Array.from(this.clients)
			.map(([uuid, clients]) =>
				clients.map((client) => ({
					main: client.frontend.handle,
					secondary: client.remote
						.filter(
							(remote) =>
								remote.username + '@' + remote.domain !== client.frontend.handle,
						)
						.map((remote) => remote.username + '@' + remote.domain),
				})),
			)
			.flat();
	}

	broadcastMessage(message: string) {
		Array.from(this.clients).forEach(([uuid]) => {
			this.sendByUserUUID(uuid, {
				type: ResponseType.API,
				data: WebsocketAPI.ResponseData.BroadcastMessage,
				message,
			});
		});
	}
}
