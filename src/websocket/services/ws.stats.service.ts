import { Inject, Injectable, forwardRef } from '@nestjs/common';

import { WsService } from './ws.service';
import { WsDashboardService } from './ws.dashboard.service';

@Injectable()
export class WsStatsService {
	constructor(
		@Inject(forwardRef(() => WsService))
		private readonly WsService: WsService,
		private readonly WsDashboardService: WsDashboardService,
	) {}

	get() {
		const connected = this.WsService.getOnlineUsers();
		const users_sockets_count = this.WsService.getSocketCount();
		const dashbaord_sockets_count = this.WsDashboardService.getSocketCount();

		return {
			total: users_sockets_count + dashbaord_sockets_count,
			instances: users_sockets_count - connected.length,
			frontend: connected.length,
			dashboard: dashbaord_sockets_count,
		};
	}
}
