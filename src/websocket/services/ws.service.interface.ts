import { UserCache } from '../../user/types/cache.types';
import { WebSocketUser } from '../types';

export abstract class WsServiceInterface {
	protected abstract saveClient(user_uuid: string, client: any): void;
	protected abstract removeClient(user_uuid: string, client: WebSocketUser): void;

	abstract send(...args: Array<any>): void;

	abstract handleConnect(user: UserCache, frontend: WebSocketUser): void;
	abstract handleDisconnect(frontend: WebSocketUser): void;

	abstract getSocketCount(): number;
}
