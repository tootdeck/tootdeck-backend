import { WebSocket } from 'ws';

import { WebSocketInterface } from '../lib/megalodon/web_socket';
import { Entity } from '../lib/megalodon/entities';

import {
	AccountStats,
	DashboardStats,
	SocketStats,
	Worker,
} from '../dashboard/properties/entities.property';
import { Interactions } from '../dashboard/properties/redis/interactions.property';

import { InstanceType } from '../database/types';

/**
 * Types
 */
export interface WebSocketUser extends WebSocket {
	user_uuid: string;
	handle: string;
	channel: 'default' | 'dashboard';
}

export interface OptionalParams {
	list_id?: string | undefined;
	hashtag?: string | undefined;
}

/**
 * Subscriptions
 */
export enum CommandType {
	Unsubscribe = 'unsubscribe',
	Subscribe = 'subscribe',
}

export enum AvaliableSubscription {
	User = 'user',
	Notification = 'user:notification',
	Public = 'public',
	Local = 'public:local',
	Hashtag = 'hashtag',
	List = 'list',
	Direct = 'direct',
}

export interface InstanceSubscription {
	uuid: string;
	name: AvaliableSubscription;
	list_id?: string | undefined;
	hashtag?: string | undefined;
}

export interface RemoteInstanceClient {
	subscriptions: Array<InstanceSubscription>;
	username: string;
	domain: string;
	type: InstanceType;
	socket: WebSocketInterface;
}

export interface ConnectedClient {
	frontend: WebSocketUser;
	remote: RemoteInstanceClient[];
}

export interface SubscribeEvent {
	handle: string;
	type: CommandType;
	stream: AvaliableSubscription;
	list?: string;
	tag?: string;
}

/**
 * Responses
 */
export enum ResponseType {
	Update = 'update',
	Notification = 'notification',
	Conversation = 'conversation',
	Delete = 'delete',
	Status_update = 'status_update',
	API = 'API',
}

export type WebsocketAvailableResponse =
	| WebsocketMirror.Status
	| WebsocketMirror.Edit
	| WebsocketMirror.Notification
	| WebsocketMirror.Conversation
	| WebsocketMirror.Delete
	| WebsocketAPI.Response;

export interface WebsocketResponse {
	type: ResponseType;
	data: any;
}

export namespace WebsocketMirror {
	export interface Data<T> {
		stream: string[];
		content: T;
	}

	export interface Response extends WebsocketResponse {
		handle: string;
		data: Data<any>;
	}

	export interface Status extends Response {
		type: ResponseType.Update;
		data: Data<Entity.Status>;
	}

	export interface Edit extends Response {
		type: ResponseType.Status_update;
		data: Data<Entity.Status>;
	}

	export interface Notification extends Response {
		type: ResponseType.Notification;
		data: Data<Entity.Notification>;
	}

	export interface Conversation extends Response {
		type: ResponseType.Conversation;
		data: Data<Entity.Conversation>;
	}

	export interface Delete extends WebsocketResponse {
		type: ResponseType.Delete;
		handle: string;
		data: string;
	}
}

export interface DashboardWebsocketData extends Omit<DashboardStats, 'workers'> {
	worker: Worker;
}

export namespace WebsocketAPI {
	export interface Response extends WebsocketResponse {
		type: ResponseType.API;
		data: ResponseData;
		handle?: string;
		// For config sync
		uuid?: string;
		// For Interactions
		interactions?: Interactions & { id: string };
		// For Dashboard
		dashboard?: DashboardWebsocketData;
		// For broadcast message
		message?: string;
	}

	export enum ResponseData {
		Refresh = 'Refresh',
		InternalServerError = 'InternalServerError',
		AppExpired = 'AppExpired',
		NewSession = 'NewSession',
		ConfigUpdate = 'ConfigUpdate',
		AwaitingSubscription = 'AwaitingSubscription',
		Reconnected = 'Reconnected',
		CouldNotConnect = 'CouldNotConnect',
		Interactions = 'Interactions',
		BroadcastMessage = 'BroadcastMessage',
	}
}

export namespace WebsocketDashboard {
	export enum ResponseType {
		Data = 0,
		User = 1,
		Status = 2,
	}

	//

	export type AvailableResponse = Data | User | Status;

	export interface Response {
		type: ResponseType;
		data: any;
	}

	export interface Data extends Response {
		type: ResponseType.Data;
		data: DashboardWebsocketData;
	}

	export interface User extends Response {
		type: ResponseType.User;
		data: {
			sockets: SocketStats;
			account: Omit<AccountStats, 'status'>;
		};
	}

	export interface Status extends Response {
		type: ResponseType.Status;
		data: {
			sockets: SocketStats;
			account: Omit<AccountStats, 'jobs_count'>;
		};
	}
}
