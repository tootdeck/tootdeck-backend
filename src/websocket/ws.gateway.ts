import { Request } from '@nestjs/common';
import {
	ConnectedSocket,
	OnGatewayConnection,
	OnGatewayDisconnect,
	OnGatewayInit,
	WebSocketGateway,
	WebSocketServer,
} from '@nestjs/websockets';
import * as cookieParser from 'cookie-parser';
import { FastifyRequest } from 'fastify';
import { Server, WebSocket } from 'ws';

import { WsService } from './services/ws.service';
import { WsDashboardService } from './services/ws.dashboard.service';
import { JWEService } from '../auth/services/jwe.service';
import { UserCacheService } from '../user/services/user.cache.service';

import { Logger } from '../utils/logger';

import { JWEType } from '../auth/types/jwe.types';
import { JWESession } from '../auth/types/sessions.types';
import { ResponseType, WebSocketUser, WebsocketAPI } from './types';

@WebSocketGateway({
	path: '/api/streaming',
	cors: true,
})
export class WsGateway implements OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit {
	private readonly logger = new Logger(WsGateway.name);

	@WebSocketServer()
	private readonly server: Server;

	constructor(
		private readonly JWEService: JWEService,
		private readonly UserCacheService: UserCacheService,
		private readonly WsService: WsService,
		private readonly WsDashboardService: WsDashboardService,
	) {}

	afterInit(server: Server) {
		this.logger.log('afterInit', 'Websocket is live');
	}

	/**
	 * handleConnect
	 */
	// #region

	private async validate(cookies: {
		[key: string]: string | undefined;
	}): Promise<JWESession | null> {
		if (!cookies['a']) {
			return null;
		}

		return this.JWEService.validate(JWEType.Access, cookies['a']).catch((e) => null);
	}

	async handleConnection(
		@ConnectedSocket() client: WebSocket,
		@Request() request: FastifyRequest,
	) {
		cookieParser()(request as any, undefined as any, () => {});

		const session = await this.validate(request.cookies);
		if (!session) {
			const response: WebsocketAPI.Response = {
				type: ResponseType.API,
				data: WebsocketAPI.ResponseData.Refresh,
			};
			client.send(JSON.stringify(response));
			client.close();
			return;
		}

		const user = await this.UserCacheService.get(session.user_uuid);
		if (!user) {
			this.logger.error('handleConnection', 'Failed to get user from cache');
			const response: WebsocketAPI.Response = {
				type: ResponseType.API,
				data: WebsocketAPI.ResponseData.InternalServerError,
			};
			client.send(JSON.stringify(response));
			client.close();
			return;
		}

		const frontend = client as WebSocketUser;
		frontend['user_uuid'] = user.uuid;
		frontend['handle'] = user.main.username + '@' + user.main.application.domain;

		const url = new URL('https://localhost' + request.url);
		if (url.searchParams.get('dashboard') !== null) {
			frontend['channel'] = 'dashboard';
			this.WsDashboardService.handleConnect(user, frontend);
		} else {
			frontend['channel'] = 'default';
			await this.WsService.handleConnect(user, frontend);
		}

		const total = this.WsDashboardService.getSocketCount() + this.WsService.getSocketCount();
		this.logger.debug('handleConnect', 'Total connected sockets: ' + total);
		this.logger.debug(
			'handleConnection',
			this.server.clients.size + ' currently connected clients.',
		);
	}

	//#endregion

	/**
	 * handleDisconnect
	 */
	// #region

	handleDisconnect(frontend: WebSocketUser) {
		switch (frontend.channel) {
			case 'default':
				this.WsService.handleDisconnect(frontend);
				break;
			case 'dashboard':
				this.WsDashboardService.handleDisconnect(frontend);
				break;
		}

		const total = this.WsDashboardService.getSocketCount() + this.WsService.getSocketCount();
		this.logger.debug('handleDisconnect', 'Total connected sockets: ' + total);
		this.logger.debug(
			'handleDisconnect',
			this.server.clients.size + ' currently connected clients.',
		);
	}

	// #endregion
}
