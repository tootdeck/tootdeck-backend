import {
	BadRequestException,
	Body,
	Controller,
	Delete,
	Get,
	HttpCode,
	NotFoundException,
	Post,
	Put,
	Query,
	Request,
	Response,
	UseGuards,
} from '@nestjs/common';
import { ApiBody, ApiResponse, ApiTags } from '@nestjs/swagger';
import { FastifyReply } from 'fastify/types/reply';

import { UserDeleteService } from './services/user.delete.service';
import { UserService } from './services/user.service';
import { CookieService } from '../auth/services/utils/cookie.service';
import { JWEService } from '../auth/services/jwe.service';

import { AccessGuard } from '../auth/guards/access.guard';
import { RefreshGuard } from '../auth/guards/refresh.guard';
import { AccountDeleteGuard } from '../auth/guards/delete.guard';

import { parseHandle } from '../utils/parseHandle';

import {
	RequestAccessGuard,
	RequestAccountDeleteGuard,
	RequestRefreshGuard,
} from '../auth/types/guards.types';
import { JWEType } from '../auth/types/jwe.types';
import { CacheBody } from './types/body.property';

@ApiTags('User')
@Controller('user')
export class UserController {
	constructor(
		private readonly JWEService: JWEService,
		private readonly CookieService: CookieService,
		private readonly UserService: UserService,
		private readonly UserDeleteService: UserDeleteService,
	) {}

	@UseGuards(AccessGuard)
	@ApiResponse({ status: 200 })
	@ApiResponse({ status: 401 })
	@ApiResponse({ status: 500 })
	@HttpCode(200)
	@Put('erase')
	async request(
		@Request() request: RequestAccessGuard,
		@Response({ passthrough: true }) response: FastifyReply,
	) {
		const user_uuid = request.access_session.user_uuid;

		const session = await this.UserDeleteService.createRequest(user_uuid);
		this.CookieService.create(response, JWEType.AccountDelete, session.token);

		return { code: session.code };
	}

	@UseGuards(AccessGuard, RefreshGuard, AccountDeleteGuard)
	@ApiResponse({ status: 200 })
	@ApiResponse({ status: 401 })
	@ApiResponse({ status: 500 })
	@HttpCode(200)
	@Delete('erase')
	async erase(
		@Request() request: RequestAccountDeleteGuard & RequestRefreshGuard,
		@Response({ passthrough: true }) response: FastifyReply,
		@Query('code') code: string,
	) {
		const user_uuid = request.access_session.user_uuid;
		const needed_code = request.delete_session.code;

		if (code !== needed_code) {
			throw new BadRequestException();
		}

		const json = await this.UserDeleteService.delete(user_uuid);
		await this.JWEService.delete(request.refresh_session);
		this.CookieService.delete(response, JWEType.Access);
		this.CookieService.delete(response, JWEType.Refresh);
		this.CookieService.delete(response, JWEType.AccountDelete);

		return json;
	}

	@UseGuards(AccessGuard)
	@ApiResponse({ status: 200 })
	@ApiResponse({ status: 401 })
	@ApiResponse({ status: 500 })
	@HttpCode(200)
	@Delete('unlink')
	async unlink(@Request() request: RequestAccessGuard, @Query('handle') _handle: string) {
		const handle = parseHandle(_handle);

		const user_uuid = request.access_session.user_uuid;

		return this.UserService.unlinkAccount(user_uuid, handle);
	}

	@UseGuards(AccessGuard)
	@ApiResponse({ status: 200 })
	@ApiResponse({ status: 401 })
	@ApiResponse({ status: 500 })
	@Get('config')
	async getConfig(@Request() request: RequestAccessGuard) {
		const user_uuid = request.access_session.user_uuid;

		return this.UserService.getConfig(user_uuid);
	}

	@UseGuards(AccessGuard)
	@ApiBody({
		type: CacheBody,
		required: true,
	})
	@ApiResponse({ status: 201 })
	@ApiResponse({ status: 401 })
	@ApiResponse({ status: 500 })
	@Post('config')
	async addConfig(
		@Request() request: RequestAccessGuard,
		@Body('uuid') uuid: string,
		@Body('config') config: string,
	) {
		if (!config) {
			throw new BadRequestException();
		}

		const user_uuid = request.access_session.user_uuid;

		return this.UserService.addConfig(user_uuid, config, uuid);
	}

	@UseGuards(AccessGuard)
	@ApiResponse({ status: 200 })
	@ApiResponse({ status: 404 })
	@ApiResponse({ status: 500 })
	@Delete('config')
	async deleteConfig(@Request() request: RequestAccessGuard) {
		const user_uuid = request.access_session.user_uuid;

		const ret = await this.UserService.deleteConfig(user_uuid);
		if (!ret) {
			throw new NotFoundException();
		}
	}
}
