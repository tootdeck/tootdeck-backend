import { Test, TestingModule } from '@nestjs/testing';

import { FullAppModule } from '../../app/app.module';

import { DBApplicationService } from '../../database/services/database.application.service';
import { DBAccountService } from '../../database/services/database.account.service';
import { DBUserService } from '../../database/services/database.user.service';
import { UserDeleteService } from './user.delete.service';

import { UserStore } from '../../database/entities/user.entity';
import { AccountStore } from '../../database/entities/account.entity';

import { InstanceType } from '../../database/types';
import { UserDeleteSession } from '../../auth/types/sessions.types';

describe('UserDeleteService', () => {
	let Application: DBApplicationService;
	let Account: DBAccountService;
	let User: DBUserService;
	let UserDelete: UserDeleteService;

	const application_domain = 'example.com';
	const application_key = 't52jyR5LOeqAK4PhXqI7rktO7CZaD6OPaRohyFVrgzI=';
	const application_secret = 'qlUrKaKNhIuUZrt1pxvGrOnxNdrMDyqPp+qkvhn4Fzw=';

	const account_username = 'tootdeck';
	const account_domain = application_domain;
	const account_token = 'xOOnkfBjAtRqtAmqWFNZEhDsjetgaBIFccIg1EPLqdM=';

	let user: UserStore;
	let main: AccountStore;
	let secondary: AccountStore;

	let session: UserDeleteSession;

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule(FullAppModule).compile();

		Application = app.get<DBApplicationService>(DBApplicationService);
		Account = app.get<DBAccountService>(DBAccountService);
		User = app.get<DBUserService>(DBUserService);
		UserDelete = app.get<UserDeleteService>(UserDeleteService);

		/**
		 * Create a valid application
		 */
		await Application.create(
			application_domain,
			InstanceType.Mastodon,
			application_key,
			application_secret,
		);

		/**
		 * Create a valid main account
		 */
		main = await Account.create(account_username, account_domain, account_token).then(
			(entity) => entity!,
		);

		/**
		 * Create a valid secondary account
		 */
		secondary = await Account.create('secondary', account_domain, account_token).then(
			(entity) => entity!,
		);

		/**
		 * Create a valid user
		 */
		user = await User.create(main).then((entity) => entity!);
		user = await User.addAccount(user, secondary).then((entity) => entity!);
	});

	describe('createRequest', () => {
		test('create request, should return entry', async () => {
			await UserDelete.createRequest(user.uuid).then((result) => {
				// @ts-ignore
				session = result;

				expect(result).toMatchObject({
					user_uuid: user.uuid,
				} as UserDeleteSession);
				expect(result.code).toBeTruthy();
			});
		});
	});

	describe('validate', () => {
		test('valid token, should return entry', async () => {
			await UserDelete.validate(session.token).then((result) => {
				expect(result).toMatchObject(session);
			});
		});

		test('invalid token, should throw', async () => {
			expect(() => UserDelete.validate('')).rejects.toThrow();
		});
	});

	describe('delete', () => {
		test('delete valid user, should return entry', async () => {
			await UserDelete.delete(session.user_uuid).then((result) => {
				expect(result).toMatchObject({
					user: 'OK',
					token: 'OK',
				});
			});
		});

		test('delete invalid user, should throw', async () => {
			expect(() => UserDelete.delete('invalid')).rejects.toThrow();
		});
	});

	afterAll(async () => {
		await User.delete(main).catch(() => {});
		await Account.delete(account_username, account_domain).catch(() => {});
		await Account.delete('secondary', account_domain).catch(() => {});
		await Application.delete(application_domain).catch(() => {});
	});
});
