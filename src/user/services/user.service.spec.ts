import { Test, TestingModule } from '@nestjs/testing';
import { isUUID } from 'class-validator';
import { randomUUID } from 'crypto';

import { FullAppModule } from '../../app/app.module';

import { DBApplicationService } from '../../database/services/database.application.service';
import { DBAccountService } from '../../database/services/database.account.service';
import { DBUserService } from '../../database/services/database.user.service';
import { UserService } from './user.service';

import { UserStore } from '../../database/entities/user.entity';
import { AccountStore } from '../../database/entities/account.entity';

import { AuthGetResponse } from '../../auth/properties/auth.get.property';

import { InstanceType } from '../../database/types';

describe('UserService', () => {
	let DBApplication: DBApplicationService;
	let DBAccount: DBAccountService;
	let DBUser: DBUserService;
	let User: UserService;

	const application_domain = 'example.com';
	const application_key = 't52jyR5LOeqAK4PhXqI7rktO7CZaD6OPaRohyFVrgzI=';
	const application_secret = 'qlUrKaKNhIuUZrt1pxvGrOnxNdrMDyqPp+qkvhn4Fzw=';

	const account_username = 'tootdeck';
	const account_domain = application_domain;
	const account_token = 'xOOnkfBjAtRqtAmqWFNZEhDsjetgaBIFccIg1EPLqdM=';

	let user: UserStore;
	let main: AccountStore;
	let secondary: AccountStore;
	let alt: AccountStore;

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule(FullAppModule).compile();

		DBApplication = app.get<DBApplicationService>(DBApplicationService);
		DBAccount = app.get<DBAccountService>(DBAccountService);
		DBUser = app.get<DBUserService>(DBUserService);
		User = app.get<UserService>(UserService);

		/**
		 * Create a valid application
		 */
		await DBApplication.create(
			application_domain,
			InstanceType.Mastodon,
			application_key,
			application_secret,
		);

		/**
		 * Create a valid main account
		 */
		main = await DBAccount.create(account_username, account_domain, account_token).then(
			(entity) => entity!,
		);

		/**
		 * Create a valid secondary account
		 */
		secondary = await DBAccount.create('secondary', account_domain, account_token).then(
			(entity) => entity!,
		);

		/**
		 * Create a valid alt account
		 */
		alt = await DBAccount.create('alt', account_domain, account_token).then(
			(entity) => entity!,
		);

		/**
		 * Create a valid user
		 */
		user = await DBUser.create(main).then((entity) => entity!);
	});

	describe('login', () => {
		test('Login a valid user', async () => {
			await User.login({
				username: main.username,
				domain: main.application.domain,
				token: 'token',
			}).then((result) => {
				expect(isUUID(result)).toBe(true);
			});
		});

		test('Login not existing acccount', async () => {
			await User.login({
				username: alt.username,
				domain: alt.application.domain,
				token: 'token',
			}).then((result) => {
				expect(isUUID(result)).toBe(true);
			});
		});

		test('Login non existing user', async () => {
			await User.login({
				username: 'username',
				domain: application_domain,
				token: 'token',
			}).then((result) => {
				expect(isUUID(result)).toBe(true);
			});
		});
	});

	describe('addAccount', () => {
		test('Add a valid unexisting account', async () => {
			await User.addAccount(
				{
					username: 'new',
					domain: application_domain,
					token: 'token',
				},
				user.uuid,
			).then((result) => {
				expect(result).toBe(user.uuid);
			});
		});

		test('Add a valid existing account', async () => {
			await User.addAccount(
				{
					username: secondary.username,
					domain: secondary.application.domain,
					token: 'token',
				},
				user.uuid,
			).then((result) => {
				expect(result).toBe(user.uuid);
			});
		});

		test('Add the same account', async () => {
			expect(() =>
				User.addAccount(
					{
						username: secondary.username,
						domain: secondary.application.domain,
						token: 'token',
					},
					user.uuid,
				),
			).rejects.toThrow();
		});

		test('Add a main account', async () => {
			expect(() =>
				User.addAccount(
					{
						username: main.username,
						domain: main.application.domain,
						token: 'token',
					},
					user.uuid,
				),
			).rejects.toThrow();
		});
	});

	describe('refresh', () => {
		test('Refresh a valid account', async () => {
			await User.refresh(
				{
					username: main.username,
					domain: main.application.domain,
					token: 'token',
				},
				main.uuid,
			).then((result) => {
				expect(result).toBe(main.uuid);
			});
		});

		test('Refresh an invalid account', async () => {
			expect(() =>
				User.refresh(
					{
						username: '',
						domain: main.application.domain,
						token: 'token',
					},
					'token',
				),
			).rejects.toThrow();
		});
	});

	describe('get', () => {
		test('Get a valid user', async () => {
			await User.get(user.uuid).then((result) => {
				expect(result).toMatchObject({
					main: {
						username: main.username,
						domain: main.application.domain,
					},
				} as AuthGetResponse);
				expect(result.secondary.length).toBe(2);
				expect(result.secondary.find((s) => s.username === 'new')).toBeTruthy();
				expect(
					result.secondary.find((s) => s.username === secondary.username),
				).toBeTruthy();
			});
		});

		test('Get an invalid user', async () => {
			expect(() => User.get(randomUUID())).rejects.toThrow();
		});
	});

	describe('unlinkAccount', () => {
		test('Unlink a valid existing account', async () => {
			await User.unlinkAccount(user.uuid, {
				username: secondary.username,
				domain: secondary.application.domain,
			}).then((result) => {
				expect(result).toBe(true);
			});
		});

		test('Unlink the same account', async () => {
			expect(() =>
				User.unlinkAccount(user.uuid, {
					username: secondary.username,
					domain: secondary.application.domain,
				}),
			).rejects.toThrow();
		});

		test('Unlink a main account', async () => {
			expect(() =>
				User.unlinkAccount(user.uuid, {
					username: main.username,
					domain: main.application.domain,
				}),
			).rejects.toThrow();
		});

		test('Unlink a valid account not linked', async () => {
			expect(() =>
				User.unlinkAccount(user.uuid, {
					username: alt.username,
					domain: alt.application.domain,
				}),
			).rejects.toThrow();
		});
	});

	describe('addConfig', () => {
		test('Add a config to a valid user', async () => {
			await User.addConfig(user.uuid, 'config').then((result) => {
				expect(result).toMatchObject({
					value: 'config',
				});
			});
		});

		test('Add a config to an invalid user', async () => {
			expect(() => User.addConfig(undefined as any, 'config')).rejects.toThrow();
		});
	});

	describe('getConfig', () => {
		test('Get a config for a valid user', async () => {
			await User.getConfig(user.uuid).then((result) => {
				expect(result).toMatchObject({
					value: 'config',
				});
			});
		});

		test('Get a config for an invalid user', async () => {
			expect(() => User.getConfig(undefined as any)).rejects.toThrow();
		});
	});

	describe('deleteConfig', () => {
		test('Delete a config for a valid user', async () => {
			await User.deleteConfig(user.uuid).then((result) => {
				expect(result).toBe(true);
			});
		});

		test('Delete an already deleted config for a valid user', async () => {
			await User.deleteConfig(user.uuid).then((result) => {
				expect(result).toBe(false);
			});
		});

		test('Delete a config for an invalid user', async () => {
			expect(() => User.deleteConfig(undefined as any)).rejects.toThrow();
		});
	});

	afterAll(async () => {
		await User.deleteConfig(user.uuid).catch(() => {});
		await DBUser.delete(main).catch(() => {});
		await DBUser.delete(alt).catch(() => {});
		await DBAccount.get('username', account_domain)
			.then((account) => DBUser.delete(account!).catch(() => {}))
			.catch(() => {});
		await DBAccount.delete(account_username, account_domain).catch(() => {});
		await DBAccount.delete('secondary', account_domain).catch(() => {});
		await DBAccount.delete('alt', account_domain).catch(() => {});
		await DBAccount.delete('username', account_domain).catch(() => {});
		await DBAccount.delete('new', account_domain).catch(() => {});
		await DBApplication.delete(application_domain).catch(() => {});
	});
});
