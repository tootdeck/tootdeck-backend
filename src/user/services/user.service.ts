import {
	BadRequestException,
	Injectable,
	InternalServerErrorException,
	NotFoundException,
} from '@nestjs/common';

import { AccountStore } from '../../database/entities/account.entity';
import { ConfigStore } from '../../database/entities/config.entity';

import { DBAccountService } from '../../database/services/database.account.service';
import { DBUserService } from '../../database/services/database.user.service';
import { UserCacheService } from './user.cache.service';
import { WsService } from '../../websocket/services/ws.service';
import { AppService } from '../../app/services/app.service';

import { Logger } from '../../utils/logger';

import { AuthGetResponse } from '../../auth/properties/auth.get.property';

import { OAuthSessionToken } from '../../auth/types/oauth.types';
import { ApiResponseError } from '../../auth/types/api.types';
import { DeleteUser } from '../../database/types';
import { InstanceHandle } from '../../utils/handle.types';
import { ResponseType, WebsocketAPI } from '../../websocket/types';

@Injectable()
export class UserService {
	private readonly logger = new Logger(UserService.name);

	constructor(
		private readonly AppService: AppService,
		private readonly DBAccountService: DBAccountService,
		private readonly DBUserService: DBUserService,
		private readonly UserCacheService: UserCacheService,
		private readonly WsService: WsService,
	) {}

	/**
	 * Utils
	 */
	//#region

	/**
	 * Create an account based on the `getToken` response
	 * Then create an user based on the newly created account
	 *
	 * @throws InternalServerErrorException
	 */
	private async createAll(session_token: OAuthSessionToken) {
		const account = await this.DBAccountService.create(
			session_token.username,
			session_token.domain,
			session_token.token,
		);
		if (!account) {
			this.logger.error(
				'create',
				`Unable to create account ${session_token.username}@${session_token.domain}`,
			);
			throw new InternalServerErrorException();
		}

		const user = await this.DBUserService.create(account);
		if (!user) {
			this.logger.error(
				'create',
				`Unable to create user ${account.username}@${account.application.domain}`,
			);
			throw new InternalServerErrorException();
		}

		return user.uuid;
	}

	/**
	 * Create an user with the provided account
	 *
	 * @throws InternalServerErrorException
	 */
	private async create(account: AccountStore) {
		const user = await this.DBUserService.create(account);
		if (!user) {
			this.logger.error(
				'create',
				`Unable to create user ${account.username}@${account.application.domain}`,
			);
			throw new InternalServerErrorException();
		}

		return user.uuid;
	}

	//#endregion

	/**
	 * Service
	 */
	//#region

	/**
	 * @throws InternalServerErrorException
	 */
	async get(user_uuid: string): Promise<AuthGetResponse> {
		const user = await this.DBUserService.getByUUID(user_uuid);
		if (!user) {
			this.logger.error('get', `Unable to get existing user ${user_uuid}`);
			throw new InternalServerErrorException();
		}

		const main = {
			username: user.main.username,
			domain: user.main.application.domain,
			type: user.main.application.type,
			valid: user.main.token !== '',
		};

		const secondary = [];
		for (const account of user.secondary) {
			secondary.push({
				username: account.username,
				domain: account.application.domain,
				type: account.application.type,
				valid: account.token !== '',
			});
		}

		const ret: any = {
			main,
			secondary,
		};

		if (this.AppService.isAdmin(main.username + '@' + main.domain)) {
			ret.admin = true;
		}

		return ret;
	}

	/**
	 * @throws InternalServerErrorException
	 */
	async login(session_token: OAuthSessionToken) {
		const account = await this.DBAccountService.get(
			session_token.username,
			session_token.domain,
		);
		if (!account) {
			return this.createAll(session_token);
		}

		const user = await this.DBUserService.get(account);
		if (!user) {
			return this.create(account);
		}

		// Dispatch info to connected user
		this.WsService.sendByUserUUID(user.uuid, {
			type: ResponseType.API,
			data: WebsocketAPI.ResponseData.NewSession,
		});

		return user.uuid;
	}

	/**
	 * @throws InternalServerErrorException
	 */
	async addAccount(session: OAuthSessionToken, user_uuid: string) {
		const user = await this.DBUserService.getByUUID(user_uuid);
		if (!user) {
			this.logger.error(
				'addAccount',
				`Unable to get existing user ${user_uuid} (@${session.username}@${session.domain})`,
			);
			throw new InternalServerErrorException();
		}

		if (
			(user.main.username === session.username &&
				user.main.application.domain === session.domain) ||
			user.secondary.find(
				(x) => x.username === session.username && x.application.domain === session.domain,
			)
		) {
			throw new BadRequestException(ApiResponseError.AccountAlreadyExist);
		}

		let account: AccountStore;
		const exist = await this.DBAccountService.get(session.username, session.domain);
		if (exist) {
			account = exist;
		} else {
			const created = await this.DBAccountService.create(
				session.username,
				session.domain,
				session.token,
			);
			if (!created) {
				this.logger.error(
					'addAccount',
					`Unable to create account ${session.username}@${session.domain} for user ${user_uuid}`,
				);
				throw new InternalServerErrorException();
			}

			account = created;
		}

		const updated = await this.DBUserService.addAccount(user, account);
		if (!updated) {
			this.logger.error('addAccount', `Unable to update user ${user_uuid}`);
			throw new InternalServerErrorException();
		}

		const update_cache = await this.UserCacheService.create(updated);
		if (!update_cache) {
			this.logger.error('addAccount', `Unable to set user cache ${user_uuid}`);
			throw new InternalServerErrorException();
		}

		return updated.uuid;
	}

	/**
	 * @throws InternalServerErrorException
	 */
	async refresh(session: OAuthSessionToken, account_uuid: string) {
		const account = await this.DBAccountService.getByUUID(account_uuid);
		if (!account) {
			this.logger.error(
				'refresh',
				`Unable to get account ${session.username}@${session.domain} (${account_uuid})`,
			);
			throw new InternalServerErrorException();
		}

		const updated = await this.DBAccountService.updateToken(
			account.username,
			account.application.domain,
			session.token,
		);
		if (!updated) {
			this.logger.error(
				'refresh',
				`Unable to refresh account ${session.username}@${session.domain} (${account_uuid})`,
			);
			throw new InternalServerErrorException();
		}
		await this.UserCacheService.purgeAccount(account);

		return updated.uuid;
	}

	/**
	 * @throws InternalServerErrorException
	 * @throws BadRequestException
	 */
	async unlinkAccount(user_uuid: string, handle_to_remove: InstanceHandle): Promise<boolean> {
		const user = await this.DBUserService.getByUUID(user_uuid);
		if (!user) {
			this.logger.error('removeAccount', `Unable to get user ${user_uuid}`);
			throw new InternalServerErrorException();
		}

		const user_to_remove = user.secondary.find(
			(x) =>
				x.username === handle_to_remove.username &&
				x.application.domain === handle_to_remove.domain,
		);
		if (!user_to_remove) {
			throw new BadRequestException();
		}

		const removed = await this.DBUserService.unlinkAccount(user.main, user_to_remove);
		switch (removed) {
			case DeleteUser.True:
				return true;
			case DeleteUser.False:
				throw new BadRequestException();
			case DeleteUser.RemoveAccount:
				const del = await this.DBAccountService.delete(
					user_to_remove.username,
					user_to_remove.application.domain,
				);
				if (!del) {
					throw new InternalServerErrorException();
				}
		}

		this.UserCacheService.delete(user.uuid);

		return true;
	}

	/**
	 * @throws InternalServerErrorException
	 * @throws NotFoundException
	 */
	async getConfig(user_uuid: string): Promise<ConfigStore> {
		const config = await this.DBUserService.getConfig(user_uuid);
		if (!config) {
			throw new NotFoundException();
		}

		return config;
	}

	/**
	 * @throws InternalServerErrorException
	 */
	async addConfig(
		user_uuid: string,
		config: string,
		client_uuid?: string,
	): Promise<ConfigStore | null> {
		const user = await this.DBUserService.getByUUID(user_uuid);
		if (!user) {
			this.logger.error('addConfig', `Unable to get user ${user_uuid}`);
			throw new InternalServerErrorException();
		}

		const ret = await this.DBUserService.addConfig(user, config);
		if (ret === null) {
			throw new InternalServerErrorException();
		}

		this.WsService.sendByUserUUID(user.uuid, {
			type: ResponseType.API,
			data: WebsocketAPI.ResponseData.ConfigUpdate,
			uuid: client_uuid,
		} as any);

		return ret.config;
	}

	/**
	 * @throws InternalServerErrorException
	 */
	async deleteConfig(user_uuid: string): Promise<boolean> {
		const user = await this.DBUserService.getByUUID(user_uuid);
		if (!user) {
			this.logger.error('deleteConfig', `Unable to get user ${user_uuid}`);
			throw new InternalServerErrorException();
		}

		const ret = await this.DBUserService.deleteConfig(user);
		if (ret === null) {
			throw new InternalServerErrorException();
		}

		return !!ret;
	}

	//#endregion
}
