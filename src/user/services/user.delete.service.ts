import { BadRequestException, Injectable, InternalServerErrorException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import { AccountStore } from '../../database/entities/account.entity';

import { DBUserService } from '../../database/services/database.user.service';
import { UserDeleteSessionService } from '../../auth/services/sessions/user.delete.session.service';
import { DBAccountService } from '../../database/services/database.account.service';
import { OAuthService } from '../../auth/services/oauth.service';
import { UserCacheService } from './user.cache.service';

import { Logger } from '../../utils/logger';

import { secretConfig } from '../../app/configs/secret.config';
import { JWETime } from '../../auth/config';

import { UserDeleteSession as UserDeleteSession } from '../../auth/types/sessions.types';
import { DeleteUser } from '../../database/types';

@Injectable()
export class UserDeleteService {
	private readonly logger = new Logger(UserDeleteService.name);
	private readonly secret: string;

	constructor(
		private readonly JwtService: JwtService,
		private readonly DBUserService: DBUserService,
		private readonly DBAccountService: DBAccountService,
		private readonly UserDeleteSessionService: UserDeleteSessionService,
		private readonly UserCacheService: UserCacheService,
		private readonly OAuthService: OAuthService,
	) {
		this.secret = secretConfig();
		if (!this.secret) {
			this.logger.error('constructor', 'Unable to read secret');
			process.exit(1);
		}
		this.logger.log('constructor', 'Secret loaded');
	}

	/**
	 * Utils
	 */
	//#region

	private async deleteToken(token_uuid: string): Promise<string> {
		return this.JwtService.signAsync(
			{ d: token_uuid },
			{
				algorithm: 'HS512',
				expiresIn: JWETime.Delete(),
				secret: this.secret,
			},
		);
	}

	//#endregion

	/**
	 * Service
	 */
	//#region

	/**
	 * @throws BadRequestException
	 */
	async validate(raw_token: string): Promise<UserDeleteSession> {
		const jwt = await this.JwtService.verifyAsync<{ d: string }>(raw_token, {
			algorithms: ['HS512'],
			secret: this.secret,
		}).catch((e) => {
			this.logger.warn('validate', 'Invalid JWT: signature verification failed');
			throw new BadRequestException();
		});

		const token_uuid = jwt.d;

		const session = await this.UserDeleteSessionService.get(token_uuid);
		if (!session) {
			this.logger.verbose('validate', 'Invalid JWT: no session');
			throw new BadRequestException();
		}
		if (!session.token) {
			this.logger.verbose('validate', 'Invalid JWT: no token');
			throw new BadRequestException();
		}

		const del = await this.UserDeleteSessionService.delete(token_uuid);
		if (!del) {
			this.logger.verbose('validate', 'Invalid delete');
			throw new InternalServerErrorException();
		}

		return session;
	}

	/**
	 * @throws InternalServerErrorException
	 */
	async createRequest(user_uuid: string): Promise<UserDeleteSession> {
		const session = await this.UserDeleteSessionService.create(user_uuid);
		if (!session) {
			this.logger.error('createRequest', 'Unable to create session');
			throw new InternalServerErrorException();
		}

		const token = await this.deleteToken(session.token_uuid);
		if (!token) {
			this.logger.error('createRequest', 'Failed to sign JWT');
			throw new InternalServerErrorException();
		}

		const update = await this.UserDeleteSessionService.updateJWE(session.token_uuid, token);
		if (!update) {
			this.logger.error('createRequest', 'Unable to set token in session');
			await this.UserDeleteSessionService.delete(session.token_uuid);
			throw new InternalServerErrorException();
		}

		return update;
	}

	/**
	 * @throws InternalServerErrorException
	 */
	async delete(user_uuid: string) {
		const user = await this.DBUserService.getByUUID(user_uuid);
		if (!user) {
			this.logger.error('delete', `Unable to get user ${user_uuid}`);
			throw new InternalServerErrorException();
		}

		// Remove secondary
		const token_to_revoke: AccountStore[] = [];
		const errors_account: string[] = [];
		for (const alt of user.secondary) {
			const remove = await this.DBUserService.unlinkAccount(user.main, alt);
			if (remove === DeleteUser.False) {
				this.logger.error(
					'delete',
					`Failed to remove account ${alt.username}@${alt.application.domain} for ${user.main.username}@${user.main.application.domain}`,
				);
				errors_account.push(`@${alt.username}@${alt.application.domain}`);
				continue;
			}

			// Remove secondary account from cache
			const update_cache = await this.UserCacheService.create(user);
			if (!update_cache) {
				this.logger.warn(
					'delete',
					`Failed to update user cache for ${user.main.username}@${user.main.application.domain}`,
				);
			}

			// Remove account
			if (remove === DeleteUser.RemoveAccount) {
				const remove_acc = await this.DBAccountService.delete(
					alt.username,
					alt.application.domain,
				);
				if (!remove_acc) {
					errors_account.push(`@${alt.username}@${alt.application.domain}`);
				}
				token_to_revoke.push(alt);
			}
		}

		// Remove main
		const remove = await this.DBUserService.delete(user.main);
		if (remove === DeleteUser.False) {
			this.logger.error(
				'delete',
				`Failed to remove user ${user.main.username}@${user.main.application.domain}`,
			);
			errors_account.push(`@${user.main.username}@${user.main.application.domain}`);
		}

		// Remove user from cache
		const update_cache = await this.UserCacheService.delete(user.uuid);
		if (!update_cache) {
			this.logger.warn(
				'delete',
				`Failed to remove user cache for ${user.main.username}@${user.main.application.domain}`,
			);
		}

		// Remove account
		if (remove === DeleteUser.RemoveAccount) {
			const remove_acc = await this.DBAccountService.delete(
				user.main.username,
				user.main.application.domain,
			);
			if (!remove_acc) {
				errors_account.push(`@${user.main.username}@${user.main.application.domain}`);
			}
			token_to_revoke.push(user.main);
		}

		// Revoke tokens
		const errors_revoke: string[] = [];
		if (process.env['NODE_ENV'] !== 'test') {
			for (const account of token_to_revoke) {
				const revoke = await this.OAuthService.revokeToken(account);
				if (!revoke) {
					this.logger.error(
						'delete',
						`Failed to revoke token for ${account.username}@${account.application.domain}`,
					);
					errors_revoke.push(`@${account.username}@${account.application.domain}`);
				}
			}
		}

		// Format response
		let response_user: string = 'OK';
		let response_token: string = 'OK';
		if (errors_account.length || errors_revoke.length) {
			this.logger.error('delete', `User deleting finished with errors`);
		}

		if (errors_account.length) {
			response_user = `Failed to remove accounts ${errors_account.join(', ')}`;
		}

		if (errors_revoke.length) {
			response_token = `Failed to revoke token for ${errors_revoke.join(', ')}`;
		}

		return {
			user: response_user,
			token: response_token,
		};
	}
	//#endregion
}
