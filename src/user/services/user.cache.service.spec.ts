import { Test, TestingModule } from '@nestjs/testing';

import { FullAppModule } from '../../app/app.module';

import { DBApplicationService } from '../../database/services/database.application.service';
import { DBAccountService } from '../../database/services/database.account.service';
import { DBUserService } from '../../database/services/database.user.service';
import { UserCacheService } from './user.cache.service';

import { UserStore } from '../../database/entities/user.entity';
import { AccountStore } from '../../database/entities/account.entity';

import { InstanceType } from '../../database/types';
import { UserCache } from '../types/cache.types';

describe('UserCacheService', () => {
	let Application: DBApplicationService;
	let Account: DBAccountService;
	let User: DBUserService;
	let UserCache: UserCacheService;

	const application_domain = 'example.com';
	const application_key = 't52jyR5LOeqAK4PhXqI7rktO7CZaD6OPaRohyFVrgzI=';
	const application_secret = 'qlUrKaKNhIuUZrt1pxvGrOnxNdrMDyqPp+qkvhn4Fzw=';

	let account_username = 'tootdeck';
	let account_domain = application_domain;
	let account_token = 'xOOnkfBjAtRqtAmqWFNZEhDsjetgaBIFccIg1EPLqdM=';

	let user_main: UserStore;
	let user_alt: UserStore;
	let main: AccountStore;
	let alt: AccountStore;
	let cache_main: UserCache;
	let cache_alt: UserCache;

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule(FullAppModule).compile();

		Application = app.get<DBApplicationService>(DBApplicationService);
		Account = app.get<DBAccountService>(DBAccountService);
		User = app.get<DBUserService>(DBUserService);
		UserCache = app.get<UserCacheService>(UserCacheService);

		/**
		 * Create a valid application
		 */
		await Application.create(
			application_domain,
			InstanceType.Mastodon,
			application_key,
			application_secret,
		);

		/**
		 * Create a valid main account
		 */
		main = await Account.create(account_username, account_domain, account_token).then(
			(entity) => entity!,
		);

		/**
		 * Create a valid alt account
		 */
		alt = await Account.create('alt', account_domain, account_token).then((entity) => entity!);

		/**
		 * Create a valid user
		 */
		user_main = await User.create(main).then((entity) => entity!);
		user_alt = await User.create(alt).then((entity) => entity!);
	});

	describe('create', () => {
		test('Create a valid entry', async () => {
			await UserCache.create(user_main.uuid).then((result) => {
				// @ts-ignore
				cache_main = result;

				expect(result).toMatchObject({
					uuid: user_main.uuid,
					main: {
						username: user_main.main.username,
						application: {
							domain: user_main.main.application.domain,
							type: InstanceType.Mastodon,
						},
					},
					secondary: [],
				});
			});
			await UserCache.create(user_alt).then(async (result) => {
				// @ts-ignore
				cache_alt = result;

				expect(result).toMatchObject({
					uuid: user_alt.uuid,
					main: {
						username: user_alt.main.username,
						application: {
							domain: user_alt.main.application.domain,
							type: InstanceType.Mastodon,
						},
					},
					secondary: [],
				});
			});
		});

		test('Create an invalid entry', async () => {
			await UserCache.create('nothing').then((result) => {
				expect(result).toBeNull();
			});
		});
	});

	describe('get', () => {
		test('Get a valid uuid', async () => {
			await UserCache.get(user_main.uuid).then((result) => {
				expect(result).toMatchObject(cache_main);
			});
		});

		test('Get a non existing uuid', async () => {
			await UserCache.delete(user_alt.uuid).then((result) => {
				expect(result).toBeTruthy();
			});
			await UserCache.get(user_alt.uuid).then((result) => {
				expect(result).toMatchObject(cache_alt);
			});
		});

		test('Get an invalid uuid', async () => {
			await UserCache.get('nothing').then((result) => {
				expect(result).toBeNull();
			});
		});
	});

	describe('delete', () => {
		test('deleting valid entry', async () => {
			await UserCache.delete(user_main.uuid).then((result) => {
				expect(result).toBe(true);
			});
			await UserCache.delete(user_alt.uuid).then((result) => {
				expect(result).toBe(true);
			});
		});

		test('deleting invalid entry', async () => {
			await UserCache.delete('nothing').then((result) => {
				expect(result).toBe(false);
			});
		});
	});

	afterAll(async () => {
		await User.getWithSessions(user_main.uuid)
			.then((result) =>
				User.deleteSessions(user_main.uuid, result?.session.map((s) => s.uuid) ?? []).catch(
					() => {},
				),
			)
			.catch(() => {});
		await User.delete(main).catch(() => {});
		await User.delete(alt).catch(() => {});
		await Account.delete(account_username, account_domain).catch(() => {});
		await Account.delete('alt', account_domain).catch(() => {});
		await Application.delete(application_domain).catch(() => {});
	});
});
