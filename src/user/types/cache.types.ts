import { InstanceType } from '../../database/types';

export interface ApplicationCache {
	type: InstanceType;
	domain: string;
}

export interface AccountCache {
	username: string;
	token: string;
	application: ApplicationCache;
}

export interface UserCache {
	uuid: string;
	main: AccountCache;
	secondary: AccountCache[];
}
