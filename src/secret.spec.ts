import { generateSecret } from './secret';

describe('Secret', () => {
	it('generate', async () => {
		expect(await generateSecret({ regen: true })).toBe(true);
	});

	it('exist', async () => {
		expect(await generateSecret()).toBe(true);
	});

	it('exist crash', async () => {
		expect(await generateSecret({ exist: true })).toBe(false);
	});

	it('write crash', async () => {
		expect(await generateSecret({ regen: true, write: true })).toBe(false);
	});
});
