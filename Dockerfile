FROM node:lts-alpine

ENV PNPM_HOME="/pnpm"
ENV COREPACK_ENABLE_DOWNLOAD_PROMPT=0
ENV PATH="$PNPM_HOME:$PATH"
RUN mkdir -p /pnpm/store
RUN npm install -g corepack@latest
RUN corepack enable pnpm && corepack prepare pnpm --activate
RUN	pnpm config set store-dir /pnpm/store

RUN apk add xz bash
ADD ffmpeg/ffmpeg.tar.xz /usr/bin/
RUN chmod +x /usr/bin/ffmpeg

RUN mkdir /app
WORKDIR /app

USER 1000
